module.exports = {
  env: {
    API_URL                 : process.env.API_URL,
    SOCKET_URL              : process.env.SOCKET_URL,
    MS_REDIRECT_URL         : process.env.MS_REDIRECT_URL,
    POST_LOGOUT_REDIRECT_URL: process.env.POST_LOGOUT_REDIRECT_URL,
    CLIENT_ID               : process.env.CLIENT_ID,
    TENANT_ID               : process.env.TENANT_ID,
    CLIENT_SECRET           : process.env.CLIENT_SECRET,
  },
}
