import { Grid } from '@material-ui/core'
import React from 'react'
import TenantNakitAkisi from '../../../components/muhasebe/tenant/NakitAkisi'
import PageHeader from '../../../components/pageHeader'
import {getTenants} from '../../../services/tenant.service'

const NakitAkisi = props => {
    const { tenants } = props
    return <>
        <PageHeader
            title='Nakit Akışı'
        />
        <div className='p-2'>
            <Grid container spacing={2}>
                {tenants.map(t => {
                    return (
                        <Grid item xs={4}>
                            <TenantNakitAkisi tenant={t}/>
                        </Grid>
                    )
                })}
            </Grid>
        </div>
    </>
}

NakitAkisi.getInitialProps = async (ctx) => {
    const tenants = await getTenants()
    return {
        tenants
    }
}

export default NakitAkisi