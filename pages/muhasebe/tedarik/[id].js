import React from 'react'
import PageHeader from '../../../components/pageHeader'
import { Button, CardHeader, IconButton, Divider, Grid, TextField, Card, CardContent } from '@material-ui/core'
import MaterialTable from 'material-table'
import { getTedarikci, getGelenFaturaBelge } from '../../../services/muhasebe.service'
import api from '../../../services/axios.service';
import { useSelector } from 'react-redux'
import numbro from 'numbro'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import InsertDriveFileOutlinedIcon from '@material-ui/icons/InsertDriveFileOutlined';
import { withRouter } from 'next/router'
var qs = require('qs');

const TedarikciBilgi = props => {
    const { tedarikci } = props

    return (
        <>
            <Card>
                <CardHeader
                    title='Tedarikci Bilgileri'
                />
                <Divider />
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                label='Adı / Ünvanı'
                                fullWidth
                                variant='outlined'
                                size='small'
                                value={tedarikci.adi}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label='Adres'
                                fullWidth
                                variant='outlined'
                                size='small'
                                value={tedarikci.adres}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label='İl'
                                fullWidth
                                variant='outlined'
                                size='small'
                                value={tedarikci.il}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label='İlçe'
                                fullWidth
                                variant='outlined'
                                size='small'
                                value={tedarikci.ilce}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label='Ülke'
                                fullWidth
                                variant='outlined'
                                size='small'
                                value={tedarikci.ulke}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label='Telefon'
                                fullWidth
                                variant='outlined'
                                size='small'
                                value={tedarikci.telefon}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label='Mail'
                                fullWidth
                                variant='outlined'
                                size='small'
                                value={tedarikci.mail}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label='Vergi Dairesi'
                                fullWidth
                                variant='outlined'
                                size='small'
                                value={tedarikci.vergiDairesi}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label='Vergi No'
                                fullWidth
                                variant='outlined'
                                size='small'
                                value={tedarikci.vergiNo}
                            />
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </>
    )
}

const TedarikciFaturalar = props => {
    const token = useSelector(state => state.auth.expertoToken)
    const { tedarikci } = props

    return (
        <Card>
            <CardHeader
                title='Faturalar'
            />
            <Divider />
            <CardContent>
                <MaterialTable
                    title="Gelen Faturalar"
                    columns={[
                        { title: 'Fatura No', field: 'faturaNo' },
                        { title: 'Tarih', field: 'faturaTarihi' },
                        {
                            title: 'Toplam', field: 'odenecekTutar', render: row => {
                                return `${numbro(row.odenecekTutar).format({ thousandSeparated: true, mantissa: 2 })} TL`
                            }
                        },
                        {
                            title: 'Durum', field: '', render: row => {
                                if (row.odendi) {
                                    return (
                                        <div className='bg-green-500 w-28 text-center px-2 py-1 text-white font-semibold rounded-lg'>
                                            <h2>ÖDENDİ</h2>
                                        </div>
                                    )
                                }
                                else {
                                    if (row.odemePlanlandi) {
                                        return (
                                            <div className='bg-blue-500 w-28 text-center px-2 py-1 text-white font-semibold rounded-lg'>
                                                <h2>PLANLANDI</h2>
                                            </div>
                                        )
                                    }
                                    else {
                                        return (
                                            <div className='bg-red-500 w-28 text-center px-2 py-1 text-white font-semibold rounded-lg'>
                                                <h2>AÇIK</h2>
                                            </div>
                                        )
                                    }
                                }
                                // 1. odendi
                                // 2. Acik
                                // 3. planlandi
                            }
                        },
                        {
                            title: 'Belge', field: '', render: row => {
                                return (
                                    <IconButton className='focus:outline-none' onClick={() => {
                                        getGelenFaturaBelge(row._id)
                                    }}>
                                        <InsertDriveFileOutlinedIcon />
                                    </IconButton>
                                )
                            }
                        },
                    ]}
                    actions={[
                        {
                            icon: () => <ArrowForwardIcon />,
                            tooltip: 'Faturaya Git',
                            onClick: (event, rowData) => {
                                props.router.push(`/muhasebe/gider/${rowData._id}`)
                            }
                        }
                    ]}
                    options={{
                        padding: 'dense'
                    }}
                    data={query => {
                        return new Promise((resolve, reject) => {
                            let filterObj = {
                                skip: query.page * query.pageSize,
                                limit: query.pageSize,
                                //populate: 'aliciFirma',
                                tedarikci: tedarikci._id,
                                //aciklama: `/${query.search}/i`,
                                sort: '-belgeTarihi'
                            }
                            if (query.orderBy) {
                                filterObj.sort = query.orderBy ? `${query.orderDirection == 'asc' ? '' : '-'}${query.orderBy.field}` : ''
                            }
                            let q = qs.stringify(filterObj)

                            api.get(`/gelenfatura?${q}`,
                                { headers: { Authorization: `Bearer ${token}` } })
                                .then(result => {
                                    if (!result.data.data) {
                                        reject()
                                    }
                                    else {
                                        resolve({
                                            data: result.data.data.data,
                                            page: result.data.data.page / query.pageSize,
                                            totalCount: result.data.data.total,
                                        })
                                    }
                                })
                        })
                    }}
                />
            </CardContent>
        </Card>
    )
}

const TedarikciBorcDurumu = props => {
    return (
        <Card>
            <CardHeader
                title='Borç / Ödeme Durumu'
            />
            <Divider />
            <CardContent>
                <div className='p-4 flex-col'>
                    <div className='flex items-center justify-between'>
                        <h2 className='text-lg font-light'>Kalan Borcumuz</h2>
                        <h2 className='text-lg '>0 TL</h2>
                    </div>
                    <div className='flex items-center justify-between'>
                        <h2 className='text-lg font-light'>Yapılan Toplam Ödeme</h2>
                        <h2 className='text-lg '>0 TL</h2>
                    </div>
                    <div className='flex items-center justify-between'>
                        <h2 className='text-lg font-light'>Gelen Fatura Tutarı</h2>
                        <h2 className='text-lg '>0 TL</h2>
                    </div>
                    <div className='flex items-center justify-between'>
                        <h2 className='text-lg font-light'>Gelen Fatura Sayısı</h2>
                        <h2 className='text-lg '>0</h2>
                    </div>

                    <div className='mt-12 w-full'>
                        <h2 className='text-lg text-center'>ÖDEME PLANI</h2>
                        <table className='w-full'>
                            <thead className='bg-green-100'>
                                <tr>
                                    <th>Tarih</th>
                                    <th>Ödenecek Tutar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align='center'></td>
                                    <td align='right'>0 TL</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </CardContent>
        </Card>
    )
}


const Tedarikci = (props) => {
    const { tedarikci, isNew } = props

    return (
        <div>
            <PageHeader
                title='Tedarikci'
                buttons={[
                    isNew ?
                        <Button variant='contained' color='primary'>Kaydet</Button>
                        :
                        <Button variant='contained' color='primary'>Güncelle</Button>
                ]}
            />
            <div className='p-2'>
                <Grid container spacing={2}>
                    <Grid item xs={8} container spacing={2}>
                        <Grid item xs={12}>
                            <TedarikciBilgi tedarikci={tedarikci} />
                        </Grid>
                        <Grid item xs={12}>
                            <TedarikciFaturalar tedarikci={tedarikci} {...props} />
                        </Grid>
                    </Grid>
                    <Grid item xs={4}>
                        <TedarikciBorcDurumu tedarikci={tedarikci} />
                    </Grid>
                </Grid>
            </div>
        </div>
    )
}

Tedarikci.getInitialProps = async (ctx) => {
    let isNew = ctx.query.id == 'new'
    let tedarikci = {
        adi: '',
        adres: '',
        il: '',
        ilce: '',
        ulke: '',
        telefon: '',
        mail: '',
        vergiDairesi: '',
        vergiNo: '',
    }

    if (!isNew) {
        tedarikci = await getTedarikci(ctx.query.id)
    }

    return {
        tedarikci,
        isNew
    }
}

export default withRouter(Tedarikci)
