import React from 'react'
import PageHeader from '../../../components/pageHeader'
import MaterialTable from 'material-table'
import api from '../../../services/axios.service';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { withRouter } from 'next/router'
import { useSelector } from 'react-redux'
import { Button, IconButton } from '@material-ui/core'
var qs = require('qs');

const Tedarikciler = props => {
    const token = useSelector(state => state.auth.expertoToken)

    return (
        <>
            <PageHeader
                title='Giderler'
                buttons={[
                    <Button variant='contained' color='primary'>Yeni Tedarikci</Button>
                ]}
            />
            <div className='p-2'>
                <MaterialTable
                    title='Tedarikciler'
                    columns={[
                        { title: 'Adı', field: 'adi' },
                        { title: 'Mail', field: 'mail' },
                        { title: 'Telefon', field: 'telefon' }
                    ]}
                    options={{
                        padding: 'dense',
                        pageSize: 25,
                        pageSizeOptions: [10, 25, 50, 100]
                    }}
                    actions={[
                        {
                            icon: () => <ArrowForwardIcon />,
                            tooltip: 'Detay',
                            onClick: (event, rowData) => {
                                props.router.push(`/muhasebe/tedarik/${rowData._id}`)
                            }
                        }
                    ]}
                    data={query => {
                        return new Promise((resolve, reject) => {
                            let filterObj = {
                                skip: query.page * query.pageSize,
                                limit: query.pageSize,
                                adi: `/${query.search}/i`,
                                sort: 'adi'
                            }
                            if (query.orderBy) {
                                filterObj.sort = query.orderBy ? `${query.orderDirection == 'asc' ? '' : '-'}${query.orderBy.field}` : ''
                            }
                            let q = qs.stringify(filterObj)
                            api.get(`/tedarikci?${q}`,
                                { headers: { Authorization: `Bearer ${token}` } })
                                .then(result => {
                                    if (!result.data.data) {
                                        reject()
                                    }
                                    else {
                                        resolve({
                                            data: result.data.data.data,
                                            page: result.data.data.page / query.pageSize,
                                            totalCount: result.data.data.total,
                                        })
                                    }
                                })

                        })
                    }}

                />
            </div>
        </>
    )
}

export default withRouter(Tedarikciler)