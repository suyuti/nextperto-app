import React from 'react'
import PageHeader from '../../../components/pageHeader'
import { makeStyles, Grid } from '@material-ui/core'
import TahsilatlarDagilimi from '../../../components/muhasebe/tahsilatlarRaporu/tahsilatlarDagilimi'
import TahsilatlarDagilimiChart from '../../../components/muhasebe/tahsilatlarRaporu/tahsilatDagilimChart'
import {getTenants} from '../../../services/tenant.service'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const TahsilatRapor = props => {
    const {tenants} = props
    const classes = useStyles()
    return <>
        <PageHeader
            title='Tahsilatlar Raporu'
        />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <TahsilatlarDagilimi tenants={tenants} onChange={(e) => {}}/>
                </Grid>
                <Grid item xs={6}>
                    <TahsilatlarDagilimiChart />
                </Grid>
            </Grid>
        </div>
    </>
}

TahsilatRapor.getInitialProps = async (ctx) => {
    let tenants = await getTenants()

    return {
        tenants
    }
}

export default TahsilatRapor