import React, { useState } from 'react'
import PageHeader from '../../../components/pageHeader'
import { Button, makeStyles, Grid, Tabs, Tab, Paper } from '@material-ui/core'
import MusteriMuhasebeInfo from '../../../components/muhasebe/musteri/MusteriMuhasebeInfo'
import MusteriIslemler from '../../../components/muhasebe/musteri/MusteriIslemler'
import TabPanel from '../../../hoc/TabPanel'
import MusteriFaturalar from '../../../components/muhasebe/musteri/MusteriFaturalar'
import MusteriTahsilatlar from '../../../components/muhasebe/musteri/MusteriTahsilatlar'
import MusteriTahsilatPlanlari from '../../../components/muhasebe/musteri/MusteriTahsilatPlanlari'
import { getFaturalarToplam, getTahsilatlarToplam, getFirma, getPlanlanmisTahsilatlar } from '../../../services/firma.service';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const MuhasebeMusteri = props => {
    const { firma, firmaToplamTahsilat,
        firmaToplamFatura,
        firmaBorc,
        planlanmisTahsilatlar
    } = props
    const classes = useStyles()
    const [selectedTab, setSelectedTab] = useState(0)

    return <>
        <PageHeader
            title='Müşteri'
            buttons={[
                <Button variant='outlined' color='primary'>Git</Button>,
                <Button variant='outlined' color='primary'>Yeni Fatura</Button>,
                <Button variant='outlined' color='primary'>Yeni Tahsilat</Button>
            ]}
        />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={8} container spacing={2}>
                    <Grid item xs={12}>
                        <Tabs
                            value={selectedTab}
                            onChange={(e, newValue) => { setSelectedTab(newValue) }}
                            component={Paper}>
                            <Tab label='Faturalar' />
                            <Tab label='Tahsilatlar' />
                        </Tabs>
                        <TabPanel value={selectedTab} index={0} ><MusteriFaturalar firma={firma} /></TabPanel>
                        <TabPanel value={selectedTab} index={1} ><MusteriTahsilatlar firma={firma} /></TabPanel>
                    </Grid>
                </Grid>
                <Grid item xs={4} container spacing={2} alignContent='flex-start'>
                    <Grid item xs={12}>
                        <MusteriMuhasebeInfo
                            firma={firma}
                            firmaToplamTahsilat={firmaToplamTahsilat}
                            firmaToplamFatura={firmaToplamFatura}
                            firmaBorc={firmaBorc}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <MusteriTahsilatPlanlari firma={firma} planlanmisTahsilatlar={planlanmisTahsilatlar}/>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    </>
}

MuhasebeMusteri.getInitialProps = async (ctx) => {
    let firma = await getFirma(ctx.query.id)
    let firmaToplamTahsilat = await getTahsilatlarToplam(firma.key)
    let firmaToplamFatura = await getFaturalarToplam(firma.key)
    let firmaBorc = firmaToplamFatura - firmaToplamTahsilat
    let planlanmisTahsilatlar = await getPlanlanmisTahsilatlar(firma.key)
    return {
        firma,
        firmaToplamTahsilat,
        firmaToplamFatura,
        firmaBorc,
        planlanmisTahsilatlar
    }
}

export default MuhasebeMusteri