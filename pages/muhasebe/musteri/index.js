import React, {useState, useEffect} from 'react'
import PageHeader from '../../../components/pageHeader'
import { Button, makeStyles, Grid } from '@material-ui/core'
import { useRouter } from 'next/router';
import TableView from '../../../components/tableview';
import { useSelector } from 'react-redux'
import AddIcon from '@material-ui/icons/Add';
import numbro from 'numbro';
import FirmaSearchToolbar from '../../../components/muhasebe/musteri/FirmaSearchToolbar';
import { getTenants } from '../../../services/tenant.service';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    genelToplam: {
        fontWeight: '600'
    },
    toplamAdet: {
        fontSize: 'x-small',
        fontWeight: '200'
    }

}))

const MuhasebeMusteriler = props => {
    const { tenants } = props
    const classes = useStyles()
    const router = useRouter()
    const token = useSelector(state => state.auth.expertoToken)
    const [filter, setFilter] = useState('')

    //useEffect(() => {
    //    setFilter(router.query.filter)
    //}, [router.query])

    const onFilterSearch = (query) => {
        let q = ''

        if (query.acik) {
            if (query.acik == 'acik') {
                q += `acik=${false}&`
            }
            else if (query.acik == 'kapali') {
                q += `acik=${true}&`
            }
        }

        if (query.firma) {
            q += `vfirma=${query.firma}&`
        }
        if (query.kesenFirma) {
            q += `vkesenFirma=${query.kesenFirma}&`
        }

        if (query.altTutar != 0 && query.ustTutar == 0) {
            q += `genelToplam>=${query.altTutar}&`
        }
        else if (query.altTutar == 0 && query.ustTutar != 0) {
            q += `genelToplam<=${query.ustTutar}&`
        }
        else if (query.altTutar != 0 && query.ustTutar != 0) {
            q = q + `filter={"$and":[{"genelToplam":{"$gte":"${query.altTutar}"}},{"genelToplam":{"$lte":"${query.ustTutar}"}}]}`
        }

        if (query.baslangicTarihi && !query.bitisTarihi) {
            q = q + `vadeTarihi>=${moment(query.baslangicTarihi).toISOString()}&`
        }
        else if (!query.baslangicTarihi && query.bitisTarihi) {
            q = q + `vadeTarihi<=${moment(query.baslangicTarihi).toISOString()}&`
        }
        else if (query.baslangicTarihi && query.bitisTarihi) {
            q = q + `filter={"$and":[{"vadeTarihi":{"$gte":"${moment(query.baslangicTarihi).toISOString()}"}},{"vadeTarihi":{"$lte":"${moment(query.bitisTarihi).toISOString()}"}}]}`
        }

        setFilter(q)
    }


    const columns = [
        { id: 'marka', label: 'Müşteri' },
        {
            id: 'fark', label: 'Alacak',
            align: 'right',
            render: row => {
                let borc = row.toplamFatura - row.toplamTahsilat
                if (borc > 0) {
                    return `${numbro(borc).format({ thousandSeparated: true, mantissa: 2 })} TL`
                }
                return `0 TL`
            }
        },
        {
            id: 'bakiye', label: 'Bakiye',
            align: 'right',
            render: row => {
                return `${numbro(row.bakiye).format({ thousandSeparated: true, mantissa: 2 })} TL`
            }
        },
        {
            id: 'toplamFatura',
            label: 'Toplam Fatura',
            align: 'right',
            render: row => {
                return <div className={classes.toplamCell}>
                    <div className={classes.toplamTutar}>
                        {`${numbro(row.toplamFatura).format({ thousandSeparated: true, mantissa: 2 })} TL`}
                    </div>
                    <div className={classes.toplamAdet}>
                        {row.toplamFaturaAdet} adet
                    </div>
                </div>
            }
        },
        {
            id: 'toplamTahsilat',
            label: 'Toplam Tahsilat',
            align: 'right',
            render: row => {
                return <div className={classes.toplamCell}>
                    <div className={classes.toplamTutar}>
                        {`${numbro(row.toplamTahsilat).format({ thousandSeparated: true, mantissa: 2 })} TL`}
                    </div>
                    <div className={classes.toplamAdet}>
                        {row.toplamTahsilatAdet} adet
                    </div>
                </div>
            }

        },
        {
            id: '', label: '', render: row => {
                return <Button variant='outlined' color='primary' size='small' onClick={() => {
                    router.push(`/muhasebe/musteri/${row._id}`)
                }}>Detay</Button>
            }
        },
    ]

    return <>
        <PageHeader
            title='Müşteriler'
            buttons={[
                <Button variant='contained' color='primary' startIcon={<AddIcon />} onClick={() => { }}>Yeni Müşteri</Button>
            ]}
        />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <FirmaSearchToolbar
                        disabled 
                        tenants={tenants}
                        onSearch={onFilterSearch} />
                </Grid>
                <Grid item xs={12}>
                    <TableView
                        title='Müşteriler'
                        columns={columns}
                        remoteUrl={`/muhasebe/firma`}
                        token={token}
                        filter={`${filter}&select=marka,key,bakiye`}
                        searchCol='marka'
                    //data={[]}
                    />
                </Grid>
            </Grid>




        </div>
    </>
}

MuhasebeMusteriler.getInitialProps = async (ctx) => {
    let tenants = await getTenants()
    return {
        tenants
    }
}

export default MuhasebeMusteriler