import React from 'react'
import PageHeader from '../../../components/pageHeader'
import { makeStyles, Grid } from '@material-ui/core'
import SatisFaturalari from '../../../components/muhasebe/satisRaporu/SatisFaturalari'
import SatisDagilimlari from '../../../components/muhasebe/satisRaporu/SatisDagilimlari'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const SatisRaporu = props => {
    const classes = useStyles()

    return <>
        <PageHeader
            title='Satışlar Raporu'
        />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <SatisFaturalari />
                </Grid>
                <Grid item xs={12}>
                    <SatisDagilimlari />
                </Grid>
            </Grid>
        </div>
    </>
}

export default SatisRaporu