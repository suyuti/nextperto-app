import React, { useRef, useState, useEffect } from 'react'
import PageHeader from "../../../components/pageHeader";
import { makeStyles, Button, Grid, Drawer, Dialog, DialogContent, DialogActions, DialogTitle, Checkbox, FormControlLabel } from '@material-ui/core';
import { getFatura, getFaturaKalemler, saveFatura, faturayiYenidenGonder } from '../../../services/muhasebe.service'
import { searchFirmalar, getFirma, getFirmalar } from '../../../services/firma.service'
import { getTenants } from '../../../services/tenant.service'
import FaturaInfo from '../../../components/muhasebe/fatura/FaturaInfo'
import SaveIcon from '@material-ui/icons/Save';
import { useRouter } from 'next/router';
import TahsilatYap from '../../../components/muhasebe/tahsilat'
import { validateFatura } from '../../../validators/fatura.validator'
import FirmaFaturalar from '../../../components/muhasebe/fatura/FirmaFaturalar';
import FirmaTahsilatlar from '../../../components/muhasebe/fatura/FirmaTahsilatlar';
import FaturaQuickInfo from '../../../components/muhasebe/fatura/FaturaQuickInfo';
import moment from 'moment'
import { uuid } from 'uuidv4';
import { Document, Page } from 'react-pdf'
import { eFaturaOnIzleme, gidenBelgeIndir } from '../../../services/efatura.service'
import { getFaturaBelge } from '../../../services/muhasebe.service'
import dynamic from 'next/dynamic'
const PdfViewer = dynamic(() => import('../../../components/pdfViewer'), { ssr: false })
import * as EFaturaCodes from '../../../constants/eFaturaStates';
import { useSelector } from 'react-redux'
import { getUrunler } from '../../../services/urun.service';
import FaturaInfo2 from '../../../components/muhasebe/fatura/FaturaInfo2'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    tahsilatEkrani: {
        width: '500px',
    },
    preview: {
        display: 'flex',
        justifyContent: 'center',
    }
}))

const Fatura = props => {
    const router = useRouter()
    const faturaRef = useRef({})
    const classes = useStyles()
    const { newFatura, firmalar, kalemler, tenants, urunler } = props
    const [form, setForm] = useState(props.fatura)
    const [openDrawer, setOpenDrawer] = useState(false)
    const [errors, setErrors] = useState(null)
    const [openFaturaPreview, setOpenFaturaPreview] = useState(false)
    const [preview, setPreview] = useState(null/*b()*/)
    const user = useSelector(state => state.auth.user)

    const aciklamaFormat = (aciklama) => {
        return ('' + aciklama).replace('{AY}', moment().locale('tr').format('MMMM-YYYY'))
    }

    const faturaHesapla = (kalemler) => {
        let faturaKdv = 0
        let faturaAraToplam = 0
        let faturaGenelToplam = 0

        for (let kalem of kalemler) {
            let araToplam = kalem.miktar * kalem.birimFiyat
            //let kdv = araToplam * (kalem.kdvOran / 100)
            //let genelToplam = araToplam + kdv

            kalem.toplam = araToplam
            //kalem.kdv = kdv
            kalem.adet = parseInt(kalem.adet)

            //faturaKdv += kdv
            faturaAraToplam += araToplam
            //faturaGenelToplam += genelToplam
            //faturaVergiMatrahi += araToplam
        }
        //faturaVergiMatrahi = faturaAraToplam * (/*fatura.kdvOrani*/180 / 100) // FIX ME
        faturaKdv = faturaAraToplam * (18 / 100)
        faturaGenelToplam = faturaAraToplam + faturaKdv

        return { faturaKdv, faturaAraToplam, faturaGenelToplam }
    }

    const getFaturaOnIzleme = async (fatura) => {
        eFaturaOnIzleme(fatura).then(resp => {
            if (resp) {
                let html = Buffer.from(resp, 'base64').toString('ascii')
                setPreview(html)
                setOpenFaturaPreview(true)
            }
        })
    }

    return <>
        <Drawer
            open={openDrawer}
            anchor='right'
            //openSecondary={true}
            //docked={true}
            onClose={() => setOpenDrawer(false)}
        >
            <div className={classes.tahsilatEkrani}>
                <TahsilatYap
                    fatura={form}
                    onCancel={() => { setOpenDrawer(false) }}
                    onOk={() => setOpenDrawer(false)}
                />
            </div>
        </Drawer>
        <PageHeader
            title='Fatura'
            buttons={[
                //(form.eFaturaDurum == EFaturaCodes.YENI_KAYIT && user.isAdmin && <Button variant='contained' color='secondary' onClick={() => {
                //    faturayiYenidenGonder(form._id).then(resp => {
                //        alert('Yeniden gonderildi')
                //    })
                //}}>Yeniden Gonder</Button>),
                (user.isAdmin && !newFatura && <Button variant='outlined' color='secondary' onClick={(e) => {
                    e.preventDefault()
                    faturayiYenidenGonder(form._id).then(resp => {
                        alert('Gonderildi')
                    })
                }} >Tekrar Gönder</Button>),
                //(!newFatura
                //    && form.belgeOid 
                //    && false
                //    //&& form.gidenBelgePdf?.length > 0 
                //    &&
                //    <Button
                //        variant='outlined'
                //        color='secondary'
                //        onClick={() => {
                //            getFaturaBelge(form._id)
                //        }}
                //    >PDF Indir</Button>),

                (newFatura && <Button variant='outlined' color='secondary' onClick={() => getFaturaOnIzleme(faturaRef.current)}>Fatura Ön İzleme</Button>),

                <Button
                    variant='outlined'
                    color='primary'
                    disabled={!form.taslak}
                    onClick={(e) => {
                        e.preventDefault()
                        let _errors = validateFatura(faturaRef.current)
                        if (_errors) {
                            setErrors(_errors)
                            return
                        }
                        if (newFatura) {
                            //form.taslak = true
                            saveFatura({ ...faturaRef.current, key: uuid(), taslak: true })
                            //saveFatura(form)
                            router.back()
                        }
                    }}
                >Taslak Olarak Kaydet</Button>,
                <Button
                    disabled={!newFatura && !form.taslak}
                    variant='contained'
                    color='primary'
                    startIcon={<SaveIcon />}
                    onClick={(e) => {
                        e.preventDefault()
                        let _errors = validateFatura(faturaRef.current)
                        if (_errors) {
                            setErrors(_errors)
                            return
                        }
                        if (newFatura) {
                            //form.taslak = false
                            //let toplamlar = faturaHesapla(faturaRef.current.kalemler)
                            let _fatura = {...faturaRef.current, key: uuid(), taslak: false}
                            for(let kalem of _fatura.kalemler) {
                                //kalem.aciklama = `${kalem.urunAdi|| ''} ${aciklamaFormat(kalem.aciklama)}`
                                kalem.aciklama = `${aciklamaFormat(kalem.aciklama)}`
                            }
                            saveFatura(
                                _fatura
                                //{ ...faturaRef.current, key: uuid(), taslak: false }
                            )

                            //saveFatura(form)
                            router.back()
                        }
                    }}
                >Kaydet ve Kes</Button>,
                (!newFatura &&
                    <Button
                        disabled={!form.acik}
                        variant='contained'
                        color='primary'
                        startIcon={<SaveIcon />}
                        onClick={(e) => {
                            e.preventDefault()
                            setOpenDrawer(true)
                        }}
                    >Tahsilat Yap</Button>)

            ]}
        />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12} md={12} lg={9}>
                    <FaturaInfo2
                        errors={errors}
                        faturaData={form}
                        newFatura={newFatura}
                        kalemler={kalemler}
                        urunler={urunler}
                        tenants={tenants}
                        faturaRef={faturaRef} />
                </Grid>
                {false &&
                    <Grid item xs={12} md={12} lg={8} container spacing={2}>
                        <Grid item xs={12} >
                            <FaturaInfo
                                errors={errors}
                                fatura={form}
                                urunler={urunler}
                                faturaKalemleri={kalemler}
                                tenants={tenants}
                                newFatura={newFatura}
                                onPreview={(e) => getFaturaOnIzleme(form)}
                                onChange={(e) => {
                                    let f = { ...form }
                                    if (e.field == 'kalemler') {
                                        let hesap = faturaHesapla(e.value, f)
                                        f.toplamKdv = hesap.faturaKdv
                                        f.araToplam = hesap.faturaAraToplam
                                        f.genelToplam = hesap.faturaGenelToplam
                                    }
                                    if (Array.isArray(e)) {
                                        for (let c of e) {
                                            f[c.field] = c.value
                                        }
                                    }
                                    else {
                                        f[e.field] = e.value
                                    }
                                    setForm(f)
                                }} />
                        </Grid>
                    </Grid>
                }
                <Grid item
                    xs={12}
                    lg={3}
                    container
                    spacing={2}
                    direction="column"
                    justify="flex-start"
                    alignItems="stretch">
                    {!newFatura &&
                        <Grid item>
                            <FaturaQuickInfo fatura={form} />
                        </Grid>
                    }
                    {faturaRef.current.vfirma &&
                        <Grid item>
                            <FirmaFaturalar firma={faturaRef.current.vfirma} />
                        </Grid>
                    }
                    {faturaRef.current.vfirma &&
                        <Grid item>
                            <FirmaTahsilatlar firma={faturaRef.current.vfirma} />
                        </Grid>
                    }
                </Grid>
            </Grid>

        </div>
        <Dialog
            fullWidth
            maxWidth='lg'
            onClose={() => { setOpenFaturaPreview(false) }} open={openFaturaPreview}>
            <DialogTitle onClose={() => { setOpenFaturaPreview(false) }}>
                Fatura Önizleme
            </DialogTitle>
            <DialogContent dividers>
                <div className={classes.preview}>
                    <div dangerouslySetInnerHTML={{ __html: preview }} />
                </div>
            </DialogContent>
            <DialogActions>
                <Button variant='contained' onClick={() => { setOpenFaturaPreview(false) }} color='primary'>Kapat</Button>
            </DialogActions>
        </Dialog>
    </>
}

Fatura.getInitialProps = async (ctx) => {
    let newFatura = ctx.query.id == 'new'

    let firma = null
    let kalemler = []
    let tenants = await getTenants()
    let urunler = await getUrunler()

    if (ctx.query.vfirma) {
        firma = await searchFirmalar(`key=${ctx.query.vfirma}&select=durum,eFaturaMi,marka,vade`)
        if (firma.length > 0) {
            firma = firma[0]
        }
    }

    let vade = 0
    let vadeTarihi = new Date()
    if (firma) {
        vade = firma.vade || 0
        vadeTarihi = moment(vadeTarihi).add(vade, 'days')
    }


    let fatura = {
        key: uuid(),
        vfirma: ctx.query.vfirma || null,
        firma: firma,
        duzenlemeTarihi: new Date(),
        aciklama: ctx.query.aciklama || '',
        kesenFirma: null,
        dovizTuru: '',
        vadeTarihi: vadeTarihi,
        vadeGun: vade,
        araToplam: 0,
        kdvOrani: 18,
        toplamKdv: 0,
        genelToplam: 0,
        taslak: true,
        acik: true,
        faturaTuru: 'SATIS',
        status: 'Yeni'
    }

    if (!newFatura) {
        fatura = await getFatura(ctx.query.id, 'populate=firma.marka')
        kalemler = await getFaturaKalemler(ctx.query.id)
    }

    return {
        newFatura,
        fatura: fatura,
        kalemler: kalemler,
        tenants: tenants,
        urunler
    }
}

export default Fatura