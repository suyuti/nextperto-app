import MaterialTable from 'material-table'
import React from 'react'
import PageHeader from '../../../components/pageHeader'
import api from '../../../services/axios.service';
import { useSelector } from 'react-redux'
import moment from 'moment'
import numbro from 'numbro'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { withRouter } from 'next/router'
var qs = require('qs');


const Faturalar = (props) => {
    const token = useSelector(state => state.auth.expertoToken)

    return (
        <div>
            <PageHeader
                title='Faturlar'
            />
            <div className='p-2'>
                <MaterialTable
                    title='Faturalar'
                    columns={[
                        { title: 'Fatura Açıklaması', field: 'aciklama' },
                        { title: 'Müşteri', field: 'firma.marka', cellStyle: { wordWrap: 'break-word' } },
                        { title: 'Firma', field: 'kesenFirma.marka', cellStyle: { textOverflow: 'ellipsis' } },
                        { title: 'Fatura No', field: 'faturaNo' },
                        { title: 'Ürünler', field: '' },
                        { title: 'Düzenleme Tarihi', field: 'duzenlemeTarihi', render: row => moment(row.duzenlemeTarihi).format('DD.MM.YYYY') },
                        { title: 'Vade Tarihi', field: 'vadeTarihi', render: row => moment(row.vadeTarihi).format('DD.MM.YYYY') },
                        { title: 'Toplam', field: 'genelToplam', render: row => `${numbro(row.genelToplam).format({ thousandSeparated: true, mantissa: 2 })} TL`, align: 'right' },
                        { title: 'Durum', field: 'durum' },
                    ]}
                    options={{
                        padding: 'dense'
                    }}
                    actions={[
                        {
                            icon: () => <ArrowForwardIcon />,
                            tooltip: 'Faturaya Git',
                            onClick: (event, rowData) => {
                                props.router.push(`/muhasebe/fatura/${rowData._id}`)
                            }
                        }
                      ]}
                    data={query => {
                        return new Promise((resolve, reject) => {
                            let filterObj = {
                                skip: query.page * query.pageSize,
                                limit: query.pageSize,
                                populate: 'firma.marka,kesenFirma.marka',
                                aciklama: `/${query.search}/i`,
                                sort: '-duzenlemeTarihi'
                            }
                            if (query.orderBy) {
                                filterObj.sort = query.orderBy ? `${query.orderDirection == 'asc' ? '' : '-'}${query.orderBy.field}` : ''
                            }
                            let q = qs.stringify(filterObj)

                            api.get(`/fatura?${q}`,
                                { headers: { Authorization: `Bearer ${token}` } }).then(result => {
                                    if (!result.data.data) {
                                        reject()
                                    }
                                    else {
                                        resolve({
                                            data: result.data.data.data,
                                            page: result.data.data.page / query.pageSize,
                                            totalCount: result.data.data.total,
                                        })
                                    }
                                })
                        })
                    }}
                />
            </div>
        </div>
    )
}

export default withRouter(Faturalar)
