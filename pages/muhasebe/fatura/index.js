import React, { useState, useEffect } from 'react'
import PageHeader from "../../../components/pageHeader";
import { makeStyles, Button, Switch, Grid, TextField, MenuItem, Paper, ButtonGroup, Typography, Box } from '@material-ui/core';
import { useRouter } from 'next/router';
import TableView from '../../../components/tableview';
import { useSelector } from 'react-redux'
import moment from 'moment'
import AddIcon from '@material-ui/icons/Add'
import { getTenants } from '../../../services/tenant.service';
import FaturaSearchToolbar from '../../../components/muhasebe/fatura/FaturaSearchToolbar';
import { getFaturaBelge } from '../../../services/muhasebe.service';
import '../../../constants/eFaturaStates'
import * as EFaturaCodes from '../../../constants/eFaturaStates';
import numbro from 'numbro';
//numbro.setLanguage('tr-TR');

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    durum: {
        textAlign: 'center',
    },
    toplamCell: {

    },
    genelToplam: {
        fontWeight: '600'
    },
    tahsilatTutari: {
        fontSize: 'x-small',
        fontWeight: '200'
    }
}))

const Faturalar = props => {
    const { tenants } = props
    const classes = useStyles()
    const router = useRouter()
    const token = useSelector(state => state.auth.expertoToken)
    const [filter, setFilter] = useState(router.query.filter)

    const onFilterSearch = (query) => {
        let q = ''

        if (query.acik) {
            if (query.acik == 'acik') {
                q += `acik=${false}&`
            }
            else if (query.acik == 'kapali') {
                q += `acik=${true}&`
            }
        }

        if (query.firma) {
            q += `vfirma=${query.firma}&`
        }
        if (query.kesenFirma) {
            q += `vkesenFirma=${query.kesenFirma}&`
        }

        if (query.altTutar != 0 && query.ustTutar == 0) {
            q += `genelToplam>=${query.altTutar}&`
        }
        else if (query.altTutar == 0 && query.ustTutar != 0) {
            q += `genelToplam<=${query.ustTutar}&`
        }
        else if (query.altTutar != 0 && query.ustTutar != 0) {
            q = q + `filter={"$and":[{"genelToplam":{"$gte":"${query.altTutar}"}},{"genelToplam":{"$lte":"${query.ustTutar}"}}]}`
        }

        if (query.baslangicTarihi && !query.bitisTarihi) {
            q = q + `vadeTarihi>=${moment(query.baslangicTarihi).toISOString()}&`
        }
        else if (!query.baslangicTarihi && query.bitisTarihi) {
            q = q + `vadeTarihi<=${moment(query.baslangicTarihi).toISOString()}&`
        }
        else if (query.baslangicTarihi && query.bitisTarihi) {
            q = q + `filter={"$and":[{"vadeTarihi":{"$gte":"${moment(query.baslangicTarihi).toISOString()}"}},{"vadeTarihi":{"$lte":"${moment(query.bitisTarihi).toISOString()}"}}]}`
        }

        setFilter(q)
    }

    useEffect(() => {
        setFilter(router.query.filter)
    }, [router.query])


    const durum = (row) => {
        if (row.status == 'Yeni') {
            return <div className='p-1 bg-blue-200 text-center border shadow rounded'>Yeni</div>
        }
        else if (row.status == 'Gonderildi') {
            return <div className='p-1 bg-yellow-200 text-center border shadow rounded'>Gönderildi</div>
        }
        else if (row.status == 'Basarili') {
            return <div className='p-1 bg-green-200 text-center border shadow rounded'>Başarılı</div>
        }
        else if (row.status == 'Ret') {
            return <div className='p-1 bg-red-200 text-center border shadow rounded'>Ret</div>
        }
        else if (row.status == 'Hata') {
            return <div className='p-1 bg-red-200 text-center boder shadow rounded '>Hata</div>
        }
        else {
            return <div className='p-1 bg-gray-200 text-center border shadow rounded'>...</div>
        }
    }

    const _durum = (row) => {
        if (row.eFaturaDurum == EFaturaCodes.YENI_KAYIT) {
            return <Box className={classes.durum} bgcolor="info.main" color="primary.contrastText" p={1}>Yeni Kayıt</Box>
        }
        else {
            if (row.eFaturaDurum == EFaturaCodes.ALINDI) {
                return <Box className={classes.durum} bgcolor="info.main" color="primary.contrastText" p={1}>Gönderildi</Box>
            }
            else if (row.eFaturaDurum == EFaturaCodes.ISLENDI) {
                if (row.gonderimDurumu == EFaturaCodes.GIB_GONDERILECEK) {
                    return <Box className={classes.durum} bgcolor="info.main" color="primary.contrastText" p={1}>GIB'e gönderilecek</Box>
                }
                else if (row.gonderimDurumu == EFaturaCodes.GIB_GONDERILDI) {
                    return <Box className={classes.durum} bgcolor="info.main" color="primary.contrastText" p={1}>GIB'e gönderildi</Box>
                }
                else if (row.gonderimDurumu == EFaturaCodes.ALICI_YANITLADI) {
                    if (row.yanitDurumu == EFaturaCodes.ALICI_YANITI_BEKLENIYOR) {
                        return <Box className={classes.durum} bgcolor="info.main" color="primary.contrastText" p={1}>Alıcıdan yanıt bekleniyor</Box>
                    }
                    else if (row.yanitDurumu == EFaturaCodes.ALICI_REDDETTI) {
                        return <Box className={classes.durum} bgcolor="error.main" color="primary.contrastText" p={1}>Alıcı Reddetti</Box>
                    }
                    else if (row.yanitDurumu == EFaturaCodes.ALICI_KABUL_ETTI) {
                        return <Box className={classes.durum} bgcolor="success.main" color="primary.contrastText" p={1}>Başarılı</Box>
                        //return "Alıcı Kabul Etti"
                    }
                    else if (row.yanitDurumu == EFaturaCodes.ALICI_YANITI_GEREKMIYOR) {
                        return <Box className={classes.durum} bgcolor="success.main" color="primary.contrastText" p={1}>Başarılı</Box>
                        //"Alıcı Yanıtı Gerekmiyor TF"
                    }
                }
                if (row.gonderimDurumu == EFaturaCodes.GIB_YANITLADI) {
                    if (row.gonderimCevabiKodu == 1200) {
                        return <Box className={classes.durum} bgcolor="info.main" color="primary.contrastText" p={1}>GIB onayladi</Box>
                    }
                    else if (row.gonderimCevabiKodu == 1300 && row.gonderimDurumu == EFaturaCodes.ALICI_YANITLADI) {
                        return <Box className={classes.durum} bgcolor="success.main" color="primary.contrastText" p={1}>Başarılı</Box>
                    }
                }
            }
            else if (row.eFaturaDurum == EFaturaCodes.ISLENEMEDI) {
                return <Box className={classes.durum} bgcolor="error.main" color="primary.contrastText" p={1}>İşlenemedi</Box>
            }
            else {
                return '.'
            }
        }
    }


    const columns = [
        { id: 'aciklama', label: 'Fatura Açıklaması' },
        { id: 'firma.marka', label: 'Firma' },
        { id: 'faturaNo', label: 'Fatura No' },
        { id: 'kesenFirma.marka', label: 'Kesen Firma' },
        { id: 'duzenlemeTarihi', label: 'Düzenleme Tarihi', render: row => { return moment(row.duzenlemeTarihi).format('DD.MM.YYYY') } },
        {
            id: 'vadeTarihi', label: 'Vade Tarihi', render: row => {
                return moment(row.vadeTarihi).format('DD.MM.YYYY')
            }
        },
        {
            id: 'genelToplam', label: 'Toplam', align: 'right',
            render: row => {
                return <div className={classes.toplamCell}>
                    <div className={classes.genelToplam}>
                        {`${numbro(row.genelToplam).format({ thousandSeparated: true, mantissa: 2 })} TL`}
                    </div>
                    <div className={classes.tahsilatTutari}>
                        {`${numbro(row.tahsilatTutari).format({ thousandSeparated: true, mantissa: 2 })} TL`}
                    </div>
                </div>
            }
        },
        {
            id: 'durum', label: 'Durum', align: 'right', render: row => {
                return durum(row)
            }
        },
        {
            id: '', label: '', render: row => {
                return <ButtonGroup>
                    <Button
                        variant='outlined'
                        color='primary'
                        size='small'
                        onClick={() => {
                            router.push(`/muhasebe/fatura/${row._id}`)
                        }}>Detay</Button>
                    <Button
                        variant='outlined'
                        color='primary'
                        size='small'
                        disabled
                        onClick={() => {
                            //getFaturaBelge(row._id)
                        }}
                    >Belge</Button>

                </ButtonGroup>
            }
        }
    ]

    return <>
        <PageHeader
            title='Faturalar'
            buttons={[
                <Button
                    variant='contained'
                    color='primary'
                    startIcon={<AddIcon />}
                    onClick={() => {
                        router.push('/muhasebe/fatura/[id]', '/muhasebe/fatura/new')
                    }}>Yeni Fatura</Button>
            ]}
        />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <FaturaSearchToolbar
                        //disabled 
                        tenants={tenants}
                        onSearch={onFilterSearch} />
                </Grid>
                <Grid item xs={12}>
                    <TableView
                        title='Faturalar'
                        columns={columns}
                        remoteUrl={`/fatura`}
                        token={token}
                        //filter={`${router.query.filter}&vkesenFirma=${selectedTenant}&select=aciklama,faturaNo,vkesenFirma,vfirma,vadeTarihi,duzenlemeTarihi,genelToplam,taslak&populate=firma.marka,kesenFirma.marka`}
                        filter={`${filter}&select=aciklama,status,eFaturaDurum,gonderimDurumu,tahsilatTutari,gonderimCevabiKodu,yanitDurumu,faturaNo,vkesenFirma,vfirma,vadeTarihi,duzenlemeTarihi,genelToplam,taslak&populate=firma.marka,kesenFirma.marka&sort=-duzenlemeTarihi`}
                        searchCol='aciklama'
                    //data={[]}
                    />
                </Grid>
            </Grid>
        </div>
    </>
}

Faturalar.getInitialProps = async (ctx) => {
    let tenants = await getTenants()
    return {
        tenants
    }
}

export default Faturalar
