import React, { useState } from 'react'
import PageHeader from '../../../components/pageHeader'
import { Button, Drawer, MenuItem, TextField } from '@material-ui/core'
import PhoneIcon from '@material-ui/icons/Phone';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import { getGelenFatura, getGelenFaturaBelge, odemeYap, odemePlanla } from '../../../services/muhasebe.service';
import numbro from 'numbro'
import moment from 'moment'
import cn from 'classnames'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import trLocale from "date-fns/locale/tr";
import MoneyInput from '../../../components/core/MoneyInput'


const FirmaBilgi = ({ firmaAdi, adresi, vd, vn, telefon, mail }) => {
    return (
        <div className=''>
            <h2 className='text-lg'>{firmaAdi}</h2>
            <h2 className='font-light'>{adresi}</h2>
            <h2 className='font-light'>{vd}</h2>
            <h2 className='font-light'>{vn}</h2>
            <div className='flex items-center space-x-2 mt-2'>
                <PhoneIcon fontSize='small' />
                <h2 className='font-light'>{telefon}</h2>
            </div>
            <div className='flex items-center space-x-2'>
                <MailOutlineIcon fontSize='small' />
                <h2 className='font-light'>{mail}</h2>
            </div>
        </div>
    )
}

const FaturaDurum = props => {
    const { fatura } = props
    if (fatura.odendi) {
        return (<div className='p-2 bg-green-400 rounded shadow flex items-center align-middle justify-center space-x-2'>
            <h2 className='text-white font-semibold text-lg'>ÖDENDİ</h2>
        </div>)
    }
    else {
        if (fatura.odemePlanlandi) {
            return (<div className='p-2 bg-blue-400 rounded shadow flex items-center space-x-2'>
                <h2 className='text-gray-100 text-lg'>{`${moment(fatura.planlananOdemeTarihi).format("DD.MM.YYYY")} Tarihine Ödeme Planlandı`}</h2>
                <Button variant='contained' size='small'>Değiştir</Button>
            </div>)
        }
        else {
            return (
                <div className='p-2 bg-red-400 rounded shadow flex items-center align-middle justify-center space-x-2'>
                    <h2 className='text-white font-semibold text-lg'>AÇIK</h2>
                </div>
            )
        }
    }
}
const FaturaBilgi = ({ fatura }) => {
    return (
        <div className='w-1/4'>
            <div className='flex justify-between items-center'>
                <h2 className='font-light'>Fatura No</h2>
                <h2 className=''>{fatura.faturaNo}</h2>
            </div>
            <div className='flex justify-between items-center'>
                <h2 className='font-light'>Fatura Tarihi</h2>
                <h2>{fatura.faturaTarihi}</h2>
            </div>
        </div>
    )
}

const FaturaDip = props => {
    const { fatura } = props
    return (
        <div className='w-1/4'>
            <table className='w-full'>
                <tbody>
                    <tr>
                        <th align='left'>Ara Toplam</th>
                        <td align='right'>{`${numbro(fatura.vergiHaricToplam).format({ thousandSeparated: true, mantissa: 2 })} TL`}</td>
                    </tr>
                    <tr>
                        <th align='left'>KDV</th>
                        <td align='right'>{`${numbro(fatura.vergiler[0].toplamVergiTutari).format({ thousandSeparated: true, mantissa: 2 })} TL`}</td>
                    </tr>
                    <tr>
                        <th align='left'>Genel Toplam</th>
                        <td className='text-xl' align='right'>{`${numbro(fatura.odenecekTutar).format({ thousandSeparated: true, mantissa: 2 })} TL`}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

const FaturaKalemler = props => {
    const { kalemler } = props
    return (
        <div className='w-full'>
            <table className='w-full'>
                <thead className='bg-green-100'>
                    <tr className=''>
                        <th className='p-2' align='left'>Ürun</th>
                        <th className='p-2' align='center'>Miktar</th>
                        <th className='p-2' align='center'>Birim Fiyat</th>
                        <th className='p-2' align='center'>Tutar</th>
                    </tr>
                </thead>
                <tbody>
                    {kalemler.map((k, i) => {
                        return (
                            <tr className={cn(
                                'hover:bg-blue-100',
                                {
                                    'bg-blue-50': (i % 2) != 0
                                })}>
                                <td align='left' className='font-light py-1 pl-2 '>{k.urunAdi}</td>
                                <td align='left'>{k.miktar}</td>
                                <td align='right'>{numbro(k.birimFiyat).format({ thousandSeparated: true, mantissa: 2 })} TL</td>
                                <td align='right'>{numbro(k.malHizmetMiktari).format({ thousandSeparated: true, mantissa: 2 })} TL</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

const OdemePlanlaForm = props => {
    const { fatura, onClose, onOk } = props
    const [plan, setPlan] = useState({
        fatura: fatura,
        odemeYapacakFirma: fatura.aliciFirma,
        planlananOdemeTarihi: null,
        planlananOdemeTutari: fatura.odenecekTutar,
        odemeTuru: '',
        createdBy: '',
        createdAt: new Date(),
        durum: 'acik',
    })

    return (
        <div className='p-4 flex-col space-y-2'>
            <h2 className='text-2xl text-center'>ÖDEME PLANLA</h2>
            <TextField
                label='Ödeme Yapacak Firma'
                fullWidth
                variant='outlined'
                size='small'
                value={plan.odemeYapacakFirma.adi}
            />
            <TextField
                label='Ödeme Tutarı'
                variant='outlined'
                fullWidth
                size='small'
                value={`${numbro(fatura.odenecekTutar).format({ thousandSeparated: true, mantissa: 2 })} TL`}
            />
            <MuiPickersUtilsProvider
                utils={DateFnsUtils} locale={trLocale}
            >
                <KeyboardDatePicker
                    //error={errors && errors.duzenlemeTarihi}
                    //helperText={errors && errors.duzenlemeTarihi}
                    autoOk
                    //disableFuture={false}
                    //minDate={moment().add(-7, 'days')}
                    //maxDate={moment()}
                    size='small'
                    clearable
                    fullWidth
                    variant="inline"
                    inputVariant="outlined"
                    label="Ödeme Planı Tarihi"
                    format="dd.MM.yyyy"
                    InputAdornmentProps={{ position: 'start' }}
                    value={plan.planlananOdemeTarihi}
                    onChange={e => {
                        let _p = {...plan}
                        _p.planlananOdemeTarihi = e
                        setPlan(_p)
                    }}
                />
            </MuiPickersUtilsProvider>
            <TextField
                label='Ödeme Türü'
                fullWidth
                variant='outlined'
                size='small'
                select
                value={plan.odemeTuru}
                onChange={e => {
                    let _p = {...plan}
                    _p.odemeTuru = e.target.value
                    setPlan(_p)
                }}
        >
                <MenuItem key='nakit' value='nakit'>Nakit</MenuItem>
                <MenuItem key='eft' value='eft'>EFT</MenuItem>
                <MenuItem key='cek' value='cek'>Çek</MenuItem>
            </TextField>
            <div className='flex justify-end space-x-2'>
                <Button variant='contained' color='primary' onClick={onClose}>İPTAL</Button>
                <Button variant='contained' color='primary' onClick={() => {
                    odemePlanla(plan).then(result => {
                        onClose()
                    })
                }}>ÖDEME PLANLA</Button>
            </div>
        </div>
    )
}

const OdemeYapForm = props => {
    const { fatura, onClose, onOk } = props
    const [odeme, setOdeme] = useState({
        fatura: fatura,
        odemeTarihi: new Date(),
        odemeTutari: fatura.odenecekTutar,
        odemeYapilanHesap: '',
        odemeTuru: '',
        createdBy: '',
        odemeYapanFirma: fatura.aliciFirma,
        createdAt: new Date()
    })

    return (
        <div className='p-4 flex-col space-y-2'>
            <h2 className='text-2xl text-center'>ÖDEME YAP</h2>
            <TextField
                label='Ödeme Yapan Firma'
                fullWidth
                variant='outlined'
                size='small'
                value={odeme.odemeYapanFirma.adi}
            />
            <TextField
                label='Ödeme Tutarı'
                fullWidth
                variant='outlined'
                size='small'
                InputProps={{
                    //readOnly,
                    inputComponent: MoneyInput
                }}
                value={odeme.odemeTutari}
                onChange={e => {
                    let _odeme = { ...odeme }
                    _odeme.odemeTutari = e.target.value
                    setOdeme(_odeme)
                }}
            />
            <TextField
                label='Ödeme Yapılan Hesap'
                fullWidth
                variant='outlined'
                size='small'
                select
                value={odeme.odemeYapilanHesap}
                onChange={e => {
                    let _odeme = { ...odeme }
                    _odeme.odemeYapilanHesap = e.target.value
                    setOdeme(_odeme)
                }}
            >
                <MenuItem key={1} value='HESAP1'>Hesap 1</MenuItem>
            </TextField>
            <TextField
                label='Ödeme Türü'
                fullWidth
                variant='outlined'
                size='small'
                select
                value={odeme.odemeTuru}
                onChange={e => {
                    let _odeme = { ...odeme }
                    _odeme.odemeTuru = e.target.value
                    setOdeme(_odeme)
                }}
            >
                <MenuItem key='nakit' value='nakit'>Nakit</MenuItem>
                <MenuItem key='eft' value='eft'>EFT</MenuItem>
                <MenuItem key='cek' value='cek'>Çek</MenuItem>
            </TextField>
            <MuiPickersUtilsProvider
                utils={DateFnsUtils} locale={trLocale}
            >
                <KeyboardDatePicker
                    //error={errors && errors.duzenlemeTarihi}
                    //helperText={errors && errors.duzenlemeTarihi}
                    autoOk
                    //disableFuture={false}
                    //minDate={moment().add(-7, 'days')}
                    //maxDate={moment()}
                    size='small'
                    clearable
                    fullWidth
                    variant="inline"
                    inputVariant="outlined"
                    label="Ödeme Tarihi"
                    format="dd.MM.yyyy"
                    InputAdornmentProps={{ position: 'start' }}
                    onChange={(e) => { }}
                    value={odeme.odemeTarihi}
                    onChange={e => {
                        let _odeme = { ...odeme }
                        _odeme.odemeTarihi = e
                        setOdeme(_odeme)
                    }}
                />
            </MuiPickersUtilsProvider>

            <div className='flex justify-end space-x-2'>
                <Button variant='contained' color='primary' onClick={onClose}>İPTAL</Button>
                <Button variant='contained' color='primary' onClick={() => {
                    odemeYap(odeme).then(result => {
                        onClose()
                    })
                }}>ÖDEME YAP</Button>
            </div>
        </div>
    )
}

const Gider = props => {
    const { fatura } = props
    const [openDrawer, setOpenDrawer] = useState(false)
    const [islem, setIslem] = useState('')

    return <>
        <PageHeader
            title='Giderler'
            buttons={[
                <Button variant='contained' color='primary' onClick={() => {
                    getGelenFaturaBelge(fatura._id)
                }}>Belge İndir</Button>,
                <Button
                    variant='contained'
                    disabled={fatura.odemePlanlandi || fatura.odendi}
                    color='primary'
                    onClick={() => {
                        setIslem('odemePlanla')
                        setOpenDrawer(true)
                    }}>Ödeme Planla</Button>,

                <Button
                    variant='contained'
                    disabled={fatura.odendi}
                    color='primary'
                    onClick={() => {
                        setIslem('odemeYap')
                        setOpenDrawer(true)
                    }}>Ödeme Yap</Button>,
            ]}
        />
        <div className='p-2 border m-2 bg-white rounded flex-col'>
            <div className='saticifirma flex p-4 justify-between '>
                <FaturaBilgi fatura={fatura} />
                <FaturaDurum fatura={fatura} />
            </div>
            <div className='flex justify-around flex-row-reverse'>
                <div className='alicifirma p-2'>
                    <FirmaBilgi
                        firmaAdi={fatura.aliciFirma.adi}
                        adresi={fatura.aliciFirma.adres}
                        vd={fatura.aliciFirma.vd}
                        vn={fatura.aliciFirma.vkn}
                        telefon={fatura.aliciFirma.kontakTelefon}
                        mail={fatura.aliciFirma.kontakMail}
                    />
                </div>
                <div className='saticifirma p-2'>
                    <FirmaBilgi
                        firmaAdi={fatura.saticiUnvan}
                        adresi={`${fatura.saticiCaddeSokak} ${fatura.saticiIlce} ${fatura.saticiSehir}`}
                        vd={fatura.saticiVergiDairesi}
                        vn={fatura.saticiVergiNo}
                        telefon={fatura.saticiTel}
                        mail={fatura.saticiEposta}
                    />
                </div>
            </div>


            <div className='fatura w-full p-4'>
                <FaturaKalemler kalemler={fatura.kalemler} />
            </div>
            <div className='kalemler flex justify-end p-4'>
                <FaturaDip fatura={fatura} />
            </div>

            <Drawer
                className=''
                anchor='right'
                open={openDrawer}
                onClose={() => setOpenDrawer(false)}
            >
                <div className='p-2'>
                    {islem == 'odemeYap' &&
                        <div className='pt-12'>
                            <OdemeYapForm
                                fatura={fatura}
                                onClose={() => setOpenDrawer(false)}
                                onOk={() => {

                                }}
                            />
                        </div>
                    }
                    {islem == 'odemePlanla' &&
                        <div className='pt-12'>
                            <OdemePlanlaForm
                                fatura={fatura}
                                onClose={() => setOpenDrawer(false)}
                                onOk={() => {

                                }}
                            />
                        </div>
                    }
                </div>
            </Drawer>
        </div>
    </>
}

Gider.getInitialProps = async (ctx) => {
    let fatura = await getGelenFatura(ctx.query.id)

    return {
        fatura
    }
}

export default Gider