import React from 'react'
import PageHeader from '../../../components/pageHeader'
import { Button, IconButton } from '@material-ui/core'
import MaterialTable from 'material-table'
import api from '../../../services/axios.service';
import moment from 'moment'
import numbro from 'numbro'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { withRouter } from 'next/router'
import { useSelector } from 'react-redux'
import InsertDriveFileOutlinedIcon from '@material-ui/icons/InsertDriveFileOutlined';
import { getGelenFatura, getGelenFaturaBelge } from '../../../services/muhasebe.service';
var qs = require('qs');

const Gider = props => {
    const token = useSelector(state => state.auth.expertoToken)
    return <>
        <PageHeader
            title='Giderler'
            buttons={[
                <Button variant='contained' color='primary'>Fiş / Fatura</Button>
            ]}
        />
        <div className='p-2'>
            <MaterialTable
                title='Gelen Faturalar'
                columns={[
                    { title: 'Kesen Firma', field: 'saticiUnvan' },
                    { title: 'Alan Firma', field: 'aliciFirma.adi' },
                    { title: 'Toplam', align: 'right', field: 'odenecekTutar', render: row => `${numbro(row.odenecekTutar).format({ thousandSeparated: true, mantissa: 2 })} TL` },
                    { title: 'Tarih', field: 'faturaTarihi' },
                    { title: 'Fatura No', field: 'faturaNo' },
                    {
                        title: 'Durum', field: '', render: row => {
                            if (row.odendi) {
                                return (
                                    <div className='bg-green-500 w-28 text-center px-2 py-1 text-white font-semibold rounded-lg'>
                                        <h2>ÖDENDİ</h2>
                                    </div>
                                )
                            }
                            else {
                                if (row.odemePlanlandi) {
                                    return (
                                        <div className='bg-blue-500 w-28 text-center px-2 py-1 text-white font-semibold rounded-lg'>
                                            <h2>PLANLANDI</h2>
                                        </div>
                                    )
                                }
                                else {
                                    return (
                                        <div className='bg-red-500 w-28 text-center px-2 py-1 text-white font-semibold rounded-lg'>
                                            <h2>AÇIK</h2>
                                        </div>
                                    )
                                }
                            }
                            // 1. odendi
                            // 2. Acik
                            // 3. planlandi
                        }
                    },
                    {
                        title: 'Belge', field: '', render: row => {
                            return (
                                <IconButton className='focus:outline-none' onClick={() => {
                                    getGelenFaturaBelge(row._id)
                                }}>
                                    <InsertDriveFileOutlinedIcon />
                                </IconButton>
                            )
                        }
                    },
                ]}
                options={{
                    padding: 'dense',
                    pageSize: 25,
                    pageSizeOptions: [10, 25, 50, 100]
                }}
                actions={[
                    {
                        icon: () => <ArrowForwardIcon />,
                        tooltip: 'Faturaya Git',
                        onClick: (event, rowData) => {
                            props.router.push(`/muhasebe/gider/${rowData._id}`)
                        }
                    }
                ]}

                data={query => {
                    return new Promise((resolve, reject) => {
                        let filterObj = {
                            skip: query.page * query.pageSize,
                            limit: query.pageSize,
                            populate: 'aliciFirma',
                            //aciklama: `/${query.search}/i`,
                            sort: '-belgeTarihi'
                        }
                        if (query.orderBy) {
                            filterObj.sort = query.orderBy ? `${query.orderDirection == 'asc' ? '' : '-'}${query.orderBy.field}` : ''
                        }
                        let q = qs.stringify(filterObj)

                        api.get(`/gelenfatura?${q}`,
                            { headers: { Authorization: `Bearer ${token}` } })
                            .then(result => {
                                if (!result.data.data) {
                                    reject()
                                }
                                else {
                                    resolve({
                                        data: result.data.data.data,
                                        page: result.data.data.page / query.pageSize,
                                        totalCount: result.data.data.total,
                                    })
                                }
                            })
                    })
                }}
            />
        </div>
    </>
}

export default withRouter(Gider)