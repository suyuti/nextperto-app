import React from 'react'
import PageHeader from '../../../components/pageHeader'
import { Accordion, Box, Button, makeStyles, AccordionSummary, Grid, Typography } from '@material-ui/core'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DateRangeIcon from '@material-ui/icons/DateRange';
import { useRouter } from 'next/router';
import TableView from '../../../components/tableview';
import { useSelector } from 'react-redux'
import moment from 'moment'
import numbro from 'numbro';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

moment.locale('tr')

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const TahsilatPlanlari = props => {
    const classes = useStyles()
    const router = useRouter()
    const token = useSelector(state => state.auth.expertoToken)
    const columns = [
        { id: 'firma.marka', label: 'Firma' },
        { id: 'tahsilatYapanFirma.marka', label: 'Alıcı Firma' },
        { id: 'createdAt', label: 'Planlama Tarihi', render: row => moment(row.createdAt).format('DD.MM.YYYY') },
        { id: 'tarih', label: 'Tarih', render: row => moment(row.tarih).format('DD.MM.YYYY') },
        { id: 'tutar', align: 'right', label: 'Tutar', render: row => `${numbro(row.tutar).format({ thousandSeparated: true, mantissa: 2 })} TL` },
        { id: 'tarih', label: 'Süre', render: row => moment(row.tarih).fromNow() },
        {
            id: 'aciklama', label: 'Durum', render: row => {
                if (row.durum == 'acik') {
                    if (moment().isBefore(row.tarih)) {
                        return <Box bgcolor="info.main" color="primary.contrastText" borderRadius={2} padding='2px' textAlign='center' fontSize={14}>Bekliyor</Box>
                    }
                    else {
                        return <Box bgcolor="error.main" color="primary.contrastText" borderRadius={2} padding='2px' textAlign='center' fontSize={14}>Ödenmedi</Box>
                    }
                }
                else if (row.durum == 'odendi') {
                    return <Box bgcolor="success.main" color="primary.contrastText" borderRadius={2} padding='2px' textAlign='center' fontSize={14}>Ödendi</Box>
                }
            }
        },
        { id: '', render: row => <Button variant='outlined' color='primary' size='small' onClick={() => { 
            router.push(`/muhasebe/tahsilatPlan/${row._id}`)
        }}>Detay</Button> },
    ]

    return <>
        <PageHeader
            title='Planlanmış Tahsilatlar'
            buttons={[
                <Button variant='contained' color='primary' startIcon={<DateRangeIcon />} onClick={() => {
                    router.push('/muhasebe/tahsilatPlan/[id]', '/muhasebe/tahsilatPlan/new')
                }}>Tahsilat Planla</Button>
            ]}
        />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1c-content"
                            id="panel1c-header">
                            <Typography>Grafik</Typography>
                        </AccordionSummary>
                    </Accordion>
                </Grid>
                <Grid item xs={12}>
                    <TableView
                        title='Planlanmış Tahsilatlar'
                        columns={columns}
                        remoteUrl={`/planlanmistahsilat`}
                        token={token}
                        filter={`select=aciklama,tarih,tutar,durum,duzenlemeTarihi,genelToplam,vfirma,vtahsilatYapanFirma&populate=firma.marka,tahsilatYapanFirma,marka`}
                        searchCol='aciklama'
                    />
                </Grid>
            </Grid>
        </div>
    </>
}

export default TahsilatPlanlari