import React, { useState } from 'react'
import PageHeader from '../../../components/pageHeader'
import { Button, makeStyles, Grid } from '@material-ui/core'
import TahsilatPlanInfo from '../../../components/muhasebe/tahsilat/TahsilatPlan'
import Tahsilat from '../tahsilat/[id]'
import FirmaPlanlanmisTahsilatlar from '../../../components/muhasebe/fatura/FirmaPlanlanmisTahsilatlar'
import FirmaTahsilatlar from '../../../components/muhasebe/fatura/FirmaTahsilatlar'
import FirmaFaturalar from '../../../components/muhasebe/fatura/FirmaFaturalar'
import {validatePlanlanmisTahsilat} from '../../../validators/planlanmisTahsilat.validator'
import {savePlanlanmisTahsilat, updatePlanlanmisTahsilat, getPlanlanmisTahsilat} from '../../../services/muhasebe.service'
import {searchFirmalar} from '../../../services/firma.service'
import { getTenants } from '../../../services/tenant.service'
import {useRouter} from 'next/router'
const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))
const TahsilatPlan = props => {
    const {newTahsilatPlani, tenants} = props
    const router = useRouter()
    const classes = useStyles()
    const [form, setForm] = useState(props.tahsilatPlani)
    const [errors, setErrors] = useState(null)
    const onChange = (value) => {
        let f = { ...form }
        f[value.field] = value.value
        setForm(f)
    }

    return <>
        <PageHeader
            title='Tahsilat Planlama'
            buttons={[
                <Button
                    disabled={form.durum == 'odendi'} 
                    variant='contained' 
                    color='primary' 
                    onClick={(e) => {
                    e.preventDefault()
                    let _errors = validatePlanlanmisTahsilat(form)
                    if (_errors) {
                        setErrors(_errors)
                        return
                    }
                    if (newTahsilatPlani) {
                        savePlanlanmisTahsilat(form)
                        router.back()
                    }
                    else {
                        updatePlanlanmisTahsilat(form)
                        router.back()
                    }
                }}>Kaydet</Button>,
                !newTahsilatPlani && <Button 
                disabled={form.durum == 'odendi'}
                color='primary' 
                variant='contained' >Tahsilat Yap</Button>
            ]}
        />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={8}>
                    <TahsilatPlanInfo 
                        tenants={tenants}
                        tahsilatPlani={form} 
                        isNew={newTahsilatPlani}
                        errors={errors} 
                        onChange={onChange} />
                </Grid>
                <Grid item
                    xs={4}
                    container
                    spacing={2}
                    direction="column"
                    justify="flex-start"
                    alignItems="stretch">
                    {form.vfirma &&
                        <Grid item>
                            <FirmaFaturalar firma={form.vfirma} />
                        </Grid>
                    }
                    {form.vfirma &&
                        <Grid item>
                            <FirmaTahsilatlar firma={form.vfirma} />
                        </Grid>
                    }
                    {form.vfirma &&
                        <Grid item>
                            <FirmaPlanlanmisTahsilatlar firma={form.vfirma} />
                        </Grid>
                    }
                </Grid>

            </Grid>
        </div>
    </>
}

TahsilatPlan.getInitialProps = async (ctx) => {
    let newTahsilatPlani = (ctx.query.id == 'new')
    let tahsilatPlani = {
        vfirma              : ctx.query.firma || '',
        vtahsilatYapanFirma : '',
        tarih               : null,
        tutar               : 0,
        aciklama            : '',
        vcreatedBy          : '',
        durum               : 'acik',
    }
    let tenants = await getTenants()

    if (!newTahsilatPlani) {
        tahsilatPlani = await getPlanlanmisTahsilat(ctx.query.id)
    }
    if (ctx.query.firma) {
        let firmalar = await searchFirmalar(`key=${ctx.query.firma}`)
        if (firmalar.length > 0) {
            tahsilatPlani.firma = firmalar[0]
        }
    }


    return {
        newTahsilatPlani,
        tahsilatPlani,
        tenants
    }
}

export default TahsilatPlan