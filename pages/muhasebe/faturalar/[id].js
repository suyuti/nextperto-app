import React, { useState } from 'react'
import PageHeader from '../../../components/pageHeader'
import { Grid, Card, Divider, CardContent, CardHeader, TextField, Typography } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { parseCookies } from 'nookies'
import api from '../../../services/axios.service'
import { useSelector } from 'react-redux'
//import { signIn, signOut, useSession } from 'next-auth/client'
import * as Actions from '../../../redux/actions'

const Fatura = props => {
    const { firmalarimiz } = props
    const [fatura, setFatura] = useState(props.fatura)
    const message = useSelector(state => state.common.message)
    const musteriler = useSelector(state => state.fatura.musteriler)
    const [session, loading] = useSession()

    const save = async (fatura) => {
        api.post(`/fatura`, fatura, {
            headers: { authorization: props.authorization },
        })
            .then(resp => {
            })
    }

    if (!session) {
        return <div className='page-content'>
            <Typography variant='h5'>Giris Yapmalisiniz</Typography>
        </div>

    }


    return <>
        <PageHeader
            title={`Fatura`}
            buttons={[
                {
                    title: 'Kaydet', disabled: false, onClick: (e) => {
                        e.preventDefault();
                        save(fatura)
                        //router.push("/muhasebe/faturalar/[id]", `/muhasebe/faturalar/new`);
                    }
                },
            ]} />
        <div className='page-content'>
            <Grid container spacing={2}>
                <Grid item xs={8}>
                    <Card>
                        <CardHeader title='Fatura' />
                        <Divider />
                        <CardContent>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <TextField
                                        label='Fatura Açıklaması'
                                        variant='outlined'
                                        fullWidth
                                        value={fatura.aciklama}
                                        onChange={(e) => {
                                            let f = { ...fatura }
                                            f.aciklama = e.target.value
                                            setFatura(f)
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={8}>
                                    <Autocomplete
                                        options={musteriler}
                                        getOptionLabel={musteri => musteri.marka}
                                        renderInput={params =>
                                            <TextField
                                                {...params}
                                                label='Musteri'
                                                variant='outlined'
                                                fullWidth
                                            />}
                                        onChange={(event, value) => {
                                            let f = { ...fatura }
                                            f.musteri = value
                                            setFatura(f)
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={4}>
                                    <Autocomplete
                                        options={firmalarimiz}
                                        getOptionLabel={firma => firma.adi}
                                        renderInput={params =>
                                            <TextField
                                                {...params}
                                                label='Fatura Kesen Firma'
                                                variant='outlined'
                                                fullWidth
                                            />}
                                    />
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField
                                        label='Tarih'
                                        variant='outlined'
                                        fullWidth />

                                </Grid>
                                <Grid item xs={4}>
                                    <TextField
                                        label='Vade'
                                        variant='outlined'
                                        fullWidth />

                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={4}>
                    <Card>
                        <CardHeader title='' />
                        <Divider />
                    </Card>
                </Grid>
            </Grid>
        </div>
    </>
}

Fatura.getServerSideProps = async (ctx) => {
    return {}
}

Fatura.getInitialProps = async (ctx) => {
    const { authorization } = parseCookies(ctx);
    const { token } = ctx.query

    // Musteriler
    // Firmalarimiz
    // Urunler
    
    ctx.store.dispatch(Actions.GetMusteriler())



    const fatura = { aciklama: 'yeni' }
    const musteriler = [
        { marka: 'A' },
        { marka: 'AB' },
        { marka: 'ABC' },
        { marka: 'DE' },
        { marka: 'FGH' },
        { marka: 'TF' },
    ]
    const firmalarimiz =
        [{ adi: 'A' }]
    return { fatura, authorization, token, musteriler, firmalarimiz };
}

//export default wrapper.withRedux(Fatura);
//export default connect((state) => state)(Fatura);
export default Fatura;
