import React from 'react'
import fetch from 'isomorphic-fetch'
import { parseCookies } from 'nookies'
import PageHeader from '../../../components/pageHeader'
import { Container, Grid, Card, CardHeader } from '@material-ui/core'
import { useRouter } from "next/router";

const Faturalar = props => {
    const { user } = props
    const router = useRouter();


    return <>
        <PageHeader
            title='Faturalar'
            buttons={[
                {
                    title: 'Yeni', disabled: false, onClick: (e) => {
                        e.preventDefault();
                        router.push("/muhasebe/faturalar/[id]", `/muhasebe/faturalar/new`);
                    }
                },
            ]} />
        <Grid container spacing={2}>
        </Grid>
    </>
}

const getFaturalar = async (authorization) => {
    const res = await fetch('http://localhost:3001/user', { headers: { authorization } })
    const data = await res.json()
    if (res.status === 200) return { authorization, user: data }
    else return { authorization }
}

Faturalar.getInitialProps = async (ctx) => {
    const { authorization } = parseCookies(ctx);
    const { token } = ctx.query
    const props = getFaturalar(authorization || token)
    return props;
}


export default Faturalar