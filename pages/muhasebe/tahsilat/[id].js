import React, { useState } from 'react'
import PageHeader from '../../../components/pageHeader'
import { Button, makeStyles, Grid } from '@material-ui/core'
import TahsilatInfo from '../../../components/muhasebe/tahsilat/TahsilatInfo'
import { uuid } from 'uuidv4';
import FirmaFaturalar from '../../../components/muhasebe/fatura/FirmaFaturalar';
import FirmaTahsilatlar from '../../../components/muhasebe/fatura/FirmaTahsilatlar';
import {validateTahsilat} from '../../../validators/tahsilat.validator'
import { tahsilatYap } from '../../../services/muhasebe.service'
import {useRouter} from 'next/router'
import { getTenants } from '../../../services/tenant.service'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))
const Tahsilat = props => {
    const {newTahsilat, tenants} = props
    const router = useRouter()
    const classes = useStyles()
    const [form, setForm] = useState(props.tahsilat)
    const [errors, setErrors] = useState(null)
    const onChange = (value) => {
        let f = { ...form }
        f[value.field] = value.value
        setForm(f)
    }
    return <>
        <PageHeader
            title='Tahsilat'
            buttons={[
                <Button variant='contained' color='primary' onClick={(e) => {
                    e.preventDefault()
                    let _errors = validateTahsilat(form)
                    if (_errors) {
                        setErrors(_errors)
                        return
                    }
                    if (newTahsilat) {
                        tahsilatYap(form)
                        router.back()
                    }
                    else {

                    }
                }}>Tahsilat Yap</Button>
            ]}
        />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={8} container spacing={2}>
                    <Grid item xs={12}>
                        <TahsilatInfo 
                            onChange={onChange} 
                            tahsilat={form}
                            tenants={tenants}
                            errors={errors} />
                    </Grid>
                </Grid>
                <Grid item
                    xs={4}
                    container
                    spacing={2}
                    direction="column"
                    justify="flex-start"
                    alignItems="stretch">
                    {form.vfirma &&
                        <Grid item>
                            <FirmaFaturalar firma={form.vfirma} />
                        </Grid>
                    }
                    {form.vfirma &&
                        <Grid item>
                            <FirmaTahsilatlar firma={form.vfirma} />
                        </Grid>
                    }
                </Grid>

            </Grid>
        </div>
    </>
}

Tahsilat.getInitialProps = async (ctx) => {
    let newTahsilat = ctx.query.id == 'new'
    let tahsilat = {
        key: uuid(),
        vfirma: '',
        tahsilatTarihi: new Date(),
        tutar: 0,
        hesap: '',
        aciklama: '',
        tahsilatTuru: 'nakit',
        cekVadeTarihi: null,
        cekNo: '',
        cekBanka: '',
    }
    let tenants = await getTenants()


    if (!newTahsilat) {
        // TODO
    }

    return {
        newTahsilat,
        tahsilat,
        tenants
    }
}

export default Tahsilat