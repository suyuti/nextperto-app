import React from 'react'
import PageHeader from '../../../components/pageHeader'
import { Button, makeStyles } from '@material-ui/core'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DateRangeIcon from '@material-ui/icons/DateRange';
import { useRouter } from 'next/router';
import TableView from '../../../components/tableview';
import { useSelector } from 'react-redux'
import moment from 'moment'
import numbro from 'numbro';


const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const Tahsilatlar = props => {
    const classes = useStyles()
    const router = useRouter()
    const token = useSelector(state => state.auth.expertoToken)
    const columns = [
        { id: 'aciklama', label: 'Fatura Açıklaması' },
        { id: 'firma.marka', label: 'Firma' },
        { id: 'tahsilatYapanFirma.marka', label: 'Alan Firma' },
        { id: 'tutar', align: 'right', label: 'Tutar', render: row => { return `${numbro(row.tutar).format({thousandSeparated: true, mantissa:2})} TL`} },
        { id: 'tahsilatTarihi', label: 'Tahsilat Tarihi', render: row => { return moment(row.tahsilatTarihi).format('DD.MM.YYYY')} },
        { id: 'tahsilatTuru', label: 'Tahsilat Türü' },
        { id: '', label: '', render: row => {
            return <Button variant='outlined' color='primary' size='small' onClick={() => {
                router.push(`/muhasebe/tahsilat/${row._id}`)
            }}>Detay</Button>
        } },
    ]

    return <>
        <PageHeader
            title='Tahsilatlar'
            buttons={[
                <Button variant='contained' color='primary' startIcon={<AttachMoneyIcon />} onClick={() => {
                    router.push('/muhasebe/tahsilat/[id]', '/muhasebe/tahsilat/new')
                }}>Yeni Tahsilat</Button>,
                <Button variant='contained' color='primary' startIcon={<DateRangeIcon />} onClick={() => {
                    router.push('/muhasebe/tahsilatPlan/[id]', '/muhasebe/tahsilatPlan/new')
                }}>Tahsilat Planla</Button>
            ]}
        />
        <div className={classes.root}>
            <TableView
                title='Tahsilatlar'
                columns={columns}
                remoteUrl={`/tahsilat`}
                token={token}
                filter={`select=aciklama,vfirma,tutar,tahsilatTarihi,tahsilatTuru,vtahsilatYapanFirma&populate=firma.marka,tahsilatYapanFirma.marka&sort=-tahsilatTarihi`}
                searchCol='aciklama'
            //data={[]}
            />
        </div>
    </>
}

export default Tahsilatlar