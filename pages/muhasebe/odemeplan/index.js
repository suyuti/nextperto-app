import React from 'react'
import { Button, IconButton } from '@material-ui/core'
import MaterialTable from 'material-table'
import { getOdemeler, getPlanlanmisOdemeler } from '../../../services/muhasebe.service'
import PageHeader from '../../../components/pageHeader'
import moment from 'moment'
import numbro from 'numbro'
import LaunchIcon from '@material-ui/icons/Launch';
import { withRouter } from 'next/router'
var qs = require('qs');

const PlanlanmisOdemeler = (props) => {
    return (
        <div>
            <PageHeader
                title='Planlanmış Ödemeler'
                buttons={[
                    <Button variant='contained' color='primary'>Yeni Ödeme Planı</Button>
                ]}
            />
            <div className='p-2'>
                <MaterialTable
                    title='Planlanmış Ödemeler'
                    columns={[
                        {
                            title: 'Ödeme Tarihi', field: 'odemeTarihi', render: row => {
                                return moment(row.planlananOdemeTarihi).format('DD.MM.YYYY')
                            }
                        },
                        {
                            title: 'Ödeme Tutarı',
                            field: 'planlananOdemeTutari',
                            align: 'right',
                            render: row => {
                                return `${numbro(row.planlananOdemeTutari).format({ thousandSeparated: true, mantissa: 2 })} TL`
                            }
                        },
                        { title: 'Ödeme Türü', field: 'odemeTuru' },
                        { title: 'Ödeme Yapacak Firma', field: 'odemeYapacakFirma.adi' },
                        {
                            title: 'Ödenecek Fatura', field: 'fatura.faturaNo',
                            render: row => {
                                return (
                                    <div className='flex items-center space-x-2'>
                                        <h2 >{row.fatura.faturaNo}</h2>
                                        <IconButton
                                            size='small'
                                            className='focus:outline-none'
                                            onClick={() => {
                                                props.router.push(`/muhasebe/gider/${row.fatura._id}`)
                                            }}
                                        >
                                            <LaunchIcon fontSize='small' />
                                        </IconButton>
                                    </div>
                                )
                            }
                        },
                        {
                            title: 'Ödenecek Firma', field: 'fatura.saticiUnvan',
                            render: row => {
                                return (
                                    <div className='flex items-center space-x-2'>
                                        <h2 >{row.fatura.saticiUnvan}</h2>
                                        <IconButton
                                            size='small'
                                            onClick={() => {
                                                props.router.push(`/muhasebe/tedarik/${row.fatura.tedarikci}`)
                                            }}

                                            className='focus:outline-none'>
                                            <LaunchIcon fontSize='small' />
                                        </IconButton>
                                    </div>
                                )
                            }
                        },
                    ]}
                    options={{
                        padding: 'dense',
                        pageSize: 10,
                        pageSizeOptions: [10, 25, 50, 100]
                    }}
                    data={query => {
                        return new Promise((resolve, reject) => {
                            let filterObj = {
                                skip: query.page * query.pageSize,
                                limit: query.pageSize,
                                populate: 'fatura,odemeYapacakFirma',
                                //aciklama: `/${query.search}/i`,
                                //sort: '-belgeTarihi'
                            }
                            if (query.orderBy) {
                                filterObj.sort = query.orderBy ? `${query.orderDirection == 'asc' ? '' : '-'}${query.orderBy.field}` : ''
                            }
                            let q = qs.stringify(filterObj)
                            getPlanlanmisOdemeler(q)
                                .then(result => {
                                    if (!result) {
                                        reject()
                                    }
                                    else {
                                        resolve({
                                            data: result.data,
                                            page: result.page / query.pageSize,
                                            totalCount: result.total,
                                        })
                                    }
                                })
                        })
                    }}
                />
            </div>
        </div>
    )
}

export default withRouter(PlanlanmisOdemeler)
