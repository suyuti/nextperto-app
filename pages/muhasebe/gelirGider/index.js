import { Grid, TextField, MenuItem, Button } from '@material-ui/core'
import numbro from 'numbro'
import React, { useEffect, useState } from 'react'
import PageHeader from '../../../components/pageHeader'
import { getGunlukRapor } from '../../../services/stats.service'
import { getTenants, getTenantToplamlar } from '../../../services/tenant.service'

const GelirGiderRaporu = props => {
    const { tenants } = props
    const [tenant, setTenant] = useState('')
    const [values, setValues] = useState({
        toplamAlacaklar: 0,
        gecikenAlacaklar: 0,
        planlananAlacaklar: 0,
        toplamOdemeler: 0
    })

    const load = async (tenant) => {
        getTenantToplamlar(tenant._id).then(resp => {
            setValues(resp)
        })    
    }

    useEffect(() => {
        if (tenant) {
            load(tenant)
        }
    }, [tenant])


    return <>
        <PageHeader
            title="Gelir Gider Raporu"
            buttons={[
                <Button variant='contained' color='secondary' onClick={() => {
                    getGunlukRapor()
                }}>pdf</Button>
            ]}
        />
        <div className='p-4'>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <div className='bg-white shadow rounded p-2'>
                        <TextField
                            label='Firma'
                            variant='outlined'
                            fullWidth
                            select
                            onChange={(e) => {
                                setTenant(e.target.value)
                            }}
                        >
                            {tenants.map(t => <MenuItem key={t.key} value={t}>{t.marka}</MenuItem>)}
                        </TextField>
                    </div>
                </Grid>
                <Grid item xs={3}>
                    <div className='border shadow bg-blue-300 rounded p-4'>
                        <div className='font-sans font-light text-lg'>Toplam Alacaklar</div>
                        <div className='text-right text-3xl font-sans font-medium'>{numbro(values.toplamAlacaklar).format({thousandSeparated: true, mantissa:2})} <span className='text-base font-light'>TL</span></div>
                    </div>
                </Grid>
                <Grid item xs={3}>
                    <div className='border shadow bg-red-300 rounded p-4'>
                        <div className='font-sans font-light text-lg'>Geciken Alacaklar</div>
                        <div className='text-right text-3xl font-sans font-medium'>{numbro(values.gecikenAlacaklar).format({thousandSeparated: true, mantissa:2})} <span className='text-base font-light'>TL</span></div>
                    </div>
                </Grid>
                <Grid item xs={3}>
                    <div className='border shadow bg-indigo-300 rounded p-4'>
                        <div className='font-sans font-light text-lg'>Planlanan Alacaklar</div>
                        <div className='text-right text-3xl font-sans font-medium'>{numbro(values.planlananAlacaklar).format({thousandSeparated: true, mantissa:2})} <span className='text-base font-light'>TL</span></div>
                    </div>
                </Grid>
                <Grid item xs={3}>
                    <div className='border shadow bg-yellow-300 rounded p-4'>
                        <div className='font-sans font-light text-lg'>Toplam Ödemeler</div>
                        <div className='text-right text-3xl font-sans font-medium'>{numbro(values.toplamOdemeler).format({thousandSeparated: true, mantissa:2})} <span className='text-base font-light'>TL</span></div>
                    </div>
                </Grid>
            </Grid>
        </div>
    </>
}

GelirGiderRaporu.getInitialProps = async (ctx) => {
    let tenants = await getTenants()
    return {
        tenants: [{key: 'ALL', marka: 'Tümü'}, ...tenants]
    }
}

export default GelirGiderRaporu
