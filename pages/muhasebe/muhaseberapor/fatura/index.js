import MaterialTable from 'material-table'
import React from 'react'
import PageHeader from '../../../../components/pageHeader'
import { getFaturalar } from '../../../../services/muhasebe.service'
import moment from 'moment'
import numbro from 'numbro'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { withRouter } from 'next/router'
var qs = require('qs');

const MuhasebeRaporFatura = ({ faturalar, ...props }) => {
    return (
        <div>
            <PageHeader
                title='Faturalar'
            />
            <div className='p-2'>
                <MaterialTable
                    title='Faturalar'
                    data={faturalar}
                    columns={[
                        { title: 'Açıklama', field: 'aciklama' },
                        { title: 'Firma', field: 'firma.marka' },
                        { title: 'Fatura No', field: 'faturaNo' },
                        { title: 'Toplam', field: 'genelToplam', align: 'right', render: row => numbro(row.genelToplam).format({ thousandSeparated: true, mantissa: 2 }) },
                        { title: 'Tahsil Edilen', field: 'tahsilatTutari', align: 'right', render: row => numbro(row.tahsilatTutari).format({ thousandSeparated: true, mantissa: 2 }) },
                        {
                            title: 'Vade Tarihi', field: 'vadeTarihi', render: row => {
                                return moment(row.vadeTarihi).format('DD.MM.YYYY')
                            }
                        },
                        {
                            title: 'Düzenleme Tarihi', field: 'duzenlemeTarihi', render: row => {
                                return moment(row.duzenlemeTarihi).format('DD.MM.YYYY')
                            }
                        },
                    ]}
                    options={{
                        //actionsColumnIndex: -1
                    }}
                    actions={[
                        {
                            icon: () => <ArrowForwardIcon />,
                            tooltip: 'Faturaya Git',
                            onClick: (event, rowData) => {
                                props.router.push(`/muhasebe/fatura/${rowData._id}`)
                            }
                        }
                    ]}
                />
            </div>
        </div>
    )
}

MuhasebeRaporFatura.getInitialProps = async (ctx) => {
    let start = moment(`2021.${ctx.query.a}.01`).startOf('month')
    let end = moment(start).endOf('month')
    let kesenFirmalar = ctx.query.f.split(',')
    let _q = {
        populate: 'firma.marka',
        filter: {
            '$and': [
                { duzenlemeTarihi: { $gte: start.toISOString() } },
                { duzenlemeTarihi: { $lte: end.toISOString() } },
                { vkesenFirma: {$in: kesenFirmalar }},
                { vfirma: {$nin: ['6f3ed423-78ac-4fe4-84c7-80aa0e90a719', 'b88dd4b5-5a4a-4cdf-97ea-0e7214218247']}}
            ]
        }
    }

    let q = qs.stringify(_q)
    let faturalar = await getFaturalar(q)
    return {
        faturalar
    }
}

export default withRouter(MuhasebeRaporFatura)
