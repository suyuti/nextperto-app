import React, { useEffect, useState } from 'react'
import PageHeader from '../../../components/pageHeader'
import dynamic from 'next/dynamic'
import { Table, TableCell, TableRow, TableHead, TableBody, Button, Grid, CircularProgress, IconButton, Tabs, Tab } from '@material-ui/core'
import { getUrunler } from '../../../services/urun.service'
import { getRaporFatura, getRaporUrun, getRaporAylikUrun } from '../../../services/muhasebe.service'
import numbro from 'numbro'
import { withRouter } from 'next/router'
import { Skeleton } from '@material-ui/lab'
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })
import RefreshIcon from '@material-ui/icons/Refresh';
import TabPanel from '../../../hoc/TabPanel'


const charOptions = {
    chart: {
        stacked: true,
        toolbar: {
            show: false
        },
    },
    legend: {
        show: true
    },
    yaxis: {
        show: false
    },
    colors: ['#00FFF6', '#FF0009'],
    tooltip: {
        enabled: true,
        y: {
            title: {
                formatter: (seriesName) => 'Toplam Fatura'
            },
            formatter: function (value, { series, seriesIndex, dataPointIndex, w }) {
                return `${numbro(value).format({ thousandSeparated: true, mantissa: 2 })} TL`
            }
        }
    },
    dataLabels: {
        enabled: false,
        formatter: function (val, opts) {
            return `${numbro(val).format({ thousandSeparated: true, mantissa: 2 })} TL`
        },
    },
    xaxis: {
        show: true,
        labels: {
            show: true,
            rotate: -90,
            rotateAlways: true
        }
    }
}

const charOptionsUrun = {
    chart: {
        type: 'bar',
        toolbar: {
            show: false
        },
    },
    legend: {
        show: true
    },
    yaxis: {
        show: true
    },
    tooltip: {
        enabled: true,
        y: {
            title: {
                formatter: (seriesName) => 'Toplam Fatura'
            },
            formatter: function (value, { series, seriesIndex, dataPointIndex, w }) {
                return `${numbro(value).format({ thousandSeparated: true, mantissa: 2 })} TL`
            }
        }
    },

    dataLabels: {
        enabled: true,
        formatter: function (val, opts) {
            return `${numbro(val).format({ thousandSeparated: true, mantissa: 2 })} TL`
        },
    },
    plotOptions: {
        bar: {
            horizontal: true,
        }
    },
    xaxis: {
        show: true,
        //labels: {
        //    show: true,
        //    rotate: -40,
        //    rotateAlways: true
        //}
    }
}

const FirmaRapor = ({ title, kesenFirma, legends, ...props }) => {
    const [tableData, setTableData] = useState(null)
    const [toplamlar, setToplamlar] = useState({})

    const load = async () => {
        setTableData(null)
        getRaporFatura({ firma: kesenFirma }).then(resp => {
            // Gelen veri yapisi: { id: 1, m: 1, y: 2021, ay: 'Ocak', adet: [5, 10], toplam: [15000, 35000] },
            // Sorgudaki kesenFirma firmalar dizisidir. Cevaptaki Adet ve toplamlar sorgudaki firmalar sirasina gore siralidir
            // Chartda firmalara gore ayri degerler stacked olarak gosterilecektir.

            let t = resp.reduce((acc, f) => {
                // Burada tum firmalarin degerleri toplaniyor. Tum toplamlari gostermek icin
                let toplam = f.toplam.reduce((a, b) => { return a + b }, 0)
                let toplamAdet = f.adet.reduce((a, b) => { return a + b }, 0)
                return { toplam: acc.toplam + toplam, adet: acc.adet + toplamAdet }
            }, { toplam: 0, adet: 0 })
            setToplamlar(t)
            setTableData(resp)
        })
    }

    useEffect(() => {
        load()
    }, [])
    const yearData = (data) => {
        let result = []
        for (let i = 0; i < 12; ++i) {
            if (data.length > i) {
                result.push(data[i])
            }
            else {
                result.push({ id: i, m: i + 1, ay: '', adet: 0, toplam: 0 })
            }
        }
        return result
    }


    return <div className='border shadow rounded bg-white w-full'>
        <div className='flex p-4 shadow flex-row justify-between items-center'>
            <h2 className='text-xl font-medium antialiased'>{title}</h2>
            <IconButton className='focus:outline-none' onClick={() => {
                load()
            }}><RefreshIcon /></IconButton>
        </div>

        {!tableData ?
            <div className='flex align-middle items-center h-52 justify-center bg-white border shadow rounded'>
                <CircularProgress />
            </div>
            :
            <div className='p-2 flex flex-col space-y-2 px-10'>
                <div className='flex justify-between'>
                    <h2 className='text-lg font-sans font-medium antialiased text-gray-600'>Toplam Fatura</h2>
                    <h2 className='text-2xl font-sans font-medium antialiased text-gray-600'>{numbro(toplamlar.toplam).format({ thousandSeparated: true, mantissa: 2 })} TL</h2>
                </div>
                <div className='flex justify-between'>
                    <h2 className='text-lg font-sans font-medium antialiased text-gray-600'>Toplam Fatura Adedi</h2>
                    <h2 className='text-2xl font-sans font-medium antialiased text-gray-600'>{toplamlar.adet}</h2>
                </div>
                <Chart
                    className='border shadow rounded'
                    options={charOptions}
                    series={
                        // Chart verileri gelen firma toplamlarina gore olusturulur.
                        // Char stacked'dir/ Bu yuzden seride her firma icin degerler hesaplanir
                        kesenFirma.map((f, kfi) => {
                            return {
                                // Legend adi firma listesine benzer sekilde isim listesi olarak props ile alinir. 
                                // Tekrar islem olmasin diye cagiran isimleri belirliyor.  Yoksa kesenFirmadan da isimler bulunabilirdi
                                // cevaplardaki degerler kesenFirma listesi ile ayni sirada oldugu icin isimler ve degerle de ayni indexden alindi
                                name: legends[kfi],
                                data: yearData(tableData).map((d, i) => {
                                    return {
                                        x: d.ay,
                                        y: d.toplam[kfi],
                                        adet: d.adet[kfi]
                                    }
                                })
                            }
                        })
                    }
                    type='bar'
                    height='350px'
                />
                <div className='w-full'>
                    <Table size='small' stickyHeader={true} >
                        <TableHead>
                            <TableRow>
                                <TableCell>Ay</TableCell>
                                <TableCell>Adet</TableCell>
                                <TableCell align='right'>Toplam</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tableData.map((data, i) => {
                                return (
                                    <TableRow hover key={data.id}>
                                        <TableCell>{data.ay}</TableCell>
                                        <TableCell>{data.adet.reduce((a, b) => a + b, 0)}</TableCell>
                                        <TableCell align='right' >{numbro(data.toplam.reduce((a, b) => a + b, 0)).format({ thousandSeparated: true, mantissa: 2 })} TL</TableCell>
                                        <TableCell ><Button variant='outlined' color='primary' size='small' onClick={() => {
                                            props.router.push(`/muhasebe/muhaseberapor/fatura?a=${data.m}&f=${kesenFirma}`)
                                        }}>Detay</Button></TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </div>
            </div>
        }
    </div>
}

const UrunRapor = ({ title }) => {
    const [urunSatislar, setUrunSatislar] = useState(null)

    const load = async () => {
        setUrunSatislar(null)
        getRaporUrun().then(resp => {
            setUrunSatislar(resp)
        })
    }

    useEffect(() => {
        load()
    }, [])

    return (
        <div className='border shadow rounded bg-white w-full'>
            <div className='flex p-4 shadow flex-row justify-between items-center'>
                <h2 className='text-xl font-medium antialiased'>{title}</h2>
                <IconButton className='focus:outline-none' onClick={() => {
                    load()
                }}><RefreshIcon /></IconButton>
            </div>
            {!urunSatislar ?
                <div className='flex align-middle items-center h-52 justify-center bg-white border shadow rounded'>
                    <CircularProgress />
                </div>
                :
                <div className='p-2 flex flex-col space-y-2'>
                    <div className='flex justify-between w-1/3 '>
                        <h2 className='text-lg font-sans font-medium antialiased text-gray-600'>Toplam Fatura</h2>
                        <h2 className='text-lg font-sans font-medium antialiased text-gray-600'></h2>
                    </div>
                    <div className='flex justify-between w-1/3 '>
                        <h2 className='text-lg font-sans font-medium antialiased text-gray-600'>Toplam Fatura Adedi</h2>
                        <h2 className='text-lg font-sans font-medium antialiased text-gray-600'></h2>
                    </div>
                    <Chart
                        className='border shadow rounded'
                        options={charOptionsUrun}
                        series={[{
                            name: 'Satislar',
                            data: urunSatislar.map((d, i) => {
                                return {
                                    x: d.urun,
                                    y: d.toplam
                                }
                            })
                        }]}
                        type='bar'
                    //height='800px'
                    />
                    <div className='w-full'>
                        <Table size='small' stickyHeader={true} >
                            <TableHead>
                                <TableRow>
                                    <TableCell>Ürün</TableCell>
                                    <TableCell align='right'>Adet</TableCell>
                                    <TableCell align='right'>Toplam</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {urunSatislar.map((data, i) => {
                                    return (
                                        <TableRow hover key={data.id}>
                                            <TableCell>{data.urun}</TableCell>
                                            <TableCell align='right'>{data.adet}</TableCell>
                                            <TableCell align='right'>{numbro(data.toplam).format({ thousandSeparated: true, mantissa: 2 })} TL</TableCell>
                                            <TableCell ><Button variant='outlined' color='primary' size='small'>Detay</Button></TableCell>
                                        </TableRow>
                                    )
                                })}
                            </TableBody>
                        </Table>
                    </div>
                </div>
            }
        </div>
    )
}

const MuhasebeRapor = (props) => {
    const [selectedTab, setSelectedTab] = useState(0)
    return (
        <div className=''>
            <PageHeader
                title='Muhasebe Rapor'
            />
            <div className='p-2'>
                <Tabs
                    indicatorColor="primary"
                    textColor="primary"
                    value={selectedTab}
                    onChange={(e, newValue) => {
                        setSelectedTab(newValue)
                    }}
                >
                    <Tab className='focus:outline-none' label='Satış'></Tab>
                    <Tab className='focus:outline-none' label='Ürün'></Tab>
                </Tabs>
                <TabPanel value={selectedTab} index={0}>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <FirmaRapor className='w-1/2' title={'ArgeEvi + Experto'} kesenFirma={['ARGEEVI', 'EXPERTO']} legends={['ArgeEvi', 'Experto']} {...props} />
                        </Grid>
                        <Grid item xs={6}>
                            <FirmaRapor className='w-1/2' title={'OSGB'} kesenFirma={['OSGB']} legends={['OSGB']} {...props} />
                        </Grid>
                    </Grid>
                </TabPanel>
                <TabPanel value={selectedTab} index={1}>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <UrunAylikRapor />
                        </Grid>
                        <Grid item xs={12}>
                            <UrunRapor className='w-1/2' title={'Ürün Bazında Satışlar'} />
                        </Grid>
                    </Grid>
                </TabPanel>
            </div>
        </div>
    )
}

const UrunAylikRapor = (props) => {
    var options = {
        chart: {
            type: 'bar',
            height: 50,
            stacked: true,
        },
        plotOptions: {
            bar: {
                horizontal: true,
            },
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        title: {
            text: 'Aylık Bazda Ürün Satışları'
        },
        xaxis: {
            categories: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
            labels: {
                formatter: function (val) {
                    return val + "K"
                }
            }
        },
        yaxis: {
            title: {
                text: undefined
            },
        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return val + "K"
                }
            }
        },
        fill: {
            opacity: 1
        },
        //legend: {
        //    position: 'top',
        //    horizontalAlign: 'left',
        //    offsetX: 40
        //}
    }


    const series = [{
        name: 'ArGe Merkezi Kurulum',
        data: [23600, 17700, 41, 37, 22, 43, 21,0,0,0,0,0]
    }, {
        name: 'Striking Calf',
        data: [153159, 32, 33, 52, 13, 43, 32,0,0,0,0,0]
    }, {
        name: 'Tank Picture',
        data: [12, 17, 11, 9, 15, 11, 20,0,0,0,0,0]
    }, {
        name: 'Bucket Slope',
        data: [9, 7, 5, 8, 6, 9, 4,0,0,0,0,0]
    }, {
        name: 'Reborn Kid',
        data: [25, 12, 19, 32, 25, 24, 10,0,0,0,0,0]
    }]


    const [urunSatislar, setUrunSatislar] = useState(null)

    const load = async () => {
        setUrunSatislar(null)
        getRaporAylikUrun().then(resp => {
            setUrunSatislar(resp)
        })
    }

    useEffect(() => {
        load()
    }, [])

    return (
        <div className='border p-2 rounded shadow'>
            <Chart
                className='border shadow rounded'
                options={options}
                series={series}
                type='bar'
            />

        </div>
    )
}

export default withRouter(MuhasebeRapor)
