import React, { useState, useRef, useEffect } from 'react'
import PageHeader from "../../../components/pageHeader";
import { Button, Grid, makeStyles, Dialog, DialogTitle, DialogActions, DialogContent } from '@material-ui/core'
import SaveIcon from '@material-ui/icons/Save'
import TopluFaturaInfo from '../../../components/muhasebe/toplufatura/TopluFaturaInfo'
import { getTopluFatura } from '../../../services/muhasebe.service'
import { getFirmalar } from '../../../services/firma.service'
import { useRouter } from 'next/router';
import { saveTopluFatura, updateTopluFatura } from '../../../services/muhasebe.service'
import TopluFaturaFirmalar from '../../../components/muhasebe/toplufatura/TopluFaturaFirmaListe';
import { getTenants } from '../../../services/tenant.service'
import { eFaturaOnIzleme, gidenBelgeIndir } from '../../../services/efatura.service'
import { uuid } from 'uuidv4';
import { getUrunler } from '../../../services/urun.service';
import TopluFatura2 from '../../../components/muhasebe/toplufatura'
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../../../redux/actions'
import moment from 'moment'
import trLocale from "date-fns/locale/tr";


const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const TopluFatura = props => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const topluFatura = useSelector(state => state.fatura.topluFatura)
    const topluFaturaListe = useSelector(state => state.fatura.topluFaturaListe)
    useEffect(() => {
        if (props.topluFatura) {
            dispatch(Actions.setTopluFatura(props.topluFatura))
        }
    }, [])


    const [form, setForm] = useState(props.topluFatura)
    const router = useRouter()
    const { newTopluFatura,  tenants, urunler } = props
    const [openFaturaPreview, setOpenFaturaPreview] = useState(false)
    const [preview, setPreview] = useState(null/*b()*/)

    const aciklamaFormat = (aciklama) => {
        return (''+aciklama).replace('{AY}', moment().locale('tr').format('MMMM-YYYY'))
    }


    const getFaturaOnIzleme = async ({ index }) => {
        let fatura = form.liste[index]
        // TODO bazi degerler hardcoded. Duzeltilmeli        
        let f = {
            key: uuid(),
            vfirma: fatura.firma.key,
            firma: null,
            duzenlemeTarihi: new Date(),
            aciklama: '',
            kesenFirma: form.vkesenFirma,
            dovizTuru: '',
            vadeTarihi: '',
            vade: 0,
            araToplam: parseFloat(fatura.birimFiyat),
            kdvOrani: parseInt(fatura.kdv),
            toplamKdv: parseFloat(fatura.toplam) - parseFloat(fatura.birimFiyat),
            genelToplam: parseFloat(fatura.toplam),
            taslak: true,
            acik: true,
            vkesenFirma: form.vkesenFirma,
            kalemler: [
                {
                    key: uuid(),
                    urun: "",
                    miktar: 1,
                    birimFiyat: parseFloat(fatura.birimFiyat),
                    toplam: parseFloat(fatura.toplam),
                    adet: 1,
                    aciklama: `${fatura.urun.adi} ${aciklamaFormat(fatura.aciklama)}`,
                    kdvOran: parseInt(fatura.kdv)
                }
            ]
        }
        /*
        
        {
            "key":"51eec2e3-d0d0-4c41-9458-3ccb9fed8ca1",
            "vfirma":"FIRMA1",
            "firma":null,
            "duzenlemeTarihi":"2021-03-15T14:27:18.956Z",
            "aciklama":"asa",
            "kesenFirma":null,
            "dovizTuru":"",
            "vadeTarihi":"2021-03-15T14:27:18.955Z",
            "vade":0,
            "araToplam":1344,
            "kdvOrani":18,
            "toplamKdv":241.92,
            "genelToplam":1585.92,
            "taslak":true,
            "acik":true,
            "vkesenFirma":"TEST_EXPERTO",
            "kalemler":[
                {
                    "key":"48e893c0-e146-4b15-b9ee-8b308f1b4c88",
                    "urun":"",
                    "miktar":"112",
                    "birimFiyat":"12",
                    "toplam":1344,
                    "adet":null,
                    "aciklama":"wwd"
                }
            ]}
        
        */
        eFaturaOnIzleme(f).then(resp => {
            if (resp) {
                let html = Buffer.from(resp, 'base64').toString('ascii')
                setPreview(html)
                setOpenFaturaPreview(true)
            }
        }).catch(e => {
            alert(e)
        })
    }

    return <>
        <PageHeader
            title='Toplu Fatura Listesi'
            buttons={[
                <Button variant='contained' color='primary' startIcon={<SaveIcon />} onClick={() => {
                    if (newTopluFatura) {
                        saveTopluFatura({...topluFatura, liste: [...topluFaturaListe]})
                    }
                    else {
                        updateTopluFatura(topluFatura._id, {...topluFatura, liste: [...topluFaturaListe]})
                    }
                    router.back()
                }}>Kaydet</Button>
            ]}
        />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <TopluFatura2 urunler={urunler} tenants={tenants} onPreview={(index) => {
                        getFaturaOnIzleme({index})
                    }} />
                </Grid>
            </Grid>
        </div>
        <Dialog
            fullWidth
            maxWidth='lg'
            onClose={() => { setOpenFaturaPreview(false) }} open={openFaturaPreview}>
            <DialogTitle onClose={() => { setOpenFaturaPreview(false) }}>
                Fatura Önizleme
            </DialogTitle>
            <DialogContent dividers>
                <div className={classes.preview}>
                    <div dangerouslySetInnerHTML={{ __html: preview }} />
                </div>
            </DialogContent>
            <DialogActions>
                <Button variant='contained' onClick={() => { setOpenFaturaPreview(false) }} color='primary'>Kapat</Button>
            </DialogActions>
        </Dialog>

    </>
}

TopluFatura.getInitialProps = async (ctx) => {
    let newTopluFatura = ctx.query.id == 'new'
    let tenants = await getTenants()
    let urunler = await getUrunler()
    let topluFatura = {
        listeAdi: '',
        faturaKesimGunu: 1,
        liste: []
    }

    if (!newTopluFatura) {
        topluFatura = await getTopluFatura(ctx.query.id)
    }

    return {
        newTopluFatura,
        topluFatura: topluFatura,
        tenants,
        urunler
    }
}

export default TopluFatura