import { Button, CircularProgress, Checkbox, IconButton, Dialog, DialogTitle, DialogActions, DialogContent, TextField, MenuItem } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import PageHeader from '../../../../components/pageHeader'
import InsertDriveFileOutlinedIcon from '@material-ui/icons/InsertDriveFileOutlined';
import numbro from 'numbro';
import { getTopluFatura, saveFatura } from '../../../../services/muhasebe.service'
import { eFaturaOnIzleme } from '../../../../services/efatura.service'
import moment from 'moment'
import { uuid } from 'uuidv4';
import MoneyInput from '../../../../components/core/MoneyInput'
import { withRouter } from 'next/router';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import trLocale from "date-fns/locale/tr";
moment.locale('tr')

const TopluFaturaKesim = (props) => {
    //const { topluFatura } = props
    const [selected, setSelected] = useState([])
    const [selectedTotal, setSelectedTotal] = useState(0)
    const [openFaturaPreview, setOpenFaturaPreview] = useState(false)
    const [preview, setPreview] = useState(null)
    const [previewPending, setPreviewPending] = useState(-1)
    const [topluFatura, setTopluFatura] = useState(props.topluFatura)

    const [showAreYouSure, setShowAreYouSure] = useState(false)
    const [showSonuc, setShowSonuc] = useState(false)
    const [faturaKesimGunu, setFaturaKesimGunu] = useState(new Date())

    const aciklamaFormat = (aciklama) => {
        return ('' + aciklama).replace('{AY}', moment().locale('tr').format('MMMM-YYYY'))
    }

    useEffect(() => {
        if (!topluFatura.sabitFiyatliMi) {
            let tf = { ...topluFatura }
            for (let f of tf.liste) {
                f.birimFiyat = 0,
                    f.kdv = 18,
                    f.toplamKdv = 0
                f.toplam = 0
            }
            setTopluFatura(tf)
        }
    }, [])


    const seciliFaturalariKes = async () => {
        // Toplu Faturalarda aynı firmaya birden fazla kayıt varsa bunlar aynı firma altında kalemler olarak toplanır.
        // Firma1 - urun 1 - 1000 TL
        // Firma1 - urun 2 - 1500 TL
        // Bunlar tek faturada birşeltirilir.
        // Firma1 kalemler [urun1, urun2] - 2500 TL olarak tek faturada birleştirilir.
        // merge
        let merged = []
        for (let i of selected) {
            let fatura = topluFatura.liste[i]
            let f = merged.find(x => x.vfirma == fatura.vfirma)
            if (f) {
                f.araToplam += fatura.birimFiyat
                f.toplamKdv += (fatura.birimFiyat * fatura.kdv / 100)
                f.genelToplam += (fatura.birimFiyat * (100 + fatura.kdv) / 100)
                f.aciklama = `${f.aciklama} ${fatura.urun ? fatura.urun.adi : ''} - ${aciklamaFormat(fatura.aciklama)}`,
                f.kalemler.push({
                    key: uuid(),
                    vfatura: f.key,
                    vurun: fatura.urun ? fatura.urun.key : '',
                    aciklama: `${fatura.urun ? fatura.urun.adi : ''} - ${aciklamaFormat(fatura.aciklama)}`,
                    birimFiyat: fatura.birimFiyat,
                    kdvOran: fatura.kdv,
                    kdv: fatura.birimFiyat * (100 + fatura.kdvOran) / 100,
                    miktar: 1,
                    toplam: fatura.toplam,
                })
            }
            else {
                //fatura.kalemler = []
                let faturaKey = uuid()
                let f2 = {
                    key: faturaKey,
                    vkesenFirma: topluFatura.vkesenFirma,
                    vfirma: fatura.firma.key,
                    araToplam: fatura.birimFiyat,
                    toplamKdv: fatura.birimFiyat * fatura.kdv / 100,
                    genelToplam: fatura.birimFiyat * (100 + fatura.kdv) / 100,
                    aciklama: `${fatura.urun ? fatura.urun.adi : ''} - ${aciklamaFormat(fatura.aciklama)}`,
                    dovizTuru: '',
                    vadeTarihi: moment().add(5, 'days'), // TODO
                    taslak: false,
                    tahsilatTutari: 0,
                    kdvOrani: 18, // TODO
                    duzenlemeTarihi: faturaKesimGunu, //new Date(),
                    status: 'Yeni',
                    faturaTuru: 'SATIS',
                    kalemler: [{
                        key: uuid(),
                        vfatura: faturaKey,
                        vurun: fatura.urun ? fatura.urun.key : '',
                        aciklama: `${fatura.urun ? fatura.urun.adi : ''} - ${aciklamaFormat(fatura.aciklama)}`,
                        birimFiyat: fatura.birimFiyat,
                        kdvOran: fatura.kdv,
                        kdv: fatura.birimFiyat * (100 + fatura.kdvOran) / 100,
                        miktar: 1,
                        toplam: fatura.toplam,
                    }]
                }
                merged.push(f2)
            }
        }
        for (let f of merged) {
            await saveFatura(f)
        }

        /*
                for (let index of selected) {
                    let fatura = topluFatura.liste[index]
                    let faturaKey = uuid()
                    let _f = {
                        key: faturaKey,
                        vkesenFirma: topluFatura.vkesenFirma,
                        vfirma: fatura.firma.key,
                        araToplam: fatura.birimFiyat,
                        toplamKdv: fatura.birimFiyat * fatura.kdv / 100,
                        genelToplam: fatura.birimFiyat * (100 + fatura.kdv) / 100,
                        kalemler: [{
                            key: uuid(),
                            vfatura: faturaKey,
                            vurun: fatura.urun ? fatura.urun.key : '',
                            aciklama: `${fatura.urun ? fatura.urun.adi : ''} - ${aciklamaFormat(fatura.aciklama)}`,
                            birimFiyat: fatura.birimFiyat,
                            kdvOran: fatura.kdv,
                            kdv: fatura.birimFiyat * (100 + fatura.kdvOran) / 100,
                            miktar: 1,
                            toplam: fatura.toplam,
                        }],
                        aciklama: `${fatura.urun ? fatura.urun.adi : ''} - ${aciklamaFormat(fatura.aciklama)}`,
                        dovizTuru: '',
                        vadeTarihi: moment().add(5, 'days'), // TODO
                        taslak: false,
                        tahsilatTutari: 0,
                        kdvOrani: 18, // TODO
                        duzenlemeTarihi: faturaKesimGunu, //new Date(),
                        status: 'Yeni',
                        faturaTuru: 'SATIS',
        
                        //topluFaturaListesi: topluFatura._id,
                        //topluFaturaKesimDonemi: moment(faturaKesimGunu).format('MM-YYYY')
                    }
        
                    await saveFatura(_f)
                }
                */
        return
    }

    return (
        <div>
            <PageHeader
                title='Toplu Fatura Kesim'
                buttons={[
                    <Button variant='contained' color='primary' disabled={selected.length == 0 || !topluFatura.active} onClick={() => {
                        setShowAreYouSure(true)
                        //seciliFaturalariKes()
                        //props.router.push(`/muhasebe/fatura`)
                    }} >Faturaları Kes</Button>
                ]}
            />
            <div className='p-2 bg-white border rounded flex flex-col space-y-4'>
                <div className='flex space-x-2'>
                    <div className='toplu-liste-bilgi w-1/2 p-4 border shadow rounded'>
                        <div className='flex justify-between'>
                            <h2>Liste Adi</h2>
                            <h2 className='text-xl font-sans'>{topluFatura.listeAdi}</h2>
                        </div>
                        <div className='flex justify-between'>
                            <h2>Fatura Kesim Günü</h2>
                            <h2 className='text-xl font-sans'>{topluFatura.faturaKesimGunu}</h2>
                        </div>
                        <div className='flex justify-between'>
                            <h2>Fatura Kesen Firma</h2>
                            <h2 className='text-xl font-sans'>{topluFatura.kesenFirma?.marka}</h2>
                        </div>
                        <div className='flex justify-between'>
                            <h2>Toplam Kesilecek Fatura Adedi</h2>
                            <h2 className='text-xl font-sans'>{selected.length}</h2>
                        </div>
                        <div className='flex justify-between'>
                            <h2>Toplam Kesilecek Fatura Tutarı</h2>
                            <h2 className='text-xl font-sans'>{numbro(selectedTotal).format({ thousandSeparated: true, mantissa: 2 })} TL</h2>
                        </div>
                    </div>

                    <div className='w-1/3 p-4 border rounded shadow'>
                        <div className='flex-col justify-between space-y-2'>
                            <h2 className='text-xl'>Faturaların Düzenleme Günü</h2>
                            <MuiPickersUtilsProvider
                                utils={DateFnsUtils} locale={trLocale}
                            >
                                <KeyboardDatePicker
                                    //error={errors && errors.vadeTarihi}
                                    //helperText={errors && errors.vadeTarihi}
                                    size='small'
                                    autoOk
                                    fullWidth
                                    //readOnly={!newFatura}
                                    //minDate={newFatura ? moment() : null}
                                    //disablePast={true}
                                    //size='small'
                                    variant="inline"
                                    inputVariant="outlined"
                                    label="Fatura Düzenleme Tarihi"
                                    format="dd.MM.yyyy"
                                    clearable
                                    InputAdornmentProps={{ position: 'start' }}
                                    value={faturaKesimGunu}
                                    onChange={e => {
                                        setFaturaKesimGunu(e)
                                    }}
                                />
                            </MuiPickersUtilsProvider>
                            <h2 className='text-xl font-bold text-red-600'>{`Faturaların düzenleme tarihi ${moment(faturaKesimGunu).format('LL')}'dir`}</h2>
                        </div>
                    </div>
                </div>
                <div className='w-full'>
                    <table className='w-full border'>
                        <colgroup>
                            <col width='80' />
                        </colgroup>
                        <thead className='bg-blue-100 border'>
                            <tr>
                                <th className='border' align='center'>
                                    <div className='flex items-center'>
                                        <Checkbox
                                            checked={selected.length == topluFatura.liste.filter(item => item.birimFiyat != 0).length && selected.length > 0}
                                            onChange={e => {
                                                if (e.target.checked) {
                                                    let s = [...selected]
                                                    let i = 0
                                                    let toplam = 0
                                                    for (let f of topluFatura.liste) {
                                                        if (selected.includes(i)) {
                                                            ++i
                                                            continue
                                                        }
                                                        if (f.birimFiyat != 0) {
                                                            s.push(i)
                                                            toplam += f.toplam
                                                        }
                                                        ++i
                                                    }
                                                    //s = topluFatura.liste.filter(item => item.birimFiyat != 0).map((item, i) => i)
                                                    //let toplam = topluFatura.liste.reduce((acc, v) => acc + v.toplam, 0)
                                                    setSelected(s)
                                                    setSelectedTotal(toplam)
                                                }
                                                else {
                                                    setSelected([])
                                                    setSelectedTotal(0)
                                                }
                                            }} />
                                        <h1>Seç</h1>
                                    </div>
                                </th>
                                <th className='border' align='center'>Firma</th>
                                <th className='border' align='center'>Ürün</th>
                                <th className='border' align='center'>Açıklama</th>
                                <th className='border' align='center'>Birim Fiyat</th>
                                <th className='border' align='center'>KDV</th>
                                <th className='border' align='center'>Toplam</th>
                                <th className='border' align='center'>Ön izleme</th>
                            </tr>
                        </thead>
                        <tbody>
                            {topluFatura.liste.map((fatura, i) => {
                                return (<tr>
                                    <td className='border px-2' align='center'>
                                        <Checkbox
                                            size='small'
                                            disabled={fatura.birimFiyat == 0}
                                            checked={selected.includes(i)}
                                            onChange={(e) => {
                                                setSelectedTotal(prev => prev + ((e.target.checked) ? 1 : -1) * fatura.toplam)
                                                let s = [...selected]
                                                if (e.target.checked) {
                                                    s.push(i)
                                                }
                                                else {
                                                    s = s.filter(item => item != i)
                                                }
                                                setSelected(s)
                                            }} /></td>
                                    <td className='border px-2'>
                                        <div className='flex justify-between items-center'>
                                            {fatura.firma?.marka}
                                            {false && <div className='bg-green-300 px-2 py-1 border rounded shadow'>
                                                Fatura Kesildi
                                            </div>}
                                        </div>
                                    </td>
                                    <td className='border px-2'>{fatura.urun?.adi}</td>
                                    <td className='border px-2'>{aciklamaFormat(fatura.aciklama)}</td>
                                    <td className='border px-2' align='right'>
                                        {topluFatura.sabitFiyatliMi ?
                                            `${numbro(fatura.birimFiyat).format({ thousandSeparated: true, mantissa: 2 })} TL`
                                            :
                                            <TextField
                                                fullWidth
                                                variant='outlined'
                                                size='small'
                                                value={fatura.birimFiyat}
                                                InputProps={{
                                                    inputComponent: MoneyInput
                                                }}
                                                onChange={e => {
                                                    let t = { ...topluFatura }
                                                    t.liste[i].birimFiyat = e.target.value
                                                    t.liste[i].toplam = t.liste[i].birimFiyat * (100 + t.liste[i].kdv) / 100
                                                    setTopluFatura(t)
                                                }}
                                            />
                                        }
                                    </td>
                                    <td className='border px-2' align='right'>
                                        {topluFatura.sabitFiyatliMi ?
                                            `${fatura.kdv} %`
                                            :
                                            <TextField
                                                fullWidth
                                                variant='outlined'
                                                size='small'
                                                select
                                                value={fatura.kdv}
                                                onChange={e => {
                                                    let t = { ...topluFatura }
                                                    t.liste[i].kdv = e.target.value
                                                    t.liste[i].toplam = t.liste[i].birimFiyat * (100 + t.liste[i].kdv) / 100
                                                    setTopluFatura(t)
                                                }}
                                            >
                                                <MenuItem key={0} value={0}>0</MenuItem>
                                                <MenuItem key={1} value={1}>1</MenuItem>
                                                <MenuItem key={8} value={8}>8</MenuItem>
                                                <MenuItem key={18} value={18}>18</MenuItem>
                                            </TextField>

                                        }

                                    </td>
                                    <td className='border px-2' align='right'>{numbro(fatura.toplam).format({ thousandSeparated: true, mantissa: 2 })} TL</td>
                                    <td className='border px-2' align='center'>
                                        {previewPending == i
                                            ?
                                            < CircularProgress size={20} />
                                            :
                                            <IconButton
                                                size='small'
                                                disabled={previewPending != -1 || fatura.birimFiyat == 0}
                                                className='focus:outline-none'
                                                onClick={e => {
                                                    debugger
                                                    let _f = {
                                                        vkesenFirma: topluFatura.vkesenFirma,
                                                        vfirma: fatura.firma.key,
                                                        araToplam: parseFloat(fatura.birimFiyat),
                                                        toplamKdv: parseFloat(fatura.birimFiyat) * parseInt(fatura.kdv) / 100,
                                                        genelToplam: parseFloat(fatura.birimFiyat) * (100 + parseInt(fatura.kdv)) / 100,
                                                        kalemler: [{
                                                            miktar: 1,
                                                            birimFiyat: parseFloat(fatura.birimFiyat),
                                                            toplam: parseFloat(fatura.toplam),
                                                            aciklama: `${fatura.urun ? fatura.urun.adi : ''} ${aciklamaFormat(fatura.aciklama)}`,
                                                            kdvOran: parseInt(fatura.kdv)
                                                        }],

                                                        kdvOrani: parseInt(fatura.kdv),
                                                        duzenlemeTarihi: faturaKesimGunu //new Date(),
                                                    }
                                                    setPreviewPending(i)
                                                    eFaturaOnIzleme(_f).then(resp => {
                                                        if (resp) {
                                                            let html = Buffer.from(resp, 'base64').toString('ascii')
                                                            setPreview(html)
                                                            setOpenFaturaPreview(true)
                                                            setPreviewPending(-1)
                                                        }
                                                    })
                                                }}
                                            >
                                                <InsertDriveFileOutlinedIcon />
                                            </IconButton>
                                        }
                                    </td>
                                </tr>)
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
            <Dialog
                fullWidth
                maxWidth='lg'
                onClose={() => { setOpenFaturaPreview(false) }} open={openFaturaPreview}>
                <DialogTitle onClose={() => { setOpenFaturaPreview(false) }}>
                    Fatura Önizleme
                </DialogTitle>
                <DialogContent dividers>
                    <div className='flex justify-center'>
                        <div dangerouslySetInnerHTML={{ __html: preview }} />
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button variant='contained' onClick={() => { setOpenFaturaPreview(false) }} color='primary'>Kapat</Button>
                </DialogActions>
            </Dialog>

            <Dialog
                fullWidth
                maxWidth='md'
                onClose={() => { setShowAreYouSure(false) }}
                open={showAreYouSure}>
                <DialogTitle >
                    Toplu Fatura Kesimi
                </DialogTitle>
                <DialogContent dividers>
                    <div className='flex-col'>
                        <h2>{selected.length} adet fatura kesilecek</h2>
                        <h2>Toplam {selectedTotal} TL</h2>
                        <h2>Emin misiniz?</h2>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button variant='contained' onClick={() => {
                        seciliFaturalariKes().then(resp => {
                            setShowAreYouSure(false)
                            setShowSonuc(true)
                        })
                    }} color='primary'>Evet</Button>
                    <Button variant='contained' onClick={() => {
                        setShowAreYouSure(false)
                    }} color='primary'>Hayır</Button>
                </DialogActions>
            </Dialog>


            <Dialog
                fullWidth
                maxWidth='sm'
                //onClose={() => { setShowAreYouSure(false) }}
                open={showSonuc}>
                <DialogTitle >
                    Toplu Fatura Kesimi
                </DialogTitle>
                <DialogContent dividers>
                    <div className='flex justify-center'>
                        <h2>Faturalar kesildi</h2>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button variant='contained' onClick={() => {
                        setShowSonuc(false)
                        props.router.back()
                    }} color='primary'>Kapat</Button>
                </DialogActions>
            </Dialog>

        </div>
    )
}

TopluFaturaKesim.getInitialProps = async (ctx) => {
    let topluFatura = await getTopluFatura(ctx.query.id)

    return {
        topluFatura
    }
}

export default withRouter(TopluFaturaKesim)
