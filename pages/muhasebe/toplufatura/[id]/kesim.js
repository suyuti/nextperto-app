import numbro from 'numbro'
import React, { useState } from 'react'
import PageHeader from '../../../../components/pageHeader'
import moment from 'moment'
import { Checkbox, Button, Dialog, DialogContent, DialogTitle, DialogActions } from '@material-ui/core'
import { getTopluFatura, saveFatura } from '../../../../services/muhasebe.service'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import { eFaturaOnIzleme } from '../../../../services/efatura.service'
import { uuid } from 'uuidv4';
import { useRouter } from 'next/router'

const TopluFaturaKesim = props => {
    const router = useRouter()
    const { topluFatura } = props
    const [faturalar, setFaturalar] = useState(topluFatura.liste)
    const [secililer, setSecililer] = useState([])
    const [duzenlemeTarihi, setDuzenlemeTarihi] = useState(new Date())
    const [preview, setPreview] = useState(null/*b()*/)
    const [openFaturaPreview, setOpenFaturaPreview] = useState(false)
    const [kesimYapildi, setKesimYapildi] = useState(false)

    const onSec = (fatura, selected) => {
        if (selected) {
            let s = [...secililer]
            s.push(fatura)
            setSecililer(s)
        }
        else {
            let s = secililer.filter(f => f._id != fatura._id)
            setSecililer(s)
        }
    }

    const onTumunuSec = (checked) => {
        if (checked) {
            let _f = [...faturalar]
            setSecililer(_f)
        }
        else {
            setSecililer([])
        }
    }

    const secilileriKes = () => {
        for (let fatura of secililer) {
            let _f = {
                key: uuid(),
                vkesenFirma: topluFatura.vkesenFirma,
                vfirma: fatura.firma.key,
                araToplam: fatura.araToplam,
                toplamKdv: fatura.araToplam * 0.18,
                genelToplam: fatura.araToplam * 1.18,
                kalemler: [{
                    miktar: 1,
                    toplam: fatura.araToplam,
                    aciklama: fatura.aciklama
                }],
                aciklama: fatura.aciklama,
                dovizTuru: '',
                vadeTarihi: moment(duzenlemeTarihi).add(5, 'days'),
                taslak: false,
                tahsilatTutari: 0,
                kdvOrani: 18,
                duzenlemeTarihi: duzenlemeTarihi,
            }

            saveFatura(_f)
        }
        setKesimYapildi(true)
        router.back()
    }

    return <>
        <PageHeader
            title='Toplu Fatura Kesimi'
            buttons={[
                <Button
                    disabled={secililer.length == 0 && kesimYapildi}
                    variant='contained'
                    color='primary'
                    onClick={(e) => secilileriKes()}
                >Seçili Faturaları Kes</Button>
            ]}
        />
        <div className='p-4'>
            <div className='p-4 bg-white shadow'>
                <div className='text-2xl'>{topluFatura.listeAdi}</div>
                <div className='flex'>
                    <div className='w-1/6 p-2'>
                        <div className='flex justify-between align-baseline'>
                            <div className='font-light'>Adet</div>
                            <div className='text-xl'>{topluFatura.liste.length}</div>
                        </div>
                        <div className='flex justify-between align-text-bottom'>
                            <div className='font-light'>Toplam</div>
                            <div className='text-xl'>{topluFatura.liste.reduce((a, b) => a + b.araToplam, 0)}</div>
                        </div>
                    </div>
                    <div className='w-1/6  p-2 border-l-2 ml-4 pl-4'>
                        <div className='flex justify-between align-baseline'>
                            <div className='font-light'>Seçili Adet</div>
                            <div className='text-xl'>{secililer.length}</div>
                        </div>
                        <div className='flex justify-between align-baseline'>
                            <div className='font-light'>Seçili Toplam</div>
                            <div className='text-xl'>{secililer.reduce((a, b) => a + b.araToplam, 0)}</div>
                        </div>
                    </div>
                    <div className='w-1/6  p-2 border-l-2 ml-4 pl-4'>
                        <div className='flex justify-between align-baseline'>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <KeyboardDatePicker
                                    //error={errors && errors.duzenlemeTarihi}
                                    //helperText={errors && errors.duzenlemeTarihi}
                                    autoOk
                                    //size='small'
                                    clearable
                                    fullWidth
                                    variant="inline"
                                    inputVariant="outlined"
                                    label="Fatura Düzenleme Tarihi"
                                    format="dd.MM.yyyy"
                                    InputAdornmentProps={{ position: 'start' }}
                                    value={duzenlemeTarihi}
                                    onChange={(e) => setDuzenlemeTarihi(e)}
                                />
                            </MuiPickersUtilsProvider>
                        </div>
                    </div>
                </div>
                <div className=''>
                    <Checkbox
                        onClick={(e) => {
                            onTumunuSec(e.target.checked)
                        }}
                    />
                </div>
            </div>

            <div className='p-4 bg-white shadow mt-2 flex flex-wrap'>
                {faturalar.map(f => {
                    return <div className='border w-1/3 p-2 m-2 rounded shadow hover:shadow-none flex'>
                        <div className='p-2'>
                            <Checkbox
                                checked={ secililer.findIndex(s => s._id == f._id) >= 0}
                                onChange={e => {
                                    onSec(f, e.target.checked)
                                }} />
                        </div>
                        <div className='w-4/6 p-2'>
                            <div className='text-lg'>{f.firma.unvan}</div>
                            <div className='text-lg font-sans font-light mt-2'>{f.aciklama}</div>
                            <div className='text-sm font-sans font-light'>{moment(f.vadeTarihi).format('DD.MM.YYYY')}</div>
                        </div>
                        <div className='p-2 flex flex-col justify-between'>
                            <div className='text-2xl font-semibold'>{numbro(f.araToplam).format({ thousandSeparated: true, mantissa: 2 })} TL</div>
                            <div className='self-end'><Button variant='outlined' color='primary' size='small'
                                onClick={() => {
                                    // TODO
                                    let _f = {
                                        vkesenFirma: topluFatura.vkesenFirma,
                                        vfirma: f.firma.key,
                                        araToplam: f.araToplam,
                                        toplamKdv: f.araToplam * 0.18,
                                        genelToplam: f.araToplam * 1.18,
                                        kalemler: [{
                                            miktar: 1,
                                            toplam: f.araToplam,
                                            aciklama: f.aciklama
                                        }],

                                        kdvOrani: 18,
                                        duzenlemeTarihi: duzenlemeTarihi,
                                    }
                                    eFaturaOnIzleme(_f).then(resp => {
                                        if (resp) {
                                            let html = Buffer.from(resp, 'base64').toString('ascii')
                                            setPreview(html)
                                            setOpenFaturaPreview(true)
                                        }
                                    })

                                }}
                            >Ön izleme</Button></div>
                        </div>
                    </div>
                })}
            </div>

        </div>
        <Dialog
            fullWidth
            maxWidth='lg'
            onClose={() => { setOpenFaturaPreview(false) }} open={openFaturaPreview}>
            <DialogTitle onClose={() => { setOpenFaturaPreview(false) }}>
                Fatura Önizleme
            </DialogTitle>
            <DialogContent dividers>
                <div className='flex justify-center'>
                    <div dangerouslySetInnerHTML={{ __html: preview }} />
                </div>
            </DialogContent>
            <DialogActions>
                <Button variant='contained' onClick={() => { setOpenFaturaPreview(false) }} color='primary'>Kapat</Button>
            </DialogActions>
        </Dialog>
    </>
}

TopluFaturaKesim.getInitialProps = async (ctx) => {
    let topluFatura = await getTopluFatura(ctx.query.id)
    return {
        topluFatura
    }
}

export default TopluFaturaKesim