import React, { useEffect, useState } from 'react'
import PageHeader from "../../../components/pageHeader";
import { makeStyles, Box, Dialog, DialogTitle, DialogContent, DialogContentText, Button, DialogActions } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import { useRouter, withRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import TableView from '../../../components/tableview';
import { deleteTopluFatura } from '../../../services/muhasebe.service'
import * as Actions from '../../../redux/actions'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const TopluFaturalar = props => {
    const classes = useStyles()
    const router = useRouter()
    const token = useSelector(state => state.auth.expertoToken)
    const [modalOpen, setModalOpen] = useState(false)
    const [silinecekId, setSilinecekId] = useState(null)
    const permissions = useSelector(state => state.auth.permissions)

    const columns = [
        { id: 'listeAdi', label: 'Liste Adı' },
        { id: 'kesenFirma.marka', label: 'Kesen Firma' },
        {
            id: '', label: 'Fatura Kesim Günü', render: row => {
                return `Her ayın ${row.faturaKesimGunu}. günü`
            }
        },
        {
            id: 'active', label: 'Aktif', render: row => {
                if (row.active) {
                    return <Box bgcolor="success.main" color="primary.contrastText" p={1}>Aktif</Box>
                }
                else {
                    return <Box bgcolor="error.main" color="primary.contrastText" p={1}>Pasif</Box>
                }
            }
        },
        {
            id: 'sabitFiyatliMi', label: 'Sabit Fiyatli mı?', render: row => {
                if (row.sabitFiyatliMi) {
                    return <Box bgcolor="info.main" color="primary.contrastText" p={1}>SABiT</Box>
                }
                else {
                    return <Box bgcolor="warning.main" color="primary.contrastText" p={1}>DEĞİŞKEN</Box>
                }

            }
        },
        {
            id: '', label: 'Toplam Fatura Adedi', align: 'right', render: row => {
                return row.liste.length
            }
        },
        {
            id: '', label: '', render: row => {
                return (<div className='flex space-x-1'>
                    <Button
                        variant='outlined'
                        color='primary'
                        size='small'
                        onClick={() => {
                            router.push(`/muhasebe/toplufatura/${row._id}`)
                        }}>Detay</Button>
                    <Button 
                        variant='outlined' 
                        color='secondary' 
                        size='small' 
                        disabled={!row.active}
                        onClick={() => {
                        router.push(`/muhasebe/toplufatura/${row._id}/kesim2`)
                    }}>Kesim</Button>
                    <Button
                        disabled={!permissions.find(p => p.key == 'topluFatura')?.dlete} 
                        variant='outlined' 
                        color='secondary' 
                        size='small' 
                        onClick={() => {
                        setModalOpen(true)
                        setSilinecekId(row._id)
                    }}>Sil</Button>
                </div>)
            }
        }
    ]


    const handleClose = () => {
        setModalOpen(false)
        setSilinecekId(null)
    }


    return <>
        <PageHeader
            title='Toplu Fatura Listeleri'
            buttons={[
                <Button
                    disabled={!permissions.find(p => p.key == 'topluFatura')?.create} 
                    variant='contained' 
                    color='primary' 
                    startIcon={<AddIcon />} 
                    onClick={() => { router.push('/muhasebe/toplufatura/[id]', '/muhasebe/toplufatura/new') }}>Yeni Liste</Button>
            ]}
        />
        <div className={classes.root}>
            <TableView
                title='Toplu Fatura Listeleri'
                columns={columns}
                remoteUrl={`/toplufatura`}
                token={token}
                filter={`select=listeAdi,liste,faturaKesimGunu,active,vkesenFirma,sabitFiyatliMi&populate=kesenFirma.marka&deleted=false`}
                searchCol='listeAdi'
            />
        </div>
        <Dialog
            open={modalOpen}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{"Dikkat"}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Toplu fatura listesi silinecek. Emin misiniz?
                    </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button
                    variant='contained'
                    onClick={() => {
                        deleteTopluFatura(silinecekId)
                        setSilinecekId(null)
                        handleClose()
                        props.router.back()
                    }}
                    color="primary">
                    Evet
                    </Button>
                <Button
                    variant='contained'
                    onClick={e => {
                        setSilinecekId(null)
                        handleClose()
                    }} color="secondary" autoFocus>
                    Hayır
                    </Button>
            </DialogActions>
        </Dialog>

    </>
}

export default withRouter(TopluFaturalar)