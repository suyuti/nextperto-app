import React, { useEffect, useState } from 'react'
import PageHeader from '../../../components/pageHeader'
import { makeStyles, Grid, Accordion, Paper, TextField, MenuItem, Box } from '@material-ui/core'
import BugunKesilecekFaturalar from '../../../components/muhasebe/guncel/bugunKesilecekFaturalar'
import BugunYapilacakTahsilatlar from '../../../components/muhasebe/guncel/bugunYapilacakTahsilatlar'
import Alacaklar from '../../../components/muhasebe/guncel/alacaklar'
import Alacaklar2 from '../../../components/muhasebe/guncel/alacaklar2'
import Verecekler from '../../../components/muhasebe/guncel/verecekler'
import GecikenTahsilatlar from '../../../components/muhasebe/guncel/gecikenTahsilatlar'
import { getTenants } from '../../../services/tenant.service'

import { getGerceklesenTahsilatlar, getGelecekTahsilatlar, getGecikenTahsilatlar, getPlanlananTahsilatlar } from '../../../services/tenant.service'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    filterRoot: {
        padding: theme.spacing(2)
    }
}))



const sureler = [
    { title: 'Haftalık', value: 'h' },
    { title: 'Aylık', value: 'a' },
    { title: 'Yıllık', value: 'y' },
    { title: 'Dönemsel', value: 'd' },
]

const defaultData = {
    name: 'Tahsilat',
    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
}

const MuhasebeGuncel = props => {
    const { tenants } = props
    const classes = useStyles()
    const [tenant, setTenant] = useState('')

    const [gerceklesenTahsilatlar, setGerceklesenTahsilatlar] = useState(null)
    const [gelecekTahsilatlar, setGelecekTahsilatlar] = useState(defaultData)
    const [gecikenTahsilatlar, setGecikenTahsilatlar] = useState(defaultData)
    const [planlananTahsilatlar, setPlanlananTahsilatlar] = useState(defaultData)

    const load = async (tenant) => {
        getGerceklesenTahsilatlar(tenant._id).then(resp => {
            setGerceklesenTahsilatlar(resp)
        })

        getPlanlananTahsilatlar(tenant._id).then(resp => {
            setPlanlananTahsilatlar(resp)
        })

        getGecikenTahsilatlar(tenant._id).then(resp => {
            setGecikenTahsilatlar(resp)
        })

        getGelecekTahsilatlar(tenant._id).then(resp => {
            setGelecekTahsilatlar(resp)
        })
    }

    useEffect(() => {
        if (tenant) {
            load(tenant)
        }
    }, [tenant])


    return <>
        <PageHeader
            title='Güncel Durum'
            buttons={[
                <div className='w-96 flex flex-row space-x-2'>
                    <div className='w-2/3'>
                        <TextField
                            label='Firma'
                            variant='outlined'
                            size='small'
                            select
                            fullWidth
                            onChange={(e) => {
                                setTenant(e.target.value)
                            }}
                        >
                            {tenants.map(t => <MenuItem key={t.key} value={t}>{t.marka}</MenuItem>)}
                        </TextField>
                    </div>
                    <div className='w-1/3'>
                        <TextField
                            disabled
                            label='Süre'
                            variant='outlined' fullWidth size='small' select>
                            {sureler.map(s => <MenuItem key={s.value} value={s.value}>{s.title}</MenuItem>)}
                        </TextField>
                    </div>
                </div>
            ]}
        />
        <div className='p-4 grid lg:grid-cols-2 gap-4'>
            {gerceklesenTahsilatlar && <div className=''>
                <Alacaklar2 options={{
                    title: 'Gerçekleşen Tahsilatlar'
                }}
                    data={gerceklesenTahsilatlar} />
            </div>}
            {gecikenTahsilatlar && <div className=''>
                <Alacaklar2 options={{
                    title: 'Geciken Tahsilatlar'
                }}
                    data={gecikenTahsilatlar} />
            </div>}
            {gelecekTahsilatlar && <div className=''>
                <Alacaklar2 options={{
                    title: 'Gelecek Tahsilatlar'
                }}
                    data={gelecekTahsilatlar} />
            </div>}
            {planlananTahsilatlar && <div className=''>
                <Alacaklar2 options={{
                    title: 'Planlanan Tahsilatlar'
                }}
                    data={planlananTahsilatlar} />
            </div>}
            {false &&
                <div>
                    <div className=''>
                        <Alacaklar2 options={{
                            title: 'Planlanan Tahsilatlar'
                        }}
                            data={planlananTahsilatlar}
                        />
                    </div>
                    <div className=''>
                        <Alacaklar2 options={{
                            title: 'Gelecek Tahsilatlar'
                        }}
                            data={gelecekTahsilatlar}

                        />
                    </div>
                    <div className=''>
                        <Alacaklar2 options={{
                            title: 'Geciken Tahsilatlar'
                        }}
                            data={gecikenTahsilatlar}
                        />
                    </div>
                </div>
            }
        </div>

        {false &&
            <div className={classes.root}>




                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <Alacaklar2 tenant={tenant} />
                    </Grid>
                    <Grid item xs={6}>
                        <Alacaklar tenant={tenant} />
                    </Grid>
                    <Grid item xs={6}>
                        <Verecekler tenant={tenant} />
                    </Grid>
                    <Grid item xs={12} md={12} lg={6}>
                        <BugunKesilecekFaturalar tenant={tenant} />
                    </Grid>
                    <Grid item xs={12} md={12} lg={6}>
                        <BugunYapilacakTahsilatlar tenant={tenant} />
                    </Grid>
                    <Grid item xs={12} md={12} lg={12}>
                        <GecikenTahsilatlar tenant={tenant} />
                    </Grid>
                </Grid>
            </div>
        }
    </>
}

MuhasebeGuncel.getInitialProps = async (ctx) => {
    const tenants = await getTenants()
    return {
        tenants: [{ key: 'ALL', marka: 'Tümü' }, ...tenants]
    }
}

export default MuhasebeGuncel