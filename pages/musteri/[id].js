import React, { useState } from 'react'
import PageHeader from "../../components/pageHeader";
import { Tabs, Tab, makeStyles, Paper } from '@material-ui/core';
import TabPanel from '../../hoc/TabPanel';
import MusteriGenelBilgiler from '../../components/musteri/MusteriGenelBilgiler'
import MusteriIletisimBilgileri from '../../components/musteri/MusteriIletisim'
import MusteriMaliBilgiler from '../../components/musteri/MusteriMaliBilgiler';
import MusteriFaturalar from '../../components/musteri/MusteriFaturalar';
import MusteriUrunler from '../../components/musteri/MusteriUrunler';
import MusteriYazismalar from '../../components/musteri/MusteriYazismalar';
import MusteriSozlesmeler from '../../components/musteri/MusteriSozlesmeler';
import MusteriKaynagi from '../../components/musteri/MusteriKaynagi'
import MusteriTeklifler from '../../components/musteri/MusteriTeklifler'
import MusteriPatentler from '../../components/musteri/MusteriPatentler'
import MusteriToplantilar from '../../components/musteri/MusteriToplantilar'
import MusteriIlgiliGorevler from '../../components/musteri/MusteriIlgiliGorevler'
import MusteriKisiler from '../../components/musteri/MusteriKisiler';
import { parseCookies, setCookie, destroyCookie } from 'nookies'
import api from '../../services/axios.service'
import { validateFirma } from '../../validators/firma.validator';
import { getFirma, saveFirma, updateFirma } from '../../services/firma.service';
import { useRouter } from 'next/router';
import { uuid } from 'uuidv4';
import { getSektorler } from '../../services/sektor.service';
import { getTenants } from '../../services/tenant.service';


const useStyles = makeStyles((theme) => ({
    root: {
        //padding: theme.spacing(2),
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        //height: 224,
        // padding: theme.spacing(2)
    },
    tabs: {
        borderRight: `1px solid ${theme.palette.divider}`,
    },
}));

const Musteri = props => {
    const { newFirma, sektorler, tenants } = props
    const [firma, setFirma] = useState(props.firma)
    const classes = useStyles();
    const [errors, setErrors] = useState(null)
    const [selectedTab, setSelectedTab] = useState(0)
    const tabChange = (event, newValue) => {
        setSelectedTab(newValue);
    };
    const router = useRouter()

    const onChange = (value) => {
        let f = { ...firma }
        let val = value.value
        if (value.field == 'vtenants') {
            
            val = value.value.map(v => v.key)
        }
        if (value.field == 'vsektor') {
            val = value.value.key
        }
        f[value.field] = val
        setFirma(f)
    }

    return <>
        <PageHeader
            title='Müşteri'
            buttons={[
                {
                    title: newFirma ? 'Kaydet' : 'Güncelle',
                    disabled: false, onClick: (e) => {
                        e.preventDefault();
                        let _errors = validateFirma(firma)
                        if (_errors) {
                            setErrors(_errors)
                            return
                        }
                        if (newFirma) {
                            saveFirma(firma)
                        }
                        else {
                            updateFirma(firma._id, firma)
                        }
                        router.back()
                    }
                },
            ]} />
        <div className={classes.root}>
            <Tabs
                orientation='vertical'
                variant='scrollable'
                value={selectedTab}
                onChange={tabChange}
                className={classes.tabs}
            >
                <Tab label='Genel Bilgiler' />
                <Tab label='İletişim' />
                <Tab label='Mali Bilgiler' />
                {!newFirma && <Tab label='Kişiler' />}
                {!newFirma && <Tab label='Müşteri Kaynağı' />}
                {!newFirma && <Tab label='Teklifler' />}
                {!newFirma && <Tab label='Sözleşmeler' />}
                {!newFirma && <Tab label='Görüşmeler' />}
                {!newFirma && <Tab label='Patentler' />}
                {!newFirma && <Tab label='Ürünleri' />}
                {!newFirma && <Tab label='Toplantılar' />}
                {!newFirma && <Tab label='İlgili Görevler' />}
                {!newFirma && <Tab label='Faturalar' />}

            </Tabs>
            <TabPanel value={selectedTab} index={0}><MusteriGenelBilgiler firma={firma} errors={errors} onChange={onChange} sektorler={sektorler} tenants={tenants}/></TabPanel>
            <TabPanel value={selectedTab} index={1}><MusteriIletisimBilgileri firma={firma} errors={errors} onChange={onChange} /></TabPanel>
            <TabPanel value={selectedTab} index={2}><MusteriMaliBilgiler firma={firma} onChange={onChange}/></TabPanel>
            {!newFirma && <TabPanel value={selectedTab} index={3}> <MusteriKisiler firma={firma}/></TabPanel>}
            {!newFirma && <TabPanel value={selectedTab} index={4}><MusteriKaynagi firma={firma}/></TabPanel>}
            {!newFirma && <TabPanel value={selectedTab} index={5}><MusteriTeklifler firma={firma}/></TabPanel>}
            {!newFirma && <TabPanel value={selectedTab} index={6}><MusteriSozlesmeler firma={firma}/></TabPanel>}
            {!newFirma && <TabPanel value={selectedTab} index={7}><MusteriYazismalar firma={firma}/></TabPanel>}
            {!newFirma && <TabPanel value={selectedTab} index={8}><MusteriPatentler firma={firma}/></TabPanel>}
            {!newFirma && <TabPanel value={selectedTab} index={9}><MusteriUrunler firma={firma}/></TabPanel>}
            {!newFirma && <TabPanel value={selectedTab} index={10}><MusteriToplantilar firma={firma}/></TabPanel>}
            {!newFirma && <TabPanel value={selectedTab} index={11}><MusteriIlgiliGorevler firma={firma}/></TabPanel>}
            {!newFirma && <TabPanel value={selectedTab} index={12}><MusteriFaturalar firma={firma}/> </TabPanel>}
        </div>
    </>
}

Musteri.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    let newFirma = ctx.query.id == 'new'
    let firma = {
        key             : uuid(),
        vtenants        : [],
        marka           : '',
        unvan           : '',
        domain          : '',
        KepAdresi       : '',
        vergiDairesi    : '',
        vergiNo         : '',
        eFaturaMi       : false,
        ciro            : '',
        kobiMi          : false,
        durum           : 'potansiyel',
        vsektor         : '',
        il              : '',
        ilce            : '',
        ulke            : '',
        adres           : '',
        vade            : 0,
        fabrikaAdres    : '',
        faturaAdres     : '',
        telefon         : '',
        webAdresi       : '',
        argeMerkezi     : false,
        tasaarimMerkezi : false,
        teknoparkFirmasi: false,
        faal                : true,
        efaturaTCKNKullan   : false,
        efaturaTCKN         : '',
        efaturaTCKNAdi      : '',
        efaturaTCKNSoyadi   : ''
    }

    let sektorler = await getSektorler()
    let tenants = await getTenants()

    if (!newFirma) {
        firma = await getFirma(ctx.query.id)
    }

    return {
        newFirma,
        firma,
        sektorler,
        tenants
    }
}

export default Musteri