import React, { useEffect, useState } from 'react'
import PageHeader from "../../components/pageHeader";
import { useRouter } from "next/router";
import GenelListe from '../../hoc/genelListe';
import TableView from '../../components/tableview';
import { Button, Typography, Drawer, makeStyles, Grid, Avatar } from '@material-ui/core';
import { useSelector } from 'react-redux'
import MusteriQuickInfo from '../../components/musteri/MusteriQuickInfo';
import api from '../../services/axios.service';
import { parseCookies, setCookie, destroyCookie } from 'nookies'
import FirmaSearchToolbar from '../../components/musteri/FirmaSearchToolbar'
import { getTenants } from '../../services/tenant.service'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    firmaBilgi: {
        width: 500
    },
    sorumlu: {
        display: 'flex',
        alignItems: 'center'
    },
    sorumluAvatar: {
        marginRight: theme.spacing(1),
        width: theme.spacing(3),
        height: theme.spacing(3),    },

}))

const Musteriler = props => {
    const {tenants} = props
    const router = useRouter();
    const token = useSelector(state => state.auth.expertoToken)
    const users = useSelector(state => state.auth.users)
    const [openFirmaInfo, setOpenFirmaInfo] = useState(false)
    const [firmaInfo, setFirmaInfo] = useState(null)
    const classes = useStyles()
    const [filterQuery, setFilterQuery] = useState('')

    useEffect(() => {

    }, [router.query.filter])

    const getFirmaInfo = async (id) => {
        const cookies = parseCookies()
        setFirmaInfo(null)
        api.get(`/firma/${id}`, {
            headers: { Authorization: `Bearer ${cookies.experto}` }
        }).then(resp => {
            setFirmaInfo(resp.data.data)
        })
    }

    const columns = [
        { id: 'marka', numeric: false, disablePadding: true, label: 'Marka', align: 'left' },
        { id: 'unvan', numeric: true, disablePadding: false, label: 'Ticari Ünvan', align: 'left' },
        { id: 'il', numeric: true, disablePadding: true, label: 'İl', align: 'left' },
        { id: 'ilce', numeric: true, disablePadding: true, label: 'İlçe', align: 'left' },
        { id: 'sektor.adi', numeric: true, disablePadding: true, label: 'Sektor', align: 'left' },
        {
            id: 'kobiMi', label: 'Kobi / Sanayi', align: 'left',
            render: (row) => {
                if (row.kobiMi) {
                    return <Typography>KOBI</Typography>
                }
                else {
                    return <Typography>SANAYI</Typography>
                }
            }
        },
        {
            id: 'sorumlu', label: 'Firma Sorumlusu', render: row => {
                if (!row.vsorumlu) {
                    return <></>
                }
                let u = users.find(u => u.key == row.vsorumlu)
                return (
                    <div className={classes.sorumlu}>
                        <Avatar className={classes.sorumluAvatar} src={u?.image} />
                        <Typography>{u?.username}</Typography>
                    </div>
                )
            }
        },
        {
            id: 'density', numeric: true, disablePadding: false, label: 'Aksiyon', align: 'right', render: (row) => {
                return (
                    <>
                        <Button variant='outlined' size='small' color='primary' onClick={() => { router.push(`/musteri/${row._id}`) }}>Detay</Button>
                        <Button variant='outlined' size='small' color='primary' onClick={() => {
                            getFirmaInfo(row._id)
                            setOpenFirmaInfo(true)
                        }}>Bilgi</Button>
                    </>
                )
            }
        },
    ]

    return <>
        <Drawer
            anchor='right'
            open={openFirmaInfo}
            onClose={() => setOpenFirmaInfo(false)}
        >
            <div className={classes.firmaBilgi}>
                <MusteriQuickInfo firmaInfo={firmaInfo} />
            </div>
        </Drawer>

        <PageHeader
            title='Müşteriler'
            buttons={[
                {
                    title: 'Yeni', disabled: false, onClick: (e) => {
                        e.preventDefault();
                        router.push("/musteri/[id]", `/musteri/new`);
                    }
                },
            ]} />
        <div className='p-2 '>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <FirmaSearchToolbar disabled tenants={tenants} onSearch={(queryString) => {
                        console.log(queryString)
                        setFilterQuery(queryString)
                    }}/>
                </Grid>
                <Grid item xs={12}>
                    <TableView
                        title='Müşteriler'
                        columns={columns}
                        remoteUrl={`/firma`}
                        token={token}
                        filter={`${router.query.filter}&select=marka,unvan,kobiMi,il,ilce,vsektor,vsorumlu&populate=sektor.adi&${filterQuery}`}
                        //filter={`select=marka,unvan,kobiMi,il,ilce,vsektor,vsorumlu&populate=sektor.adi`}
                        searchCol='marka'
                        options={{
                            rowsPerPage: 100
                        }}
                    />
                </Grid>
            </Grid>
        </div>
    </>
}
Musteriler.getInitialProps = async (ctx) => {
    let tenants = await getTenants()
    return {tenants}
}
export default Musteriler
