import React, { useState } from 'react'
import PageHeader from "../../components/pageHeader";
import MaterialTable from 'material-table';
import api from '../../services/axios.service';
import { useSelector } from 'react-redux'
import { Button, MenuItem, TextField } from '@material-ui/core'
import { getTenants } from '../../services/tenant.service';
import { getSektorler } from '../../services/sektor.service';
var qs = require('qs');

const HEPSI = 'HEPSI'
const HICBIRI = 'HICBIRI'

const durumlar = [
    { title: 'Hepsi', value: HEPSI },
    { title: 'Hiçbiri', value: HICBIRI },
    { title: 'Aktif', value: 'aktif' },
    { title: 'Pasif', value: 'pasif' },
    { title: 'Potansiyel', value: 'potansiyel' },
    { title: 'Sorunlu', value: 'sorunlu' }
]

const kobiSanayi = [
    { title: 'Hepsi', value: HEPSI },
    { title: 'Hiçbiri', value: HICBIRI },
    { title: 'Kobi', value: 'kobi' },
    { title: 'Sanayi', value: 'sanayi' },
]

const eFatura = [
    { title: 'Hepsi', value: HEPSI },
    { title: 'Hiçbiri', value: HICBIRI },
    { title: 'EFatura', value: 'efatura' },
    { title: 'EArşiv', value: 'earsiv' },
]

const argeTasarim = [
    { title: 'Hepsi', value: HEPSI },
    { title: 'Hiçbiri', value: HICBIRI },
    { title: 'Agre Merkezi', value: 'arge' },
    { title: 'Tasarım Merkezi', value: 'tasarim' },
    { title: 'Boş', value: 'Bos' },
]

const faal = [
    { title: 'Hepsi', value: HEPSI },
    { title: 'Hiçbiri', value: HICBIRI },
    { title: 'Faal', value: 'faal' },
    { title: 'Kapalı', value: 'kapali' },
]


const Musteriler = (props) => {
    const { tenants, sektorler } = props
    const token = useSelector(state => state.auth.expertoToken)
    const users = useSelector(state => state.auth.users)
    const tableRef = React.createRef();
    const [filter, setFilter] = useState({
        durum: null,
        tenant: null,
        vsektor: null,
        kobiMi: null,
        efatura: null,
        arge: null,
        faal: null,
        vsorumlu: null
    })

    const prepFilter = (setting) => {
        let f = { ...filter }
        if (setting) {
            if (setting.item == 'kobiMi') {

            }
            f[setting.item] = setting.value
        }
        else {
            delete f[setting.item]
        }
        setFilter(f)
    }

    const filterToQueryString = (filter) => {
        if (!filter.durum) {
            delete filter.durum
        }
        else {
            //if (filter.durum == HICBIRI)
            //filter.durum 
        }
        if (!filter.tenant) {
            delete filter.tenant
        }
        if (!filter.vsektor) {
            delete filter.vsektor
        }
        if (!filter.kobiMi) {
            delete filter.kobiMi
        }
        if (!filter.efatura) {
            delete filter.efatura
        }
        if (!filter.arge) {
            delete filter.arge
        }
        if (!filter.faal) {
            delete filter.faal
        }
        if (!filter.vsorumlu) {
            delete filter.vsorumlu
        }
        return qs.stringify(filter)
    }

    const onTemizle = () => {
        setFilter({
            durum: null,
            tenant: null,
            vsektor: null,
            kobiMi: null,
            efatura: null,
            arge: null,
            faal: null,
            vsorumlu: null
        })
    }

    return (
        <div>
            <PageHeader
                title='Müşteriler'
            />
            <div className='p-2 flex flex-col space-y-2'>
                <div className='flex flex-col p-2 bg-white border shadow rounded'>
                    <div className='flex flex-wrap'>
                        <div className='w-1/6 p-1'>
                            <TextField
                                fullWidth
                                label='Durum'
                                variant='outlined'
                                size='small'
                                select
                                value={filter.durum}
                                onChange={e => {
                                    prepFilter({ item: 'durum', value: e.target.value })
                                }}
                            >
                                {durumlar.map((durum, i) => {
                                    return <MenuItem key={durum.value} value={durum.value}>{durum.title}</MenuItem>
                                })}
                            </TextField>
                        </div>
                        <div className='w-1/6 p-1'>
                            <TextField
                                fullWidth
                                label='Bağlı Olduğu Firma'
                                variant='outlined'
                                size='small'
                                select
                                value={filter.tenant}
                                onChange={e => {
                                    prepFilter({ item: 'tenant', value: e.target.value })
                                }}
                            >
                                {[...[
                                    { _id: HEPSI, adi: 'Hepsi' },
                                    { _id: HICBIRI, adi: 'Hiçbiri' }
                                ], ...tenants].map((tenant, i) => {
                                    return <MenuItem key={tenant._id} value={tenant._id}>{tenant.adi}</MenuItem>
                                })}
                            </TextField>
                        </div>
                        <div className='w-1/6 p-1'>
                            <TextField
                                fullWidth
                                label='Sektör'
                                variant='outlined'
                                size='small'
                                select
                                value={filter.vsektor}
                                onChange={e => {
                                    prepFilter({ item: 'vsektor', value: e.target.value })
                                }}
                            >
                                {[...[
                                    { key: HEPSI, adi: 'Hepsi' },
                                    { key: HICBIRI, adi: 'Hiçbiri' }
                                ], ...sektorler].map((sektor, i) => {
                                    return <MenuItem key={sektor._id} value={sektor.key}>{sektor.adi}</MenuItem>
                                })}
                            </TextField>
                        </div>
                        <div className='w-1/6 p-1'>
                            <TextField
                                fullWidth
                                label='Kobi/Sanayi'
                                variant='outlined'
                                size='small'
                                select
                                value={filter.kobiMi}
                                onChange={e => {
                                    prepFilter({ item: 'kobiMi', value: e.target.value })
                                }}
                            >
                                {kobiSanayi.map((_kobiSanayi, i) => {
                                    return <MenuItem key={_kobiSanayi.value} value={_kobiSanayi.value}>{_kobiSanayi.title}</MenuItem>
                                })}
                            </TextField>
                        </div>
                        <div className='w-1/6 p-1'>
                            <TextField
                                fullWidth
                                label='EFatura'
                                variant='outlined'
                                size='small'
                                select
                                value={filter.efatura}
                                onChange={e => {
                                    prepFilter({ item: 'efatura', value: e.target.value })
                                }}
                            >
                                {eFatura.map((ef, i) => {
                                    return <MenuItem key={ef.value} value={ef.value}>{ef.title}</MenuItem>
                                })}
                            </TextField>
                        </div>
                        <div className='w-1/6 p-1'>
                            <TextField
                                fullWidth
                                label='Arge/Tasarım'
                                variant='outlined'
                                size='small'
                                select
                                value={filter.arge}
                                onChange={e => {
                                    prepFilter({ item: 'arge', value: e.target.value })
                                }}
                            >
                                {argeTasarim.map((at, i) => {
                                    return <MenuItem key={at.value} value={at.value}>{at.title}</MenuItem>
                                })}
                            </TextField>
                        </div>
                        <div className='w-1/6 p-1'>
                            <TextField
                                fullWidth
                                label='Faaliyet'
                                variant='outlined'
                                size='small'
                                select
                                value={filter.faal}
                                onChange={e => {
                                    prepFilter({ item: 'faal', value: e.target.value })
                                }}
                            >
                                {faal.map((f, i) => {
                                    return <MenuItem key={f.value} value={f.value}>{f.title}</MenuItem>
                                })}
                            </TextField>
                        </div>
                        <div className='w-1/6 p-1'>
                            <TextField
                                fullWidth
                                label='Sorumlu'
                                variant='outlined'
                                size='small'
                                select
                                value={filter.vsorumlu}
                                onChange={e => {
                                    prepFilter({ item: 'vsorumlu', value: e.target.value })
                                }}
                            >
                                {[...[
                                    { key: HEPSI, username: 'Hepsi' },
                                    { key: HICBIRI, username: 'Hiçbiri' }
                                ], ...users].map((u, i) => {
                                    return <MenuItem key={u.key} value={u.key}>{u.username}</MenuItem>
                                })}
                            </TextField>
                        </div>
                    </div>
                    <div className='flex justify-end space-x-1'>
                        <Button className='focus:outline-none' variant='outlined' color='primary' size='small' onClick={onTemizle}>Temizle</Button>
                        <Button className='focus:outline-none' variant='contained' color='primary' size='small' onClick={() => {
                            tableRef.current && tableRef.current.onQueryChange()
                        }}>Sorgula</Button>
                    </div>
                    <div className='bg-green-300 p-2'>
                        <p>{JSON.stringify(filter)}</p>
                        <p>{filterToQueryString(filter)}</p>
                    </div>
                </div>

                <MaterialTable
                    title='Müşteriler'
                    tableRef={tableRef}
                    options={{
                        padding: 'dense'
                    }}
                    columns={[
                        {
                            title: 'Marka', field: 'marka'
                        },
                        { title: 'Ticari Ünvan', field: 'unvan' },
                        { title: 'Sektör', field: 'sektor.adi' },
                        { title: 'İl', field: 'il' },
                        { title: 'İlçe', field: 'ilce' },
                        {
                            title: 'Durum', field: 'durum', render: row => {
                                return <div className='w-16'>
                                    {row.durum == 'aktif' && <div className='bg-green-400 p-1 text-center text-white rounded text-xs'>Aktif</div>}
                                    {row.durum == 'pasif' && <div className='bg-gray-600 p-1 text-center text-white rounded text-xs'>Pasif</div>}
                                    {row.durum == 'potansiyel' && <div className='bg-blue-400 p-1 text-center text-white rounded text-xs'>Potansiyel</div>}
                                    {row.durum == 'sorunlu' && <div className='bg-red-400 p-1 text-center text-white rounded text-xs'>Sorunlu</div>}
                                </div>

                            }
                        },
                        {
                            title: 'Kobi/Sanayi', field: 'kobiMi', render: row => {
                                if (row.kobiMi) {
                                    return <div className='w-16 bg-blue-400 p-1 text-center text-white rounded'>Kobi</div>
                                }
                                else {
                                    return <div className='w-16 bg-red-400 p-1 text-center text-white rounded'>Sanayi</div>
                                }
                            }
                        },
                        { title: 'Firma Sorumlusu', field: 'vsorumlu' },
                    ]}

                    data={query => {
                        return new Promise((resolve, reject) => {
                            let q = filterToQueryString(filter)
                            //let q = qs.stringify(filter)

                            api.get(`/firma?skip=${query.page * query.pageSize}&limit=${query.pageSize}&populate=sektor&${q}`, { headers: { Authorization: `Bearer ${token}` } }).then(result => {
                                if (!result.data.data) {
                                    reject()
                                }
                                else {
                                    resolve({
                                        data: result.data.data.data,
                                        page: result.data.data.page / query.pageSize,
                                        totalCount: result.data.data.total,
                                    })
                                }
                            })
                        })
                    }}
                />
            </div>
        </div>
    )
}
Musteriler.getInitialProps = async (ctx) => {
    let tenants = await getTenants()
    let sektorler = await getSektorler()
    return {
        tenants,
        sektorler
    }
}
export default Musteriler
