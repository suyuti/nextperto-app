import React, { useState, useEffect } from 'react'
import PageHeader from '../components/pageHeader'
import axios from 'axios'
import { Button, Tabs, Tab, Grid } from '@material-ui/core'
import { NextPage } from 'next'
import { useDispatch, useSelector } from 'react-redux'
import * as Actions from '../redux/actions'
import TabPanel from '../hoc/TabPanel.js'
import GorevChart from '../components/dashboard/gorevChart'
import ToplantiChart from '../components/dashboard/toplantiChart'
import TakvimPanel from '../components/dashboard/takvimPanel'
import withAuth from '../hoc/withAuth.js'
import { connect } from 'react-redux';
import GorevList from '../components/gorev/gorevList'
import ToplantiList from '../components/toplanti/toplantiList'
import Performance from '../components/performance'
import GorevTimeline from '../components/gorev/gorevTimeline'
import EkipGuncel from '../components/dashboard/ekipGuncel'
import DashboardDefaultActions from '../components/dashboard/dashboardActions'

const Home = (props) => {
    const dispatch = useDispatch()
    const button = useSelector(state => state.auth.expertoToken)
    const permissions = useSelector(state => state.auth.permissions)
    const [faturalar, setFaturalar] = useState({})
    const [selectedTab, setSelectedTab] = useState(0)
    const user = useSelector(state => state.auth.user)
    const ekip = useSelector(state => state.auth.ekip)
    //const ekip = []

    return <>
        <Tabs
            value={selectedTab}
            onChange={(e, newValue) => {
                setSelectedTab(newValue)
            }}>
            <Tab label='Güncel' />
            {ekip.length > 0 &&
                <Tab label='Ekip' />
            }
            <Tab label='Takvim' />
        </Tabs>
        <TabPanel value={selectedTab} index={0}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <DashboardDefaultActions />
                </Grid>
                {false && <Grid item xs={12}>
                    <GorevTimeline />
                </Grid>}
                <Grid item xs={8} container spacing={2}>
                    <Grid item xs={6}>
                        <GorevChart />
                    </Grid>
                    <Grid item xs={6}>
                        <ToplantiChart />
                    </Grid>
                    <Grid item xs={6}>
                        <GorevList sorumlular={user.key} limit={10} acik title='Son 10 Görev' />
                    </Grid>
                    <Grid item xs={6}>
                        <ToplantiList katilimcilar={user.key} acik title='Gelecek Toplantılarım' />
                    </Grid>
                </Grid>
                <Grid item xs={4}>
                    <Performance />
                </Grid>
            </Grid>
        </TabPanel>
        {ekip.length > 0 &&
            <TabPanel value={selectedTab} index={1}>
                <EkipGuncel />
            </TabPanel>
        }
        <TabPanel value={selectedTab} index={
            ekip.length > 0 ? 2 : 1
        }>
            <Grid container spacing={2}>
                <TakvimPanel />
            </Grid>
        </TabPanel>
    </>
}

Home.getInitialProps = async (ctx) => {
    return {
        props: {
            button: 'test'
        }
    }
}

const mapStateToProps = (state) => {
    return { user: state.auth.user };
};
const mapDispatchToProps = (dispatch) => {
    return {
        login: () => { dispatch(Actions.silentLogin()) }
    }
}
//export default Home
//export default connect(mapStateToProps)(withAuth(Home))
export default connect(mapStateToProps, mapDispatchToProps)(withAuth(Home))
//export default withAuth(Home)
