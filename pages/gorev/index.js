import React, { useState, useRef, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'
import { Button, Box, IconButton, TextField, MenuItem, Tooltip, Avatar, Menu, Dialog, DialogTitle, DialogContent, DialogActions, DialogContentText } from '@material-ui/core'
import { withRouter } from 'next/router';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import RemoteAutoComplete from '../../components/autocomplete';
import { searchFirmalar } from '../../services/firma.service'
import trLocale from "date-fns/locale/tr";
import PageHeader from '../../components/pageHeader';
import MaterialTable from 'material-table';
import api from '../../services/axios.service';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import * as Actions from '../../redux/actions'
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import ThumbDownAltOutlinedIcon from '@material-ui/icons/ThumbDownAltOutlined';
import { gorevTamamla } from '../../services/gorev.service'
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
var qs = require('qs');
moment.locale('tr')

const GorevFilter = props => {
    const { onQuery, filterRef } = props
    const ekip = useSelector(state => state.auth.ekip)
    const users = useSelector(state => state.auth.users)
    const user = useSelector(state => state.auth.user)
    const filter = useSelector(state => state.common.filter)
    const dispatch = useDispatch()
    const [firmalar, setFirmalar] = useState([])

    return (
        <div className='bg-white p-2 rounded shadow flex space-x-2'>
            <div className='w-1/6'>
                <RemoteAutoComplete
                    disabled={filter.icGorev}
                    results={firmalar}
                    size='small'
                    label='Firma'
                    labelField='marka'
                    value={filter.firma || ''}
                    onSelected={e => {
                        dispatch(Actions.setFilter({ ...filter, firma: e }))
                    }}
                    fetchRemote={(searchText) => {
                        searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                            setFirmalar(resp)
                        })
                    }}
                />
            </div>
            <div className='w-1/12'>
                <TextField
                    label='İç Görev mi?'
                    fullWidth
                    variant='outlined'
                    size='small'
                    select
                    value={filter.icGorev}
                    onChange={e => {
                        dispatch(Actions.setFilter({ ...filter, icGorev: e.target.value }))
                    }}
                >
                    <MenuItem key={true} value={true}>Evet</MenuItem>
                    <MenuItem key={false} value={false}>Hayır</MenuItem>
                </TextField>
            </div>
            <div className='w-1/6'>
                <TextField
                    label='Sorumlu'
                    fullWidth
                    variant='outlined'
                    size='small'
                    select
                    value={filter.sorumlu || ''}
                    onChange={e => {
                        dispatch(Actions.setFilter({ ...filter, sorumlu: e.target.value }))
                        //setForm(prev => { return { ...prev, sorumlu: e.target.value } })
                    }}
                >
                    {[user.key, ...ekip].map((e, i) => {
                        let user = users.find(u => u.key == e)
                        return (<MenuItem key={i} value={user.key}>{user.username}</MenuItem>)
                    })}
                </TextField>
            </div>
            <div className='w-1/6'>
                <TextField
                    label='Durum'
                    fullWidth
                    variant='outlined'
                    size='small'
                    select
                    value={filter.sonuc || ''}
                    onChange={e => {
                        dispatch(Actions.setFilter({ ...filter, sonuc: e.target.value }))
                    }
                        //setForm(prev => { return { ...prev, sonuc: e.target.value } })
                    }
                >
                    <MenuItem key='hepsi' value='hepsi'>HEPSI</MenuItem>
                    <MenuItem key='acik' value='acik'>AÇIK</MenuItem>
                    <MenuItem key='basarili' value='basarili'>BAŞARILI</MenuItem>
                    <MenuItem key='gecbasarili' value='gecbasarili'>GECİKMELİ BAŞARILI</MenuItem>
                    <MenuItem key='basarisiz' value='basarisiz'>BAŞARISIZ</MenuItem>
                    <MenuItem key='iptal' value='iptal'>İPTAL</MenuItem>
                    <MenuItem key='gecikme' value='gecikme'>GECİKME</MenuItem>
                    <MenuItem key='onayBekleniyor' value='onayBekleniyor'>ONAY BEKLENİYOR</MenuItem>
                </TextField>
            </div>
            <div className='w-1/6'>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={trLocale}>
                    <KeyboardDatePicker
                        //error={errors && errors.start}
                        //helperText={errors && errors.start}
                        autoOk
                        fullWidth
                        clearable
                        variant="inline"
                        inputVariant="outlined"
                        label="Tarih"
                        size='small'
                        format="dd.MM.yyyy"
                        maxDate={filter.bitisTarih || undefined}
                        InputAdornmentProps={{ position: 'start' }}
                        value={filter.baslangicTarih || null}
                        onChange={(e) =>
                            dispatch(Actions.setFilter({ ...filter, baslangicTarih: e }))
                            //setForm(prev => { return { ...prev, baslangicTarih: e } })
                        }
                    />
                </MuiPickersUtilsProvider>
            </div>
            <div className='w-1/6'>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={trLocale}>
                    <KeyboardDatePicker
                        //error={errors && errors.start}
                        //helperText={errors && errors.start}
                        autoOk
                        fullWidth
                        clearable
                        variant="inline"
                        inputVariant="outlined"
                        label="Tarih"
                        size='small'
                        format="dd.MM.yyyy"
                        minDate={filter.baslangicTarih || undefined}
                        InputAdornmentProps={{ position: 'start' }}
                        value={filter.bitisTarih || null}
                        onChange={(e) =>
                            dispatch(Actions.setFilter({ ...filter, bitisTarih: e }))
                            //setForm(prev => { return { ...prev, bitisTarih: e } })
                        }
                    />
                </MuiPickersUtilsProvider>
            </div>
            <div className='w-1/6 flex space-x-2 justify-end'>
                <Button variant='outlined' color='primary' size='small' onClick={() => {
                    dispatch(Actions.setFilter({
                        baslangicTarih: null,
                        bitisTarih: null,
                        sonuc: '',
                        sorumlu: '',
                        firma: ''
                    }))
                    //firmaRef.current.clear()
                }
                }>Temizle</Button>
                <Button variant='contained' color='primary' size='small' onClick={() => onQuery({})}>Sorgula</Button>
            </div>
        </div>
    )
}


const Gorevler = (props) => {
    const dispatch = useDispatch()
    const permissions = useSelector(state => state.auth.permissions)
    const token = useSelector(state => state.auth.expertoToken)
    const user = useSelector(state => state.auth.user)
    const ekip = useSelector(state => state.auth.ekip)
    const users = useSelector(state => state.auth.users)
    const [tableFilter, setTableFilter] = useState(null)
    const [queryFilter, setQueryFilter] = useState(null) // Sorguda bazi query parametreleri filter altinda gonderilemiyor
    //const [page, setPage] = useState(0)
    const tableRef = React.createRef();
    const filterRef = React.createRef();
    const [pageLoad, setPageLoad] = useState(false)
    const [page, setPage] = useState(0)
    const [usePrevPage, setUsePrevPage] = useState(false)
    const [resetPage, setResetPage] = useState(false)
    const prevUrl = useSelector(state => state.common.prevUrl)
    const prevPage = useSelector(state => state.common.prevPage)
    const gFilter = useSelector(state => state.common.filter)
    const pageHeaderBack = useSelector(state => state.common.pageHeaderBack)
    const [sort, setSort] = useState(null)
    const [anchorEl, setAnchorEl] = useState(null)
    const [seciliKayit, setSeciliKayit] = useState(null)
    const [table, setTable] = useState(null)
    const [showGorevBasarisiz, setShowGorevBasarisiz] = useState(false)
    const [sonucAciklama, setSonucAciklama] = useState('')



    const convertFilterToQuery = (filter) => {
        let tf = {
            vfirma: filter.firma?.key || undefined,
            sonuc: filter.sonuc || undefined,
            vsorumlu: filter.sorumlu || undefined, //? filter.sorumlu.key : undefined
            icGorevMi: filter.icGorev
        }
        let qf = {}

        if (filter.sonuc == 'hepsi') {
            delete tf.sonuc
        }
        else if (filter.sonuc == 'gecbasarili') {
            tf.sonuc = 'basarili'
            tf.filter = {
                gecikmeGun: {
                    $gt: 0
                }
            }
        }
        else if (filter.sonuc == 'gecikme') {
            tf.sonuc = 'acik'
            tf.filter = {
                sonTarih: {
                    $lt: moment().startOf('day').toISOString()
                }
            }
        }
        else if (filter.sonuc == 'onayBekleniyor') {
            tf.sonuc = undefined
            tf.onayBekleniyor = true
        }

        if (filter.icGorev) {
            tf.vfirma = undefined
            tf.icGorevMi = filter.icGorev
        }
        else {
            tf.icGorevMi = filter.icGorev
        }

        //if (filter.icGorev) {
        //    tf.filter = {
        //        vfirma: {
        //            $exists: true,
        //            $eq: null
        //        }
        //    }
        //}
        //else {
        //    tf.filter = {
        //        vfirma: {
        //            $exists: true,
        //            $eq: filter.firma?.key || ''
        //        }
        //    }
        //}

        if (filter.baslangicTarih && !filter.bitisTarih) {
            tf.filter = {
                sonTarih: {
                    $gte: filter.baslangicTarih
                }
            }
        }
        else if (!filter.baslangicTarih && filter.bitisTarih) {
            tf.filter = {
                sonTarih: {
                    $lte: filter.bitisTarih
                }
            }
        }
        else if (filter.baslangicTarih && filter.bitisTarih) {
            tf.filter = {
                sonTarih: {
                    $gte: filter.baslangicTarih,
                    $lte: filter.bitisTarih
                }
            }
        }

        if (filter.ekip || filter.sorumlu == '') {
            tf.vsorumlu = ekip.join(',')
        }

        if (filter.createdBy) {
            tf.vcreatedBy = filter.createdBy
            delete tf.vsorumlu
        }

        
        //setTableFilter(tf)
        //setQueryFilter(qf)

        return tf
    }

    const convertRouterToQuery = (rq) => {
        let f = {}
        let filter = {}
        setSort(null)
        if (rq == 'acikgorevlerim') {
            f = convertFilterToQuery({
                sonuc: 'acik',
                sorumlu: user.key,
            })
            filter = {
                sonuc: 'acik',
                sorumlu: user.key,
                baslangicTarih: null,
                bitisTarih: null
            }
        }
        else if (rq == 'gecikengorevlerim') {
            f = convertFilterToQuery({
                sonuc: 'gecikme',
                sorumlu: user.key,
            })
            filter = {
                sonuc: 'gecikme',
                sorumlu: user.key,
                baslangicTarih: null,
                bitisTarih: null
            }
        }
        else if (rq == 'tumgorevlerim') {
            f = convertFilterToQuery({
                //sonuc: 'gecikme',
                sorumlu: user.key,
            })
            filter = {
                //sonuc: 'gecikme',
                sorumlu: user.key,
                baslangicTarih: null,
                bitisTarih: null,
            }
            setSort('-sonTarih')
        }
        else if (rq == 'ekibimingorevleri') {
            f = convertFilterToQuery({
                sonuc: 'acik',
                ekip: true
                //sorumlu: user.key,
            })
            filter = {
                sonuc: 'acik',
                ekip: true,
                //sorumlu: user.key,
                baslangicTarih: null,
                bitisTarih: null
            }
        }
        else if (rq == 'olusturdugumgorevler') {
            f = convertFilterToQuery({
                sonuc: 'acik',
                ekip: true,
                createdBy: user.key
                //sorumlu: user.key,
            })
            filter = {
                sonuc: 'acik',
                ekip: true,
                //sorumlu: user.key,
                baslangicTarih: null,
                bitisTarih: null
            }
        }
        dispatch(Actions.setFilter(filter))
        return f
    }

    useEffect(() => {
        if (prevUrl == '/gorev/[id]') {
            setUsePrevPage(true)
            if (pageHeaderBack) {
                setTableFilter(convertFilterToQuery(gFilter))
                //convertFilterToQuery(gFilter)
            }
            else {
                setTableFilter(convertRouterToQuery(props.router.query.filter))
                //convertRouterToQuery(props.router.query.filter)
            }
        }
        else {
            setTableFilter(convertRouterToQuery(props.router.query.filter))
            //convertRouterToQuery(props.router.query.filter)
            setResetPage(true)
            setUsePrevPage(false)
        }
        dispatch(Actions.resetPageHeaderBack())
        dispatch(Actions.setPrevUrl(props.router.route))
        //return () => {
        //    dispatch(Actions.setPrevUrl(props.router.route))
        //}
    }, [props.router.query])

    useEffect(() => {
        if (tableRef.current) {
            tableRef.current.onQueryChange()
        }
    }, [tableFilter])

    useEffect(() => {
        setTable(tableRef.current)
    }, [tableRef.current])

    return (
        <div>
            <PageHeader
                title='Görevler'
                buttons={[
                    {
                        title: 'Yeni',
                        disabled: !permissions ? true : !permissions.find(p => p.key == 'gorev')?.create,
                        onClick: (e) => {
                            e.preventDefault();
                            props.router.push("/gorev/[id]", `/gorev/new`);
                        }
                    },
                ]} />
            <div className='p-2 flex-col space-y-2'>
                <GorevFilter
                    onQuery={() => {
                        setResetPage(true)
                        setUsePrevPage(false)
                        setTableFilter(convertFilterToQuery(gFilter))
                    }} />
                <MaterialTable
                    title='Görevler'
                    tableRef={tableRef}
                    columns={[
                        { title: 'Görev', field: 'baslik' },
                        {
                            title: 'Firma', field: 'firma.marka', render: row => {
                                if (row.firma == null) {
                                    return (<div className='text-red-600 font-light'>
                                        İç Görev
                                    </div>)
                                }
                                else {
                                    return <h2>{row.firma.marka}</h2>
                                }
                            }
                        },
                        { title: 'Son Tarih', field: 'sonTarih', render: row => moment(row.sonTarih).format('DD.MM.YYYY') },
                        {
                            title: 'Durum', field: 'sonuc', render: row => {
                                if (row.sonuc == 'acik') {
                                    if (moment(row.sonTarih).startOf('day').isBefore(moment().startOf('day'))) {
                                        return (
                                            <div className=''>
                                                <div className='flex items-center'>
                                                    <div className='bg-yellow-500 w-32 text-center p-1 rounded shadow text-white'>GECİKEN</div>
                                                    <IconButton size='small' className='focus:outline-none' onClick={(e) => {
                                                        setSeciliKayit(row)
                                                        setAnchorEl(e.currentTarget)
                                                    }}>
                                                        <DoubleArrowIcon />
                                                    </IconButton>
                                                    {row.onayBekleniyor && <Tooltip title='Onay Bekleniyor'><HourglassEmptyIcon /></Tooltip> }
                                                </div>
                                                <h2 className='text-gray-800 text-sm font-light'>{moment(row.sonTarih).fromNow()}</h2>
                                            </div>
                                        )
                                    }
                                    else if (moment(row.sonTarih).startOf('day').isSame(moment().startOf('day'))) {
                                        //return <div className='bg-green-500 w-32 text-center p-1 rounded shadow text-white font-semibold'>BUGÜN SON</div>
                                        return (
                                            <div className='flex items-center'>
                                                <div className='bg-green-500 w-32 text-center p-1 rounded shadow text-white'>BUGÜN SON</div>
                                                <IconButton size='small' className='focus:outline-none' onClick={(e) => {
                                                    setSeciliKayit(row)
                                                    setAnchorEl(e.currentTarget)
                                                }}>
                                                    <DoubleArrowIcon />
                                                </IconButton>
                                                {row.onayBekleniyor && <Tooltip title='Onay Bekleniyor'><HourglassEmptyIcon /></Tooltip> }
                                            </div>
                                        )
                                    }
                                    else {
                                        return (
                                            <div className='flex items-center'>
                                                <div className='bg-blue-500 w-32 text-center p-1 rounded shadow text-white'>ACIK</div>
                                                <IconButton size='small' className='focus:outline-none' onClick={(e) => {
                                                    setSeciliKayit(row)
                                                    setAnchorEl(e.currentTarget)
                                                }}>
                                                    <DoubleArrowIcon />
                                                </IconButton>
                                                {row.onayBekleniyor && <Tooltip title='Onay Bekleniyor'><HourglassEmptyIcon /></Tooltip> }
                                            </div>
                                        )
                                    }
                                }
                                else if (row.sonuc == 'basarili') {
                                    if (row.gecikmeGun > 0) {
                                        return <div className='flex space-x-2 items-center'>
                                            <div className='bg-green-500 w-32 text-center p-1 rounded shadow text-white font-semibold'>BASARILI</div>
                                            <div className='bg-red-500 p-1 w-20 rounded shadow font-semibold text-white text-center'>{row.gecikmeGun} gün</div>
                                        </div>
                                    }
                                    else {
                                        return <div className='bg-green-500 w-32 text-center p-1 rounded shadow text-white font-semibold'>BASARILI</div>
                                    }
                                }
                                else if (row.sonuc == 'basarisiz') {
                                    return <div className='bg-red-500 w-32 text-center p-1 rounded shadow text-white'>BASARISIZ</div>
                                }
                                else if (row.sonuc == 'iptal') {
                                    return <div className='bg-gray-500 w-32 text-center p-1 rounded shadow text-white'>IPTAL</div>
                                }
                            }
                        },
                        {
                            title: 'Sorumlu', field: 'sorumlu', render: row => {
                                if (row.sorumlu) {
                                    let _user = users.find(u => u.key == row.sorumlu.key)
                                    return (
                                        <div className='flex items-center space-x-2'>
                                            <Avatar className='h-4 w-4 border-2 border-gray-400 shadow' src={_user?.image} />
                                            <h2 className=''>{row.sorumlu.username}</h2>
                                        </div>
                                    )
                                }
                                return (<div></div>)
                            }
                        }
                    ]}
                    actions={[
                        {
                            icon: () => <ArrowForwardIcon />,
                            tooltip: 'Detay',
                            onClick: (event, rowData) => {
                                dispatch(Actions.setPrevPage(page))
                                props.router.push(`/gorev/${rowData._id}`)
                            }
                        }
                    ]}
                    options={{
                        padding: 'dense',
                        pageSize: 10,
                        pageSizeOptions: [10, 25, 50, 100],
                    }}
                    localization={{
                        body: { emptyDataSourceMessage: 'Gösterilecek kayıt yok' },
                        toolbar: { searchPlaceholder: 'Ara' }
                    }}

                    data={query => {
                        return new Promise((resolve, reject) => {
                            let p = query.page
                            if (usePrevPage) {
                                p = prevPage
                            }
                            if (resetPage) {
                                p = 0
                            }
                            let _q = {
                                skip: p * query.pageSize,
                                limit: query.pageSize,
                                populate: 'firma,sorumlu',
                                baslik: `/${query.search}/i`,
                                //filter: tableFilter,
                                sort: sort || 'sonTarih'
                            }
                            if (query.orderBy) {
                                _q.sort = query.orderBy ? `${query.orderDirection == 'asc' ? '' : '-'}${query.orderBy.field}` : ''
                            }
                            let q = qs.stringify({ ..._q, ...tableFilter }
                                //, { skipNulls: true }
                            )
                            api.get(`/gorev?${q}`,
                                { headers: { Authorization: `Bearer ${token}` } })
                                .then(result => {
                                    if (!result.data.data) {
                                        reject()
                                    }
                                    setPage(p)
                                    setResetPage(false)
                                    setPageLoad(false)
                                    setUsePrevPage(false)
                                    resolve({
                                        data: result.data.data.data,
                                        page: result.data.data.page / query.pageSize,
                                        totalCount: result.data.data.total,
                                    })
                                })
                                .catch(e => alert(JSON.stringify(e)))
                        })
                    }}
                />
            </div>

            <div>
                <Menu
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={() => { setAnchorEl(null) }}
                >
                    <MenuItem onClick={() => {
                        gorevTamamla(seciliKayit._id, {
                            sonuc: 'basarili',
                            vkapatan: user.key,
                            kapatmaTarihi: new Date()
                        })
                            .then(resp => {
                                if (table) {
                                    table.onQueryChange()
                                }
                            })
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <DoneIcon />
                            <h2>Görevi Başarılı Tamamla</h2>
                        </div>
                    </MenuItem>
                    <MenuItem onClick={() => {
                        setShowGorevBasarisiz(true)
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <ThumbDownAltOutlinedIcon />
                            <h2>Görevi Başarısız Tamamla</h2>
                        </div>
                    </MenuItem>
                    <MenuItem onClick={() => {
                        gorevTamamla(seciliKayit._id, {
                            sonuc: 'iptal',
                            vkapatan: user.key,
                            kapatmaTarihi: new Date()
                        })
                            .then(resp => {
                                if (table) {
                                    table.onQueryChange()
                                }
                            })
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <CloseIcon />
                            <h2>Görevi İptal Et</h2>
                        </div>
                    </MenuItem>
                </Menu>

                <Dialog
                    open={showGorevBasarisiz}
                    onClose={() => setShowGorevBasarisiz(false)}
                >
                    <DialogTitle>Görevi Başarısız Kapatma</DialogTitle>
                    <DialogContent>
                        <DialogContentText>Görevin başarısızlık nedenini açıklayın</DialogContentText>
                        <TextField
                            autoFocus
                            label='Başarısızlık Nedeni'
                            variant='outlined'
                            fullWidth
                            rows={3}
                            rowsMax={Infinity}
                            multiline
                            value={sonucAciklama}
                            onChange={(e) => {
                                setSonucAciklama(e.target.value)
                            }}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button variant='contained' color='primary' onClick={() => {
                            setSonucAciklama('')
                            setShowGorevBasarisiz(false)
                        }}>Vazgeç</Button>
                        <Button variant='contained' color='primary' onClick={() => {
                            gorevTamamla(seciliKayit._id, {
                                sonuc: 'basarisiz',
                                sonucAciklama: sonucAciklama,
                                vkapatan: user.key,
                                kapatmaTarihi: new Date()
                            })
                                .then(resp => {
                                    if (table) {
                                        table.onQueryChange()
                                    }
                                })
                            setAnchorEl(null)
                            setSonucAciklama('')
                            setShowGorevBasarisiz(false)
                        }}>Tamam</Button>
                    </DialogActions>
                </Dialog>

            </div>


        </div>
    )
}

export default withRouter(Gorevler)
