import React, { useEffect, useState } from 'react'
import PageHeader from "../../components/pageHeader";
import { Grid, Badge, Button, ButtonGroup, Typography, Drawer, makeStyles } from '@material-ui/core';
import GorevInfo from '../../components/gorev/GorevInfo'
import api from '../../services/axios.service'
import { useRouter, withRouter } from "next/router";
import GorevHistory from '../../components/gorev/GorevHistory'
import GorevSorumlu from '../../components/gorev/GorevSorumlu';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import GorevTamamlama from '../../components/gorev/GorevTamamlama';
import { parseCookies, setCookie, destroyCookie } from 'nookies'
import { useDispatch, useSelector } from 'react-redux'
import GorevSonucDurum from '../../components/gorev/GorevSonucDurumu';
import { getGorev } from '../../services/gorev.service'
const { uuid } = require('uuidv4');
import { validateGorev } from '../../validators/gorev.validator'
import { searchFirmalar } from '../../services/firma.service'
import { gorevTamamla } from '../../services/gorev.service'
import GorevYorum from '../../components/gorev/gorevYorum'
import * as Actions from '../../redux/actions'
import { getOnayForItem } from '../../services/onay.service';

const options = ['Başarılı Tamamlandı', 'Başarısız Tamamlandı', 'İptal'];

function SplitButton() {
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);
    const [selectedIndex, setSelectedIndex] = React.useState(0);

    const handleClick = () => {
        console.info(`You clicked ${options[selectedIndex]}`);
    };

    const handleMenuItemClick = (event, index) => {
        setSelectedIndex(index);
        setOpen(false);
    };

    const handleToggle = () => {
        setOpen(prevOpen => !prevOpen);
    };

    const handleClose = event => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }

        setOpen(false);
    };

    return (
        <>
            <ButtonGroup variant="contained" color="primary" ref={anchorRef} aria-label="split button">
                <Button onClick={handleClick}>{options[selectedIndex]}</Button>
                <Button
                    color="primary"
                    size="small"
                    aria-controls={open ? 'split-button-menu' : undefined}
                    aria-expanded={open ? 'true' : undefined}
                    aria-label="select merge strategy"
                    aria-haspopup="menu"
                    onClick={handleToggle}
                >
                    <ArrowDropDownIcon />
                </Button>
            </ButtonGroup>
            <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                {({ TransitionProps, placement }) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                        }}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                                <MenuList id="split-button-menu">
                                    {options.map((option, index) => (
                                        <MenuItem
                                            key={option}
                                            //disabled={index === 2}
                                            selected={index === selectedIndex}
                                            onClick={event => handleMenuItemClick(event, index)}
                                        >
                                            {option}
                                        </MenuItem>
                                    ))}
                                </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </>
    );
}


const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(2)
    },
    gorevTamamlama: {
        marginTop: 60,
        //marginLeft: 10,
        //marginRight: 10,
        //flexGrow: 1,
    },
    drawer: {
        width: '15%',
        flexShrink: 0
    }
}));

const Gorev = props => {
    const dispatch = useDispatch()
    const {router} = props; //useRouter();
    const classes = useStyles()
    const user = useSelector(state => state.auth.user)
    const { newGorev, gorev, onayTalepleri } = props
    const [form, setForm] = useState(
        {
            ...gorev,
            //vcreatedBy: user.key,
            //createdAt: new Date()
        } || {
            //vcreatedBy: user.key,
            //createdAt: new Date()
        })
    const [openDrawer, setOpenDrawer] = useState(false)
    const anchorRef = React.useRef(null);
    const [gorevKapatmaBilgileri, setGorevKapatmaBilgileri] = useState({})
    const [errors, setErrors] = useState(null)

    const onGorevChanged = async (value) => {
        let f = { ...form }
        f[value.field] = value.value
        setForm(f)
    }

    const onGorevKapatmaChange = async (value) => {
        let kapatmaBilgi = { ...gorevKapatmaBilgileri }
        kapatmaBilgi[value.field] = value.value
        kapatmaBilgi.vkapatan = user.key
        kapatmaBilgi.kapatmaTarihi = new Date()
        setGorevKapatmaBilgileri(kapatmaBilgi)
    }

    const save = async () => {
        const cookies = parseCookies()
        if (newGorev) {
            api.post(`/gorev`, form, {
                headers: { Authorization: `Bearer ${cookies.experto}` }
            }).then(resp => {
                if (resp.status == 200) {
                    router.back()
                }
            })
        }
        else {
            // gorev kapatma durumu varsa eklenir
            let f = { ...form, ...gorevKapatmaBilgileri }

            api.put(`/gorev/${gorev._id}`, f, {
                headers: { Authorization: `Bearer ${cookies.experto}` }
            }).then(resp => {
                if (resp.status == 200) {
                    router.back()
                }
            })
        }
    }

    useEffect(() => {
        return () => {
            dispatch(Actions.setPrevUrl(router.route))
        }
    }, [])

    return <>
        <Drawer
            className={classes.drawer}
            anchor={'right'}
            open={openDrawer}
            onClose={() => setOpenDrawer(false)}
        >
            <div className={classes.gorevTamamlama}>
                <GorevTamamlama
                    onChange={onGorevKapatmaChange}
                    //onChange={onGorevChanged}
                    onCancel={() => {
                        setGorevKapatmaBilgileri({})
                        setOpenDrawer(false)
                    }}
                    onOk={(secim) => {
                        gorevTamamla(gorev._id, gorevKapatmaBilgileri)
                        setOpenDrawer(false)
                    }}
                />
            </div>
        </Drawer>
        <PageHeader
            title={newGorev ? 'Yeni Görev' : 'Görev'}
            buttons={[
                !newGorev && <Button variant='outlined' color='secondary' disabled={gorev.sonuc != 'acik'} onClick={() => setOpenDrawer(true)}>Tamamla</Button>,
                {
                    title: newGorev ? 'Kaydet' : 'Güncelle',
                    disabled: gorev.sonuc != 'acik',
                    onClick: (e) => {
                        e.preventDefault();
                        let _errors = validateGorev(form)
                        if (_errors) {
                            setErrors(_errors)
                        }
                        else {
                            save()
                        }
                        //router.push("/gorev");
                    }
                },
            ]} />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12} lg={8} container spacing={2}>
                    <Grid item xs={12}>
                        <GorevInfo
                            errors={errors}
                            onayTalepleri={onayTalepleri}
                            //gorev={props.gorev}
                            gorev={form}
                            musteriler={props.musteriler}
                            onGorevChanged={onGorevChanged}
                            //router = {props.router}
                        />
                    </Grid>
                    {!newGorev &&
                        <Grid item xs={12}>
                            <GorevYorum gorev={gorev} />
                        </Grid>
                    }
                </Grid>
                <Grid
                    item
                    xs={12} lg={4}
                    container
                    spacing={2}
                    direction="column"
                    justify="flex-start"
                    alignItems="stretch">

                    {gorev.sonuc != 'acik' &&
                        <Grid item >
                            <GorevSonucDurum gorev={gorev} />
                        </Grid>
                    }

                    <Grid item>
                        <GorevSorumlu
                            onChange={onGorevChanged}
                            sorumlu={gorev.vsorumlu}
                            error={errors}
                        />
                    </Grid>
                    {!newGorev &&
                        <Grid item>
                            <GorevHistory gorev={gorev} />
                        </Grid>
                    }
                </Grid>
            </Grid>
        </div>
    </>
}


Gorev.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)

    let newGorev = ctx.query.id == 'new'
    let firma = null
    if (ctx.query.firma) {
        let firmalar = await searchFirmalar(`key=${ctx.query.firma}`)
        if (firmalar.length > 0) {
            firma = firmalar[0]
        }
    }
    let gorev = {
        key: uuid(),
        sonTarih: null,
        sonuc: 'acik',
        oncelik: 'Orta',
        firma: firma || null
    };

    let onayTalepleri = {data:[]}

    if (!newGorev) {
        gorev = await getGorev(ctx.query.id)
        onayTalepleri = await getOnayForItem(ctx.query.id)
    }

    return {
        newGorev,
        gorev,
        onayTalepleri : onayTalepleri.data.length > 0 ? onayTalepleri.data : undefined
    }
}


export default withRouter(Gorev)