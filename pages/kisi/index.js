import React, { useState, useRef, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'
import { Button, Box, IconButton, TextField, MenuItem, Avatar, Menu, Dialog, DialogTitle, DialogContent, DialogActions, DialogContentText } from '@material-ui/core'
import { withRouter } from 'next/router';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import RemoteAutoComplete from '../../components/autocomplete';
import { searchFirmalar } from '../../services/firma.service'
import trLocale from "date-fns/locale/tr";
import PageHeader from '../../components/pageHeader';
import MaterialTable from 'material-table';
import api from '../../services/axios.service';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import * as Actions from '../../redux/actions'
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import ThumbDownAltOutlinedIcon from '@material-ui/icons/ThumbDownAltOutlined';
import { gorevTamamla } from '../../services/gorev.service'
var qs = require('qs');
moment.locale('tr')

const KisiFilter = props => {
    const { onQuery, filterRef } = props
    const ekip = useSelector(state => state.auth.ekip)
    const users = useSelector(state => state.auth.users)
    const user = useSelector(state => state.auth.user)
    const filter = useSelector(state => state.common.filter)
    const dispatch = useDispatch()
    const [firmalar, setFirmalar] = useState([])

    return (
        <div className='bg-white p-2 rounded shadow flex space-x-2 justify-between'>
            <div className='flex flex-grow space-x-2'>
                <div className='w-1/6'>
                    <RemoteAutoComplete
                        results={firmalar}
                        size='small'
                        label='Firma'
                        labelField='marka'
                        value={filter.firma || ''}
                        onSelected={e => {
                            dispatch(Actions.setFilter({ ...filter, firma: e }))
                        }}
                        fetchRemote={(searchText) => {
                            searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                                setFirmalar(resp)
                            })
                        }}
                    />
                </div>
                <div className='w-1/6'>
                    <TextField
                        label='Aktif'
                        fullWidth
                        variant='outlined'
                        size='small'
                        select
                        value={filter.active}
                        onChange={e => {
                            dispatch(Actions.setFilter({ ...filter, active: e.target.value }))
                        }}
                    >
                        <MenuItem key={'hepsi'} value={'hepsi'}>Hepsi</MenuItem>
                        <MenuItem key={'calisiyor'} value={'calisiyor'}>Çalışıyor</MenuItem>
                        <MenuItem key={'calismiyor'} value={'calismiyor'}>Çalışmıyor</MenuItem>
                    </TextField>
                </div>
            </div>
            <div className='w-1/6 flex space-x-2 justify-end'>
                <Button variant='outlined' color='primary' size='small' onClick={() => {
                    dispatch(Actions.setFilter({
                        active: 'calisiyor',
                        firma: ''
                    }))
                    //firmaRef.current.clear()
                }
                }>Temizle</Button>
                <Button variant='contained' color='primary' size='small' onClick={() => onQuery({})}>Sorgula</Button>
            </div>
        </div>
    )
}


const Kisiler = (props) => {
    const dispatch = useDispatch()
    const permissions = useSelector(state => state.auth.permissions)
    const token = useSelector(state => state.auth.expertoToken)
    const user = useSelector(state => state.auth.user)
    const ekip = useSelector(state => state.auth.ekip)
    const users = useSelector(state => state.auth.users)
    const [tableFilter, setTableFilter] = useState(null)
    const [queryFilter, setQueryFilter] = useState(null) // Sorguda bazi query parametreleri filter altinda gonderilemiyor
    //const [page, setPage] = useState(0)
    const tableRef = React.createRef();
    const filterRef = React.createRef();
    const [pageLoad, setPageLoad] = useState(false)
    const [page, setPage] = useState(0)
    const [usePrevPage, setUsePrevPage] = useState(false)
    const [resetPage, setResetPage] = useState(false)
    const prevUrl = useSelector(state => state.common.prevUrl)
    const prevPage = useSelector(state => state.common.prevPage)
    const gFilter = useSelector(state => state.common.filter)
    const pageHeaderBack = useSelector(state => state.common.pageHeaderBack)
    const [sort, setSort] = useState(null)
    const [anchorEl, setAnchorEl] = useState(null)
    const [seciliKayit, setSeciliKayit] = useState(null)
    const [table, setTable] = useState(null)
    const [showGorevBasarisiz, setShowGorevBasarisiz] = useState(false)
    const [sonucAciklama, setSonucAciklama] = useState('')



    const convertFilterToQuery = (filter) => {
        let tf = {
            vfirma: filter.firma?.key || undefined,
        }
        let qf = {}

        if (filter.active == 'hepsi') {
            delete tf.active
        }
        else if (filter.active == 'calisiyor') {
            tf.active = true
        }
        else if (filter.active == 'calismiyor') {
            tf.active = false
        }
        return tf
    }

    const convertRouterToQuery = (rq) => {
        let f = {}
        let filter = {}
        setSort(null)
        //if (rq == 'acikgorevlerim') {
            f = convertFilterToQuery({
                active: 'calisiyor',
            })
            filter = {
                active: 'calisiyor',
            }
        //}
        dispatch(Actions.setFilter(filter))
        return f
    }

    useEffect(() => {
        if (prevUrl == '/kisi/[id]') {
            setUsePrevPage(true)
            if (pageHeaderBack) {
                setTableFilter(convertFilterToQuery(gFilter))
            }
            else {
                setTableFilter(convertRouterToQuery(props.router.query.filter))
            }
        }
        else {
            setTableFilter(convertRouterToQuery(props.router.query.filter))
            setResetPage(true)
            setUsePrevPage(false)
        }
        dispatch(Actions.resetPageHeaderBack())
        dispatch(Actions.setPrevUrl(props.router.route))
    }, [props.router.query])

    useEffect(() => {
        if (tableRef.current) {
            tableRef.current.onQueryChange()
        }
    }, [tableFilter])

    useEffect(() => {
        setTable(tableRef.current)
    }, [tableRef.current])

    return (
        <div>
            <PageHeader
                title='Firma Kişileri'
                buttons={[
                    {
                        title: 'Yeni',
                        disabled: !permissions ? true : !permissions.find(p => p.key == 'kisi')?.create,
                        onClick: (e) => {
                            e.preventDefault();
                            props.router.push("/kisi/[id]", `/kisi/new`);
                        }
                    },
                ]} />
            <div className='p-2 flex-col space-y-2'>
                <KisiFilter
                    onQuery={() => {
                        setResetPage(true)
                        setUsePrevPage(false)
                        setTableFilter(convertFilterToQuery(gFilter))
                    }} />
                <MaterialTable
                    title='Kişiler'
                    tableRef={tableRef}
                    columns={[
                        { title: 'Adı Soyadi', field: 'ad', render: row => { return `${row.adi} ${row.soyadi}` } },
                        { title: 'Ünvanı', field: 'unvani' },
                        { title: 'Mail', field: 'mail' },
                        { title: 'Cep Telefon', field: 'cepTelefon' },
                        { title: 'İş Telefon', field: 'isTelefon' },
                        {
                            title: 'Firma', field: 'firma.marka', render: row => {
                                if (row.firma == null) {
                                    return (<div className='text-red-600 font-light'>
                                    </div>)
                                }
                                else {
                                    return <h2>{row.firma.marka}</h2>
                                }
                            }
                        },
                        { title: 'Çalışıyor', field: 'active', render: row => {
                            if (row.active) {
                                return (
                                    <div className=''>Çalışıyor</div>
                                )
                            }
                            else {
                                return (
                                    <div className='text-red-500'>Çalışmıyor</div>
                                )
                            }
                        } },
                    ]}
                    actions={[
                        {
                            icon: () => <ArrowForwardIcon />,
                            tooltip: 'Detay',
                            onClick: (event, rowData) => {
                                dispatch(Actions.setPrevPage(page))
                                props.router.push(`/kisi/${rowData._id}`)
                            }
                        }
                    ]}
                    options={{
                        padding: 'dense',
                        pageSize: 10,
                        pageSizeOptions: [10, 25, 50, 100],
                    }}
                    localization={{
                        body: { emptyDataSourceMessage: 'Gösterilecek kayıt yok' },
                        toolbar: { searchPlaceholder: 'Ara' }
                    }}

                    data={query => {
                        return new Promise((resolve, reject) => {
                            let p = query.page
                            if (usePrevPage) {
                                p = prevPage
                            }
                            if (resetPage) {
                                p = 0
                            }
                            let _q = {
                                skip: p * query.pageSize,
                                limit: query.pageSize,
                                populate: 'firma,sorumlu',
                                //baslik: `/${query.search}/i`,
                                //filter: tableFilter,
                                sort: sort || 'adi'
                            }
                            if (query.orderBy) {
                                _q.sort = query.orderBy ? `${query.orderDirection == 'asc' ? '' : '-'}${query.orderBy.field}` : ''
                            }
                            let q = qs.stringify({ ..._q, ...tableFilter }
                                //, { skipNulls: true }
                            )
                            api.get(`/kisi?${q}`,
                                { headers: { Authorization: `Bearer ${token}` } })
                                .then(result => {
                                    if (!result.data.data) {
                                        reject()
                                    }
                                    setPage(p)
                                    setResetPage(false)
                                    setPageLoad(false)
                                    setUsePrevPage(false)
                                    resolve({
                                        data: result.data.data.data,
                                        page: result.data.data.page / query.pageSize,
                                        totalCount: result.data.data.total,
                                    })
                                })
                                .catch(e => alert(JSON.stringify(e)))
                        })
                    }}
                />
            </div>

            <div>
                <Menu
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={() => { setAnchorEl(null) }}
                >
                    <MenuItem onClick={() => {
                        gorevTamamla(seciliKayit._id, {
                            sonuc: 'basarili',
                            vkapatan: user.key,
                            kapatmaTarihi: new Date()
                        })
                            .then(resp => {
                                if (table) {
                                    table.onQueryChange()
                                }
                            })
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <DoneIcon />
                            <h2>Görevi Başarılı Tamamla</h2>
                        </div>
                    </MenuItem>
                    <MenuItem onClick={() => {
                        setShowGorevBasarisiz(true)
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <ThumbDownAltOutlinedIcon />
                            <h2>Görevi Başarısız Tamamla</h2>
                        </div>
                    </MenuItem>
                    <MenuItem onClick={() => {
                        gorevTamamla(seciliKayit._id, {
                            sonuc: 'iptal',
                            vkapatan: user.key,
                            kapatmaTarihi: new Date()
                        })
                            .then(resp => {
                                if (table) {
                                    table.onQueryChange()
                                }
                            })
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <CloseIcon />
                            <h2>Görevi İptal Et</h2>
                        </div>
                    </MenuItem>
                </Menu>

                <Dialog
                    open={showGorevBasarisiz}
                    onClose={() => setShowGorevBasarisiz(false)}
                >
                    <DialogTitle>Görevi Başarısız Kapatma</DialogTitle>
                    <DialogContent>
                        <DialogContentText>Görevin başarısızlık nedenini açıklayın</DialogContentText>
                        <TextField
                            autoFocus
                            label='Başarısızlık Nedeni'
                            variant='outlined'
                            fullWidth
                            rows={3}
                            rowsMax={Infinity}
                            multiline
                            value={sonucAciklama}
                            onChange={(e) => {
                                setSonucAciklama(e.target.value)
                            }}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button variant='contained' color='primary' onClick={() => {
                            setSonucAciklama('')
                            setShowGorevBasarisiz(false)
                        }}>Vazgeç</Button>
                        <Button variant='contained' color='primary' onClick={() => {
                            gorevTamamla(seciliKayit._id, {
                                sonuc: 'basarisiz',
                                sonucAciklama: sonucAciklama,
                                vkapatan: user.key,
                                kapatmaTarihi: new Date()
                            })
                                .then(resp => {
                                    if (table) {
                                        table.onQueryChange()
                                    }
                                })
                            setAnchorEl(null)
                            setSonucAciklama('')
                            setShowGorevBasarisiz(false)
                        }}>Tamam</Button>
                    </DialogActions>
                </Dialog>

            </div>


        </div>
    )
}

export default withRouter(Kisiler)
