import React, { useState } from 'react'
import PageHeader from '../../components/pageHeader'
import { Button, makeStyles, Grid } from '@material-ui/core'
import KisiInfo from '../../components/kisi/KisiInfo'
import { uuid } from 'uuidv4'
import { getKisi, saveKisi, updateKisi } from '../../services/kisi.service'
import { useRouter } from 'next/router'
import { validateKisi } from '../../validators/kisi.validator'
import { getFirma, searchFirmalar } from '../../services/firma.service'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const Kisi = props => {
    const { newKisi } = props
    const router = useRouter()
    const [form, setForm] = useState(props.kisi)
    const classes = useStyles()
    const [errors, setErrors] = useState(null)

    return <>
        <PageHeader
            title='Kişi'
            buttons={[
                <Button variant='contained' color='primary'
                onClick={() => {
                    let _error = validateKisi(form)
                    if (_error) {
                        setErrors(_error)
                        return
                    }
                    if (newKisi) {
                        saveKisi(form)
                    }
                    else {
                        updateKisi(form._id, form)
                    }
                    router.back()
                }}
                >Kaydet</Button>
            ]}
        />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={8}>
                    <KisiInfo
                        errors={errors}
                        kisi={form}
                        onChange={(value) => {
                            let f = { ...form }
                            f[value.field] = value.value
                            setForm(f)
                        }} />
                </Grid>
            </Grid>
        </div>
    </>
}

Kisi.getInitialProps = async (ctx) => {
    let newKisi = ctx.query.id == 'new'
    let firma = null
    if (ctx.query.firma) {
        let firmalar = await searchFirmalar(`key=${ctx.query.firma}`)
        if (firmalar.length > 0) {
            firma = firmalar[0]
        }
    }
    let kisi = {
        key         : uuid(),
        vfirma      : ctx.query.firma || '',
        adi         : '',
        soyadi      : '',
        unvani      : '',
        mail        : '',
        cinsiyet    : 'ERKEK',
        cepTelefon  : '',
        isTelefon   : '',
        dahili      : '',
        departman   : '',
        dogumGunu   : null,
        active      : true,
        firma       : firma || null
    }

    if (!newKisi) {
        kisi = await getKisi(ctx.query.id)
    }

    return {
        newKisi,
        kisi
    }
}

export default Kisi