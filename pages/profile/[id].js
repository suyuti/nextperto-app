import { Grid, makeStyles } from '@material-ui/core'
import React, {useState} from 'react'
import GorevList from '../../components/gorev/gorevList'
import ToplantiList from '../../components/toplanti/toplantiList'
import PageHeader from '../../components/pageHeader'
import {useRouter} from 'next/router'
import {useSelector} from 'react-redux'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const Profile = props => {
    const classes = useStyles()
    const router = useRouter()
    const users = useSelector(state => state.auth.users)
    const [me, setMe] = useState(users.find(u => u._id == router.query.id))

    return <>
        <PageHeader
            title='Profil'
        />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={3}>
                    <GorevList
                        title='Görevler'
                        sorumlular={me.key}
                        acik
                        //limit={100}
                    />
                </Grid>
                <Grid item xs={3}>
                    <ToplantiList katilimcilar={me.key} acik title='Toplantılar' />
                </Grid>
                <Grid item xs={3}>
                    {/* Sorumlu oldugu musteriler */}
                </Grid>
                <Grid item xs={3}>
                    {/* Islem tarihcesi */}
                </Grid>
            </Grid>
        </div>
    </>
}

export default Profile