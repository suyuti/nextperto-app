import React, { useState, useRef } from 'react'
import PageHeader from '../../components/pageHeader'
import { Card, Grid, CardHeader, Divider, CardContent, makeStyles, TextField, MenuItem, List, ListItem, ListItemText, Popover, InputAdornment, LinearProgress } from '@material-ui/core'
import { searchFirmalar } from '../../services/firma.service'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import RemoteAutoComplete from '../../components/autocomplete';
import { DataGrid } from '@material-ui/data-grid';
import MaterialTable from 'material-table';
import api from '../../services/axios.service';
import { useSelector } from 'react-redux'
import Fatura from '../../components/muhasebe/fatura2'
var qs = require('qs');

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const urunler = [
    {_id: 1, adi: 'Urun 1', birimFiyat: 1, kdvOran: 1},
    {_id: 2, adi: 'Urun 2', birimFiyat: 2, kdvOran: 8},
    {_id: 3, adi: 'Urun 3xZX z csdsdfewf ewf ewf wfwfwddas', birimFiyat: 3, kdvOran: 0},
    {_id: 4, adi: 'Urun 4', birimFiyat: 4, kdvOran: 18},
    {_id: 5, adi: 'Urun 5', birimFiyat: 5, kdvOran: 18},
    {_id: 6, adi: 'Urun 6', birimFiyat: 6, kdvOran: 18},
]
const columns = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'firstName', headerName: 'First name', width: 130 },
    { field: 'lastName', headerName: 'Last name', width: 130 },
    {
        field: 'age',
        headerName: 'Age',
        type: 'number',
        width: 90,
    },
    {
        field: 'fullName',
        headerName: 'Full name',
        description: 'This column has a value getter and is not sortable.',
        sortable: false,
        width: 160,
        valueGetter: (params) =>
            `${params.getValue('firstName') || ''} ${params.getValue('lastName') || ''}`,
    },
];

const rows = [
    { id: 1, lastName: 'Snow', firstName: 'Jon', age: 35 },
    { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
    { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
    { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16 },
    { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null },
    { id: 6, lastName: 'Melisandre', firstName: null, age: 150 },
    { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
    { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36 },
    { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
];

const TestPage = props => {
    const clasess = useStyles()
    const textFieldRef = useRef()
    const [anchorEl, setAnchorEl] = useState(textFieldRef.current)
    const [loading, setLoading] = useState(false)
    const [open, setOpen] = useState(false)
    const [selectedItem, setSelectedItem] = useState(null)
    const [searchText, setSearchText] = useState('')
    const [results, setResults] = useState([
    ])
    const token = useSelector(state => state.auth.expertoToken)

    const fetchData = async () => {
        setLoading(true)
        searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
            setResults(resp)
            setLoading(false)
            setOpen(true)
        })
    }


    return <>
        <PageHeader
            title='Test Page'
        />
        <div className={clasess.root}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Fatura urunler={urunler}/>
                </Grid>
                <Grid item xs={6}>
                    <Card>
                        <CardHeader
                            title='Remote autocomplete'
                        />
                        <Divider />
                        <CardContent>
                            {loading && <LinearProgress />}
                            <TextField
                                variant='outlined'
                                fullWidth
                                ref={textFieldRef}
                                onKeyUp={e => {
                                    if (e.key == 'Enter') {
                                        //setOpen(true)
                                        fetchData()
                                    }
                                }}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <ExpandMoreIcon />
                                        </InputAdornment>
                                    ),
                                }}
                                value={searchText}
                                onChange={(e) => {
                                    setSearchText(e.target.value)
                                }}
                            />
                            <Popover
                                open={open}
                                onClose={() => setOpen(false)}
                                anchorEl={textFieldRef.current}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}                            >
                                <List >
                                    {results.map(r => {
                                        return <ListItem button onClick={(e) => {
                                            setSelectedItem(r)
                                            setSearchText(r.marka)
                                            setOpen(false)
                                        }}>
                                            <ListItemText primary={r.marka} />
                                        </ListItem>
                                    })}
                                </List>

                            </Popover>
                        </CardContent>
                    </Card>

                </Grid>
                <Grid item xs={6}>
                    <RemoteAutoComplete
                        results={results}
                        labelField='unvan'
                        fetchRemote={(searchText) => {
                            searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                                setResults(resp)
                            })
                        }}
                    />
                </Grid>
                <Grid item xs={6}>
                    <p>
                        API URL:
        {JSON.stringify(process.env.HELLO)}
                    </p>
                        Others
      <p>
                        {JSON.stringify(process.env)}
                    </p>

                </Grid>
                <Grid item xs={6}>
                    <Card>
                        <CardHeader title='Handsontable' />
                        <Divider />
                        <CardContent>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12}>
                    <div className='h-screen'>
                        <DataGrid
                            rows={rows}
                            columns={columns}
                            pageSize={5}
                            checkboxSelection
                            filterModel={{
                                items: [
                                    { columnField: 'fullName', operatorValue: 'contains', value: 'rice' },
                                ],
                            }}
                        />
                    </div>
                </Grid>
                <Grid item xs={12}>
                    <div className='p-2 bg-white border rounded shadow'>

                    </div>
                </Grid>
                <Grid item xs={12}>
                    <MaterialTable
                        title="Remote Data Preview"
                        columns={[
                            { title: 'Marka', field: 'marka' },
                            { title: 'Ticari Ünvan', field: 'marka' },
                            { title: 'İl', field: 'il' },
                            { title: 'İlçe', field: 'ilce' },
                            { title: 'Sektor', field: 'sektor.adi' },
                            { title: 'Kobi/Sanayi', field: 'kobiMi' },
                            { title: 'Firma Sorumlusu', field: 'vsorumlu' },
                        ]}
                        options={{
                            padding:'dense'
                        }}
                        data={query =>
                            new Promise((resolve, reject) => {
                                let qObj = {
                                    skip    : query.page * query.pageSize,
                                    limit   : query.pageSize,
                                    durum   :'aktif',
                                    marka   : `/${query.search}/i`,
                                    populate:'sektor.adi',
                                    filter  : {

                                    }
                                }
                                if (query.orderBy) {
                                    qObj.sort = query.orderBy? `${query.orderDirection=='asc'?'':'-'}${query.orderBy.field}` : ''
                                }
                                let q = new URLSearchParams(qObj).toString()

                                api.get(`/firma?${q}`, 
                                //api.get(`/firma?skip=${query.page * query.pageSize}&limit=${query.pageSize}&durum=aktif&marka=/${query.search}/i`, 
                                { headers: { Authorization: `Bearer ${token}` } })

                                    //let url = 'https://reqres.in/api/users?'
                                    //url += 'per_page=' + query.pageSize
                                    //url += '&page=' + (query.page + 1)
                                    //fetch(url)
                                    //.then(response => response.json())
                                    .then(result => {
                                        console.log(result)
                                        resolve({
                                            data: result.data.data.data,
                                            page: result.data.data.page / query.pageSize,
                                            totalCount: result.data.data.total,
                                        })
                                    })
                            })
                        }
                    />
                </Grid>

                <Grid item xs={12}>
                    <MaterialTable
                        title="Gorevler"
                        columns={[
                            { title: 'Başlık', field: 'baslik' },
                            { title: 'Son Tarıh', field: 'sonTarih' },
                            { title: 'Ic Gorev', field: 'icGorevMi' },
                            { title: 'Firma', field: 'firma.marka' },
                        ]}
                        options={{
                            padding:'dense'
                        }}
                        data={query =>
                            new Promise((resolve, reject) => {
                                let qObj = {
                                    skip    : query.page * query.pageSize,
                                    limit   : query.pageSize,
                                    //durum   :'aktif',
                                    //marka   : `/${query.search}/i`,
                                    populate:'firma.marka',
                                    
                                    filter  : {
                                        '$and' : [
                                            {sonTarih: {'$gte': '2021-03-01'}},
                                            {sonTarih: {'$lte': '2021-03-15'}},
                                            {icGorevMi: true}
                                        ]
                                    }
                                }
                                if (query.orderBy) {
                                    qObj.sort = query.orderBy? `${query.orderDirection=='asc'?'':'-'}${query.orderBy.field}` : ''
                                }
                                let q = qs.stringify(qObj) //new URLSearchParams(qObj).toString()

                                api.get(`/gorev?${q}`, 
                                { headers: { Authorization: `Bearer ${token}` } })
                                    .then(result => {
                                        console.log(result)
                                        if (!result.data.data) {
                                            reject()
                                        }
                                        resolve({
                                            data: result.data.data.data,
                                            page: result.data.data.page / query.pageSize,
                                            totalCount: result.data.data.total,
                                        })
                                    })
                            })
                        }
                    />
                </Grid>

            </Grid>
        </div>
    </>
}

export default TestPage