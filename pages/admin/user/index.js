import React from 'react'
import PageHeader from '../../../components/pageHeader'
import TableView from '../../../components/tableview'
import { Button, Typography } from '@material-ui/core'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'

const Users = props => {
    const router = useRouter()
    const token = useSelector(state => state.auth.expertoToken)

    return <>
        <PageHeader
            title='Kullanıcı Yönetimi'
            buttons={[
                {
                    title: 'Yeni',
                    onClick: (e) => {

                    }
                }
            ]}
        />
        <TableView
            title='Kullanıcılar'
            columns={[
                { label: 'Adı', id: 'username' },
                { label: 'EMail', id: 'email' },
                {
                    label: 'Aktif', render: row => {
                        if (row.active) {
                            return <Typography>Aktif</Typography>
                        }
                        else {
                            return <Typography>Pasif</Typography>
                        }
                    }
                },
                {
                    align: 'right', render: row => <Button variant='contained' color='primary' onClick={e => {
                        router.push(`/admin/user/${row._id}`)
                    }}>Detay</Button>
                }
            ]}
            remoteUrl={`/user`}
            token={token}
            filter={`select=username,active,email`}
            searchCol='username'
        />
    </>
}

export default Users