import React, { useState } from 'react'
import PageHeader from '../../../components/pageHeader'
import {
    Card, CardHeader, Divider, CardContent, Grid,
    TextField, FormControl, FormControlLabel, Switch,
    Checkbox, FormLabel, List, ListItem, ListItemText,
    ListSubheader, ListItemAvatar, Avatar, Chip
} from '@material-ui/core'
import api from '../../../services/axios.service'
import { useRouter } from 'next/router'
import { parseCookies, setCookie, destroyCookie } from 'nookies'
import { Autocomplete } from '@material-ui/lab'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

const icon = <CheckBoxOutlineBlankIcon />;
const checkedIcon = <CheckBoxIcon />;


function UserRoles(roles, userRoles, onChange) {
    return <>
        <FormControl component="fieldset" >
            <FormLabel component="legend">Roller</FormLabel>
            {roles.map(role => {
                let i = userRoles.findIndex(r => r == role._id)
                return (
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={i >= 0 || false}
                                onChange={(e) => onChange(role._id, e.target.checked)}
                                name="checkedB"
                                color="primary"
                            />
                        }
                        label={role.adi}
                    />
                )
            })}
        </FormControl>
    </>
}

const UserDetail = props => {
    const router = useRouter()
    const { isNew, roles } = props
    const [form, setForm] = useState(props.user)


    const value = () => {
        let result = []
        for (let role of roles) {
            if (form.vroles.includes(role.key)) {
                result.push(role)
            }
        }
        return result
    }

    return <>
        <PageHeader
            title='Kullanıcı'
            buttons={[
                {
                    title: 'Kaydet',
                    onClick: () => {
                        const cookies = parseCookies()
                        if (isNew) {
                            api.post(`/user`, form,
                                {
                                    headers: { Authorization: `Bearer ${cookies.experto}` }
                                }).then(resp => {
                                    router.back()
                                })
                        }
                        else {
                            api.put(`/user/${router.query.id}`, form, {
                                headers: { Authorization: `Bearer ${cookies.experto}` }
                            }).then(resp => {
                                router.back()
                            })
                        }
                    }
                }]}
        />
        <Card>
            <CardHeader
                title='Kullanıcı'
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <TextField
                            label='Adi'
                            fullWidth
                            variant='outlined'
                            value={form.username}
                            onChange={(e) => {
                                let f = { ...form }
                                f.username = e.target.value
                                setForm(f)
                            }}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label='Soyadi'
                            fullWidth
                            variant='outlined'
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label='EMail'
                            fullWidth
                            variant='outlined'
                            value={form.email}
                            onChange={(e) => {
                                let f = { ...form }
                                f.email = e.target.value
                                setForm(f)
                            }}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <FormControl component="fieldset">
                            <FormControlLabel
                                value={form.active}
                                control={<Switch
                                    color="primary"
                                    checked={form.active}
                                    onChange={(e) => {
                                        let f = { ...form }
                                        f.active = e.target.checked
                                        setUser(f)
                                    }}
                                />}
                                label="Aktif"
                                labelPlacement="top"
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label='Oid'
                            fullWidth
                            variant='outlined'
                            value={form?.oid}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label='Resim'
                            fullWidth
                            variant='outlined'
                            value={form?.image}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Autocomplete
                            options={roles || []}
                            multiple
                            disableCloseOnSelect
                            getOptionLabel={option => {
                                return `${option.adi}`;
                            }}
                            renderTags={(tagValue, getTagProps) =>
                                tagValue.map((opt, index) => {
                                    //let unvan = opt.unvani ? `[${opt.unvani}]` : ''
                                    let r = roles.find(r => r.key == opt.key)
                                    return <Chip
                                        //avatar={<Avatar src={opt.image} />}
                                        label={r.adi}
                                        variant="outlined"
                                        {...getTagProps({ index })}
                                    />
                                })
                            }
                            renderOption={(option, { selected }) => {
                                return <React.Fragment>
                                    <Checkbox
                                        icon={icon}
                                        checkedIcon={checkedIcon}
                                        style={{ marginRight: 8 }}
                                        checked={selected}
                                    />
                                    <ListItem dense>
                                        <ListItemText primary={option.adi} />
                                    </ListItem>
                                </React.Fragment>
                            }}
                            renderInput={params => (
                                <TextField
                                    //error={error}
                                    //helperText={error}
                                    fullWidth
                                    {...params}
                                    variant="outlined"
                                    label='Roller'
                                    placeholder='Rol seçin'
                                />
                            )}
                            value={value()}
                            onChange={(event, newValue) => {
                                let f = { ...form }
                                f.vroles = newValue.map(n => n.key)
                                setForm(f)
                            }}

                        />
                        {false && roles && UserRoles(roles, user.mroles, onRoleChanged)}
                    </Grid>
                    <Grid item xs={6}>
                        <List subheader={
                            <ListSubheader component="div" id="nested-list-subheader">
                                Tenants
                            </ListSubheader>
                        }>
                            {form.tenants.map(t => (<>
                                <ListItem>
                                    <ListItemText primary={t.adi} />
                                </ListItem>
                                <Divider />
                            </>))}

                        </List>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

UserDetail.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)

    let reqUser = await api.get(
        //`/user/${ctx.query.id}?select=username,email,oid,tenants,vroles,active&populate=roles`,
        `/user/${ctx.query.id}`,
        {
            headers: { Authorization: `Bearer ${cookies.experto}`, 'Content-type': 'application/json' }
        })
    let reqRoles = await api.get(`/role?active=true&skip=0&limit=100&select=adi,active,key`,
        {
            headers: { 'Content-type': 'application/json', Authorization: `Bearer ${cookies.experto}` }
        })

    return {
        isNew: ctx.query.id == 'new',
        user: reqUser.data.data,
        roles: reqRoles.data.data.data
    }
}

export default UserDetail