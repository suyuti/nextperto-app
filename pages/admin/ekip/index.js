import React from 'react'
import { getEkibim } from '../../../services/ekip.service'
import { useRouter } from 'next/router'
import PageHeader from '../../../components/pageHeader'
import TableView from '../../../components/tableview'
import { Button, Typography, makeStyles } from '@material-ui/core'
import { useSelector } from 'react-redux'

const useStyles =  makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const Ekip = props => {
    const classes = useStyles()
    const router = useRouter()
    const token = useSelector(state => state.auth.expertoToken)
    const user = useSelector(state => state.auth.user)
    return <>
        <PageHeader
            title='Ekipler'
            buttons={[
                {
                    title: 'Yeni',
                    onClick: (e) => {
                        router.push('/admin/ekip/:id', '/admin/ekip/new')
                    }
                }
            ]}
        />
        <div className={classes.root}>
            <TableView
                title='Ekipler'
                columns={[
                    { label: 'Adı', id: 'adi' },
                    { label: 'Yöneticisi', id: 'yonetici.username' },
                    { label: 'Ekip', id: '', render: row => { return `${row.uyeler?.length}` } },
                    {
                        label: 'Aktif', render: row => {
                            if (row.active) {
                                return <Typography>Aktif</Typography>
                            }
                            else {
                                return <Typography>Pasif</Typography>
                            }
                        }
                    },
                    {
                        align: 'right', render: row => <Button variant='contained' color='primary' onClick={e => {
                            router.push(`/admin/ekip/${row._id}`)
                        }}>Detay</Button>
                    }
                ]}
                remoteUrl={`/ekip`}
                token={token}
                filter={`select=adi,active,vyonetici,vuyeler&populate=yonetici,uyeler`}
                searchCol='adi'
                data={[]}
            />
        </div>
    </>
}

export default Ekip