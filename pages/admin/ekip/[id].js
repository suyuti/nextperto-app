import React, { useState } from 'react'
import PageHeader from '../../../components/pageHeader'
import {
    Card, Divider, CardContent, Grid, TextField, CardHeader, MenuItem, Button, makeStyles, ListItem,
    ListItemSecondaryAction, FormControl, FormControlLabel, Switch,
    ListItemAvatar, List, ListItemText, Avatar, Checkbox
} from '@material-ui/core'
import api from '../../../services/axios.service'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { parseCookies, setCookie, destroyCookie } from 'nookies'
import { uuid } from 'uuidv4'
import { getEkip, saveEkip, updateEkip } from '../../../services/ekip.service'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const Ekip = props => {
    const { newEkip } = props
    const [form, setForm] = useState(props.ekip)
    const classes = useStyles()
    const users = useSelector(state => state.auth.users)
    const router = useRouter()

    return <>
        <PageHeader
            title='Ekip'
            buttons={[
                <Button variant='contained' color='primary' onClick={() => {
                    if (newEkip) {
                        saveEkip(form)
                    }
                    else {
                        updateEkip(form._id, form)
                    }
                    router.back()
                }}>Kaydet</Button>
            ]}
        />
        <div className={classes.root}>
            <Card>
                <CardHeader
                    title='Ekip'
                />
                <Divider />
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item xs={4}>
                            <TextField
                                label='Ekip Adı'
                                variant='outlined'
                                fullWidth
                                value={form.adi}
                                onChange={(e) => {
                                    let d = { ...form }
                                    d.adi = e.target.value
                                    //d.key = e.target.value.toUpperCase().replaceAll(' ', '_')
                                    setForm(d)
                                }}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label='Yoneticisi'
                                variant='outlined'
                                fullWidth
                                value={form.vyonetici}
                                select
                                onChange={(e) => {
                                    let d = { ...form }
                                    d.vyonetici = e.target.value
                                    setForm(d)
                                }}
                            >
                                {users.map(d => <MenuItem key={d._id} value={d.key}>{d.username}</MenuItem>)}
                            </TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <FormControl component="fieldset">
                                <FormControlLabel
                                    value={form.active}
                                    control={
                                        <Switch
                                            color="primary"
                                            checked={form.active}
                                            onChange={(e) => {
                                                let r = { ...form }
                                                r.active = e.target.checked
                                                setForm(r)
                                            }} />}
                                    label={form.active ? 'Aktif' : "Pasif"}
                                    labelPlacement="top"
                                />
                            </FormControl>
                        </Grid>
                        <Grid item xs={4}>
                            <List>
                                {users.map(u => <ListItem>
                                    <ListItemAvatar >
                                        <Avatar src={u.image} />
                                    </ListItemAvatar>
                                    <ListItemText primary={u.username} />
                                    <ListItemSecondaryAction>
                                        <Checkbox
                                            edge='end'
                                            onChange={(e) => {
                                                if (e.target.checked) {
                                                    let f = { ...form }
                                                    f.vuyeler.push(u.key)
                                                    setForm(f)
                                                }
                                                else {
                                                    let f = { ...form }
                                                    f.vuyeler = f.vuyeler.filter(vu => vu != u.key)
                                                    setForm(f)
                                                }
                                            }}
                                            checked={
                                                form.vuyeler.findIndex(vu => vu == u.key) >= 0
                                            }
                                            inputProps={{ 'aria-labelledby': u._id }}
                                        />
                                    </ListItemSecondaryAction>
                                    <Divider />
                                </ListItem>)}
                            </List>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </div>
    </>
}

Ekip.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    var newEkip = ctx.query.id == 'new'
    let ekip = {
        key: uuid(),
        adi: '',
        vyonetici: null,
        vuyeler: []
    }

    if (!newEkip) {
        ekip = await getEkip(ctx.query.id)
    }

    return {
        newEkip,
        ekip
    }
}

export default Ekip