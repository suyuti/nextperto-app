import React from 'react'
import { Button } from '@material-ui/core'
import PageHeader from '../../../components/pageHeader'
import TableView from '../../../components/tableview'
import moment from 'moment'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'

const Tenant = props => {
    const token = useSelector(state => state.auth.expertoToken)
    const router = useRouter()

    return <>
        <PageHeader
            title='Tenants'
            buttons={[
                {
                    title: 'Yeni',
                    onClick: () => {
                        router.push(`/admin/tenant/:id`, `/admin/tenant/new`)
                    }
                }
            ]}
        />
        <TableView
            title="Tenants"
            columns={[
                { id: 'adi', label: 'Adı' },
                { id: 'validFor', label: 'Geçerli', render: row => { return moment(row.validFor).format('DD.MM.YYYY') } },
                { label: 'Aksiyon', render: row => <Button variant='contained' size='small' color='primary' onClick={() => {
                    router.push(`/admin/tenant/${row._id}`)}}>Detay</Button> }
            ]}
            remoteUrl={`/tenant`}
            token={token}
            filter={`select=adi,validFor`}
            searchCol='adi'
        />
    </>
}

export default Tenant