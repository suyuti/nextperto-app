import React, { useState } from 'react'
import PageHeader from '../../../components/pageHeader'
import { Card, Grid, CardHeader, Divider, CardContent, TextField, makeStyles, Button } from '@material-ui/core'
import api from '../../../services/axios.service'
import { parseCookies } from 'nookies'
import { useRouter } from 'next/router'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import { gidenBelgeleriSorgula } from '../../../services/tenant.service'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const Tenant = props => {
    const { isNew } = props
    const [form, setForm] = useState(props.tenant)
    const router = useRouter()
    const classes = useStyles()

    return <>
        <PageHeader
            title='Tenant'
            buttons={[
                <Button variant='contained' color='secondary' onClick={() => {
                    gidenBelgeleriSorgula(form._id)
                }}>Belgeleri Sorgula</Button>,
                {
                    title: 'Kaydet',
                    onClick: () => {
                        const cookies = parseCookies()
                        if (isNew) {
                            api.post(`/tenant`, f, {
                                headers: { Authorization: `Bearer ${cookies.experto}` }
                            })
                                .then(resp => {
                                    router.back()
                                })
                        }
                        else {
                            //let f = { ...form }
                            //delete f._id
                            api.put(`/tenant/${form._id}`, form, {
                                headers: { Authorization: `Bearer ${cookies.experto}` }
                            })
                                .then(resp => {
                                    router.back()
                                })
                        }
                    }
                }
            ]}
        />
        <div className={classes.root}>

            <Card>
                <CardHeader
                    title='Tenant bilgi'
                />
                <Divider />
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                label='Adı'
                                variant='outlined'
                                value={form.adi}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.adi = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label='Marka'
                                variant='outlined'
                                value={form.marka}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.marka = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label='Ticari Unvan'
                                variant='outlined'
                                value={form.unvan}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.unvan = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label='Vergi Dairesi'
                                variant='outlined'
                                value={form.vd}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.vd = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label='Vergi No'
                                variant='outlined'
                                value={form.vkn}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.vkn = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label='Adres'
                                variant='outlined'
                                value={form.adres}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.adres = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label='Il'
                                variant='outlined'
                                value={form.il}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.il = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label='İlçe'
                                variant='outlined'
                                value={form.ilce}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.ilce = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label='Ülke'
                                variant='outlined'
                                value={form.ulke}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.ulke = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Divider />
                        </Grid>

                        <Grid item xs={12}>
                            <TextField
                                label='QNB EFatura baglanti'
                                variant='outlined'
                                value={form.efaturaUrl}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.efaturaUrl = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label='QNB EFatura user'
                                variant='outlined'
                                value={form.efaturaUser}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.efaturaUser = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label='QNB EFatura parola'
                                variant='outlined'
                                value={form.efaturaPwd}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.efaturaPwd = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Divider />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label='QNB EArşiv baglanti'
                                variant='outlined'
                                value={form.earsivUrl}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.earsivUrl = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label='QNB EArşiv user'
                                variant='outlined'
                                value={form.earsivUser}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.earsivUser = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label='QNB EArşiv parola'
                                variant='outlined'
                                value={form.earsivPwd}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.earsivPwd = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Divider />
                        </Grid>

                        <Grid item xs={6}>
                            <TextField
                                label='Kontak mail'
                                variant='outlined'
                                value={form.kontakMail}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.kontakMail = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label='Kontak Telefon'
                                variant='outlined'
                                value={form.kontakTelefon}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.kontakTelefon = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label='Fatura Kodu'
                                variant='outlined'
                                value={form.faturaKodu}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.faturaKodu = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                InputProps={{
                                    readOnly: true,
                                }}
                                label='Son Kesilen Fatura No'
                                variant='outlined'
                                value={form.sonFaturaNo}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.sonFaturaNo = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                InputProps={{
                                    readOnly: true,
                                }}
                                label='Son Kesilen Fatura Tarihi'
                                variant='outlined'
                                value={form.sonFaturaTarihi}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.sonFaturaTarihi = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label='Geçmiş Fatura Kodu'
                                variant='outlined'
                                value={form.gecmisTarihFaturaKodu}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.gecmisTarihFaturaKodu = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                InputProps={{
                                    readOnly: true,
                                }}
                                label='Son Kesilen Geçmiş Fatura No'
                                variant='outlined'
                                value={form.sonGecmisTarihFaturaNo}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.sonGecmisTarihFaturaNo = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                InputProps={{
                                    readOnly: true,
                                }}
                                label='Son Kesilen Geçmiş Fatura Tarihi'
                                variant='outlined'
                                value={form.sonGecmisTarihFaturaTarihi}
                                onChange={(e) => {
                                    let f = { ...form }
                                    f.sonGecmisTarihFaturaTarihi = e.target.value
                                    setForm(f)
                                }}
                                fullWidth
                            />
                        </Grid>

                        <Grid item xs={3}>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <KeyboardDatePicker
                                    //error={error && error.tarih}
                                    //helperText={error && error.tarih}
                                    autoOke
                                    fullWidth
                                    variant="inline"
                                    inputVariant="outlined"
                                    label='Geçerlilik Tarihi'
                                    format="dd.MM.yyyy"
                                    InputAdornmentProps={{ position: 'start' }}
                                    value={form.validFor || new Date()}
                                    onChange={(e) => {
                                        let f = { ...form }
                                        f.validFor = e
                                        setForm(f)
                                    }}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>

        </div>
    </>
}

Tenant.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    let isNew = ctx.query.id == 'new'
    let reqTenant
    if (!isNew) {
        reqTenant = await api.get(`/tenant/${ctx.query.id}`,
            {
                headers: { Authorization: `Bearer ${cookies.experto}`, 'Content-type': 'application/json' }
            })

    }

    return {
        isNew: isNew,
        tenant: isNew ? {} : reqTenant.data.data
    }
}

export default Tenant