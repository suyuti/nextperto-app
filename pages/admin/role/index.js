import React from 'react'
import { useRouter } from 'next/router'
import PageHeader from '../../../components/pageHeader'
import TableView from '../../../components/tableview'
import { Button, Typography } from '@material-ui/core'
import { useSelector } from 'react-redux'

const Roles = props => {
    const router = useRouter()
    const token = useSelector(state => state.auth.expertoToken)

    return <>
        <PageHeader
            title='Roller'
            buttons={[
                {
                    title: 'Yeni',
                    onClick: (e) => {
                        router.push('/admin/role/:id', '/admin/role/new')
                    }
                }
            ]}
        />
        <TableView
            title='Roller'
            columns={[
                { label: 'Adı', id: 'adi' },
                {
                    label: 'Aktif', render: row => {
                        if (row.active) {
                            return <Typography>Aktif</Typography>
                        }
                        else {
                            return <Typography>Pasif</Typography>
                        }
                    }
                },
                {
                    align: 'right', render: row => <Button variant='contained' color='primary' onClick={e => {
                        router.push(`/admin/role/${row._id}`)
                    }}>Detay</Button>
                }
            ]}
            remoteUrl={`/role`}
            token={token}
            filter={`select=adi,active`}
            searchCol='adi'
            data={[]}
        />
    </>
}
export default Roles