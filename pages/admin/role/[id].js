import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import PageHeader from '../../../components/pageHeader'
import api from '../../../services/axios.service'
import {
    Card, CardHeader, Divider, CardContent, Grid,
    Typography, TextField, FormControl, FormLabel,
    FormControlLabel, Checkbox, Table, TableBody,
    TableHead, TableCell, TableRow, Switch
} from '@material-ui/core'
import { parseCookies, setCookie, destroyCookie } from 'nookies'

function Permissions(permissions, rolePermissions, handleCheckbox) {
    return <>
        <FormControl component="fieldset" >
            <FormLabel component="legend">İzinler</FormLabel>
            <Table size='small'>
                <TableHead>
                    <TableRow>
                        <TableCell>Aksiyon</TableCell>
                        <TableCell>Oluşturma</TableCell>
                        <TableCell>Görme</TableCell>
                        <TableCell>Değiştirme</TableCell>
                        <TableCell>Silme</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {permissions.map(p => {
                        let i = rolePermissions.findIndex(rp => rp.key == p.key)
                        if (i >= 0) {
                            return (
                                <TableRow>
                                    <TableCell>{p.label}</TableCell>
                                    <TableCell><Checkbox onChange={e => handleCheckbox(p.key, e.target.checked, 'create')} checked={rolePermissions[i].create} /></TableCell>
                                    <TableCell><Checkbox onChange={e => handleCheckbox(p.key, e.target.checked, 'read')} checked={rolePermissions[i].read} /></TableCell>
                                    <TableCell><Checkbox onChange={e => handleCheckbox(p.key, e.target.checked, 'write')} checked={rolePermissions[i].write} /></TableCell>
                                    <TableCell><Checkbox onChange={e => handleCheckbox(p.key, e.target.checked, 'dlete')} checked={rolePermissions[i].dlete} /></TableCell>
                                </TableRow>
                            )
                        }
                        else {
                            return (
                                <TableRow>
                                    <TableCell>{p.label}</TableCell>
                                    <TableCell><Checkbox onChange={e => handleCheckbox(p.key, e.target.checked, 'create')} checked={p.create} /></TableCell>
                                    <TableCell><Checkbox onChange={e => handleCheckbox(p.key, e.target.checked, 'read')} checked={p.read} /></TableCell>
                                    <TableCell><Checkbox onChange={e => handleCheckbox(p.key, e.target.checked, 'write')} checked={p.write} /></TableCell>
                                    <TableCell><Checkbox onChange={e => handleCheckbox(p.key, e.target.checked, 'dlete')} checked={p.dlete} /></TableCell>
                                </TableRow>
                            )
                        }
                    })}
                </TableBody>
            </Table>
        </FormControl>
    </>
}

const Role = props => {
    const { permissions, newRole } = props
    const [role, setRole] = useState(props.role)
    const router = useRouter()

    return <>
        <PageHeader
            title='Rol'
            buttons={[
                {
                    title: 'Kaydet',
                    onClick: (e) => {
                        const cookies = parseCookies()
                        if (newRole) {
                            api.post(`/role`, role, {
                                headers: { Authorization: `Bearer ${cookies.experto}` }
                            }).then(resp => router.back())
                        }
                        else {
                            api.put(`/role/${role._id}`, role, {
                                headers: { Authorization: `Bearer ${cookies.experto}` }
                            }).then(resp => router.back())
                        }
                    }
                }
            ]}
        />
        <Card>
            <CardHeader
                title='Rol'
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={10}>
                        <TextField
                            label='Rol adı'
                            variant='outlined'
                            fullWidth
                            value={role.adi}
                            onChange={(e) => {
                                let r = { ...role }
                                r.adi = e.target.value
                                r.key = e.target.value.toUpperCase().replaceAll(' ', '_')
                                setRole(r)
                            }}
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <FormControl component="fieldset">
                            <FormControlLabel
                                value={role.active}
                                control={
                                    <Switch
                                        color="primary"
                                        checked={role.active}
                                        onChange={(e) => {
                                            let r = { ...role }
                                            r.active = e.target.checked
                                            setRole(r)
                                        }} />}
                                label={role.active ? 'Aktif' : "Pasif"}
                                labelPlacement="top"
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        {Permissions(permissions.data, role.permissions, (key, checked, property) => {
                            let r = { ...role }
                            let i = r.permissions.findIndex(rp => rp.key == key)
                            if (i >= 0) {
                                r.permissions[i][property] = checked
                            }
                            else {
                                r.permissions.push({
                                    key: key,
                                    create: property == 'create' ? checked : false,
                                    read: property == 'read' ? checked : false,
                                    write: property == 'write' ? checked : false,
                                    remove: property == 'remove' ? checked : false
                                })
                            }
                            setRole(r)
                        })}
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

Role.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    var newRole = false
    var role
    var permissions = []
    let clientPermissions = await api.get(`/permission`, {
        headers: { Authorization: `Bearer ${cookies.experto}`, 'Content-type': 'application/json' }
    })
    permissions = clientPermissions.data

    if (ctx.query.id == 'new') {
        newRole = true
        role = {
            adi: '',
            active: false,
            permissions: []
        }
    }
    else {
        let client = await api.get(`/role/${ctx.query.id}`, {
            headers: { Authorization: `Bearer ${cookies.experto}`, 'Content-type': 'application/json' }
        })
        role = client.data.data
    }

    return {
        role: role,
        newRole: newRole,
        permissions: permissions
    }
}

export default Role