import React from 'react'
import PageHeader from '../../../components/pageHeader'
import { Card, CardHeader, Divider, CardContent, Grid, Typography, IconButton, makeStyles } from '@material-ui/core'
import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import EditIcon from '@material-ui/icons/Edit';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import api from '../../../services/axios.service'
import { useRouter } from 'next/router';

const useStyles = makeStyles(theme => ({
    item: {
        display: 'flex'
    }
}))

const OrgItem = data => {
    const router = useRouter()
    const classes= useStyles()
    const isChildren = data.children != null
    if (isChildren) {
        return <TreeItem 
            key={data.adi} 
            nodeId={data.adi} 
            label={
            <div className={classes.item}>
                <Typography>{data.adi}</Typography>
                <div className={classes.item}>
                    <Typography>yonetici</Typography>
                    <IconButton
                        onClick={() => {
                            router.push(`/admin/organizasyon/${data._id}`)
                        }}
                    >
                        <EditIcon />
                    </IconButton>
                </div>
            </div>}>
            {data.children.map(node =>  {return OrgItem(node)})}
        </TreeItem>
    }
    else {
        return <TreeItem key={data.adi} nodeId={data.adi} label={data.adi} />
    }
}


const Organizasyon = props => {
    const {departmanlar} = props

    return <>
        <PageHeader
            title='Organizasyon'
        />
        <Card>
            <CardHeader
                title='Organizasyon'
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TreeView
                            defaultCollapseIcon={<ExpandMoreIcon />}
                            defaultExpandIcon={<ChevronRightIcon />}
                        >
                            {departmanlar.map(dep => {
                                return OrgItem(dep)
                            })}
                        </TreeView>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

Organizasyon.getInitialProps = async (ctx) => {
    let deps = await api.get('/departmen?type=asTree')
    return {
        departmanlar: deps.data
    }
}

export default Organizasyon