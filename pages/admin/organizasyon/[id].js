import React from 'react'
import PageHeader from '../../../components/pageHeader'
import { Card, CardHeader, Divider, CardContent, TextField, Grid, MenuItem, Avatar, List, ListItem, ListItemAvatar, ListItemText, ListItemSecondaryAction, Checkbox } from '@material-ui/core'
import api from '../../../services/axios.service'

const Departman = props => {
    const { personeller, departmanlar } = props

    return <>
        <PageHeader
            title='Departman'
        />
        <Card>
            <CardHeader
                title='Departman'
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <TextField
                            label='Departman Adı'
                            variant='outlined'
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            label='Yöneticisi'
                            variant='outlined'
                            fullWidth
                            select
                        >
                            {personeller.map(p => <MenuItem key={p._id} value={p._id}>{p.displayName}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            label='Bağlı olduğu departman'
                            variant='outlined'
                            fullWidth
                            select
                        >
                            {departmanlar.map(p => <MenuItem key={p._id} value={p._id}>{p.adi}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={12}>
                        <List dense>
                            {personeller.map(p => {
                                return (
                                    <>
                                        <ListItem>
                                            <ListItemAvatar>
                                                <Avatar />
                                            </ListItemAvatar>
                                            <ListItemText primary={p.displayName}></ListItemText>
                                            <ListItemSecondaryAction>
                                                <Checkbox 
                                                    edge='end'
                                                />
                                            </ListItemSecondaryAction>
                                        </ListItem>
                                    </>
                                )
                            })}
                        </List>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

Departman.getInitialProps = async (ctx) => {
    let clientPersonel = await api.get(`/users`)
    let clientDepartman = await api.get(`/departmen?type=asList`)

    return {
        personeller: clientPersonel.data,
        departmanlar: clientDepartman.data
    }
}

export default Departman