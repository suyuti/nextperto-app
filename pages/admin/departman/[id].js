import React, { useState } from 'react'
import PageHeader from '../../../components/pageHeader'
import { Card, Divider, CardContent, Grid, TextField, CardHeader, MenuItem, Button, makeStyles } from '@material-ui/core'
import api from '../../../services/axios.service'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { parseCookies, setCookie, destroyCookie } from 'nookies'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const Departman = props => {
    const { personeller, departmanlar, newDepartman } = props
    const [departman, setDepartman] = useState(props.departman)
    const classes = useStyles()
    const users = useSelector(state => state.auth.users)
    const router = useRouter()

    return <>
        <PageHeader
            title='Departman'
            buttons={[
                <Button variant='contained' color='primary' onClick={() => {
                    const cookies = parseCookies()
                    if (newDepartman) {
                        api.post(`/departman`, departman,
                            {
                                headers: { Authorization: `Bearer ${cookies.experto}` }
                            })
                            .then(resp => {
                                router.back()
                            })
                    }
                    else {
                        api.put(`/departman/${departman._id}`, departman,
                            {
                                headers: { Authorization: `Bearer ${cookies.experto}` }
                            })
                            .then(resp => {
                                router.back()
                            })
                    }
                }}>Kaydet</Button>
            ]}
        />
        <div className={classes.root}>
            <Card>
                <CardHeader
                    title='Deparman'
                />
                <Divider />
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item xs={4}>
                            <TextField
                                label='Departman Adı'
                                variant='outlined'
                                fullWidth
                                value={departman.adi}
                                onChange={(e) => {
                                    let d = { ...departman }
                                    d.adi = e.target.value
                                    d.key = e.target.value.toUpperCase().replaceAll(' ', '_')
                                    setDepartman(d)
                                }}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label='Yoneticisi'
                                variant='outlined'
                                fullWidth
                                value={departman.vyonetici}
                                select
                                onChange={(e) => {
                                    let d = { ...departman }
                                    d.vyonetici = e.target.value
                                    setDepartman(d)
                                }}
                            >
                                {users.map(d => <MenuItem key={d._id} value={d.key}>{d.username}</MenuItem>)}
                            </TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label='Bağlı Olduğu Departman'
                                variant='outlined'
                                fullWidth
                                select
                                value={departman.parent}
                                onChange={e => {
                                    let d = { ...departman }
                                    d.parent = e.target.value
                                    setDepartman(d)
                                }}
                            >
                                {departmanlar.map(d => <MenuItem key={d._id} value={d._id}>{d.adi}</MenuItem>)}
                            </TextField>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </div>
    </>
}

Departman.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    var newDepartman = ctx.query.id == 'new'
    let departman = {
        adi: '',
        vyonetici: null,
        ustDepartman: null
    }
    let clientDeps = await api.get(`/departman?type=asList&select=adi,vyonetici,key&populate=yonetici`,
        {
            headers: { Authorization: `Bearer ${cookies.experto}`, 'Content-type': 'application/json' }
        }
    )
    let departmanlar = clientDeps.data.data.data

    if (!newDepartman) {
        let client = await api.get(`/departman/${ctx.query.id}?select=adi,vyonetici&populate=yonetici`,
            {
                headers: { Authorization: `Bearer ${cookies.experto}`, 'Content-type': 'application/json' }
            }
        )
        departman = client.data.data

    }

    return {
        //personeller: [],
        departmanlar: departmanlar,
        newDepartman: newDepartman,
        departman: departman
    }
}

export default Departman