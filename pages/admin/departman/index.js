import React from 'react'
import PageHeader from '../../../components/pageHeader'
import {useRouter} from 'next/router'
import DepartmenTree from '../../../components/departman/departmanTree'
import api from '../../../services/axios.service'
import { parseCookies, setCookie, destroyCookie } from 'nookies'

const Departmanlar = props => {
    const router = useRouter()
    const {departmanlar} = props

    return <>
        <PageHeader 
            title='Departmanlar'
            buttons = {[
                {
                    title: 'Yeni',
                    onClick: () => {
                        router.push(`/admin/departman/:id`, '/admin/departman/new')
                    }
                }
            ]}
        />
        <DepartmenTree departmanlar={departmanlar}/>
    </>
}

Departmanlar.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    let depClient = await api.get(`/departman?type=asTree&select=vyonetici,adi&populate=yonetici`, {
        headers: { Authorization: `Bearer ${cookies.experto}`, 'Content-type': 'application/json' }
    })
    return {
        departmanlar: depClient.data.data.data
    }
}

export default Departmanlar