import React, { useState, useEffect } from 'react'
import PageHeader from '../../../components/pageHeader'
import { TextField, MenuItem, Table, TableHead, TableRow, TableCell, TableBody, Checkbox, makeStyles, Paper, Typography } from '@material-ui/core'
import api from '../../../services/axios.service'
import yetkiler from '../../../constants/yetki.json'
import {useRouter} from 'next/router'

const useStyles = makeStyles((theme) => (
    {
        root: {
            padding: 10,
            margin: 10
        },
        table: {
            marginTop: 10,
            backgroundColor: 'white'
        }
    }
))

const Yetkiler = props => {
    const router  = useRouter()
    const { roles } = props
    const [selectedRole, setSelectedRole] = useState(null)
    const [tableData, setTableData] = useState(yetkiler)
    const classes = useStyles()

    const handleRoleChanged = (roleId) => {
        let role = roles.find(r => r._id == roleId)
        setSelectedRole(role)

        for (var yetki of yetkiler) {
            let i = role.permissions.findIndex(p => p.key == yetki.key)
            if (i >= 0) {
                yetki.create = role.permissions[i].create
                yetki.read = role.permissions[i].read
                yetki.write = role.permissions[i].write
                yetki.remove = role.permissions[i].remove
            }
            else {
                yetki.create = false
                yetki.read = false
                yetki.write = false
                yetki.remove = false
            }
        }
        setTableData(yetkiler)
    }

    const handleCheckbox = (key, checked, t) => {

    }

    const onSave = () => {
        api.put(`/role/${selectedRole._id}`, selectedRole).then(resp => {
            router.back()
        })
    }

    return <>
        <PageHeader
            title='Yetkilendirmeler'
            buttons={[
                { title: 'Kaydet', onClick: (e) => { onSave()} }
            ]}
        />
        <div >
            <Paper className={classes.root}>
                <TextField
                    label='Rol'
                    variant='outlined'
                    fullWidth
                    onChange={(e) => {
                        handleRoleChanged(e.target.value)
                        //let role = roles.find(r => r._id == e.target.value)
                        //setSelectedRole(role)
                    }}
                    select
                >
                    {roles ?.map(role => <MenuItem key={role._id} value={role._id}>{role.adi}</MenuItem>)}
                </TextField>
                <Typography variant='h6'>Yetkiler</Typography>
                <Table size='small' className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Aksiyon</TableCell>
                            <TableCell>Olusturma</TableCell>
                            <TableCell>Gorme</TableCell>
                            <TableCell>Guncelleme</TableCell>
                            <TableCell>Silme</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {tableData.map(y => {
                            return (
                                <TableRow>
                                    <TableCell>{y.title}</TableCell>
                                    <TableCell><Checkbox onChange={e => handleCheckbox(y.key, e.target.checked, 'create')} checked={y.create} /></TableCell>
                                    <TableCell><Checkbox onChange={e => handleCheckbox(y.key, e.target.checked, 'read')} checked={y.read} /></TableCell>
                                    <TableCell><Checkbox onChange={e => handleCheckbox(y.key, e.target.checked, 'write')} checked={y.write} /></TableCell>
                                    <TableCell><Checkbox onChange={e => handleCheckbox(y.key, e.target.checked, 'remove')} checked={y.remove} /></TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </Paper>
        </div>
    </>
}

Yetkiler.getInitialProps = async (ctx) => {
    let resp = await api.get(`/role`)
    return {
        roles: resp.data
    }
}

export default Yetkiler