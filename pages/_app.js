import React, { useEffect } from "react";
import DateFnsUtils from '@date-io/date-fns'; // choose your lib
import {
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import Layout from '../components/core/Layout'
//import './styles.css'
import '../styles/tailwind.css';

import { Provider } from 'react-redux'
import { useStore } from '../redux/store'
import Auth from "../components/auth";

import { SnackbarProvider } from 'notistack';
import SocketWrapper from "../components/core/SocketWrapper";
import SnackbarWrapper from "../components/core/Snacks/SnackbarWrapper";
import ModalDialogWrapper from "../components/core/ModalDialogWrapper";

export default function App({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState)

  /*
    return (
      <Provider store={store}>
          <Layout>
            <Component {...pageProps} />
          </Layout>
      </Provider>
    )
    */
  return (
    <Provider store={store}>
      <SnackbarWrapper>
        <Auth store={store} {...pageProps}>
          <SocketWrapper>
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </SocketWrapper>
        </Auth>
      </SnackbarWrapper>
    </Provider>
  )
}

/*

const App = ({ Component, pageProps }) => {
  return (<>
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </MuiPickersUtilsProvider>
  </>)
}
export default wrapper.withRedux(App)
*/
// Redux kullanim?
// 
// https://codesandbox.io/s/next-redux-wrapper-demo-7n2t5?file=/pages/_app.tsx:0-869
// https://github.com/kirill-konshin/next-redux-wrapper
