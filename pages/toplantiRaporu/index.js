import { CircularProgress, Card, CardContent, CardHeader, Divider, Grid, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText, Tooltip } from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import PageHeader from "../../components/pageHeader";
import AddIcon from '@material-ui/icons/Add';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { getToplantiYapilmamisFirmalar, getToplantiYapilmisFirmalar } from '../../services/toplanti.service'
import { withRouter } from 'next/router'

const ToplantiRaporu = (props) => {
    //const { toplantiYapilmamisFirmalar, toplantiYapilmisFirmalar } = props
    const [toplantiYapilmamisFirmalar, setToplantiYapilmamisFirmalar] = useState(null)
    const [toplantiYapilmisFirmalar, setToplantiYapilmisFirmalar] = useState(null)


    const load = async () => {
        getToplantiYapilmisFirmalar().then(resp => {
            setToplantiYapilmisFirmalar(resp)
        })
        getToplantiYapilmamisFirmalar().then(resp => {
            setToplantiYapilmamisFirmalar(resp)
        })
    }

    useEffect(() => {
        load()
    }, [])

    return (
        <div className=''>
            <PageHeader
                title='Toplantı Raporları'
            ></PageHeader>
            <div className='p-2'>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <Card className='border-red-500 border-t-4'>
                            <CardHeader title='Bu Ay Toplantı Yapılmamış Firmalar' />
                            <Divider />
                            <CardContent>
                                <List>
                                    {!toplantiYapilmamisFirmalar ?
                                        <div className=''>
                                            <CircularProgress />
                                        </div>
                                        :
                                        toplantiYapilmamisFirmalar.map((firma, i) => {
                                            return (
                                                <ListItem className='border-b-2'>
                                                    <ListItemText primary={firma.marka} secondary={
                                                        <div className='flex flex-row space-x-1 pl-3'>
                                                            {firma.argeMerkezi &&
                                                                <div className='border border-gray-400 rounded-full px-2 text-xs font-sans bg-red-200 text-gray-700'>ArGe</div>
                                                            }
                                                            {firma.tasarimMerkezi &&
                                                                <div className='border border-gray-400 rounded-full px-2 text-xs font-sans  bg-blue-200 text-gray-700'>Tasarım</div>
                                                            }
                                                        </div>} />
                                                    <ListItemSecondaryAction>
                                                        <Tooltip title='Firmaya git'>
                                                            <IconButton
                                                                edge='end'
                                                                className='focus:outline-none'
                                                                onClick={() => {
                                                                    props.router.push(`/musteri/${firma._id}`)
                                                                }}
                                                            >
                                                                <ArrowForwardIcon />
                                                            </IconButton>
                                                        </Tooltip>
                                                        <Tooltip title='Toplantı Oluştur'>
                                                            <IconButton
                                                                edge='end'
                                                                className='focus:outline-none'
                                                                onClick={() => {
                                                                    props.router.push(`/toplanti/new?firma=${firma.key}`)
                                                                }}>
                                                                <AddIcon />
                                                            </IconButton>
                                                        </Tooltip>
                                                    </ListItemSecondaryAction>
                                                </ListItem>)
                                        })}
                                </List>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={6}>
                        <Card className='border-green-500 border-t-4'>
                            <CardHeader title='Bu Ay Toplantı Yapılmış Firmalar' />
                            <Divider />
                            <CardContent>
                                <List>
                                    {!toplantiYapilmisFirmalar
                                        ?
                                        <div className=''>
                                            <CircularProgress />
                                        </div>
                                        :
                                        toplantiYapilmisFirmalar.map((firma, i) => {
                                            return (
                                                <ListItem className='border-b-2'>
                                                    <ListItemText primary={firma.marka} secondary={
                                                        <div className='flex flex-row space-x-1 pl-3'>
                                                            {firma.argeMerkezi &&
                                                                <div className='border border-gray-400 rounded-full px-2 text-xs font-sans bg-red-200 text-gray-700'>ArGe</div>
                                                            }
                                                            {firma.tasarimMerkezi &&
                                                                <div className='border border-gray-400 rounded-full px-2 text-xs font-sans  bg-blue-200 text-gray-700'>Tasarım</div>
                                                            }
                                                        </div>} />
                                                    <ListItemSecondaryAction>
                                                        <Tooltip title='Toplantıya git'>
                                                            <IconButton edge='end' className='focus:outline-none'>
                                                                <ArrowForwardIcon />
                                                            </IconButton>
                                                        </Tooltip>
                                                    </ListItemSecondaryAction>
                                                </ListItem>)
                                        })}
                                </List>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </div>
        </div>
    )
}

//ToplantiRaporu.getInitialProps = async (ctx) => {
//let toplantiYapilmamisFirmalar = await getToplantiYapilmamisFirmalar();
//let toplantiYapilmisFirmalar = await getToplantiYapilmisFirmalar();
//    return {
//        toplantiYapilmisFirmalar,
//        toplantiYapilmamisFirmalar
//    }
//}

export default withRouter(ToplantiRaporu)
