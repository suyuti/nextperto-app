import { Card, CardHeader, Divider, Grid, CardContent, Avatar, Button } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import PageHeader from '../../components/pageHeader'
import { getOnay, iptalet, onayla, reddet } from '../../services/onay.service'
import { parseCookies } from 'nookies'
import { useSelector } from 'react-redux'
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt'
import { withRouter } from 'next/router'
const moment = require('moment')

const Onay = (props) => {
    const { onay } = props
    const users = useSelector(state => state.auth.users)
    const user = useSelector(state => state.auth.user)
    const [talepEden, setTalepEden] = useState(null)
    const [onayVeren, setOnayVeren] = useState(null)

    useEffect(() => {
        if (onay) {
            let _talepEden = users.find(u => u._id == onay.onayIsteyen)
            setTalepEden(_talepEden)
            if (onay.onaylayan) {
                let _onaylayan = users.find(u => u._id == onay.onaylayan)
                setOnayVeren(_onaylayan)
            }
        }
    }, [])

    return (
        <div>
            <PageHeader
                title='Onay'
                buttons={[
                    (onay.onayIsteyen == user._id && onay.onayDurumu == 'acik') && <Button variant='contained' color='primary'
                        onClick={e => {
                            e.preventDefault()
                            iptalet(onay._id)
                            .then(resp => props.router.back())
                        }}
                    >İptal Et</Button>,
                    (onay.onayIstenen == user._id && onay.onayDurumu == 'acik') && <Button variant='contained' color='primary'
                        onClick={e => {
                            e.preventDefault()
                            onayla(onay._id)
                            .then(resp => props.router.back())
                        }}
                    >Onayla</Button>,
                    (onay.onayIstenen == user._id && onay.onayDurumu == 'acik') && <Button variant='contained' color='secondary'
                        onClick={e => {
                            e.preventDefault()
                            reddet(onay._id)
                            .then(resp => props.router.back())
                        }}
                    >Reddet</Button>
                ]}
            />
            <div className='p-2 flex'>
                <Card className='w-3/4'>
                    <CardHeader
                        title="Onay Talebi"
                        action={<Button
                            variant="outlined"
                            color="primary"
                            endIcon={<ArrowRightAltIcon />}
                            onClick={e => {
                                e.preventDefault()
                                if (onay.refCollection == 'Gorev') {
                                    props.router.push(`/gorev/${onay.referenceItem}`)
                                }
                            }}
                        >ilgili Kayıt</Button>}
                    />
                    <Divider />
                    <CardContent>
                        <div className='w-2/3 flex-col space-y-2'>
                            <div className='onayisteyen flex items-center justify-between'>
                                <h2 className='font-light'>Onay Isteyen</h2>
                                <div className='flex items-center space-x-2'>
                                    <Avatar className='h-4 w-4' style={{ height: '30px', width: '30px' }} src={talepEden?.image}></Avatar>
                                    <h2>{talepEden?.username}</h2>
                                </div>
                            </div>
                            <div className='istektarihi flex items-center justify-between'>
                                <h2 className='font-light'>Onay Talep Tarihi</h2>
                                <div className='flex items-center space-x-2'>
                                    <h2>{moment(onay.istemeTarihi).format('HH:mm DD.MM.YYYY')}</h2>
                                </div>
                            </div>
                            <div className='konu flex items-center justify-between'>
                                <h2 className='font-light'>Onay Konusu</h2>
                                <div className='flex items-center space-x-2'>
                                    <h2>{onay.refCollection}</h2>
                                </div>
                            </div>
                            <div className='detay flex items-center justify-between'>
                                <h2 className='font-light'>Değişiklik Detayı</h2>
                                <div className='flex-col items-center space-x-2'>
                                    {onay.diff?.map((key, i) => {
                                        return (
                                            <div className='flex-col p-2'>
                                                <h2 className='font-normal text-center'>{key.field}</h2>
                                                <div className='flex justify-between items-center space-x-4'>
                                                    <h2 className='w-1/2 font-extralight text-xs'>{key.prev}</h2>
                                                    <ArrowRightAltIcon />
                                                    <h2 className='w-1/2 font-extralight text-xs'>{key.next}</h2>
                                                </div>
                                            </div>
                                        )
                                    })}

                                </div>
                            </div>
                            {onay.onayDurumu != 'acik' &&
                                <>
                                    <div className='border-b-2 border-gray-300 h-1 w-full'></div>
                                    <div className='Sonuc flex items-center justify-between'>
                                        <h2 className='font-light'>Onay Konusu</h2>
                                        {onay.onayDurumu == 'onay' && <div className='bg-green-500 w-32 text-center p-1 rounded shadow text-white'>ONAY</div>}
                                        {onay.onayDurumu == 'iptal' && <div className='bg-gray-500 w-32 text-center p-1 rounded shadow text-white'>İPTAL</div>}
                                        {onay.onayDurumu == 'red' && <div className='bg-red-500 w-32 text-center p-1 rounded shadow text-white'>RED</div>}
                                    </div>
                                    <div className='onayveren flex items-center justify-between'>
                                        <h2 className='font-light'>Onay Veren</h2>
                                        <div className='flex items-center space-x-2'>
                                            <Avatar className='h-4 w-4' style={{ height: '30px', width: '30px' }} src={onayVeren?.image}></Avatar>
                                            <h2>{onayVeren?.username}</h2>
                                        </div>
                                    </div>
                                    <div className='onaytarihi flex items-center justify-between'>
                                        <h2 className='font-light'>Onay Tarihi</h2>
                                        <div className='flex items-center space-x-2'>
                                            <h2>{moment(onay.onayTarihi).format('HH:mm DD.MM.YYYY')}</h2>
                                        </div>
                                    </div>
                                </>
                            }
                        </div>
                    </CardContent>
                </Card>
            </div>
        </div>
    )
}

Onay.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    let onay = await getOnay(ctx.query.id)
    return {
        onay
    }
}

export default withRouter(Onay)
