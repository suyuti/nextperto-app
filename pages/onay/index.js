import React, { useState, useRef, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'
import { Button, Box, IconButton, TextField, MenuItem, Avatar, Menu, Dialog, DialogTitle, DialogContent, DialogActions, DialogContentText } from '@material-ui/core'
import { withRouter } from 'next/router';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import RemoteAutoComplete from '../../components/autocomplete';
import { searchFirmalar } from '../../services/firma.service'
import trLocale from "date-fns/locale/tr";
import PageHeader from '../../components/pageHeader';
import MaterialTable from 'material-table';
import api from '../../services/axios.service';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import * as Actions from '../../redux/actions'
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import ThumbDownAltOutlinedIcon from '@material-ui/icons/ThumbDownAltOutlined';
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import { getGorev, gorevTamamla } from '../../services/gorev.service'
import CallMadeIcon from '@material-ui/icons/CallMade';
import { onayla, reddet, iptalet } from '../../services/onay.service'
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt'


var qs = require('qs');
moment.locale('tr')

const OnayFilter = props => {
    const { onQuery, filterRef } = props
    const ekip = useSelector(state => state.auth.ekip)
    const users = useSelector(state => state.auth.users)
    const user = useSelector(state => state.auth.user)
    const filter = useSelector(state => state.common.filter)
    const dispatch = useDispatch()
    const [firmalar, setFirmalar] = useState([])

    return (
        <div className='bg-white p-2 rounded shadow flex justify-between'>
            <div className='flex flex-grow space-x-2'>
                <div className='w-1/6'>
                    <TextField
                        label='Durum'
                        fullWidth
                        variant='outlined'
                        size='small'
                        select
                        value={filter.durum || null}
                        onChange={e => {
                            dispatch(Actions.setFilter({ ...filter, durum: e.target.value }))
                        }}
                    >
                        <MenuItem key={'hepsi'} value={'hepsi'}>Hepsi</MenuItem>
                        <MenuItem key={'acik'} value={'acik'}>Açık</MenuItem>
                        <MenuItem key={'onaylanmis'} value={'onaylanmis'}>Onaylanmış</MenuItem>
                        <MenuItem key={'reddedilmis'} value={'reddedilmis'}>Reddedilmiş</MenuItem>
                        <MenuItem key={'iptaledilmis'} value={'iptaledilmis'}>İptal Edilmiş</MenuItem>
                    </TextField>
                </div>
                <div className='w-1/6'>
                    <TextField
                        label='Talep'
                        fullWidth
                        variant='outlined'
                        size='small'
                        select
                        value={filter.talep || null}
                        onChange={e => {
                            dispatch(Actions.setFilter({ ...filter, talep: e.target.value }))
                        }}
                    >
                        <MenuItem key={'hepsi'} value={'hepsi'}>Hepsi</MenuItem>
                        <MenuItem key={'bendenIstenen'} value={'bendenIstenen'}>Benden İstenenler</MenuItem>
                        <MenuItem key={'benimTaleplerim'} value={'benimTaleplerim'}>Benim Taleplerim</MenuItem>
                    </TextField>
                </div>
                <div className='w-1/6'>
                    <TextField
                        label='Talep Eden'
                        fullWidth
                        variant='outlined'
                        size='small'
                        select
                        value={filter.talepeden || null}
                        onChange={e => {
                            dispatch(Actions.setFilter({ ...filter, talepeden: e.target.value }))
                            //setForm(prev => { return { ...prev, sorumlu: e.target.value } })
                        }}
                    >
                        {[user.key, ...ekip].map((e, i) => {
                            if (!users) {
                                return <></>
                            }
                            let user = users?.find(u => u.key == e)
                            return (<MenuItem key={i} value={user._id}>{user.username}</MenuItem>)
                        })}
                    </TextField>
                </div>
                <div className='w-1/6'>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={trLocale}>
                        <KeyboardDatePicker
                            //error={errors && errors.start}
                            //helperText={errors && errors.start}
                            autoOk
                            fullWidth
                            clearable
                            variant="inline"
                            inputVariant="outlined"
                            label="Tarih"
                            size='small'
                            format="dd.MM.yyyy"
                            maxDate={filter.bitisTarih || undefined}
                            InputAdornmentProps={{ position: 'start' }}
                            value={filter.baslangicTarih || null}
                            onChange={(e) =>
                                dispatch(Actions.setFilter({ ...filter, baslangicTarih: e }))
                                //setForm(prev => { return { ...prev, baslangicTarih: e } })
                            }
                        />
                    </MuiPickersUtilsProvider>
                </div>
                <div className='w-1/6'>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={trLocale}>
                        <KeyboardDatePicker
                            //error={errors && errors.start}
                            //helperText={errors && errors.start}
                            autoOk
                            fullWidth
                            clearable
                            variant="inline"
                            inputVariant="outlined"
                            label="Tarih"
                            size='small'
                            format="dd.MM.yyyy"
                            minDate={filter.baslangicTarih || undefined}
                            InputAdornmentProps={{ position: 'start' }}
                            value={filter.bitisTarih || null}
                            onChange={(e) =>
                                dispatch(Actions.setFilter({ ...filter, bitisTarih: e }))
                                //setForm(prev => { return { ...prev, bitisTarih: e } })
                            }
                        />
                    </MuiPickersUtilsProvider>
                </div>
            </div>
            <div className='flex space-x-2 justify-end'>
                <Button variant='outlined' color='primary' size='small' onClick={() => {
                    dispatch(Actions.setFilter({
                        baslangicTarih: null,
                        bitisTarih: null,

                        durum: null,
                        talep: null,
                        talepeden: null
                    }))
                    //firmaRef.current.clear()
                }
                }>Temizle</Button>
                <Button variant='contained' color='primary' size='small' onClick={() => onQuery({})}>Sorgula</Button>
            </div>
        </div>
    )
}

const OnayDetail = props => {
    const { onay } = props
    const [ref, setRef] = useState(null)
    const [baslik, setBaslik] = useState('')


    const load = async () => {
        if (onay.refCollection == 'Gorev') {
            let gorev = await getGorev(onay.referenceItem)
            setRef(gorev)
            setBaslik(gorev.baslik)
        }
    }

    useEffect(() => {
        load()
    }, [])

    return (
        <div className='bg-gray-50 p-2'>
            <div className='bg-white border shadow rounded p-4 flex'>
                <div className='w-1/3 flex-col border-r-2'>
                    <h2 className='font-semibold my-4'>{onay.refCollection}</h2>
                    <div className='flex space-x-4 items-center justify-between p-2'>
                        <h2 className='font-light'>{baslik}</h2>
                        <IconButton
                            size='small'
                            className='focus:outline-none'
                            onClick={e => {
                                e.preventDefault()

                                if (onay.refCollection == 'Gorev') {
                                    props.router.push(`/gorev/${onay.referenceItem}`)
                                }
                            }}
                        >
                            <CallMadeIcon fontSize='small' />
                        </IconButton>
                    </div>
                </div>
                <div className='w-1/3 flex-col border-r-2'>
                    <h2 className='text-center font-semibold text-lg'>Talep edilen degisiklik</h2>
                    <div className='w-5/6'>
                        {onay.diff?.map((key, i) => {
                            return (
                                <div className='flex-col p-2'>
                                    <h2 className='font-normal text-center'>{key.field}</h2>
                                    <div className='flex justify-between items-center space-x-4'>
                                        <h2 className='w-1/2 font-extralight text-xs'>{key.prev}</h2>
                                        <ArrowRightAltIcon />
                                        <h2 className='w-1/2 font-extralight text-xs'>{key.next}</h2>
                                    </div>
                                </div>
                            )
                        })}

                    </div>
                </div>
                <div className='w-1/3'></div>
            </div>
        </div>
    )
}


const Onaylar = (props) => {
    const dispatch = useDispatch()
    const permissions = useSelector(state => state.auth.permissions)
    const token = useSelector(state => state.auth.expertoToken)
    const user = useSelector(state => state.auth.user)
    const ekip = useSelector(state => state.auth.ekip)
    const users = useSelector(state => state.auth.users)
    const [tableFilter, setTableFilter] = useState(null)
    const [queryFilter, setQueryFilter] = useState(null) // Sorguda bazi query parametreleri filter altinda gonderilemiyor
    //const [page, setPage] = useState(0)
    const tableRef = React.createRef();
    const filterRef = React.createRef();
    const [pageLoad, setPageLoad] = useState(false)
    const [page, setPage] = useState(0)
    const [usePrevPage, setUsePrevPage] = useState(false)
    const [resetPage, setResetPage] = useState(false)
    const prevUrl = useSelector(state => state.common.prevUrl)
    const prevPage = useSelector(state => state.common.prevPage)
    const gFilter = useSelector(state => state.common.filter)
    const pageHeaderBack = useSelector(state => state.common.pageHeaderBack)
    const [sort, setSort] = useState(null)
    const [anchorEl, setAnchorEl] = useState(null)
    const [seciliKayit, setSeciliKayit] = useState(null)
    const [table, setTable] = useState(null)
    const [showGorevBasarisiz, setShowGorevBasarisiz] = useState(false)
    const [sonucAciklama, setSonucAciklama] = useState('')



    const convertFilterToQuery = (filter) => {
        debugger
        let tf = {
            //vfirma: filter.firma?.key || undefined,
            //sonuc: filter.sonuc || undefined,
            //vsorumlu: filter.sorumlu || undefined, //? filter.sorumlu.key : undefined
            //icGorevMi: filter.icGorev
        }
        let qf = {}

        if (filter.durum == 'acik') {
            tf.onayDurumu = 'acik'
        }
        else if (filter.durum == 'onaylanmis') {
            tf.onayDurumu = 'onay'
        }
        else if (filter.durum == 'reddedilmis') {
            tf.onayDurumu = 'red'
        }
        else if (filter.durum == 'iptaledilmis') {
            tf.onayDurumu = 'iptal'
        }
        else if (filter.durum == 'hepsi') {
            delete tf.onayDurumu
        }


        if (filter.talep == 'bendenIstenen') {
            tf.onayIstenen = user._id
        }
        else if (filter.talep == 'benimTaleplerim') {
            tf.onayIsteyen = user._id
        }
        else if (filter.talep == 'hepsi') {
            tf.filter = {
                $or : [
                    {onayIstenen: user._id},
                    {onayIsteyen: user._id}
                ]
            }
            //tf.onayIsteyen = user._id
            //tf.onayIstenen = user._id
        }

        if (filter.talepeden) {
            tf.onayIsteyen = filter.talepeden
        }


        if (filter.baslangicTarih && !filter.bitisTarih) {
            tf.filter = {
                istemeTarihi: {
                    $gte: filter.baslangicTarih
                }
            }
        }
        else if (!filter.baslangicTarih && filter.bitisTarih) {
            tf.filter = {
                istemeTarihi: {
                    $lte: filter.bitisTarih
                }
            }
        }
        else if (filter.baslangicTarih && filter.bitisTarih) {
            tf.filter = {
                istemeTarihi: {
                    $gte: filter.baslangicTarih,
                    $lte: filter.bitisTarih
                }
            }
        }

        //if (filter.ekip || filter.sorumlu == '') {
        //    tf.vsorumlu = ekip.join(',')
       // }

        //if (filter.createdBy) {
        //    tf.vcreatedBy = filter.createdBy
        //    delete tf.vsorumlu
        //}

        return tf
    }

    const convertRouterToQuery = (rq) => {
        let f = {}
        let filter = {}
        setSort(null)
        debugger
        if (rq == 'onaylarim') {
            f = convertFilterToQuery({
                durum: 'acik',
                onayIstenen: user._id
            })
            filter = {
                durum: 'acik',
                talep: 'bendenIstenen'
            }
        }
        dispatch(Actions.setFilter(filter))
        return f
    }

    useEffect(() => {
        if (prevUrl == '/onay/[id]') {
            setUsePrevPage(true)
            if (pageHeaderBack) {
                setTableFilter(convertFilterToQuery(gFilter))
                //convertFilterToQuery(gFilter)
            }
            else {
                setTableFilter(convertRouterToQuery(props.router.query.filter))
                //convertRouterToQuery(props.router.query.filter)
            }
        }
        else {
            setTableFilter(convertRouterToQuery(props.router.query.filter))
            //convertRouterToQuery(props.router.query.filter)
            setResetPage(true)
            setUsePrevPage(false)
        }
        dispatch(Actions.resetPageHeaderBack())
        dispatch(Actions.setPrevUrl(props.router.route))
        //return () => {
        //    dispatch(Actions.setPrevUrl(props.router.route))
        //}
    }, [props.router.query])

    useEffect(() => {
        if (tableRef.current) {
            tableRef.current.onQueryChange()
        }
    }, [tableFilter])

    useEffect(() => {
        setTable(tableRef.current)
    }, [tableRef.current])

    return (
        <div>
            <PageHeader
                title='Onaylar'
                buttons={[
                ]} />
            <div className='p-2 flex-col space-y-2'>
                <OnayFilter
                    onQuery={() => {
                        setResetPage(true)
                        setUsePrevPage(false)
                        setTableFilter(convertFilterToQuery(gFilter))
                    }} />
                <MaterialTable
                    title='Onaylar'
                    tableRef={tableRef}
                    columns={[
                        {
                            title: 'Talep Eden', field: '', render: row => {
                                let _user = users.find(u => u._id == row.onayIsteyen)
                                return (
                                    <div className='flex items-center space-x-2'>
                                        <Avatar className='h-4 w-4 border-2 border-gray-400 shadow' src={_user?.image} />
                                        <h2 className=''>{_user.username}</h2>
                                    </div>
                                )
                            }
                        },
                        {
                            title: 'Talep Tarihi', field: '', render: row => { return moment(row.istemeTarihi).format("HH:mm DD.MM.YYYY") }
                        },
                        {
                            title: 'Onaylayan', field: '', render: row => {
                                if (!row.onaylayan) {
                                    return <></>
                                }
                                let _user = users.find(u => u._id == row.onaylayan)
                                return (
                                    <div className='flex items-center space-x-2'>
                                        <Avatar className='h-4 w-4 border-2 border-gray-400 shadow' src={_user?.image} />
                                        <h2 className=''>{_user.username}</h2>
                                    </div>
                                )
                            }
                        },
                        {
                            title: 'Onay Tarihi', field: '', render: row => {
                                if (!row.onayTarihi) {
                                    return <></>
                                }
                                return moment(row.onayTarihi).format("HH:mm DD.MM.YYYY")
                            }
                        },
                        { title: 'Konu', field: 'refCollection' },
                        {
                            title: 'Durum', field: 'sonuc', render: row => {
                                if (row.onayDurumu == 'acik') {
                                    return (
                                        <div className='flex items-center'>
                                            <div className='bg-blue-500 w-32 text-center p-1 rounded shadow text-white'>AÇIK</div>
                                            <IconButton size='small' className='focus:outline-none' onClick={(e) => {
                                                setSeciliKayit(row)
                                                setAnchorEl(e.currentTarget)
                                            }}>
                                                <DoubleArrowIcon />
                                            </IconButton>
                                        </div>
                                    )
                                }
                                else if (row.onayDurumu == 'onay') {
                                    return <div className='bg-green-500 w-32 text-center p-1 rounded shadow text-white'>ONAY</div>
                                }
                                else if (row.onayDurumu == 'iptal') {
                                    return <div className='bg-gray-500 w-32 text-center p-1 rounded shadow text-white'>İPTAL</div>
                                }
                                else if (row.onayDurumu == 'red') {
                                    return <div className='bg-red-500 w-32 text-center p-1 rounded shadow text-white'>RED</div>
                                }
                            }
                        },
                    ]}
                    actions={[
                        {
                            icon: () => <ArrowForwardIcon />,
                            tooltip: 'Detay',
                            onClick: (event, rowData) => {
                                dispatch(Actions.setPrevPage(page))
                                props.router.push(`/onay/${rowData._id}`)
                            }
                        }
                    ]}
                    options={{
                        padding: 'dense',
                        pageSize: 10,
                        pageSizeOptions: [10, 25, 50, 100],
                    }}
                    localization={{
                        body: { emptyDataSourceMessage: 'Gösterilecek kayıt yok' },
                        toolbar: { searchPlaceholder: 'Ara' }
                    }}

                    data={query => {
                        return new Promise((resolve, reject) => {
                            let p = query.page
                            if (usePrevPage) {
                                p = prevPage
                            }
                            if (resetPage) {
                                p = 0
                            }
                            let _q = {
                                skip: p * query.pageSize,
                                limit: query.pageSize,
                                //populate: 'firma,sorumlu',
                                //baslik: `/${query.search}/i`,
                                //filter: tableFilter,
                                sort: sort || '-istemeTarihi'
                            }
                            if (query.orderBy) {
                                _q.sort = query.orderBy ? `${query.orderDirection == 'asc' ? '' : '-'}${query.orderBy.field}` : ''
                            }
                            let q = qs.stringify({ ..._q, ...tableFilter }
                                //, { skipNulls: true }
                            )
                            api.get(`/onay?${q}`,
                                { headers: { Authorization: `Bearer ${token}` } })
                                .then(result => {
                                    if (!result.data.data) {
                                        reject()
                                    }
                                    setPage(p)
                                    setResetPage(false)
                                    setPageLoad(false)
                                    setUsePrevPage(false)
                                    resolve({
                                        data: result.data.data.data,
                                        page: result.data.data.page / query.pageSize,
                                        totalCount: result.data.data.total,
                                    })
                                })
                                .catch(e => alert(JSON.stringify(e)))
                        })
                    }}

                    detailPanel={row => {
                        return <OnayDetail onay={row} {...props} />
                    }}
                />
            </div>

            <div>
                <Menu
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={() => { setAnchorEl(null) }}
                >
                    <MenuItem onClick={() => {
                        onayla(seciliKayit._id)
                            .then(resp => { })
                        //gorevTamamla(seciliKayit._id, {
                        //    sonuc: 'basarili',
                        //    vkapatan: user.key,
                        //    kapatmaTarihi: new Date()
                        //})
                        //    .then(resp => {
                        //        if (table) {
                        //            table.onQueryChange()
                        //        }
                        //    })
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <ThumbUpAltOutlinedIcon />
                            <h2>Onayla</h2>
                        </div>
                    </MenuItem>
                    <MenuItem onClick={() => {
                        reddet(seciliKayit._id)
                            .then(resp => {

                            })
                        //setShowGorevBasarisiz(true)
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <ThumbDownAltOutlinedIcon />
                            <h2>Reddet</h2>
                        </div>
                    </MenuItem>
                </Menu>

                <Dialog
                    open={showGorevBasarisiz}
                    onClose={() => setShowGorevBasarisiz(false)}
                >
                    <DialogTitle>Görevi Başarısız Kapatma</DialogTitle>
                    <DialogContent>
                        <DialogContentText>Görevin başarısızlık nedenini açıklayın</DialogContentText>
                        <TextField
                            autoFocus
                            label='Başarısızlık Nedeni'
                            variant='outlined'
                            fullWidth
                            rows={3}
                            rowsMax={Infinity}
                            multiline
                            value={sonucAciklama}
                            onChange={(e) => {
                                setSonucAciklama(e.target.value)
                            }}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button variant='contained' color='primary' onClick={() => {
                            setSonucAciklama('')
                            setShowGorevBasarisiz(false)
                        }}>Vazgeç</Button>
                        <Button variant='contained' color='primary' onClick={() => {
                            gorevTamamla(seciliKayit._id, {
                                sonuc: 'basarisiz',
                                sonucAciklama: sonucAciklama,
                                vkapatan: user.key,
                                kapatmaTarihi: new Date()
                            })
                                .then(resp => {
                                    if (table) {
                                        table.onQueryChange()
                                    }
                                })
                            setAnchorEl(null)
                            setSonucAciklama('')
                            setShowGorevBasarisiz(false)
                        }}>Tamam</Button>
                    </DialogActions>
                </Dialog>

            </div>


        </div>
    )
}

export default withRouter(Onaylar)
