import { Button, Grid, makeStyles } from '@material-ui/core'
import React, { useState } from 'react'
import PageHeader from '../../components/pageHeader'
import { parseCookies, setCookie, destroyCookie } from 'nookies'
const { uuid } = require('uuidv4');
import { getGorusme, saveGorusme, updateGorusme } from '../../services/gorusme.service'
import { getFirmaCalisanKisiler, searchFirmaKisiler, searchFirmalar } from '../../services/firma.service'
import { useSelector } from 'react-redux'
import { useRouter } from "next/router";
import GorusmeInfo from '../../components/gorusme/GorusmeInfo'
import { validateGorusme } from '../../validators/gorusme.validator'

const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(2)
    },
}));

const Gorusme = props => {
    const router = useRouter();
    const classes = useStyles()
    const user = useSelector(state => state.auth.user)
    const { newGorusme, gorusme } = props
    const [form, setForm] = useState(
        {
            ...gorusme,
            vcreatedBy: user.key,
            createdAt: new Date()
        } || {
            vcreatedBy: user.key,
            createdAt: new Date()
        }
    )
    const [errors, setErrors] = useState(null)
    const [firmaKisiler, setFirmaKisiler] = useState(props.firmaKisiler || [])

    const getMusteriPersoneller = async (firma) => {
        let kisiler = await getFirmaCalisanKisiler(firma)
        if (kisiler) {
            setFirmaKisiler(kisiler)
        }
    }


    const onGorusmeChange = async (value) => {
        let f = { ...form }
        if (Array.isArray(value)) {
            for (let v of value) {
                let val = v.value
                if (v.field == 'vfirma') {
                    setFirmaKisiler([])
                    getMusteriPersoneller(v.value._id)
                    val = v.value.key
                }
                f[v.field] = val
                setForm(f)
            }
        }
        else {
            let val = value.value
            if (value.field == 'vfirma') {
                setFirmaKisiler([])
                getMusteriPersoneller(value.value._id)
                val = value.value.key
            }
            f[value.field] = val
            setForm(f)
        }
    }

    return (
        <>
            <PageHeader
                title= { newGorusme ?  'Yeni Görüşme' : 'Görüşme'}
                buttons={[
                    <Button
                        variant='contained'
                        color='primary'
                        disabled={!newGorusme}
                        onClick={() => {
                            let _errors = validateGorusme(form)
                            if (_errors) {
                                setErrors(_errors)
                                return
                            }
                            if (newGorusme) {
                                saveGorusme(form)
                            }
                            else {
                                updateGorusme(form)
                            }
                            router.back()
                        }}>Kaydet</Button>
                ]}
            />
            <div className={classes.root}>
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={8}>
                        <GorusmeInfo errors={errors} gorusme={form} kisiler={firmaKisiler} onChange={onGorusmeChange} />
                    </Grid>
                </Grid>
            </div>
        </>
    )
}

Gorusme.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    let newGorusme = ctx.query.id == 'new'
    let firma = null
    let firmaKisiler = null
    if (ctx.query.firma) {
        let firmalar = await searchFirmalar(`key=${ctx.query.firma}`)
        if (firmalar.length > 0) {
            firma = firmalar[0]
        }

        firmaKisiler = await searchFirmaKisiler(`vfirma=${ctx.query.firma}`)
    }

    let gorusme = {
        key: uuid(),
        not: '',
        tarih: new Date(),
        vfirma: '',
        vkisi: '',
        vuser: '',
        kanal: '',
        icGorusmeMi: false,
        vcreatedBy: '',
        firma: firma || null,
    }

    if (!newGorusme) {
        gorusme = await getGorusme(ctx.query.id)
    }

    return {
        newGorusme: newGorusme,
        gorusme: gorusme,
        firmaKisiler: firmaKisiler || null
    }
}

export default Gorusme