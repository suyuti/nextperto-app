import React, { useState, useRef, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'
import { Button, Box, IconButton, TextField, MenuItem, Avatar, Menu, Dialog, DialogTitle, DialogContent, DialogActions, DialogContentText } from '@material-ui/core'
import { withRouter } from 'next/router';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import RemoteAutoComplete from '../../components/autocomplete';
import { searchFirmalar } from '../../services/firma.service'
import trLocale from "date-fns/locale/tr";
import PageHeader from '../../components/pageHeader';
import MaterialTable from 'material-table';
import api from '../../services/axios.service';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import * as Actions from '../../redux/actions'
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import ThumbDownAltOutlinedIcon from '@material-ui/icons/ThumbDownAltOutlined';
import { gorevTamamla } from '../../services/gorev.service'
var qs = require('qs');
moment.locale('tr')

const GorusmeFilter = props => {
    const { onQuery, filterRef } = props
    const ekip = useSelector(state => state.auth.ekip)
    const users = useSelector(state => state.auth.users)
    const user = useSelector(state => state.auth.user)
    const filter = useSelector(state => state.common.filter)
    const dispatch = useDispatch()
    const [firmalar, setFirmalar] = useState([])

    return (
        <div className='bg-white p-2 rounded shadow flex space-x-2'>
            <div className='w-1/6'>
                <RemoteAutoComplete
                    results={firmalar}
                    size='small'
                    label='Firma'
                    labelField='marka'
                    value={filter.firma || ''}
                    onSelected={e => {
                        dispatch(Actions.setFilter({ ...filter, firma: e }))
                        //setForm(prev => { return { ...prev, firma: e } })
                    }}
                    fetchRemote={(searchText) => {
                        searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                            setFirmalar(resp)
                        })
                    }}
                />
            </div>
            <div className='w-1/6'>
                <TextField
                    label='Görüşme Yapan'
                    fullWidth
                    variant='outlined'
                    size='small'
                    select
                    value={filter.vcreatedBy || ''}
                    onChange={e => {
                        dispatch(Actions.setFilter({ ...filter, vcreatedBy: e.target.value }))
                    }}
                >
                    {[user.key, ...ekip].map((e, i) => {
                        let user = users.find(u => u.key == e)
                        return (<MenuItem key={i} value={user.key}>{user.username}</MenuItem>)
                    })}
                </TextField>
            </div>
            <div className='w-1/6'>
                <TextField
                    label='Kanal'
                    fullWidth
                    variant='outlined'
                    size='small'
                    select
                    value={filter.kanal || ''}
                    onChange={e => {
                        dispatch(Actions.setFilter({ ...filter, kanal: e.target.value }))
                    }
                    }
                >
                    <MenuItem key='hepsi' value='hepsi'>HEPSI</MenuItem>
                    <MenuItem key='Telefon' value='Telefon'>TELEFON</MenuItem>
                    <MenuItem key='Mail' value='Mail'>MAİL</MenuItem>
                    <MenuItem key='Yüzyüze' value='Yüzyüze'>YÜZYÜZE</MenuItem>
                    <MenuItem key='Whatsapp' value='Whatsapp'>WHATSAPP</MenuItem>
                    <MenuItem key='VideoKonferans' value='VideoKonferans'>VİDEO KONFERANS</MenuItem>
                </TextField>
            </div>
            <div className='w-1/6'>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={trLocale}>
                    <KeyboardDatePicker
                        //error={errors && errors.start}
                        //helperText={errors && errors.start}
                        autoOk
                        fullWidth
                        clearable
                        variant="inline"
                        inputVariant="outlined"
                        label="Tarih"
                        size='small'
                        format="dd.MM.yyyy"
                        maxDate={filter.bitisTarih || undefined}
                        InputAdornmentProps={{ position: 'start' }}
                        value={filter.baslangicTarih || null}
                        onChange={(e) =>
                            dispatch(Actions.setFilter({ ...filter, baslangicTarih: e }))
                            //setForm(prev => { return { ...prev, baslangicTarih: e } })
                        }
                    />
                </MuiPickersUtilsProvider>
            </div>
            <div className='w-1/6'>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={trLocale}>
                    <KeyboardDatePicker
                        //error={errors && errors.start}
                        //helperText={errors && errors.start}
                        autoOk
                        fullWidth
                        clearable
                        variant="inline"
                        inputVariant="outlined"
                        label="Tarih"
                        size='small'
                        format="dd.MM.yyyy"
                        minDate={filter.baslangicTarih || undefined}
                        InputAdornmentProps={{ position: 'start' }}
                        value={filter.bitisTarih || null}
                        onChange={(e) =>
                            dispatch(Actions.setFilter({ ...filter, bitisTarih: e }))
                            //setForm(prev => { return { ...prev, bitisTarih: e } })
                        }
                    />
                </MuiPickersUtilsProvider>
            </div>
            <div className='w-1/6 flex space-x-2 justify-end'>
                <Button variant='outlined' color='primary' size='small' onClick={() => {
                    dispatch(Actions.setFilter({
                        baslangicTarih: null,
                        bitisTarih: null,
                        kanal: '',
                        vcreatedBy: '',
                        firma: ''
                    }))
                    //firmaRef.current.clear()
                }
                }>Temizle</Button>
                <Button variant='contained' color='primary' size='small' onClick={() => onQuery({})}>Sorgula</Button>
            </div>
        </div>
    )
}


const Gorusmeler = (props) => {
    const dispatch = useDispatch()
    const permissions = useSelector(state => state.auth.permissions)
    const token = useSelector(state => state.auth.expertoToken)
    const user = useSelector(state => state.auth.user)
    const ekip = useSelector(state => state.auth.ekip)
    const users = useSelector(state => state.auth.users)
    const [tableFilter, setTableFilter] = useState(null)
    const [queryFilter, setQueryFilter] = useState(null) // Sorguda bazi query parametreleri filter altinda gonderilemiyor
    //const [page, setPage] = useState(0)
    const tableRef = React.createRef();
    const filterRef = React.createRef();
    const [pageLoad, setPageLoad] = useState(false)
    const [page, setPage] = useState(0)
    const [usePrevPage, setUsePrevPage] = useState(false)
    const [resetPage, setResetPage] = useState(false)
    const prevUrl = useSelector(state => state.common.prevUrl)
    const prevPage = useSelector(state => state.common.prevPage)
    const gFilter = useSelector(state => state.common.filter)
    const pageHeaderBack = useSelector(state => state.common.pageHeaderBack)
    const [sort, setSort] = useState(null)
    const [anchorEl, setAnchorEl] = useState(null)
    const [seciliKayit, setSeciliKayit] = useState(null)
    const [table, setTable] = useState(null)
    const [showGorevBasarisiz, setShowGorevBasarisiz] = useState(false)
    const [sonucAciklama, setSonucAciklama] = useState('')



    const convertFilterToQuery = (filter) => {
        debugger
        let tf = {
            vfirma: filter.firma?.key || undefined,
            kanal: filter.kanal || undefined,
            vcreatedBy: filter.vcreatedBy || undefined //? filter.sorumlu.key : undefined
        }
        let qf = {}

        if (filter.kanal == 'hepsi') {
            delete tf.kanal
        }

        if (filter.baslangicTarih && !filter.bitisTarih) {
            tf.filter = {
                tarih: {
                    $gte: filter.baslangicTarih
                }
            }
        }
        else if (!filter.baslangicTarih && filter.bitisTarih) {
            tf.filter = {
                tarih: {
                    $lte: filter.bitisTarih
                }
            }
        }
        else if (filter.baslangicTarih && filter.bitisTarih) {
            tf.filter = {
                tarih: {
                    $gte: filter.baslangicTarih,
                    $lte: filter.bitisTarih
                }
            }
        }

        if (filter.ekip || filter.vcreatedBy == '') {
            tf.vcreatedBy = ekip.join(',')
        }

        //if (filter.createdBy) {
        //    tf.vcreatedBy = filter.createdBy
        //    delete tf.vsorumlu
        //}

        return tf
    }

    const convertRouterToQuery = (rq) => {
        let f = {}
        let filter = {}
        setSort(null)

        // TODO

        //if (rq == 'acikgorevlerim') {
            f = convertFilterToQuery({
                //sonuc: 'acik',
                vcreatedBy: user.key,
            })
            filter = {
                //sonuc: 'acik',
                vcreatedBy: user.key,
                baslangicTarih: null,
                bitisTarih: null
            }
        //}
        dispatch(Actions.setFilter(filter))
        return f
    }

    useEffect(() => {
        if (prevUrl == '/gorusme/[id]') {
            setUsePrevPage(true)
            if (pageHeaderBack) {
                setTableFilter(convertFilterToQuery(gFilter))
            }
            else {
                setTableFilter(convertRouterToQuery(props.router.query.filter))
            }
        }
        else {
            setTableFilter(convertRouterToQuery(props.router.query.filter))
            setResetPage(true)
            setUsePrevPage(false)
        }
        dispatch(Actions.resetPageHeaderBack())
        dispatch(Actions.setPrevUrl(props.router.route))
    }, [props.router.query])

    useEffect(() => {
        if (tableRef.current) {
            tableRef.current.onQueryChange()
        }
    }, [tableFilter])

    useEffect(() => {
        setTable(tableRef.current)
    }, [tableRef.current])

    return (
        <div>
            <PageHeader
                title='Görüşmeler'
                buttons={[
                    {
                        title: 'Yeni',
                        disabled: !permissions ? true : !permissions.find(p => p.key == 'gorusme')?.create,
                        onClick: (e) => {
                            e.preventDefault();
                            props.router.push("/gorusme/[id]", `/gorusme/new`);
                        }
                    },
                ]} />
            <div className='p-2 flex-col space-y-2'>
                <GorusmeFilter
                    onQuery={() => {
                        setResetPage(true)
                        setUsePrevPage(false)
                        setTableFilter(convertFilterToQuery(gFilter))
                    }} />
                <MaterialTable
                    title='Görüşmeler'
                    tableRef={tableRef}
                    columns={[
                        { title: 'Konu', field: 'not' },
                        {
                            title: 'Firma', field: 'firma.marka', render: row => {
                                if (row.firma == null) {
                                    return (<div className='text-red-600 font-light'>
                                        İç Görüşme
                                    </div>)
                                }
                                else {
                                    return <h2>{row.firma.marka}</h2>
                                }
                            }
                        },
                        { title: 'Tarih', field: 'tarih', render: row => moment(row.tarih).format('DD.MM.YYYY HH:mm') },
                        { title: 'Kanal', field: 'kanal' },
                        {
                            field: 'kisi.adi', title: 'Görüşülen', render: row => {
                                if (row.icGorusmeMi) {
                                    let u = users.find(u => u.key == row.vkisi)
                                    return (<div className='flex items-center space-x-2'>
                                        <img className='h-4 w-4 rounded-full border bg-gray-400' src={u?.image}></img>
                                        <h2 className=''>{u.username}</h2>
                                    </div>)
                                }
                                else {
                                    return (<div className='flex space-x-2 items-center '>
                                        <img className='h-8 w-8 rounded-full border bg-gray-400' src={row.kisi?.image}></img>
                                        <div className='flex-col'>
                                            <h2 className='font-light'>{`${row.kisi?.adi} ${row.kisi?.soyadi}`}</h2>
                                            <h2 className='font-light text-xs'>{`${row.kisi? row.kisi.unvani : ''}`}</h2>
                                            <h2 className='font-light text-xs'>{row.kisi?.cepTelefon}</h2>
                                        </div>
                                    </div>
                                    )
                                }
                            }
                        },
                        {
                            field: 'gorusulen',
                            title: 'Görüşme Yapan',
                            render: row => {
                                let u = users.find(u => u.key == row.vcreatedBy)
                                return (
                                    <div className='flex items-center space-x-2'>
                                        <img className='h-8 w-8 rounded-full  bg-gray-400 border-2 border-gray-400 shadow' src={u?.image}></img>
                                        <h2 className=''>{u.username}</h2>
                                    </div>
                                )
                            }
                        },
                    ]}
                    actions={[
                        {
                            icon: () => <ArrowForwardIcon />,
                            tooltip: 'Detay',
                            onClick: (event, rowData) => {
                                dispatch(Actions.setPrevPage(page))
                                props.router.push(`/gorusme/${rowData._id}`)
                            }
                        }
                    ]}
                    options={{
                        padding: 'dense',
                        pageSize: 100,
                        pageSizeOptions: [10, 25, 50, 100],
                        //grouping: true
                    }}
                    localization={{
                        body: { emptyDataSourceMessage: 'Gösterilecek kayıt yok' },
                        toolbar: { searchPlaceholder: 'Ara' }
                    }}

                    data={query => {
                        return new Promise((resolve, reject) => {
                            let p = query.page
                            if (usePrevPage) {
                                p = prevPage
                            }
                            if (resetPage) {
                                p = 0
                            }
                            let _q = {
                                skip: p * query.pageSize,
                                limit: query.pageSize,
                                populate: 'firma,kisi',
                                not: `/${query.search}/i`,
                                //filter: tableFilter,
                                sort: sort || '-tarih'
                            }
                            if (query.orderBy) {
                                _q.sort = query.orderBy ? `${query.orderDirection == 'asc' ? '' : '-'}${query.orderBy.field}` : ''
                            }
                            let q = qs.stringify({ ..._q, ...tableFilter })
                            api.get(`/gorusme?${q}`,
                                { headers: { Authorization: `Bearer ${token}` } })
                                .then(result => {
                                    if (!result.data.data) {
                                        reject()
                                    }
                                    setPage(p)
                                    setResetPage(false)
                                    setPageLoad(false)
                                    setUsePrevPage(false)
                                    resolve({
                                        data: result.data.data.data,
                                        page: result.data.data.page / query.pageSize,
                                        totalCount: result.data.data.total,
                                    })
                                })
                                .catch(e => alert(JSON.stringify(e)))
                        })
                    }}
                />
            </div>

            <div>
                <Menu
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={() => { setAnchorEl(null) }}
                >
                    <MenuItem onClick={() => {
                        gorevTamamla(seciliKayit._id, {
                            sonuc: 'basarili',
                            vkapatan: user.key,
                            kapatmaTarihi: new Date()
                        })
                            .then(resp => {
                                if (table) {
                                    table.onQueryChange()
                                }
                            })
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <DoneIcon />
                            <h2>Görevi Başarılı Tamamla</h2>
                        </div>
                    </MenuItem>
                    <MenuItem onClick={() => {
                        setShowGorevBasarisiz(true)
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <ThumbDownAltOutlinedIcon />
                            <h2>Görevi Başarısız Tamamla</h2>
                        </div>
                    </MenuItem>
                    <MenuItem onClick={() => {
                        gorevTamamla(seciliKayit._id, {
                            sonuc: 'iptal',
                            vkapatan: user.key,
                            kapatmaTarihi: new Date()
                        })
                            .then(resp => {
                                if (table) {
                                    table.onQueryChange()
                                }
                            })
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <CloseIcon />
                            <h2>Görevi İptal Et</h2>
                        </div>
                    </MenuItem>
                </Menu>

                <Dialog
                    open={showGorevBasarisiz}
                    onClose={() => setShowGorevBasarisiz(false)}
                >
                    <DialogTitle>Görevi Başarısız Kapatma</DialogTitle>
                    <DialogContent>
                        <DialogContentText>Görevin başarısızlık nedenini açıklayın</DialogContentText>
                        <TextField
                            autoFocus
                            label='Başarısızlık Nedeni'
                            variant='outlined'
                            fullWidth
                            rows={3}
                            rowsMax={Infinity}
                            multiline
                            value={sonucAciklama}
                            onChange={(e) => {
                                setSonucAciklama(e.target.value)
                            }}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button variant='contained' color='primary' onClick={() => {
                            setSonucAciklama('')
                            setShowGorevBasarisiz(false)
                        }}>Vazgeç</Button>
                        <Button variant='contained' color='primary' onClick={() => {
                            gorevTamamla(seciliKayit._id, {
                                sonuc: 'basarisiz',
                                sonucAciklama: sonucAciklama,
                                vkapatan: user.key,
                                kapatmaTarihi: new Date()
                            })
                                .then(resp => {
                                    if (table) {
                                        table.onQueryChange()
                                    }
                                })
                            setAnchorEl(null)
                            setSonucAciklama('')
                            setShowGorevBasarisiz(false)
                        }}>Tamam</Button>
                    </DialogActions>
                </Dialog>

            </div>


        </div>
    )
}

export default withRouter(Gorusmeler)
