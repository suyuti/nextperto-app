import React, { useEffect, useState } from 'react'
import PageHeader from "../../components/pageHeader";
import TableView from '../../components/tableview';
import { Box, Button, ButtonGroup, makeStyles, Avatar, Typography, Grid } from '@material-ui/core';
import { useRouter } from "next/router";
import moment from 'moment'
import { useSelector } from 'react-redux'
import ToplantiSearchToolbar from '../../components/toplanti/ToplantiSearchToolbar';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    sorumlu: {
        display: 'flex',
        //flexFlow: 'column',
        alignItems: 'center'
    },
    sorumluAvatar: {
        marginRight: theme.spacing(1),
        width: theme.spacing(4),
        height: theme.spacing(4),
    },
}))


const Toplantilar = props => {
    const router = useRouter();
    const permissions = useSelector(state => state.auth.permissions)
    const token = useSelector(state => state.auth.expertoToken)
    const user = useSelector(state => state.auth.user)
    const users = useSelector(state => state.auth.users)
    const ekip = useSelector(state => state.auth.ekip)
    const classes = useStyles()

    const prepareQuery = (q) => {
        if (q.q == 'current') {
            return `status=acik&vorganizerKatilimcilar=${user.key}`
        }
        else if (q.q == 'past') {
            return `status=tamamlandi&vorganizerKatilimcilar=${user.key}`
        }
        else if (q.e == 'true') {
            return `vorganizerKatilimcilar=${ekip.map(u => u).join(',')}`
        }
    }

    const [filter, setFilter] = useState(prepareQuery(router.query))

    const columns = [
        { id: 'baslik', numeric: false, disablePadding: true, label: 'Konu', align: 'left' },
        { id: 'firma.marka', numeric: true, disablePadding: false, label: 'Firma', align: 'left' },
        { id: 'yer', numeric: true, disablePadding: false, label: 'Konum', align: 'left' },
        {
            id: 'createdBy', label: 'Oluşturan', render: row => {
                let _user = users?.find(u => u.key == row.vcreatedBy)
                return <>
                    <div className={classes.sorumlu}>
                        <Avatar className={classes.sorumluAvatar} src={_user?.image} />
                        <Typography variant='body2'>{_user?.username}</Typography>
                    </div>
                </>
            }
        },
        {
            id: 'start', numeric: true, disablePadding: false, label: 'Tarih', align: 'right',
            render: row => {
                return moment(row.start).format('DD.MM.YYYY')
            }
        },
        {
            id: 'saat', numeric: true, disablePadding: false, label: 'Saat', align: 'right'
        },
        {
            id: '', label: 'Durum',
            render: row => {
                let today = moment()
                let tarih = moment(row.start)
                let kararGirmeSonTarih = moment(row.start).add(2, 'days')

                let bugun = today.isSame(tarih, 'days')
                let gecikme = today.isAfter(kararGirmeSonTarih, 'days')
                let kararGirilmeTarihinde = today.isSameOrAfter(tarih, 'days') && today.isSameOrBefore(kararGirmeSonTarih, "days")

                if (bugun && row.status == 'acik') {
                    return <Box bgcolor="info.main" color="primary.contrastText" p={1}>Bugün</Box>
                }
                else if (gecikme && row.status == 'acik') {
                    return <Box bgcolor="error.main" color="primary.contrastText" p={1}>Gecikme</Box>
                }
                else if (kararGirilmeTarihinde && row.status == 'acik') {
                    return <Box bgcolor="warning.main" color="primary.contrastText" p={1}>Karar Girilebilir</Box>
                }
            }
        },
        { id: 'kategori', numeric: true, disablePadding: false, label: 'Kategori', align: 'left' },
        {
            render: row =>
                <ButtonGroup>
                    <Button
                        size='small'
                        variant='outlined'
                        color='primary'
                        onClick={() => {
                            router.push(`/toplanti/${row._id}`)
                        }}>Detay</Button>
                    <Button
                        size='small'
                        variant='outlined'
                        color='primary'
                        disabled={false}
                        onClick={() => {
                            router.push(`/toplanti/${row._id}/kararlar?firma=${row.firma._id}`)
                        }}>Kararlar</Button>

                </ButtonGroup>
        }
    ]

    const onData = async (page, perPage, searchText) => {

    }

    const onFilterSearch = (query) => {
        // query nesnesi arama yapabilmek icin query stringe donusturulur
        let q = query.status ? `status=${query.status}&` : ''
        if (query.firma) {
            q = q + `vfirma=${query.firma}&`
        }
        if (query.katilimci) {
            q = q + `vorganizerKatilimcilar=${query.katilimci}&`
        }

        if (query.baslangicTarihi && !query.bitisTarihi) {
            q = q + `start>=${moment(query.baslangicTarihi).toISOString()}&`
        }
        else if (!query.baslangicTarihi && query.bitisTarihi) {
            q = q + `start<=${moment(query.bitisTarihi).toISOString()}&`
        }
        else if (query.baslangicTarihi && query.bitisTarihi) {
            q = q + `filter={"$and":[{"start":{"$gte":"${moment(query.baslangicTarihi).toISOString()}"}},{"start":{"$lte":"${moment(query.bitisTarihi).toISOString()}"}}]}`
        }
        setFilter(q)
    }

    useEffect(() => {
        setFilter(prepareQuery(router.query))
    }, [router.query])

    return (
        <>
            <PageHeader
                title='Toplantılar'
                buttons={[
                    {
                        title: 'Yeni',
                        disabled: !permissions ? true : !permissions.find(p => p.key == 'toplanti')?.create,
                        onClick: (e) => {
                            e.preventDefault();
                            router.push("/toplanti/[id]", `/toplanti/new`);
                        }
                    },
                ]} />
            <div className={classes.root}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <ToplantiSearchToolbar
                            //disabled 
                            onSearch={onFilterSearch}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TableView
                            title='Toplantılar'
                            columns={columns}
                            remoteUrl={`/toplanti`}
                            token={token}
                            //filter={`organizerKatilimcilar=${user._id}&select=baslik,tarih,firma.marka,yer,createdBy,kategori,status&populate=firma.marka&sort=tarih`}
                            filter={`${filter}&select=baslik,start,saat,vfirma,yer,vcreatedBy,kategori,status&populate=firma.marka&sort=start`}
                            searchCol='baslik'
                            data={[]}
                            onData={onData}
                        />
                    </Grid>
                </Grid>
            </div>
        </>
    )
}

export default Toplantilar