import React, { useState, useEffect } from 'react'
import PageHeader from "../../components/pageHeader";
import { Grid, Badge, Button, makeStyles, Drawer } from '@material-ui/core';
import ToplantiInfo from '../../components/toplanti/ToplantiInfo';
import ToplantiHistory from '../../components/toplanti/ToplantiHistory';
import api from '../../services/axios.service';
import ToplantiKatilim from '../../components/toplanti/ToplantiKatilim';
import { useRouter } from "next/router";
import { parseCookies, setCookie, destroyCookie } from 'nookies'
import { useSelector, useDispatch } from 'react-redux'
import { getFirmaCalisanKisiler, searchFirmalar } from '../../services/firma.service'
import { saveToplanti, updateToplanti, toplantiTamamla, getToplanti } from '../../services/toplanti.service'
import ToplantiTamamlama from '../../components/toplanti/ToplantiTamamlama'
import ToplantiZamanlayici from '../../components/toplanti/ToplantiZamanlayici';
import * as Actions from '../../redux/actions'
import ToplantiGorevleri from '../../components/toplanti/ToplantiGorevleri'
import moment from 'moment'
const { uuid } = require('uuidv4');
import ToplantiList from '../../components/toplanti/toplantiList'
import FirmaToplantilari from '../../components/toplanti/firmaToplantilari';
import { validateToplanti } from '../../validators/toplanti.validator';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(2)
    },
}));


const Toplanti = props => {
    const router = useRouter();
    const classes = useStyles()
    const dispatch = useDispatch()
    const user = useSelector(state => state.auth.user)

    const { musteriler, newToplanti, toplanti, firmaKisiler } = props
    const [form, setForm] = useState(
        {
            ...toplanti,
            vcreatedBy: user.key,
            createdAt: new Date()
        } || {
            vcreatedBy: user.key,
            createdAt: new Date()
        })



    const [kisiler, setKisiler] = useState(firmaKisiler || [])
    const users = useSelector(state => state.auth.users)
    if (!users) {
        return <></>
    }
    const [expertoPersonel, setExpertoPersonel] = useState(users? users.map(u => { return { key: u.key, adi: u.username, unvani: '', image: u.image, _id: u._id, mail: u.email } }): [])
    const [openDrawer, setOpenDrawer] = useState(false)
    //const [drawerType, setDrawerType] = useState('')
    const toplantiRightPanelType = useSelector(state => state.common.toplantiRightPanelType)
    const showToplantiRightPanel = useSelector(state => state.common.showToplantiRightPanel)
    const [toplantiKapatmaBilgileri, setToplantiKapatmaBilgileri] = useState({})
    const [seciliFirmaId, setSeciliFirmaId] = useState(props.seciliFirmaId)
    const [errors, setErrors] = useState(null)

    const save = async () => {
        saveToplanti(form)
        router.push(`/toplanti?q=current`)
    }

    const toplantiyiTamamla = async () => {
        toplantiTamamla(toplanti._id, toplantiKapatmaBilgileri)
        router.push(`/toplanti?q=current`)
    }

    const update = async () => {
        updateToplanti(form)
    }

    const getMusteriPersoneller = async (firma) => {
        let kisiler = await getFirmaCalisanKisiler(firma)
        if (kisiler) {
            setKisiler(kisiler)
        }
    }

    const onToplantiChanged = async (value) => {
        let f = { ...form }
        let val = value.value
        if (value.field == 'vfirma') {
            setKisiler([])
            getMusteriPersoneller(value.value._id)
            val = value.value.key
            setSeciliFirmaId(value.value._id)
        }
        else if (value.field == 'start') {
            if (f.saat != '') {
                let hm = f.saat.split(':')
                let hour = parseInt(hm[0])
                let min  = parseInt(hm[1])    
                f.start = moment(value.value).set({hour:hour, minute:min, second: 0, millisecond: 0})
            }
            else {
                f.start = moment(value.value)
            }

            if (f.sure != '') {
                f.end = moment(f.start).add(f.sure, 'minutes')
            }
        }
        else if (value.field == 'saat') {
            let hm = value.value.split(':')
            let hour = parseInt(hm[0])
            let min  = parseInt(hm[1])

            f.saat = value.value
            f.start = moment(f.start).set({hour:hour, minute:min, second: 0, millisecond: 0})

            if (f.sure != '') {
                f.end = moment(f.start).add(f.sure, 'minutes')
            }
        }
        else if (value.field == 'sure') {
            f.end = moment(f.start).add(value.value, 'minutes')
        }

        f[value.field] = val


        setForm(f)
    }

    const onFirmaKatilimciChange = async (firmaKatilimcilar) => {
        let f = { ...form }
       
        f.vfirmaKatilimcilar = firmaKatilimcilar
        setForm(f)
    }

    const onExpertoKatilimciChange = async (organizerKatilimcilar) => {
        let f = { ...form }
        f.vorganizerKatilimcilar = organizerKatilimcilar
        setForm(f)
    }

    const onToplantiKapatmaBilgiChange = async (value) => {
        let kapatmaBilgi = { ...toplantiKapatmaBilgileri }
        kapatmaBilgi[value.field] = value.value
        kapatmaBilgi.kapatan = user.key
        kapatmaBilgi.kapatmaTarihi = new Date()
        setToplantiKapatmaBilgileri(kapatmaBilgi)
    }

    return <>
        <Drawer
            className={classes.drawer}
            anchor='right'
            open={showToplantiRightPanel}
            onClose={() => dispatch(Actions.hideToplantiRightPanel())}
        >
            {toplantiRightPanelType == 'tamamlama' && <ToplantiTamamlama
                onChange={onToplantiKapatmaBilgiChange}
                onCancel={() => {
                    setToplantiKapatmaBilgileri({})
                    dispatch(Actions.hideToplantiRightPanel())
                    //setOpenDrawer(false)
                }}
                onOk={() => {
                    //save()
                    toplantiyiTamamla()
                    dispatch(Actions.hideToplantiRightPanel())
                    //setOpenDrawer(false)
                }}
            />}
            {toplantiRightPanelType == 'zamanlama' && <ToplantiZamanlayici />}
        </Drawer>
        <PageHeader
            title= {newToplanti ? 'Yeni Toplantı' : 'Toplantı' } 
            buttons={[
                !newToplanti &&
                <Button
                    disabled={toplanti.status != 'acik'} 
                    variant='outlined' 
                    color='secondary' 
                    onClick={(e) => {
                    e.preventDefault()
                    dispatch(Actions.setToplantiRightPanelType('tamamlama'))
                    //setOpenDrawer(true)
                }
                }>Toplantıyı Tamamla</Button>
                ,
                !newToplanti && <Badge color='secondary' badgeContent={props.kararlar}>
                    <Button variant='contained' color='primary'
                        onClick={(e) => {
                            e.preventDefault()
                            router.push(`/toplanti/${router.query.id}/kararlar?firma=${seciliFirmaId}`)
                        }}
                    >Toplantı Kararları</Button>
                </Badge>,

                {
                    title: newToplanti ? 'Kaydet' : 'Güncelle',
                    disabled: toplanti.status != 'acik',
                    onClick: (e) => {
                        e.preventDefault();
                        let _errors = validateToplanti(form)
                        if (_errors) {
                            setErrors(_errors)
                        }
                        else {
                            if (newToplanti) {
                                save()
                            }
                            else {
                                update()
                            }
                            router.back();
                        }
                    }
                },
            ]} />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12} lg={8} container spacing={2} alignContent='flex-start'>
                    <Grid item xs={12}>
                        <ToplantiInfo
                            errors={errors}
                            musteriler={musteriler}
                            toplanti={form}
                            onToplantiChanged={onToplantiChanged} />
                    </Grid>
                    {toplanti.status == 'tamamlandi' &&
                        <Grid item xs={12}>
                            <ToplantiGorevleri toplanti={toplanti} />
                        </Grid>
                    }
                </Grid>
                <Grid
                    item
                    xs={12} lg={4}
                    container
                    spacing={2}
                    direction="column"
                    justify="flex-start"
                    alignItems="stretch">
                    <Grid item>
                        <ToplantiKatilim
                            error={errors && errors.firmaKatilim}
                            baslik='Firma Katılımcılar'
                            kisiler={kisiler}
                            onChange={onFirmaKatilimciChange}
                            secilenKisiler={form.vfirmaKatilimcilar} />
                    </Grid>
                    <Grid item>
                        <ToplantiKatilim
                            error={errors && errors.organizerKatilim}
                            baslik='Experto Katılımcılar'
                            kisiler={expertoPersonel}
                            onChange={onExpertoKatilimciChange}
                            secilenKisiler={form.vorganizerKatilimcilar} />
                    </Grid>
                    {!newToplanti &&
                        <Grid item>
                            <FirmaToplantilari firmaKey={toplanti.vfirma} />
                        </Grid>
                    }
                    {!newToplanti &&
                        <Grid item>
                            <ToplantiHistory toplanti={toplanti} />
                        </Grid>
                    }
                </Grid>
            </Grid>
        </div>
    </>
}

Toplanti.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    //let client = await api.get(`/firma?select=marka,key`, {
    //    headers: { Authorization: `Bearer ${cookies.experto}` }
    //})
    let newToplanti = ctx.query.id == 'new'
    var toplantiClient = null;
    var firmaKisilerClient = null;
    var kararlar = null;
    let seciliFirma = null;

    let firmaKisiler = undefined
    let toplanti = undefined
    let seciliFirmaId = undefined

    if (!newToplanti) {
        toplantiClient = await api.get(`/toplanti/${ctx.query.id}?populate=firma`, {
            headers: { Authorization: `Bearer ${cookies.experto}` }
        })
        kararlar = await api.get(`/toplanti/${ctx.query.id}/karar`, {
            headers: { Authorization: `Bearer ${cookies.experto}` }
        })

        toplanti = toplantiClient.data.data

        //let firmaClient = await api.get()
        //seciliFirma = client.data.data.data.find(f => f.key == toplantiClient.data.data.vfirma)
        if (toplanti.firma) {
            seciliFirmaId = toplanti.firma._id
            firmaKisilerClient = await api.get(`/firma/${toplanti.firma._id}/kisi?active=true`, {
                headers: { Authorization: `Bearer ${cookies.experto}` }
            })
            if (firmaKisilerClient.status == 200) {
                firmaKisiler = firmaKisilerClient.data.data.data
            }
        }
    }

    let firma = null
    if (ctx.query.firma) {
        let firmalar = await searchFirmalar(`key=${ctx.query.firma}`)
        if (firmalar.length > 0) {
            firma = firmalar[0]

            // firmanin kisileri de getirilir
            let seciliFirmaninKisileriClient = await api.get(`/firma/${firma._id}/kisi`, {
                headers: { Authorization: `Bearer ${cookies.experto}` }
            })
            if (seciliFirmaninKisileriClient.status == 200) {
                firmaKisiler = seciliFirmaninKisileriClient.data.data.data
            }
        }
    }


    let newToplantiObj = {
        key: uuid(),
        vfirma: '',
        baslik: '',
        kategori: '',
        sure: '',
        saat: '',
        tur: '',
        start: null,
        firmaKisiler: [],
        vfirmaKatilimcilar: [],
        vorganizerKatilimcilar: [],
        status:'acik',
        firma: firma || null
    }

    return {
        musteriler      : [], //client.data.data.data,
        newToplanti     : newToplanti,
        toplanti        : newToplanti ? newToplantiObj : toplantiClient.data.data,
        firmaKisiler    : newToplanti ? firmaKisiler : firmaKisiler, //firmaKisilerClient.data.data.data,
        kararlar        : newToplanti ? null : kararlar.data.data.data.length,
        seciliFirmaId   : seciliFirmaId //toplanti.firma ? toplanti.firma._id : undefined
    }
}

export default Toplanti