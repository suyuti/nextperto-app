import React, { useState, useRef, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'
import { Button, Box, IconButton, TextField, MenuItem, Avatar, Menu, Dialog, DialogTitle, DialogContent, DialogActions, DialogContentText, CircularProgress } from '@material-ui/core'
import { withRouter } from 'next/router';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import RemoteAutoComplete from '../../components/autocomplete';
import { searchFirmalar } from '../../services/firma.service'
import trLocale from "date-fns/locale/tr";
import PageHeader from '../../components/pageHeader';
import MaterialTable from 'material-table';
import api from '../../services/axios.service';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import * as Actions from '../../redux/actions'
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import ThumbDownAltOutlinedIcon from '@material-ui/icons/ThumbDownAltOutlined';
import { gorevTamamla } from '../../services/gorev.service'
import { getToplantiById } from '../../services/toplanti.service'
import { getKisiler } from '../../services/kisi.service';
var qs = require('qs');
moment.locale('tr')

const ToplantiDetail = props => {
    const { toplanti } = props
    const token = useSelector(state => state.auth.token)
    const users = useSelector(state => state.auth.users)
    const [firmaKatilimcilar, setFirmaKatilimcilar] = useState(null)
    const [gecikme, setGecikme] = useState(false)

    useEffect(() => {
        let filter = {
            key: toplanti.vfirmaKatilimcilar.join(',')
        }
        let query = qs.stringify(filter)
        getKisiler(query).then(resp => {
            setFirmaKatilimcilar(resp)
        })

        //----------------------------
        let twoDaysAgo = moment().add(-2, 'days').startOf('day')
        if (moment(toplanti.start).isBefore(twoDaysAgo) && toplanti.status == 'acik') {
            setGecikme(true)
        }
    }, [])

    return (
        <div className='p-2 bg-gray-100 flex-col space-y-2'>
            <div className='flex bg-white border shadow rounded p-4'>
                <div className='flex-col w-6/12 border-r-2 p-4 space-y-2'>
                    <h2 className='text-center uppercase font-semibold'>Toplantı Bilgileri</h2>
                    <div className='flex justify-between'>
                        <h2 className='font-light '>Firma</h2>
                        <h2 className='text-left'>{toplanti.firma?.marka}</h2>
                    </div>
                    <div className='flex justify-between'>
                        <h2 className='font-light'>Konu</h2>
                        <p className='w-4/6'>{toplanti.baslik}</p>
                    </div>
                    <div className='flex justify-between'>
                        <h2 className='font-light'>Açıklama</h2>
                        <p className='w-4/6'>{toplanti.aciklama}</p>
                    </div>
                    <div className='flex justify-between'>
                        <h2 className='font-light'>Gündem</h2>
                        <h2 className='w-4/6'>{toplanti.gundem}</h2>
                    </div>
                    <div className='h-1 w-full border-b-2 my-4'></div>
                    <h2 className='text-center uppercase font-semibold'>Toplantı Zamanı</h2>
                    <div className='flex justify-between'>
                        <h2 className='font-light'>Tarih</h2>
                        <h2 className='w-4/6'>{`${moment(toplanti.start).format('DD.MM.YYYY')} ${toplanti.saat}`}</h2>
                    </div>
                    <div className='flex justify-between'>
                        <h2 className='font-light'>Süre</h2>
                        <h2 className='w-4/6'>{`${toplanti.sure / 60} saat`}</h2>
                    </div>
                    <div className='flex justify-between'>
                        <h2 className='font-light'>Konum</h2>
                        <h2 className='w-4/6'>{toplanti.yer}</h2>
                    </div>
                </div>
                <div className='flex-col w-4/12 border-r-2 p-2'>
                    <h2 className='font-semibold uppercase text-center'>katılımcılar</h2>
                    <h2 className='uppercase my-2'>Experto Katılımcılar</h2>
                    {toplanti.vorganizerKatilimcilar.map((ek, i) => {
                        let _user = users.find(u => u.key == ek)
                        return (
                            <div className='flex space-x-2 items-center '>
                                <img className='h-8 w-8 rounded-full border-2 border-gray-200 shadow bg-gray-400' src={_user?.image}></img>
                                <h2 className='font-light'>{_user.username}</h2>
                            </div>
                        )
                    })}
                    <div className='h-1 w-full border-b-2 my-4'></div>
                    {toplanti.firma && (
                        <>
                            <h2 className='uppercase my-2'>Firma Katılımcılar</h2>
                            {
                                firmaKatilimcilar ?
                                    firmaKatilimcilar.map((k, i) => {
                                        return (
                                            <div className='flex space-x-2 items-center '>
                                                <img className='h-8 w-8 rounded-full border bg-gray-400' src={k?.image}></img>
                                                <div className=''>
                                                    <h2 className='font-light'>{`${k.adi} ${k.soyadi} / ${k.unvani}`}</h2>
                                                    <h2 className='font-light text-sm'>{k.cepTelefon}</h2>
                                                </div>
                                            </div>
                                        )
                                    })
                                    :
                                    <CircularProgress />
                            }
                        </>)
                    }
                </div>
                <div className='flex-col w-2/12 p-2 space-y-2'>
                    {gecikme &&
                        <div className='bg-red-100 p-2 text-center border shadow rounded'>
                            <h1 className='text-xl font-light'>Toplantı kararları toplantıdan sonra 2 gün içinde girilmelidir.</h1>
                        </div>
                    }
                    <Button
                        disabled={toplanti.status == 'acik' && moment().isBefore(toplanti.start)}
                        variant='outlined'
                        color='primary'
                        size='small'>Toplantı Kararları</Button>
                </div>
            </div>
        </div>
    )
}

const ToplantiFilter = props => {
    const { onQuery, filterRef } = props
    const ekip = useSelector(state => state.auth.ekip)
    const users = useSelector(state => state.auth.users)
    const user = useSelector(state => state.auth.user)
    const filter = useSelector(state => state.common.filter)
    const dispatch = useDispatch()
    const [firmalar, setFirmalar] = useState([])

    useEffect(() => {
        dispatch(Actions.setFilter({
            firma: '',
            katilimci: '',
            durum: ''
        }))
    }, [])

    return (
        <div className='bg-white p-2 rounded shadow flex space-x-2'>
            <div className='w-1/6'>
                <RemoteAutoComplete
                    results={firmalar}
                    size='small'
                    label='Firma'
                    labelField='marka'
                    value={filter.firma}
                    onSelected={e => {
                        dispatch(Actions.setFilter({ ...filter, firma: e }))
                    }}
                    fetchRemote={(searchText) => {
                        searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                            setFirmalar(resp)
                        })
                    }}
                />
            </div>
            <div className='w-1/6'>
                <TextField
                    label='Katılımcı'
                    fullWidth
                    variant='outlined'
                    size='small'
                    select
                    value={filter.katilimci || ''}
                    onChange={e => {
                        dispatch(Actions.setFilter({ ...filter, katilimci: e.target.value }))
                    }}
                >
                    {[user.key, ...ekip].map((e, i) => {
                        let user = users.find(u => u.key == e)
                        return (<MenuItem key={i} value={user.key}>{user.username}</MenuItem>)
                    })}
                </TextField>
            </div>
            <div className='w-1/6'>
                <TextField
                    label='Durum'
                    fullWidth
                    variant='outlined'
                    size='small'
                    select
                    value={filter.durum || ''}
                    onChange={e => {
                        dispatch(Actions.setFilter({ ...filter, durum: e.target.value }))
                    }
                        //setForm(prev => { return { ...prev, sonuc: e.target.value } })
                    }
                >
                    <MenuItem key='hepsi' value='hepsi'>HEPSI</MenuItem>
                    <MenuItem key='karargir' value='karargir'>KARAR GİRİLMELİ</MenuItem>
                    <MenuItem key='geciken' value='geciken'>GECİKEN TOPLANTILAR</MenuItem>
                    <MenuItem key='gelecek' value='gelecek'>GELECEK TOPLANTILAR</MenuItem>
                    <MenuItem key='bugun' value='bugun'>BUGÜNKÜ TOPLANTILAR</MenuItem>
                    <MenuItem key='buhafta' value='buhafta'>BU HAFTAKİ TOPLANTILAR</MenuItem>
                    <MenuItem key='sonrakihafta' value='sonrakihafta'>SONRAKİ HAFTA TOPLANTILAR</MenuItem>
                    <MenuItem key='buay' value='buay'>BU AY TOPLANTILAR</MenuItem>
                    <MenuItem key='tamamlanmis' value='tamamlanmis'>TAMAMLANMIŞ TOPLANTILAR</MenuItem>
                    <MenuItem key='iptal' value='iptal'>İPTAL EDİLMİŞ TOPLANTILAR</MenuItem>
                </TextField>
            </div>
            <div className='w-1/6'>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={trLocale}>
                    <KeyboardDatePicker
                        //error={errors && errors.start}
                        //helperText={errors && errors.start}
                        autoOk
                        fullWidth
                        clearable
                        variant="inline"
                        inputVariant="outlined"
                        label="Tarih"
                        size='small'
                        format="dd.MM.yyyy"
                        maxDate={filter.bitisTarih || undefined}
                        InputAdornmentProps={{ position: 'start' }}
                        value={filter.baslangicTarih || null}
                        onChange={(e) =>
                            dispatch(Actions.setFilter({ ...filter, baslangicTarih: e }))
                            //setForm(prev => { return { ...prev, baslangicTarih: e } })
                        }
                    />
                </MuiPickersUtilsProvider>
            </div>
            <div className='w-1/6'>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={trLocale}>
                    <KeyboardDatePicker
                        //error={errors && errors.start}
                        //helperText={errors && errors.start}
                        autoOk
                        fullWidth
                        clearable
                        variant="inline"
                        inputVariant="outlined"
                        label="Tarih"
                        size='small'
                        format="dd.MM.yyyy"
                        minDate={filter.baslangicTarih || undefined}
                        InputAdornmentProps={{ position: 'start' }}
                        value={filter.bitisTarih || null}
                        onChange={(e) =>
                            dispatch(Actions.setFilter({ ...filter, bitisTarih: e }))
                            //setForm(prev => { return { ...prev, bitisTarih: e } })
                        }
                    />
                </MuiPickersUtilsProvider>
            </div>
            <div className='w-1/6 flex space-x-2 justify-end'>
                <Button variant='outlined' color='primary' size='small' onClick={() => {
                    dispatch(Actions.setFilter({
                        baslangicTarih: null,
                        bitisTarih: null,
                        sonuc: '',
                        sorumlu: '',
                        firma: ''
                    }))
                    //firmaRef.current.clear()
                }
                }>Temizle</Button>
                <Button variant='contained' color='primary' size='small' onClick={() => onQuery({})}>Sorgula</Button>
            </div>
        </div>
    )
}


const Toplantilar = (props) => {
    const dispatch = useDispatch()
    const permissions = useSelector(state => state.auth.permissions)
    const token = useSelector(state => state.auth.expertoToken)
    const user = useSelector(state => state.auth.user)
    const ekip = useSelector(state => state.auth.ekip)
    const users = useSelector(state => state.auth.users)
    const [tableFilter, setTableFilter] = useState(null)
    const [queryFilter, setQueryFilter] = useState(null) // Sorguda bazi query parametreleri filter altinda gonderilemiyor
    //const [page, setPage] = useState(0)
    const tableRef = React.createRef();
    const filterRef = React.createRef();
    const [pageLoad, setPageLoad] = useState(false)
    const [page, setPage] = useState(0)
    const [usePrevPage, setUsePrevPage] = useState(false)
    const [resetPage, setResetPage] = useState(false)
    const prevUrl = useSelector(state => state.common.prevUrl)
    const prevPage = useSelector(state => state.common.prevPage)
    const gFilter = useSelector(state => state.common.filter)
    const pageHeaderBack = useSelector(state => state.common.pageHeaderBack)
    const [sort, setSort] = useState(null)
    const [anchorEl, setAnchorEl] = useState(null)
    const [seciliKayit, setSeciliKayit] = useState(null)
    const [table, setTable] = useState(null)
    const [showGorevBasarisiz, setShowGorevBasarisiz] = useState(false)
    const [sonucAciklama, setSonucAciklama] = useState('')



    const convertFilterToQuery = (filter) => {
        let tf = {
            vfirma: filter.firma?.key || undefined,
            vorganizerKatilimcilar: filter.katilimci || undefined //? filter.sorumlu.key : undefined
        }

        if (filter.durum == 'hepsi') {
            delete tf.durum
        }
        else if (filter.durum == 'gelecek') {
            tf.filter = {
                status: 'acik',
                start: {
                    $gt: moment().endOf('day').toISOString()
                }
            }
        }
        else if (filter.durum == 'bugun') {
            tf.filter = {
                status: 'acik',
                start: {
                    $gte: moment().startOf('day').toISOString(),
                    $lte: moment().endOf('day').toISOString()
                }
            }
        }
        else if (filter.durum == 'buhafta') {
            tf.filter = {
                status: 'acik',
                start: {
                    $gte: moment().startOf('week').toISOString(),
                    $lte: moment().endOf('week').toISOString()
                }
            }
        }
        else if (filter.durum == 'sonrakihafta') {
            tf.filter = {
                status: 'acik',
                start: {
                    $gte: moment().add(1, 'week').startOf('week').toISOString(),
                    $lte: moment().add(1, 'week').endOf('week').toISOString()
                }
            }
        }
        else if (filter.durum == 'buay') {
            tf.filter = {
                status: 'acik',
                start: {
                    $gte: moment().startOf('month').toISOString(),
                    $lte: moment().endOf('month').toISOString()
                }
            }
        }
        else if (filter.durum == 'tamamlanmis') {
            tf.filter = {
                status: 'tamamlandi'
            }
        }
        else if (filter.durum == 'iptal') {
            tf.filter = {
                status: 'iptal'
            }
        }
        else if (filter.durum == 'karargir') {
            tf.filter = {
                status: 'acik',
                start: {
                    $lt: moment().startOf('day').toISOString(),
                    $gt: moment().add(-2, 'days').startOf('day').toISOString()
                }
            }
        }
        else if (filter.durum == 'geciken') {
            tf.filter = {
                status: 'acik',
                start: {
                    $lt: moment().add(-2, 'days').startOf('day').toISOString()
                }
            }
        }

        if (filter.baslangicTarih && !filter.bitisTarih) {
            tf.filter = {
                start: {
                    $gte: filter.baslangicTarih
                }
            }
        }
        else if (!filter.baslangicTarih && filter.bitisTarih) {
            tf.filter = {
                start: {
                    $lte: filter.bitisTarih
                }
            }
        }
        else if (filter.baslangicTarih && filter.bitisTarih) {
            tf.filter = {
                start: {
                    $gte: moment(filter.baslangicTarih).startOf('day').toISOString(),
                    $lte: moment(filter.bitisTarih).endOf('day').toISOString()
                }
            }
        }

        if (!filter.katilimci) {
            tf = {...tf,
                vorganizerKatilimcilar: ekip.join(',')
            }
        }

        //if (filter.ekip || filter.sorumlu == '') {
        //    tf.vsorumlu = ekip.join(',')
        //}

        if (filter.createdBy) {
            tf.vcreatedBy = filter.createdBy
            delete tf.vsorumlu
        }
        //setTableFilter(tf)
        //setQueryFilter(qf)

        return tf
    }

    const convertRouterToQuery = (rq) => {
        let f = {}
        let filter = {}
        setSort(null)
        if (rq == 'toplantilarim') {
            f = convertFilterToQuery({
                status: 'acik',
                katilimci: user.key,
            })
            filter = {
                status: 'acik',
                katilimci: user.key,
                baslangicTarih: null,
                bitisTarih: null
            }
        }
        else if (rq == 'ekibimintoplantilari') {
            f = convertFilterToQuery({
                status: 'acik',
                ekip: true
                //sorumlu: user.key,
            })
            filter = {
                status: 'acik',
                ekip: true,
                //sorumlu: user.key,
                baslangicTarih: null,
                bitisTarih: null
            }
        }
        dispatch(Actions.setFilter(filter))
        return f
    }

    useEffect(() => {
        if (prevUrl == '/toplanti/[id]') {
            setUsePrevPage(true)
            if (pageHeaderBack) {
                setTableFilter(convertFilterToQuery(gFilter))
                //convertFilterToQuery(gFilter)
            }
            else {
                setTableFilter(convertRouterToQuery(props.router.query.filter))
                //convertRouterToQuery(props.router.query.filter)
            }
        }
        else {
            setTableFilter(convertRouterToQuery(props.router.query.filter))
            //convertRouterToQuery(props.router.query.filter)
            setResetPage(true)
            setUsePrevPage(false)
        }
        dispatch(Actions.resetPageHeaderBack())
        dispatch(Actions.setPrevUrl(props.router.route))
        //return () => {
        //    dispatch(Actions.setPrevUrl(props.router.route))
        //}
    }, [props.router.query])

    useEffect(() => {
        if (tableRef.current) {
            tableRef.current.onQueryChange()
        }
    }, [tableFilter])

    useEffect(() => {
        setTable(tableRef.current)
    }, [tableRef.current])

    return (
        <div>
            <PageHeader
                title='Toplantılar'
                buttons={[
                    {
                        title: 'Yeni',
                        disabled: !permissions ? true : !permissions.find(p => p.key == 'toplanti')?.create,
                        onClick: (e) => {
                            e.preventDefault();
                            props.router.push("/toplanti/[id]", `/toplanti/new`);
                        }
                    },
                ]} />
            <div className='p-2 flex-col space-y-2'>
                <ToplantiFilter
                    onQuery={() => {
                        setResetPage(true)
                        setUsePrevPage(false)
                        setTableFilter(convertFilterToQuery(gFilter))
                    }} />
                <MaterialTable
                    title='Toplantılar'
                    tableRef={tableRef}
                    columns={[
                        { title: 'Toplantı', field: 'baslik' },
                        {
                            title: 'Firma', field: 'firma.marka', render: row => {
                                if (row.firma == null) {
                                    return (<div className='text-red-600 font-light'>
                                        İç Toplantı
                                    </div>)
                                }
                                else {
                                    return <h2>{row.firma.marka}</h2>
                                }
                            }
                        },
                        { title: 'Konum', field: 'yer' },
                        {
                            title: 'Oluşturan', field: 'vcreatedBy', render: row => {
                                if (row.vcreatedBy) {
                                    let _user = users.find(u => u.key == row.vcreatedBy)
                                    return (
                                        <div className='flex items-center space-x-2'>
                                            <Avatar className='h-4 w-4 border-2 border-gray-400 shadow' src={_user?.image} />
                                            <h2 className=''>{_user.username}</h2>
                                        </div>
                                    )
                                }
                                return (<div></div>)
                            }
                        },

                        { title: 'Toplantı Tarihi', field: 'start', render: row => moment(row.start).format('DD.MM.YYYY') },
                        {
                            title: 'Durum', field: 'status', render: row => {

                                if (row.status == 'tamamlandi') {
                                    return <div className='bg-purple-500 w-32 text-center p-1 rounded shadow text-white'>TAMAMLANDI</div>
                                }
                                if (row.status == 'iptal') {
                                    return <div className='bg-gray-500 w-32 text-center p-1 rounded shadow text-white'>İPTAL</div>
                                }

                                let today = moment()
                                let tarih = moment(row.start)
                                let kararGirmeSonTarih = moment(row.start).add(2, 'days')

                                let bugun = today.isSame(tarih, 'days')
                                let gelecekte = today.isBefore(tarih, 'days')
                                let gecikme = today.isAfter(kararGirmeSonTarih, 'days')
                                let kararGirilmeTarihinde = today.isSameOrAfter(tarih, 'days') && today.isSameOrBefore(kararGirmeSonTarih, "days")

                                if (row.status == 'acik' && gelecekte) {
                                    return <div className='bg-blue-500 w-32 text-center p-1 rounded shadow text-white'>AÇIK</div>
                                }

                                if (bugun && row.status == 'acik') {
                                    return <div className='bg-green-500 w-32 text-center p-1 rounded shadow text-white'>BUGÜN</div>
                                }
                                else if (gecikme && row.status == 'acik') {
                                    return <div className='bg-red-500 w-32 text-center p-1 rounded shadow text-white'>GECİKME</div>
                                }
                                else if (kararGirilmeTarihinde && row.status == 'acik') {
                                    return <div className='bg-yellow-500 w-32 text-center p-1 rounded shadow text-white'>KARAR GİRİLMELİ</div>
                                }
                            }
                        },
                    ]}
                    actions={[
                        {
                            icon: () => <ArrowForwardIcon />,
                            tooltip: 'Detay',
                            onClick: (event, rowData) => {
                                dispatch(Actions.setPrevPage(page))
                                props.router.push(`/toplanti/${rowData._id}`)
                            }
                        }
                    ]}
                    options={{
                        padding: 'dense',
                        pageSize: 10,
                        pageSizeOptions: [10, 25, 50, 100],
                    }}
                    localization={{
                        body: { emptyDataSourceMessage: 'Gösterilecek kayıt yok' },
                        toolbar: { searchPlaceholder: 'Ara' }
                    }}

                    data={query => {
                        return new Promise((resolve, reject) => {
                            let p = query.page
                            if (usePrevPage) {
                                p = prevPage
                            }
                            if (resetPage) {
                                p = 0
                            }
                            let _q = {
                                skip: p * query.pageSize,
                                limit: query.pageSize,
                                populate: 'firma,sorumlu',
                                baslik: `/${query.search}/i`,
                                sort: sort || 'start'
                            }
                            if (query.orderBy) {
                                _q.sort = query.orderBy ? `${query.orderDirection == 'asc' ? '' : '-'}${query.orderBy.field}` : ''
                            }
                            let q = qs.stringify({ ..._q, ...tableFilter })
                            api.get(`/toplanti?${q}`,
                                { headers: { Authorization: `Bearer ${token}` } })
                                .then(result => {
                                    if (!result.data.data) {
                                        reject()
                                    }
                                    setPage(p)
                                    setResetPage(false)
                                    setPageLoad(false)
                                    setUsePrevPage(false)
                                    resolve({
                                        data: result.data.data.data,
                                        page: result.data.data.page / query.pageSize,
                                        totalCount: result.data.data.total,
                                    })
                                })
                                .catch(e => alert(JSON.stringify(e)))
                        })
                    }}

                    detailPanel={row => {
                        return <ToplantiDetail toplanti={row} />
                    }}
                />
            </div>

            <div>
                <Menu
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={() => { setAnchorEl(null) }}
                >
                    <MenuItem onClick={() => {
                        gorevTamamla(seciliKayit._id, {
                            sonuc: 'basarili',
                            vkapatan: user.key,
                            kapatmaTarihi: new Date()
                        })
                            .then(resp => {
                                if (table) {
                                    table.onQueryChange()
                                }
                            })
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <DoneIcon />
                            <h2>Görevi Başarılı Tamamla</h2>
                        </div>
                    </MenuItem>
                    <MenuItem onClick={() => {
                        setShowGorevBasarisiz(true)
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <ThumbDownAltOutlinedIcon />
                            <h2>Görevi Başarısız Tamamla</h2>
                        </div>
                    </MenuItem>
                    <MenuItem onClick={() => {
                        gorevTamamla(seciliKayit._id, {
                            sonuc: 'iptal',
                            vkapatan: user.key,
                            kapatmaTarihi: new Date()
                        })
                            .then(resp => {
                                if (table) {
                                    table.onQueryChange()
                                }
                            })
                        setAnchorEl(null)
                    }}>
                        <div className='flex space-x-2 items-center'>
                            <CloseIcon />
                            <h2>Görevi İptal Et</h2>
                        </div>
                    </MenuItem>
                </Menu>

                <Dialog
                    open={showGorevBasarisiz}
                    onClose={() => setShowGorevBasarisiz(false)}
                >
                    <DialogTitle>Görevi Başarısız Kapatma</DialogTitle>
                    <DialogContent>
                        <DialogContentText>Görevin başarısızlık nedenini açıklayın</DialogContentText>
                        <TextField
                            autoFocus
                            label='Başarısızlık Nedeni'
                            variant='outlined'
                            fullWidth
                            rows={3}
                            rowsMax={Infinity}
                            multiline
                            value={sonucAciklama}
                            onChange={(e) => {
                                setSonucAciklama(e.target.value)
                            }}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button variant='contained' color='primary' onClick={() => {
                            setSonucAciklama('')
                            setShowGorevBasarisiz(false)
                        }}>Vazgeç</Button>
                        <Button variant='contained' color='primary' onClick={() => {
                            gorevTamamla(seciliKayit._id, {
                                sonuc: 'basarisiz',
                                sonucAciklama: sonucAciklama,
                                vkapatan: user.key,
                                kapatmaTarihi: new Date()
                            })
                                .then(resp => {
                                    if (table) {
                                        table.onQueryChange()
                                    }
                                })
                            setAnchorEl(null)
                            setSonucAciklama('')
                            setShowGorevBasarisiz(false)
                        }}>Tamam</Button>
                    </DialogActions>
                </Dialog>

            </div>


        </div>
    )
}

export default withRouter(Toplantilar)
