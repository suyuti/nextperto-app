import React, { useState } from 'react'
import PageHeader from "../../../components/pageHeader";
import KararTable from '../../../components/toplanti/karar/KararTable';
import axios from 'axios';
import { useRouter } from "next/router";
import { Badge, Button, makeStyles } from '@material-ui/core';
const _ = require('lodash')
import { getToplantiKararlari, saveToplantiKararlari, getToplantiById, getToplantiRapor } from '../../../services/toplanti.service'
import { useSelector } from 'react-redux'
import { getFirmaCalisanKisiler } from '../../../services/firma.service';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import { uuid } from 'uuidv4';
import dynamic from 'next/dynamic'
const moment = require('moment')

const ToplantiKararTable = dynamic(
    () => import('../../../components/toplantiKarar/ToplantiKararTable'),
    {
        loading: () => <p>loading...</p>,
        ssr: false
    }
);


//import { HotTable, HotColumn, BaseEditorComponent } from '@handsontable/react';
import { Grid } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(2)
    },
}));


const ToplantiKararlar = props => {
    const { firmaPersonelleri, toplanti } = props
    const router = useRouter();
    const [rows, setRows] = useState(props.kararlar.length == 0 ? [] : props.kararlar)
    const users = useSelector(state => state.auth.users)
    const classes = useStyles()
    const [errors, setErrors] = useState(null)

    const setSorumlularRef = (data) => {
        for (let d of data) {
            //if (_.isEmpty(d)) {
            if (d.durum == null && d.firmaSorumlu == null && d.karar == null && d.organizerSorumlu == null && d.tarih == null) {
                continue
            }
            let uo = users.find(u => u.username == d.organizerSorumlu)
            let uf = firmaPersonelleri.find(u => u.adi == d.firmaSorumlu)

            d.vorganizerSorumlu = uo?.key
            d.vfirmaSorumlu = uf?.key
        }
        console.log(data)
        setRows(data)
    }

    const isValid = (data) => {
        let errors = []
        let i = 0
        for (let d of data) {
            if (d.karar == undefined || d.karar.trim().length == 0) {
                errors.push({row: i, col: 'karar'})
            }
            if (d.vorganizerSorumlu == undefined) {
                errors.push({row: i, col: 'organizerSorumlu'})
            }
            if (!moment(d.tarih, 'DD.MM.YYYY', true).isValid()) {
                errors.push({row: i, col: 'tarih'})
            }
            if (d.durum == undefined) {
                errors.push({row: i, col: 'durum'})
            }
            i++
        }
        return errors
    }

    const save = () => {
        if (rows[rows.length - 1]) {
            //if (_.isEmpty(rows[rows.length - 1])) {
            let lastRow = rows[rows.length - 1]
            if (lastRow.durum == null &&
                lastRow.firmaSorumlu == null &&
                lastRow.karar == null &&
                lastRow.organizerSorumlu == null &&
                lastRow.tarih == null) {
                rows.pop()
            }
        }
        let _errors = isValid(rows)
        if (_errors.length > 0) {
            alert('Eksik kayıtlar var. Kaydedilemez.')
            setErrors(_errors)
            return
        }

        // kararalara uuid eklenir
        for (let karar of rows) {
            karar.key = uuid()
        }
        saveToplantiKararlari(router.query.id, rows).then(results => {
            setRows(results)
            router.push(`/toplanti/${router.query.id}`)
        })
    }

    return <>
        <PageHeader
            title='Toplantı Kararları'
            buttons={[
                <Button
                    variant='outlined'
                    color='primary'
                    startIcon={<PictureAsPdfIcon />}
                    onClick={(e) => {
                        e.preventDefault()
                        getToplantiRapor(toplanti._id)
                    }}
                >Toplanti Raporu</Button>,
                <Button variant='contained' color='primary' onClick={() => router.back()}>İptal</Button>,
                {
                    title: 'Kaydet',
                    disabled: toplanti.status != 'acik',
                    onClick: (e) => {
                        e.preventDefault();
                        save()
                    }
                },
            ]} />
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <ToplantiKararTable
                    errors={errors}
                        onChange={(data) => {
                            //alert('333')
                            //console.log(data)
                            // find vorganizerSorumlu
                            setSorumlularRef(data)



                            //setRows(data)
                        }}
                        kararlar={rows}
                        organizerPersoneller={users}
                        firmaPersoneller={['', ...firmaPersonelleri,]} // TODO Liste basina bos string eklendi. Autocomplete'de strickt modda secimsiz birakmak mumkun degil. Bos secilerek giderildi.
                    />
                </Grid>
            </Grid>


            {false &&

                <KararTable
                    data={rows}
                    organizerPersoneller={users}
                    firmaPersonelleri={firmaPersonelleri}
                    onChange={(e, i) => {
                        // i: kacinci satir
                        // e: data
                        let r = [...rows]
                        r[i] = { ...e, vToplanti: toplanti.key }

                        let lastRow = r[r.length - 1]
                        //if (lastRow) {
                        if (!_.isEmpty(lastRow)) {
                            r.push({})
                        }
                        //}
                        setRows(r)
                    }} />
            }
        </div>
    </>
}

ToplantiKararlar.getInitialProps = async (ctx) => {
    let kararlar = await getToplantiKararlari(ctx.query.id)
    let firmaPersonelleri = []
    if (ctx.query.firma != 'undefined') {
        firmaPersonelleri = await getFirmaCalisanKisiler(ctx.query.firma)
    }
    let toplanti = await getToplantiById(ctx.query.id)

    return {
        kararlar            : kararlar,
        firmaPersonelleri   : firmaPersonelleri,
        toplanti            : toplanti
    }
}

export default ToplantiKararlar