import React, { useState, useEffect } from 'react'
import PageHeader from "../../components/pageHeader";
import { useRouter, withRouter } from "next/router";
import { useDispatch, useSelector } from 'react-redux'
import MaterialTable from 'material-table';
import api from '../../services/axios.service';
import {Button} from '@material-ui/core'
import * as Actions from '../../redux/actions'

const columns = [
    { title: 'Urun Adi', field: 'adi' },
]

const Urunler = props => {
    const dispatch = useDispatch()
    const expertoUser = useSelector(state => state.auth.expertoUser)
    const token = useSelector(state => state.auth.expertoToken)
    const permissions = useSelector(state => state.auth.permissions)
    const user = useSelector(state => state.auth.user)
    let urunPermissions = permissions.find(p => p.key == 'urun')

    useEffect(() => {
        return () => {
            dispatch(Actions.setPrevUrl(props.router.route))
        }
    }, [])

    return <>
        <PageHeader
            title='Ürünler'
            buttons={[
                {
                    title: 'Yeni', disabled: !urunPermissions.create, onClick: (e) => {
                        e.preventDefault();
                        props.router.push("/urun/[id]", `/urun/new`);
                    }
                },
            ]} />
        <div className='p-2'>
            <MaterialTable
                title='Ürünler'
                options={{
                    padding: 'dense',
                    pageSize: 25,
                    pageSizeOptions: [10, 25, 50, 100]
                }}
                columns={[
                    {title:'Ürün Adı', field: 'adi'},
                    {title: 'Detay', render: row => <div className=''><Button variant='outlined' size='small' color='primary' onClick={() => {
                        props.router.push(`/urun/${row._id}`)
                    }}>Detay</Button></div>}
                ]}
                data={query => {
                    return new Promise((resolve, reject) => {
                        api.get(`/urun`,
                            { headers: { Authorization: `Bearer ${token}` } })
                            .then(result => {
                                if (!result.data.data) {
                                    reject()
                                }
                                else {
                                    resolve({
                                        data: result.data.data.data,
                                        page: result.data.data.page / query.pageSize,
                                        totalCount: result.data.data.total,
                                    })
                                }
                            })
                    })
                }}
            />
        </div>
    </>
}

export default withRouter(Urunler)