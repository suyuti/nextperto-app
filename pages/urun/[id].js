import React, { useState } from 'react'
import { uuid } from 'uuidv4'
import PageHeader from '../../components/pageHeader'
import { getUrun, saveUrun, updateUrun } from '../../services/urun.service'
import { Card, CardHeader, Divider, Grid, CardContent, TextField, Checkbox, Button, MenuItem } from '@material-ui/core'
import { withRouter } from 'next/router'
import { validateUrun } from '../../validators/urun.validator'
import { useSelector } from 'react-redux'

const Urun = (props) => {
    const { urun, newUrun } = props
    const [form, setForm] = useState(urun)
    const [errors, setErrors] = useState(null)
    const user = useSelector(state => state.auth.user)
    const permissions = useSelector(state => state.auth.permissions)
    let urunPermissions = permissions.find(p => p.key == 'urun')

    return (
        <div>
            <PageHeader
                title='Ürün'
                buttons={[
                    newUrun ?
                        <Button
                            disabled={!urunPermissions.create}
                            variant='contained'
                            color='primary'
                            onClick={() => {
                                let _errors = validateUrun(form)
                                if (_errors) {
                                    setErrors(_errors)
                                    return
                                }
                                else {
                                    saveUrun(form)
                                }
                                props.router.back()
                            }}
                        >Kaydet</Button>
                        :
                        <Button
                            disabled={!urunPermissions.write}
                            variant='contained'
                            color='primary'
                            onClick={() => {
                                let _errors = validateUrun(form)
                                if (_errors) {
                                    setErrors(_errors)
                                    return
                                }
                                else {
                                    updateUrun(form._id, form)
                                }
                                props.router.back()
                            }}

                        >Güncelle</Button>
                ]}
            />
            <div className='p-2'>
                <Grid container spacing={2}>
                    <Grid item xs={10}>
                        <Card>
                            <CardHeader
                                title='Ürün Detayları'
                            />
                            <Divider />
                            <CardContent>
                                <Grid container spacing={2}>
                                    <Grid item xs={12}>
                                        <TextField
                                            error={errors && errors.adi}
                                            helperText={errors && errors.adi}
                                            label='Adı'
                                            variant='outlined'
                                            fullWidth
                                            size='small'
                                            value={form.adi}
                                            onChange={e => {
                                                let f = { ...form }
                                                f.adi = e.target.value
                                                setForm(f)
                                            }}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <div className='flex items-center'>
                                            <Checkbox
                                                checked={form.onOdemeTutariVar}
                                                onChange={e => {
                                                    let f = { ...form }
                                                    f.onOdemeTutariVar = e.target.checked
                                                    setForm(f)
                                                }}
                                            />
                                            <TextField
                                                disabled={!form.onOdemeTutariVar}
                                                label='Ön Ödeme Tutarı'
                                                variant='outlined'
                                                fullWidth
                                                size='small'
                                                value={form.onOdemeTutariVar ? form.onOdemeTutari : ''}
                                                onChange={e => {
                                                    let f = { ...form }
                                                    f.onOdemeTutari = e.target.value
                                                    setForm(f)
                                                }}
                                            />
                                        </div>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <div className='flex items-center'>
                                            <Checkbox
                                                checked={form.basariTutariVar}
                                                onChange={e => {
                                                    let f = { ...form }
                                                    f.basariTutariVar = e.target.checked
                                                    setForm(f)
                                                }}
                                            />
                                            <TextField
                                                disabled={!form.basariTutariVar}
                                                label='Başarı Tutarı'
                                                variant='outlined'
                                                fullWidth
                                                size='small'
                                                value={form.basariTutariVar ? form.basariTutari : ''}
                                                onChange={e => {
                                                    let f = { ...form }
                                                    f.basariTutari = e.target.value
                                                    setForm(f)
                                                }}
                                            />
                                        </div>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <div className='flex items-center'>
                                            <Checkbox
                                                checked={form.sabitTutarVar}
                                                onChange={e => {
                                                    let f = { ...form }
                                                    f.sabitTutarVar = e.target.checked
                                                    setForm(f)
                                                }}
                                            />
                                            <TextField
                                                disabled={!form.sabitTutarVar}
                                                label='Sabit Tutar'
                                                variant='outlined'
                                                fullWidth
                                                size='small'
                                                value={form.sabitTutarVar ? form.sabitTutar : ''}
                                                onChange={e => {
                                                    let f = { ...form }
                                                    f.sabitTutar = e.target.value
                                                    setForm(f)
                                                }}
                                            />
                                        </div>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <div className='flex items-center'>
                                            <Checkbox
                                                checked={form.yuzdeTutariVar}
                                                onChange={e => {
                                                    let f = { ...form }
                                                    f.yuzdeTutariVar = e.target.checked
                                                    setForm(f)
                                                }}
                                            />
                                            <TextField
                                                disabled={!form.yuzdeTutariVar}
                                                label='Yüzde Tutarı'
                                                variant='outlined'
                                                fullWidth
                                                size='small'
                                                value={form.yuzdeTutariVar ? form.yuzdeTutari : ''}
                                                onChange={e => {
                                                    let f = { ...form }
                                                    f.yuzdeTutari = e.target.value
                                                    setForm(f)
                                                }}
                                            />
                                        </div>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <div className='flex items-center'>
                                            <Checkbox
                                                checked={form.raporBasiOdemeTutariVar}
                                                onChange={e => {
                                                    let f = { ...form }
                                                    f.raporBasiOdemeTutariVar = e.target.checked
                                                    setForm(f)
                                                }}
                                            />
                                            <TextField
                                                disabled={!form.raporBasiOdemeTutariVar}
                                                label='Rapor Başına Ödeme Tutarı'
                                                variant='outlined'
                                                fullWidth
                                                size='small'
                                                value={form.raporBasiOdemeTutariVar ? form.raporBasiOdemeTutari : ''}
                                                onChange={e => {
                                                    let f = { ...form }
                                                    f.raporBasiOdemeTutari = e.target.value
                                                    setForm(f)
                                                }}
                                            />
                                        </div>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <div className='flex items-center'>
                                            <TextField
                                                label='KDV Oranı'
                                                variant='outlined'
                                                fullWidth
                                                size='small'
                                                value={form.kdvOrani}
                                                onChange={e => {
                                                    let f = { ...form }
                                                    f.kdvOrani = e.target.value
                                                    setForm(f)
                                                }}
                                                select
                                            >
                                                <MenuItem key={0} value={0}>0</MenuItem>
                                                <MenuItem key={1} value={1}>1</MenuItem>
                                                <MenuItem key={8} value={8}>8</MenuItem>
                                                <MenuItem key={18} value={18}>18</MenuItem>
                                            </TextField>
                                        </div>
                                    </Grid>
                                    {user.isAdmin && (
                                        <Grid item xs={12}>
                                            <TextField
                                                label='Key'
                                                variant='outlined'
                                                color='secondary'
                                                fullWidth
                                                size='small'
                                                value={form.key}
                                                onChange={e => {
                                                    let f = { ...form }
                                                    f.key = e.target.value
                                                    setForm(f)
                                                }}
                                            />
                                        </Grid>
                                    )}
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </div>
        </div>
    )
}

Urun.getInitialProps = async (ctx) => {
    let newUrun = ctx.query.id == 'new'
    let urun = {
        key: uuid(),
        adi: '',
        onOdemeTutariVar: false
    }

    if (!newUrun) {
        urun = await getUrun(ctx.query.id)
    }

    return {
        newUrun,
        urun
    }
}
export default withRouter(Urun)
