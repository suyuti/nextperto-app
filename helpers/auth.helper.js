const getPermissions = user => {
    var permissions = []
    for (let role of user.roles) {
        for (let permission of role.permissions) {
            let i = permissions.findIndex(p => p.key == permission.key)
            if (i < 0) {
                permissions.push(permission)
            }
            else {
                let p = permissions[i]
                p.create = p.create | permission.create
                p.read = p.read | permission.read
                p.write = p.write | permission.write
                p.dlete = p.dlete | permission.dlete
                permissions[i] = p
            }
        }
    }
    return permissions
}

module.exports = {
    getPermissions
}