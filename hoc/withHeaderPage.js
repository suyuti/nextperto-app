import React, {Component} from 'react'
import PageHeader from "../components/pageHeader";

const withHeaderPage = (WrappedComponent, params) => {
    return class extends Component {
        render() {
            const {title, buttons} = this.props
            return (
                <div className='general-page'>
                    <PageHeader
                        title={params.title}
                        buttons={[
                            {
                                title: 'Yeni', disabled: false, onClick: (e) => {
                                    e.preventDefault();
                                    //router.push("/muhasebe/faturalar/[id]", `/muhasebe/faturalar/new`);
                                }
                            },
                        ]} />

                    <WrappedComponent {...this.props} />
                </div>
            )
        }
    }
}


export default withHeaderPage
    