import React, {Component} from 'react'
import './index.module.css'

const withPage = (WrappedComponent) => {
    return class extends Component {
        render() {
            return (
                <div className='general-page'>
                    <WrappedComponent {...this.props} />
                </div>
            )
        }
    }
}


export default withPage
    