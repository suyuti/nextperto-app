const withAuth = (Component) => {
    return class AuthWrapper extends React.Component {
        //async componentDidMount() {
        //    console.log(this.props.auth)
        //    if (!this.props.auth.isAuth) {
        //        this.props.login()
        //    }
        //}
        render() {
            if (!this.props.user) {
                return <h1>Unauth</h1>
            }
            return <Component {...this.props} />
        }
    }
}


export default withAuth