import React from 'react'

const withPermission = (Component) => {
    return class PermissionComponent extends React.Component {
        render() {
            return <Component {...this.props} />
        }
    }
}

export default withPermission