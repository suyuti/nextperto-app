import React from 'react';

export default Component => ({
  // Destructure props here, which filters them at the same time.
  //tried,
  //token,
  //getDashboardFromStorage, 
  //getUserFromStorage, 
  //onUnauthenticated, 
  ...props
}) => {
  // if user is not logged and we 've not checked the localStorage
  //if (!token && !tried) {
    // try load the data from local storage
    //getDashboardFromStorage();
    //getUserFromStorage();
  //} else {
    // if the user has no token or we tried to load from localStorage
    //onUnauthenticated();
  //}

  // if the user has token render the component PASSING DOWN the props.
  return <Component {...props} />;
};