import React from 'react';
import { mount } from 'enzyme';
import withAuth from './withAuth';

describe('withAuth HoC', () => {
  let WrappedComponent;
  let onUnauthenticated;

  beforeEach(() => {
    WrappedComponent = jest.fn(() => null).mockName('WrappedComponent');
    // mock the different functions to check if they were called or not.
    onUnauthenticated = jest.fn().mockName('onUnauthenticated');
  });

  it('should call onUnauthenticated if blah blah', async () => {
    const Component = withAuth(WrappedComponent);
    await mount(
      <Component 
        passThroughProp
        onUnauthenticated={onUnauthenticated} 
        token={false}
        tried
      />
    );

    expect(onUnauthenticated).toHaveBeenCalled();

    // Make sure props on to the wrapped component are passed down
    // to the original component, and that it is not polluted by the
    // auth HoC's store props.
    expect(WrappedComponent).toHaveBeenLastCalledWith({
      passThroughProp: true
    }, {});
  });
});
