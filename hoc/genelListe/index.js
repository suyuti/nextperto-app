import React from 'react'
import { Grid, Paper, TextField, Button } from '@material-ui/core'
import MaterialTable from 'material-table'
import axios from 'axios'
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';

const GenelListe = props => {
    const { title, columns, url } = props
    const tableRef = React.createRef()
    const [open, setOpen] = React.useState(false);
    const [options, setOptions] = React.useState([]);
    const loading = open && options.length === 0;
    //const [data, setData] = React.useState([])
    const [searchText, setSearchText] = React.useState('')
    const [pageInfo, setPageInfo] = React.useState({page:0, pageSize: 5})

    const search = async () => {
        axios.get(`${url}?search=${searchText}&per_page=${pageInfo.pageSize}&page=${pageInfo.page}`).then(resp => {
            setOptions(resp.data.data)
        })
    }

    return (
        <>
            <Grid>
                <Grid item xs={12}>
                    <Paper>
                        <TextField
                            fullWidth
                            variant='outlined'
                            label='Ara'
                            onChange={(e) => {
                                setSearchText(e.target.value)
                            }}
                            onKeyPress={(e) => {
                                if (e.key == 'Enter') {
                                    search()
                                }
                            }}
                        />
                        <Button variant='outlined' color='primary' onClick={() => search(0, 10)}>Ara</Button>
                    </Paper>
                </Grid>
            </Grid>
        </>
    )
}
export default GenelListe