import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { TextField, MenuItem } from '@material-ui/core'
import * as Actions from '../../redux/actions'
const ChangeMeAs = props => {
    const users = useSelector(state => state.auth.users)
    const dispatch = useDispatch()

    return (
        <>
            <TextField
                label='User As'
                variant='outlined'
                fullWidth
                select
                onChange={(e) => {
                    let user = {...e.target.value}
                    user.isAdmin = true // Keep admin property
                    dispatch(Actions.changeMe(user))
                }}
            >
                {users?.map(u => {
                    return <MenuItem key={u.key} value={u}>{u.username}</MenuItem>
                })}
            </TextField>
        </>
    )
}

export default ChangeMeAs