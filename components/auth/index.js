import React, { Component } from 'react'
import * as Actions from '../../redux/actions'
import {login} from '../../services/auth.service'

class Auth extends Component {
    constructor(props) {
        super(props)
    }

    async componentDidMount() {
        //this.props.store.dispatch(login())
        this.props.store.dispatch(Actions.login_1())
        
        //this.props.store.dispatch(Actions.silentLogin())
    }

    render() {
        return (
            <>
                {this.props.children}
            </>)
    }
}

export default Auth
