import React, {useState} from 'react'
import { Card, CardHeader, Divider, CardContent } from '@material-ui/core'
import dynamic from 'next/dynamic'
//import Chart from 'react-apexcharts';
const Chart = dynamic(() => import('react-apexcharts'), {ssr:false})

const options = {
    chart: {
        width: 380,
        type: 'pie',
    },
    labels: ['Kapalı', 'Açık', 'Geciken'],
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                show: false
            }
        }
    }]
}

function MyChart() {
    const [options, setOptions] = useState({chart: {
      width: 380,
      type: 'pie',
  },
  labels: ['Kapalı', 'Açık', 'Geciken'],
  responsive: [{
      breakpoint: 480,
      options: {
          chart: {
              width: 200
          },
          legend: {
              show: false
          }
      }
  }]});
    const [series, setSeries] = useState([
      
        4,
        1,
        9,
    ]);
    return (
      <div className='chart'>
        <Chart options={options} series={series} type='pie' />
        <style jsx>{`
          .chart {
            width: 350px;
            margin: auto;
          }
        `}</style>
      </div>
    );
  }

const ToplantiChart = props => {
    const {values} = props
    return <>
        <Card>
            <CardHeader 
                title='Toplantılarım'
                subheader='Geliştiriliyor'
            />
            <Divider />
            <CardContent>
                <div>
                    <MyChart />
                </div>

            </CardContent>
        </Card>
    </>
}

export default ToplantiChart