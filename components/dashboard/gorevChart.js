import React, { useState, useEffect } from 'react'
import { Card, CardHeader, Divider, CardContent } from '@material-ui/core'
import dynamic from 'next/dynamic'
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })
import { getGorevStats } from '../../services/stats.service'
import {useSelector} from 'react-redux'

const options = {
    chart: {
        width: 380,
        type: 'pie',
    },
    labels: ['Kapalı', 'Açık', 'Geciken'],
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                show: false
            }
        }
    }]
}

function MyChart() {
    return (
        <div className='chart'>
            <Chart options={options} series={series} type='pie' />
            <style jsx>{`
          .chart {
            width: 350px;
            margin: auto;
          }
        `}</style>
        </div>
    );
}

const GorevChart = props => {
    const { values } = props
    const user = useSelector(state => state.auth.user)
    const [options, setOptions] = useState({
        chart: {
            width: 380,
            type: 'pie',
        },
        labels: ['Açık', 'Başarılı', 'Başarısız', 'İptal'],
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 200
                },
                legend: {
                    show: false
                }
            }
        }]
    });
    const [series, setSeries] = useState([
        0,
        0,
        0,
        0
    ]);

    const load = async () => {
        getGorevStats(`vsorumlu=${user.key}`).then(resp => {
            let s = []
            s.push(resp[0].acik)
            s.push(resp[0].basarili)
            s.push(resp[0].basarisiz)
            s.push(resp[0].iptal)
            setSeries(s)
        })
    }

    useEffect(() => {
        load()
    }, [])

    return <>
        <Card>
            <CardHeader
                title='Görevlerim'
                subheader='Geliştiriliyor'
            />
            <Divider />
            <CardContent>
                <div>
                    <Chart options={options} series={series} type='pie' width='350' />
                </div>
            </CardContent>
        </Card>
    </>
}

export default GorevChart