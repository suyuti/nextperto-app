import React from 'react'
import { withRouter } from 'next/router'

const DashboardDefaultActions = (props) => {
    return (
        <div className="flex flex-row space-x-2 bg-white p-4 border-gray-400 shadow rounded justify-end">
            <button
                className='bg-yellow-500 shadow text-white font-semibold text-lg px-4 py-2 rounded focus:outline-none hover:shadow-lg hover:bg-green-800'
                onClick={() => {
                    props.router.push("/gorev/[id]", `/gorev/new`);
                }}
            >GÖREV</button>
            <button
                className='bg-green-500 shadow text-white font-semibold text-lg px-4 py-2 rounded focus:outline-none hover:shadow-lg hover:bg-green-800'
                onClick={() => {
                    props.router.push("/gorusme/[id]", `/gorusme/new`);
                }}
            >GÖRÜŞME</button>
            <button
                className='bg-blue-500 shadow text-white font-semibold text-lg px-4 py-2 rounded focus:outline-none hover:shadow-lg hover:bg-blue-800'
                onClick={() => {
                    props.router.push("/toplanti/[id]", `/toplanti/new`);
                }}
            >TOPLANTI</button>
        </div>
    )
}

export default withRouter(DashboardDefaultActions)
