import React from 'react'
import { Grid } from '@material-ui/core'
import GorevChart from '../gorevChart'
import ToplantiChart from '../toplantiChart'
import EkipList from '../ekipList'
import GorevList from '../../gorev/gorevList'
import ToplantiList from '../../toplanti/toplantiList'
const queryHelper = require('../../../helpers/query.helper')
import { useSelector } from 'react-redux'

const EkipGuncel = props => {
    const ekip = useSelector(state => state.auth.ekip)
    let ekipQuery = ekip.map(u => u).join(',')//queryHelper.gorevEkibim(ekip)

    return <>
        <Grid container spacing={2}>
            <Grid item xs={9} container spacing={2}>
                <Grid item xs={6}>
                    <GorevChart />
                </Grid>
                <Grid item xs={6}>
                    <ToplantiChart />
                </Grid>
                <Grid item xs={6}>
                    <GorevList
                        title='Ekibimin Görevleri'
                        sorumlular={ekipQuery}
                        acik
                    //limit={100}
                    />
                </Grid>
                <Grid item xs={6}>
                    <ToplantiList katilimcilar={ekipQuery} acik title='Ekibimin Toplantıları' />
                </Grid>
            </Grid>
            <Grid item xs={3} container spacing={2}>
                <Grid item xs={12}>
                    <EkipList />
                </Grid>
            </Grid>
        </Grid>
    </>
}

export default EkipGuncel