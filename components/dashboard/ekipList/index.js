import React, { useState, useEffect } from 'react'
import { Card, ListItem, ListItemText, ListItemAvatar, List, Avatar, ListSubheader, makeStyles, Paper } from '@material-ui/core'
import { getEkibim } from '../../../services/ekip.service'
import { useSelector } from 'react-redux'
import Link from "../../../components/core/Link";

const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const EkipList = props => {
    const classes = useStyles()
    const user = useSelector(state => state.auth.user)
    const users = useSelector(state => state.auth.users)
    const ekip = useSelector(state => state.auth.ekip)
    const [list, setList] = useState(ekip)

    const load = async () => {
        getEkibim(user).then(resp => {
            setList(resp)
        })
    }

    useEffect(() => {
        //load()
    }, [])

    return <>
        <Paper>
            <div className={classes.root} >
                <List subheader={<ListSubheader>Ekip</ListSubheader>}>
                    {ekip.map((l, i) => {
                        let u = users.find(u => u.key == l)
                        if (u) {
                            return (
                                <ListItem button href={`/profile/${u._id}`} component={Link}>
                                    <ListItemAvatar>
                                        <Avatar src={u.image} />
                                    </ListItemAvatar>
                                    <ListItemText primary={u.username} secondary='' />
                                </ListItem>
                            )
                        }
                    }
                    )}
                </List>
            </div>
        </Paper>
    </>
}
export default EkipList