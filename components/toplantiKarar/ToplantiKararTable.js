import React, { useState, useEffect } from 'react'
import { BaseEditorComponent, HotColumn, HotTable } from '@handsontable/react';
import 'handsontable/dist/handsontable.full.css';
import moment from 'moment'
import { Avatar } from '@material-ui/core'

class PersonelColumnRenderer extends BaseEditorComponent {
    constructor(props) {
        super(props)
        this.editorRef = React.createRef()
        this.editorContainerStyle = {
            display: "none",
            position: "absolute",
            left: 0,
            top: 0,
            zIndex: 999,
            background: "#fff",
            padding: "15px",
            border: "1px solid #cecece"
        };

        this.state = {
            renderResult: null,
            value: ""
        };
    }

    setValue(value, callback) {
        this.setState((state, props) => {
            return { value: value };
        }, callback);
    }

    getValue() {
        return this.state.value;
    }
    open() {
        this.editorRef.current.style.display = "block";
    }

    close() {
        this.editorRef.current.style.display = "none";

        this.setState({
            pickedColor: null
        });
    }

    prepare(row, col, prop, td, originalValue, cellProperties) {
        super.prepare(row, col, prop, td, originalValue, cellProperties);

        const tdPosition = td.getBoundingClientRect();

        this.editorRef.current.style.left = tdPosition.left + "px";
        this.editorRef.current.style.top = tdPosition.top + "px";
    }

    render() {
        let renderResult = null;

        if (this.props.isEditor) {
            renderResult = (
                <button
                    style={{ width: "100%", height: "33px", marginTop: "10px" }}
                >
                    Apply
                </button>
            );
        } else if (this.props.isRenderer) {
            const colorboxStyle = {
                background: this.props.value,
                width: "21px",
                height: "21px",
                float: "left",
                marginRight: "5px"
            };

            renderResult = (
                <>
                    <div style={colorboxStyle} />
                    <div>{this.props.value}</div>
                </>
            );
        }

        return <>{renderResult}</>;
    }
}

export default function HotComponent(props) {
    const { onChange, kararlar, organizerPersoneller, firmaPersoneller, errors } = props
    const [tableUpdated, setTableUpdated] = useState(false)
    const [data, setData] = useState(kararlar.map(k => {
        // HotTable'da kolon autocomplete ise datasource nesne listesi olmuyor. String listesi olabiliyor.
        // Bu yüzden data geldiğinde autocomplete kolonlar için tabloya uygun hale getiriliyor.
        // organizerSorumlu ve firmaSorumlu alanları gelen kullanıcı listelerinden seçilip isimleri şeklinde yazılıyor.
        let firmaPersoneli = firmaPersoneller.find(p => p.key == k.vfirmaSorumlu)
        let organizerPersoneli = organizerPersoneller.find(p => p.key == k.vorganizerSorumlu)
        //debugger
        let karar = {
            _id: k._id,
            karar: k.karar,
            //vorganizerSorumlu: k.vorganizerSorumlu,
            organizerSorumlu: organizerPersoneli?.username,
            firmaSorumlu: firmaPersoneli?.adi,
            tarih: moment(k.tarih).format('DD.MM.YYYY'),
            durum: k.durum
        }
        return karar
    }))

    const validator = (val, callback) => {
        if (val == undefined || val.trim().length == 0) {
            callback(false)
        }
        else {
            callback(true)
        }
    }

    const tableRef = React.useRef()

    useEffect(() => {
        if (tableRef) {
          //  debugger
          //  tableRef.current.hotInstance.validators.registerValidator('karar', validator)
            //tableRef.current
        }
    }, [])

    const initialGridSettings = {
        gridSettings: {
            manualColumnResize: true,
            manualRowResize: true,
            rowHeaders: true,
            colHeaders: true,
            rowHeights: 60,
            stretchH: 'all',
            licenseKey: 'non-commercial-and-evaluation',
            minSpareRows: 1
        },

        colIdSettings: {
            title: 'No',
            readOnly: true,
            width: 50,
            className: 'htCenter'
        },

        colKararSettings: {
            title: 'Karar',
            width: 150,
            className: 'htLeft',
            data: 'karar',
            validator: validator, 
            allowInvalid: true
        },

        colIlgiliSettings: {
            title: 'İlgili Kişi',
            width: 150,
            //type: 'handsontable',
            //handsontable: {
            //    colHeaders: ['Adi', 'Unvan'],
            //    autoColumnSize: true,
            //    data: organizerPersoneller,//[{name: '1'},{name: '2'}],
            //    //afterChange: (changes) => {
            //    //    alert(JSON.stringify(changes))
            //    //},
            //    //afterSelection: (row, col, row2, col2, preventScrolling, selectionLayerLevel) => {
            //    //    alert(row)
            //    //},
            //    getValue: () => {
            //        //console.log(v)
            //        try {
            //            console.log(this)
            //        debugger
            //        var selection = tableRef.current.hotInstance.getSelected()
            //        var d = tableRef.current.hotInstance.getSourceDataAtRow(Math.max(selection[0], 0))
            //        var n = d.username;
            //        return n
            //        }
            //        catch(e) {
            //            console.log(e)
            //            debugger
            //        }
            //        //return getSourceDataAtRow(Math.max(selection[0], 0)).name;
            //    }
            //}
            className: 'htCenter',
            type: 'autocomplete',
            source: organizerPersoneller.map(u => u.username),//[''],
            strict: true,
            data: 'organizerSorumlu',
            validator: validator, 
            allowInvalid: true

        },

        colFirmaIlgiliSettings: {
            title: 'Firma İlgili Kişi',
            width: 150,
            className: 'htCenter',
            type: 'autocomplete',
            source: firmaPersoneller.map(p => p.adi), // [''],
            strict: true,
            data: 'firmaSorumlu'
        },

        colSonTarihSettings: {
            title: 'Son Tarih',
            width: 150,
            type: 'date',
            data: 'tarih',
            dateFormat: 'DD.MM.YYYY',
            //correctFormat: true,
            className: 'htCenter',
            datePickerConfig: {
                // First day of the week (0: Sunday, 1: Monday, etc)
                firstDay: 1,
                showWeekNumber: true,
                numberOfMonths: 1,
                //disableDayFn: function (date) {
                //    // Disable Sunday and Saturday
                //    return date.getDay() === 0 || date.getDay() === 6;
                //}
            },
            //validator: validator, 
            //allowInvalid: true

        },

        colDurumSettings: {
            title: 'Durum',
            width: 150,
            data: 'durum',
            type: 'autocomplete',
            source: ['Açık', 'Bilgi', 'Tamamlandı', 'İptal', "Başarısız"],
            strict: true,
            allowInvalid: false,
            validator: validator, 
            //allowInvalid: true

        },

        colVIlgiliSettings: {
            width: 0,
            readOnly: true,
        },

        colVFirmaIlgiliSettings: {
            width: 0,
            readOnly: true,
        }
    }

    const afterChange = (changes, source) => {
        if (source == 'loadData') {
            return
        }
        for (let change of changes) {
            let i = organizerPersoneller.find(u => u.username == change[3])
            console.log(change)
            alert(i.key)
        }
    }

    // https://stackoverflow.com/questions/17839550/handsontable-replace-autocomplete-values-with-key-before-posting
    /*const afterChange = (changes, source) => {
        if (source == 'loadData') {
            return
        }
        var rowIndex = 0, columnID = 1, oldTextVal = 2, newTextVal = 3, ntv = '', nv = '';
        for(let change of changes) {
            debugger
            console.log(change)            
        }

        //changes.each(function () {
        //    debugger
        //    if (this[columnID] === 'CategoryID') {
        //        // Do nothing...
        //        //To make sure nothing else happens when this column is set through below
        //    }
        //    else if (this[columnID] === 'CategoryName') {
        //        ntv = this[newTextVal];
        //        //This is where I do my mapping using a dropdown.
        //        nv = $('#CategoriesFilterDropdown option').filter(function () { return $(this).text() === ntv; }).val();
        //        //13 is my CategoryID column
        //        $container.handsontable('setDataAtCell', this[rowIndex], 13, nv);
        //    }
        //});
    }*/



    return (
        <div>
            <HotTable
                ref={tableRef}
                settings={initialGridSettings.gridSettings}
                data={data}
                afterChange={(changes, source) => {
                    if (changes) {
                        setTableUpdated(true)
                        onChange(data)
                    }
                }}
            >
                <HotColumn settings={initialGridSettings.colKararSettings} />
                <HotColumn settings={initialGridSettings.colIlgiliSettings} />
                <HotColumn settings={initialGridSettings.colFirmaIlgiliSettings} />
                <HotColumn settings={initialGridSettings.colSonTarihSettings} />
                <HotColumn settings={initialGridSettings.colDurumSettings} />
            </HotTable>
        </div>
    );
}
