import React from 'react'
import { Paper, Button, Grid, makeStyles, TextField, MenuItem } from '@material-ui/core'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import { useSelector } from 'react-redux';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const KisiSearchToolbar = props => {
    const {disabled} = props
    const classes = useStyles()
    const users = useSelector(state => state.auth.users)

    return <>
        <Paper>
            <div className={classes.root}>
                <Grid container spacing={2}>
                    <Grid item xs={2}>
                        <TextField
                            disabled={disabled}
                            fullWidth
                            label='Firma'
                            variant='outlined'
                            size='small'
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <TextField
                            disabled={disabled}
                            fullWidth
                            label='Ünvan'
                            variant='outlined'
                            size='small'
                            select
                        >
                            <MenuItem key={'ALL'} value={'ALL'}>Tümü</MenuItem>
                            {users.map(u => <MenuItem key={u.key} value={u.key}>{u.username}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={2}>
                    </Grid>
                    <Grid item xs={2}>
                    </Grid>
                    <Grid item xs={2}>
                    </Grid>
                    <Grid item xs={2}>
                        <Button 
                            disabled={disabled}
                            variant='contained' 
                            color='primary'>Sorgula</Button>
                    </Grid>

                </Grid>
            </div>
        </Paper>
    </>
}

export default KisiSearchToolbar