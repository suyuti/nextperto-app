import React, { useState } from 'react'
import { RadioGroup, Radio, Card, CardHeader, Divider, CardContent, Grid, TextField, FormControlLabel, FormControl, Switch, FormLabel } from '@material-ui/core'
import { searchFirmalar } from '../../services/firma.service'
import RemoteAutoComplete from '../../components/autocomplete';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';

const KisiInfo = props => {
    const { onChange, kisi, errors } = props
    //const [form, setForm] = useState(props.kisi)
    const [results, setResults] = useState([])

    return <>
        <Card>
            <CardHeader
                title="Kişi Bilgileri"
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={10}>
                        <RemoteAutoComplete
                            label='Çalıştığı Firma'
                            error={errors?.firma}
                            results={results}
                            labelField='marka'
                            value={kisi.firma?.marka}
                            onSelected={(f) => {
                                onChange({ field: 'vfirma', value: f.key })
                            }}
                            fetchRemote={(searchText) => {
                                searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                                    setResults(resp)
                                })
                            }}
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <FormControl component="fieldset">
                            <FormControlLabel
                                value={kisi.active}
                                control={
                                    <Switch
                                        color="primary"
                                        checked={kisi.active}
                                        onChange={(e) => {
                                            onChange({field: 'active', value:e.target.checked})
                                            //let r = { ...kisi }
                                            //r.active = e.target.checked
                                            //setForm(r)
                                        }} />}
                                label={kisi.active ? 'Aktif' : "Pasif"}
                                labelPlacement="top"
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={5}>
                        <TextField
                            error={errors && errors.adi}
                            helperText={errors && errors.adi}
                            fullWidth
                            variant='outlined'
                            label='Adı'
                            value={kisi.adi}
                            onChange={(e) => onChange({ field: 'adi', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={5}>
                        <TextField
                            error={errors && errors.soyadi}
                            helperText={errors && errors.soyadi}
                            fullWidth
                            variant='outlined'
                            label='Soyadı'
                            value={kisi.soyadi}
                            onChange={(e) => onChange({ field: 'soyadi', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <FormControl component="fieldset">
                            <FormLabel component="legend">Cinsiyet</FormLabel>
                            <RadioGroup
                                aria-label="gender"
                                name="cinsiyet"
                                value={kisi.cinsiyet}
                                onChange={(e) => onChange({ field: 'cinsiyet', value: e.target.value })}
                            >
                                <FormControlLabel value="ERKEK" control={<Radio />} label="Erkek" />
                                <FormControlLabel value="KADIN" control={<Radio />} label="Kadın" />
                            </RadioGroup>
                        </FormControl>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            fullWidth
                            variant='outlined'
                            label='Ünvanı'
                            value={kisi.unvani}
                            onChange={(e) => onChange({ field: 'unvani', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            fullWidth
                            variant='outlined'
                            label='Bölümü'
                            value={kisi.bolumu}
                            onChange={(e) => onChange({ field: 'bolumu', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            error={errors && errors.email}
                            helperText={errors && errors.email}
                            fullWidth
                            variant='outlined'
                            label='Mail'
                            value={kisi.mail}
                            onChange={(e) => onChange({ field: 'mail', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            fullWidth
                            variant='outlined'
                            label='Cep Telefonu'
                            value={kisi.cepTelefon}
                            onChange={(e) => onChange({ field: 'cepTelefon', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            fullWidth
                            variant='outlined'
                            label='İş Telefonu'
                            value={kisi.isTelefon}
                            onChange={(e) => onChange({ field: 'isTelefon', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            fullWidth
                            variant='outlined'
                            label='Dahili'
                            value={kisi.dahili}
                            onChange={(e) => onChange({ field: 'dahili', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            fullWidth
                            disabled
                            variant='outlined'
                            label='TC Kimlik no'
                            value={kisi.tckn}
                            onChange={(e) => onChange({ field: 'tckn', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                error={errors && errors.dogumGunu}
                                helperText={errors && errors.dogumGunu}
                                clearable
                                autoOk
                                fullWidth
                                variant="inline"
                                inputVariant="outlined"
                                label="Doğum Günü"
                                format="dd.MM.yyyy"
                                InputAdornmentProps={{ position: 'start' }}
                                value={kisi.dogumGunu || ''}
                                onChange={(e) => onChange({ field: 'dogumGunu', value: e })}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>

                </Grid>
            </CardContent>
        </Card>
    </>
}

export default KisiInfo