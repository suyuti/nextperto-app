import React from 'react'
import { makeStyles } from '@material-ui/core'
import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { Typography, IconButton, Divider } from '@material-ui/core';
import { useSelector } from 'react-redux';
import EditIcon from '@material-ui/icons/Edit'
import {useRouter} from 'next/router'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    item: {
        padding: theme.spacing(2),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    itemInfo: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
}))


const DepartmenTree = props => {
    const classes = useStyles()
    const { departmanlar } = props
    const users = useSelector(state => state.auth.users)
    const router = useRouter()

    const TreeRender = data => {
        const isChildren = data.children !== null;
        let yonetici = users.find(p => p.key === data.vyonetici)

        if (isChildren) {
            return (
                <TreeItem
                    key={data.adi}
                    nodeId={data.adi}
                    label={
                        <div className={classes.item}>
                            <Typography variant="body1">{data.adi}</Typography>
                            <div className={classes.itemInfo}>
                                <div className="text">Yönetici: {yonetici && yonetici.username}</div>
                                <IconButton
                                    onClick={e => {
                                        //dispatch(Actions.getDepartmanlarAsList());
                                        //dispatch(Actions.getDepartman(data._id));
                                        //props.history.push(`/departmanlar/${data._id}`);
                                        e.preventDefault();
                                        router.push(`/admin/departman/${data._id}`)
                                    }}>
                                    <EditIcon />
                                </IconButton>
                            </div>
                        </div>
                    }>
                    {data.children.map((node, idx) => TreeRender(node))}
                </TreeItem>
            );
        }
        return (
            <Fragment>
                <TreeItem key={data.adi} nodeId={data.adi} label={data.adi} />
            </Fragment>
        );
    };


    return <>
        <TreeView
            defaultCollapseIcon={<ExpandMoreIcon />}
            defaultExpandIcon={<ChevronRightIcon />}>
            {departmanlar.map(d => TreeRender(d))}
        </TreeView>
    </>
}

export default DepartmenTree