import React from 'react'
import { TextField } from '@material-ui/core'

const DataTableSearch = props => {
    return <>
        <TextField 
            label='Arama'
            variant='outlined'
            fullWidth
        />
    </>
}

export default DataTableSearch