import React from 'react'
import { Table, TableContainer, Paper, TableFooter, TableHead, TableBody, TableRow, TableCell, TablePagination } from '@material-ui/core'

const DataTable = props => {
    const { columns, rows } = props
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return (
        <>
            <TableContainer component={Paper}>
                <Table stickyHeader>
                    <TableHead>
                        <TableRow>
                            {columns.map((col, colIndex) => (
                                <TableCell 
                                    key={col.id} 
                                    align={col.align} 
                                    style={{ minWidth: col.minWidth }}
                                    sortDirection={orderBy === col.id ? order : false}
                                    >{col.label}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                            return (
                                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                                    {columns.map((column) => {
                                        const value = row[column.id];
                                        if (column.render) {
                                            return (<TableCell key={column.id} align={column.align}>
                                                {column.render(row)}
                                            </TableCell>
                                            )
                                        }
                                        else {
                                            return (
                                                <TableCell key={column.id} align={column.align}>
                                                    {column.format && typeof value === 'number' ? column.format(value) : value}
                                                </TableCell>
                                            );
                                        }
                                    })}
                                </TableRow>
                            );
                        })}
                    </TableBody>
                    <TableFooter></TableFooter>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />        </>
    )
}

export default DataTable