import React from 'react'
import DataTable from './components/DataTable'
import DataTableSearch from './components/DataTableSearch';
import DataTableToolbar from './components/DataTableToolbar';
import { Button } from '@material-ui/core';
import DataTable2 from './components/DataTable2'

const TableView = props => {
    const { title, remoteUrl, columns, token, data, onData, filter, searchCol, options } = props
    return <>
        <div><DataTableToolbar /></div>
        <div><DataTable2
            title={title}
            remoteUrl={remoteUrl}
            columns={columns}
            token={token}
            onData={onData}
            data={data}
            filter={filter}
            searchCol={searchCol}
            options={options}
        /></div>
        <div></div>
    </>
}

export default TableView