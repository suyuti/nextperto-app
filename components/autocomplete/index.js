import React, { useState, useRef, useEffect } from 'react'
import {
    Card, Grid, CardHeader, Divider, CardContent, makeStyles,
    TextField, MenuItem, List, ListItem, ListItemText, Popover, InputAdornment,
    LinearProgress, Button, IconButton, CircularProgress
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SearchIcon from '@material-ui/icons/Search';
import CloseIcon from '@material-ui/icons/Close';

const RemoteAutoComplete = props => {
    const { fetchRemote, results, labelField, onSelected, value, error, label, size, disabled, readOnly, renderResultItem } = props

    const [searchText, setSearchText] = useState(value)
    const [open, setOpen] = useState(false)
    const [loading, setLoading] = useState(false)
    const [selectedItem, setSelectedItem] = useState(null)
    const [showCancelBtn, setShowCancelBtn] = useState(value != null)

    const textFieldRef = useRef()

    useEffect(() => {
        if (value == null) {
            setSelectedItem(null)
            setSearchText('')
            onSelected({ key: null })
            setShowCancelBtn(false)
            setOpen(false)
        }
        else {
            setSearchText(value['labelField'])
        }
    }, [value])

    useEffect(() => {
        if (results.length > 0) {
            setOpen(true)
        }
        setLoading(false)
    }, [results])

    return <>
        <TextField
            error={error}
            helperText={error}
            ref={textFieldRef}
            variant='outlined'
            disabled={disabled}
            fullWidth
            label={label}
            value={disabled ? '' : searchText}
            size={size || 'medium'}
            onChange={(e) => {
                setSearchText(e.target.value)
            }}
            InputProps={{
                readOnly: readOnly,
                startAdornment: (
                    <> {loading && (
                        <InputAdornment position="start">
                            <CircularProgress size={30} />
                        </InputAdornment>
                    )}</>
                ),
                endAdornment: (
                    <>
                        {showCancelBtn &&
                            <IconButton
                                disabled={readOnly}
                                size={size || 'medium'}
                                onClick={() => {
                                    setSelectedItem(null)
                                    setSearchText('')
                                    onSelected({ key: null })
                                    setShowCancelBtn(false)
                                    setOpen(false)
                                }}>
                                <CloseIcon />
                            </IconButton>
                        }
                        <InputAdornment position="end">
                            <SearchIcon />
                        </InputAdornment>
                    </>
                ),
            }}
            onKeyUp={e => {
                if (e.key == 'Enter') {
                    setLoading(true)
                    fetchRemote(searchText)
                }
                else if (e.key == 'Escape') {
                    if (loading) {

                    }
                }
            }}
        />
        <Popover
            className=' '
            open={open}
            onClose={() => setOpen(false)}
            anchorEl={textFieldRef.current}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
            }}
            PaperProps={{
                style: { width: '40%' },
            }}
        >
            {renderResultItem ?
                <div className='flex-col'>
                    {results.map((r, i) => {
                        return (
                            <div className='hover:bg-gray-100' onClick={e => {
                                setSelectedItem(r)
                                setSearchText(r[labelField])
                                onSelected(r)
                                setShowCancelBtn(true)
                                setOpen(false)
                            }}>
                                {renderResultItem(r, i)}
                            </div>)
                    })}
                </div>
                :
                <div className=''>
                    <List
                        className='w-full'
                    >
                        {results.map(r => {
                            return <ListItem
                                className='w-full'
                                button onClick={(e) => {
                                    setSelectedItem(r)
                                    setSearchText(r[labelField])
                                    onSelected(r)
                                    setShowCancelBtn(true)
                                    setOpen(false)
                                }}>
                                <ListItemText primary={r[labelField]} />
                            </ListItem>
                        })}
                    </List>
                </div>
            }
        </Popover>
    </>
}

export default RemoteAutoComplete