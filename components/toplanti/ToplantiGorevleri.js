import React, { useState, useEffect } from 'react'
import {
    Card, CardHeader, Divider,
    CardContent, Typography,
    Table, TableBody,
    TableRow, TableCell,
    TableHead, Button, Grid, makeStyles,
    Box, Avatar
} from '@material-ui/core'
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import { useRouter } from 'next/router';
import dynamic from 'next/dynamic'
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })
import { getToplantiGorevleri } from '../../services/toplanti.service'
import moment from 'moment'
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { useSelector } from 'react-redux'

const useStyles = makeStyles(theme => ({
    chart: {},
    sorumlu: {
        display: 'flex',
        alignItems: 'center'
    },
    sorumluAvatar: {
        marginRight: theme.spacing(1)
    },

}))

const options = {
    chart: {
        width: 380,
        type: 'pie',
    },
    labels: ['Başarılı', 'Açık', 'Geciken'],
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                show: false
            }
        }
    }]
}
const series = [1, 2, 3]


function durumComponent(durum) {
    if (durum == 'acik') {
        return <Box bgcolor="success.main" color="primary.contrastText" p={1}>Açık</Box>
    }
    else if (durum == 'basarili') {
        return <Box bgcolor="info.main" color="primary.contrastText" p={1}>Tamamlandı</Box>
    }
    else if (durum == 'basarisiz') {
        return <Box bgcolor="error.main" color="primary.contrastText" p={1}>Başarısız</Box>
    }
}

const ToplantiGorevleri = props => {
    const classes = useStyles()
    const { toplanti } = props
    const [gorevler, setGorevler] = useState([])
    const router = useRouter()
    const [chartValues, setChartValues] = useState([0, 0, 0])
    const users = useSelector(state => state.auth.users)

    useEffect(() => {
        const load = async (toplantiId) => {
            let _gorevler = await getToplantiGorevleri(toplantiId)
            let acik = 0
            let kapali = 0
            let geciken = 0
            for (let gorev of _gorevler) {
                if (gorev.sonuc == 'acik') {
                    acik = acik + 1
                }
                else if (gorev.sonuc == 'basarili') {
                    kapali = kapali + 1
                }
                //if (gorev.sonuc == 'acik') {
                //    acik = acik + 1
                //}
            }
            setChartValues([kapali, acik, geciken])
            setGorevler(_gorevler)
        }
        if (toplanti) {
            load(toplanti._id)
        }
    }, [])


    return <>
        <Card>
            <CardHeader
                title={<Typography variant='h6'>İlgili Görevler</Typography>}
                subheader='Toplantı kararlarından oluşan görevler'
                avatar={<FormatListBulletedIcon />}
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={8}>
                        <Table size='small'>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Görev</TableCell>
                                    <TableCell>Sorumlu</TableCell>
                                    <TableCell>Son Tarih</TableCell>
                                    <TableCell>Durum</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {gorevler.map(gorev => {
                                    let _user = users.find(u => u.key == gorev.vsorumlu)
                                    return (
                                        <TableRow hover key={gorev._id}>
                                            <TableCell>{gorev.baslik}</TableCell>
                                            <TableCell>
                                                <div className={classes.sorumlu}>
                                                    <Avatar className={classes.sorumluAvatar} src={_user ?.image} />
                                                    <Typography variant='body2'>{_user?.username}</Typography>
                                                </div>
                                            </TableCell>
                                            <TableCell>{moment(gorev.sonTarih).format('DD.MM.YYYY')}</TableCell>
                                            <TableCell>{durumComponent(gorev.sonuc)}</TableCell>
                                            <TableCell>
                                                <Button
                                                    variant='outlined'
                                                    color='primary'
                                                    size='small'
                                                    endIcon={<ChevronRightIcon />}
                                                    onClick={() => { router.push(`/gorev/${gorev._id}`) }}>Detay</Button></TableCell>
                                        </TableRow>
                                    )
                                })}
                            </TableBody>
                        </Table>
                    </Grid>
                    <Grid item xs={4}>
                        <div className={classes.chart}>
                            <Chart options={options} series={chartValues} type='pie' />
                        </div>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default ToplantiGorevleri