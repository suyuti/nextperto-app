import React, { useState, useEffect } from 'react'
import { Grid, TextField, MenuItem, Typography, Paper, Button, makeStyles } from '@material-ui/core'
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import ToplantiKararDurumlari from '../../../constants/ToplantiKararDurumlari.json'
import KararTableRow from './KararTableRow.js';
const _ = require('lodash')

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
    },
    row: {
        marginTop: theme.spacing(1),
        width: '100%'
    }
}))

const KararTable = props => {
    const { onChange, data, organizerPersoneller, firmaPersonelleri } = props
    const classes = useStyles()
    // const [rows, setRows] = useState(data.length == 0 ? [{}] : data)

    // Karar - Ilgili Personel - Firma Ilgili Kisi - Son Tarih - Durum
    //  4               2                2               2         2

    /*    useEffect(() => {
            onChange(rows)
        }, [rows])
    
        const onRowChange = async (e, index) => {
            let r = [...rows]
            r[index] = e
    
            let lastRow = r[r.length - 1]
            if (lastRow) {
                if (!_.isEmpty(lastRow)) {
                    r.push({})
                }
            }
            setRows(r)
        }
    */
    return <>
        <Paper>
            <div className={classes.root}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <Grid container >
                        <Grid item xs={4}><Typography variant='body1'>Karar</Typography></Grid>
                        <Grid item xs={2}><Typography variant='body1'>İlgili Personel</Typography></Grid>
                        <Grid item xs={2}><Typography variant='body1'>Firma İlgili Kişi</Typography></Grid>
                        <Grid item xs={2}><Typography variant='body1'>Son Tarih</Typography></Grid>
                        <Grid item xs={2}><Typography variant='body1'>Durum</Typography></Grid>
                        {data.map((row, rowIndex) => {
                            return <div className={classes.row}> <KararTableRow
                                row={row}
                                onChange={(e) => onChange(e, rowIndex)}
                                organizerPersoneller={organizerPersoneller}
                                firmaPersonelleri={firmaPersonelleri} />
                            </div>
                        })}
                    </Grid>
                </MuiPickersUtilsProvider>
            </div>
        </Paper>    </>
}

export default KararTable