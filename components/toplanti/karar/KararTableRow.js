import React, { useState } from 'react'
import { Grid, TextField, MenuItem, IconButton, ListItem, ListItemAvatar, ListItemText, Avatar } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import ToplantiKararDurumlari from '../../../constants/ToplantiKararDurumlari.json'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';

const KararTableRow = props => {
    const { row, onChange, firmaPersonelleri, organizerPersoneller } = props
    const [data, setData] = useState(row)

    return <>
        <Grid item xs={12} container>
            <Grid item xs={4}>
                <TextField
                    variant='outlined'
                    fullWidth
                    multiline
                    rows={1}
                    rowsMax={Infinity}
                    value={data.karar}
                    onChange={(e) => {
                        let d = { ...data }
                        d.karar = e.target.value
                        setData(d)
                    }}
                    onBlur={() => onChange(data)}
                    onKeyPress={(e) => {
                        if (e.key === 'Enter') {
                            onChange(data)
                        }
                    }}
                />
            </Grid>
            <Grid item xs={2}>
                <Autocomplete
                    value={organizerPersoneller.find(p => p.key == data.vorganizerSorumlu)}
                    options={organizerPersoneller || []}
                    getOptionLabel={opt => opt.username}
                    renderOption={(option, state) => {
                        return <ListItem dense>
                            <ListItemAvatar>
                                <Avatar src={option.image} />
                            </ListItemAvatar>
                            <ListItemText primary={option.username} />
                        </ListItem>
                    }}
                    renderInput={params => <TextField {...params} label='Personel' variant='outlined' />}
                    onChange={(e, v, r) => {
                        if (v) {
                            let d = {...data}
                            d.vorganizerSorumlu = v.key
                            setData(d)
                            onChange(d)
                        }
                    }}
                />
            </Grid>
            <Grid item xs={2}>
                <Autocomplete
                    value={firmaPersonelleri.find(p => p.key == data.vfirmaSorumlu)}
                    options={firmaPersonelleri || []}
                    getOptionLabel={opt => opt.adi}
                    renderOption={(option, state) => {
                        return <ListItem dense>
                            <ListItemAvatar>
                                <Avatar src={option.image} />
                            </ListItemAvatar>
                            <ListItemText primary={option.adi} secondary={option.unvani} />
                        </ListItem>
                    }}
                    renderInput={params => <TextField {...params} label='Firma Personeli' variant='outlined' />}
                    onChange={(e, v, r) => {
                        if (v) {
                            let d = {...data}
                            d.vfirmaSorumlu = v.key
                            setData(d)
                            onChange(d)
                        }
                    }}
                />
            </Grid>
            <Grid item xs={2}><KeyboardDatePicker
                disableToolbar
                fullWidth
                autoOk
                variant="inline"
                inputVariant="outlined"
                format="dd.MM.yyyy"
                id="date-picker-inline"
                value={data.tarih}
                clearable
                //value={selectedDate}
                onChange={(e) => {
                    let d = { ...data }
                    d.tarih = e
                    setData(d)
                    onChange(d)
                }}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
            /></Grid>
            <Grid item xs={2}>
                <div style={{ display: "flex" }}>
                    <TextField 
                        variant='outlined' 
                        fullWidth 
                        select 
                        value={data.durum}
                        onChange={e => {
                            let d ={...data}
                            d.durum = e.target.value
                            setData(d)
                            onChange(d)
                        }}>
                        {ToplantiKararDurumlari.map(durum => <MenuItem key={durum.value} value={durum.value}>{durum.label}</MenuItem>)}
                    </TextField>
                    <IconButton ><DeleteOutlineIcon /></IconButton>
                </div>
            </Grid>
        </Grid>

    </>
}

export default KararTableRow