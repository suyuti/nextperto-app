import React from 'react'
import { Card, CardHeader, TextField, MenuItem, CardContent, Divider, Chip, Avatar, Typography, ListItem, ListItemText, ListItemAvatar, Checkbox } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import { validateEmail } from '../../helpers/check.helper';

const icon = <CheckBoxOutlineBlankIcon />;
const checkedIcon = <CheckBoxIcon />;

const ToplantiKatilim = props => {
    const { baslik, kisiler, onChange, secilenKisiler, error } = props

    const value = () => {
        if (!secilenKisiler) {
            return []
        }
        let s = []
        for (let k of kisiler) {
            if (secilenKisiler.includes(k.key)) {
                s.push(k)
            }
        }
        return s
    }

    return <>
        <Card>
            <CardHeader
                title={<Typography variant='h6'>{baslik}</Typography>}
                avatar={<PersonAddIcon />}
            />
            <Divider />
            <CardContent>
                <Autocomplete
                    multiple
                    disableCloseOnSelect
                    value={value()}
                    //value={ kisiler.every(k => secilenKisiler?.includes(k._id)) }
                    options={kisiler || []}
                    getOptionLabel={option => {
                        //let unvan = option.unvani ? `[${option.unvani}]` : ''
                        return `${option.adi} ${option.soyadi || ''}`;
                    }}
                    renderTags={(tagValue, getTagProps) =>
                        tagValue.map((opt, index) => {
                            let unvan = opt.unvani ? `[${opt.unvani||''}]` : ''
                            return <Chip
                                avatar={<Avatar src={opt.image} />}
                                label={`${opt.adi} ${unvan||''}`}
                                variant="outlined"
                                {...getTagProps({ index })}
                            />
                        })
                    }
                    getOptionDisabled={(option) => !validateEmail(option.mail)}
                    renderOption={(option, { selected }) => {
                        if (!validateEmail(option.mail)) {
                            return (
                                <>
                                    <ErrorOutlineIcon className='text-red-500' />
                                    <ListItem dense disabled>
                                        <ListItemAvatar>
                                            <Avatar src={option.image} />
                                        </ListItemAvatar>
                                        <ListItemText primary={`${option.adi} ${option.soyadi || ''} [MAIL YOK]`} secondary={option.unvani || ''} />
                                    </ListItem>
                                </>
                            )
                        }
                        else {
                            return (
                                <React.Fragment>
                                    <Checkbox
                                        
                                        icon={icon}
                                        checkedIcon={checkedIcon}
                                        style={{ marginRight: 8 }}
                                        checked={selected}
                                    />
                                    <ListItem dense >
                                        <ListItemAvatar>
                                            <Avatar src={option.image} />
                                        </ListItemAvatar>
                                        <ListItemText primary={`${option.adi} ${option.soyadi || ''}`} secondary={option.unvani || ''} />
                                    </ListItem>
                                </React.Fragment>
                            )
                        }
                    }}
                    //renderOption={(option, state) => {
                    //    return <ListItem>
                    //        <ListItemAvatar>
                    //            <Avatar src={option.image} />
                    //        </ListItemAvatar>
                    //        <ListItemText primary={option.adi} secondary={option.unvani}/>
                    //    </ListItem>
                    //}}
                    onChange={(event, newValue) => {
                        onChange(newValue.map(v => v.key))
                    }}

                    renderInput={params => (
                        <TextField
                            error={error}
                            helperText={error}
                            fullWidth
                            {...params}
                            variant="outlined"
                            label={baslik}
                            placeholder={baslik}
                        />
                    )}
                />
            </CardContent>
        </Card>
    </>
}
export default ToplantiKatilim