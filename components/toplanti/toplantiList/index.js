import React, { useEffect, useState } from 'react'
import { Table, Typography, Tooltip, TableHead, TableRow, Card, Avatar, Divider, CardContent, CardHeader, TableCell, TableBody, makeStyles, Button, IconButton } from '@material-ui/core'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import SyncIcon from '@material-ui/icons/Sync';
import { getToplanti } from '../../../services/toplanti.service'
import moment from 'moment'
import { useRouter } from 'next/router';
import LinearProgress from '@material-ui/core/LinearProgress';
import {useSelector} from 'react-redux'

const useStyles = makeStyles(theme => ({
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    kimden: {
        display: 'flex',
        alignItems: 'center'
    },
    textContainer: {
        display: 'block',
        width: '100px',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
    }

}))

const ToplantiList = props => {
    const classes = useStyles()
    const { title, subTitle, katilimcilar, limit, acik } = props
    const [toplantilar, setToplantilar] = useState([])
    const router = useRouter()
    const [loading, setLoading] = useState(false)
    const user = useSelector(state => state.auth.user)

    const load = async () => {
        setLoading(true)
        getToplanti(`select=baslik,vfirma,saat&skip=0${limit? `&limit=${limit}`: ''}&populate=firma.marka&sort=start${acik?'&status=acik' : ''}&vorganizerKatilimcilar=${katilimcilar}`).then(resp => {
            setToplantilar(resp)
            setLoading(false)
        })
    }

    useEffect(() => {
        load()
    }, [])

    return <>
    {loading && <LinearProgress />}
        <Card>
            <CardHeader
                title={title}
                subheader={subTitle}
                action={<IconButton onClick={() => {
                    load()
                }}><SyncIcon /></IconButton>}
            />
            <Divider />
            <CardContent>
                <Table size='small'>
                    <TableHead>
                        <TableRow>
                            <TableCell>Firma</TableCell>
                            <TableCell>Konu</TableCell>
                            <TableCell>Tarih</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {toplantilar.map(t => {
                            return (
                                <TableRow hover>
                                    <TableCell>{t.firma?.marka}</TableCell>
                                    <TableCell>
                                        <Tooltip title={<Typography variant='body1'>{t.baslik}</Typography>}>
                                            <div className={classes.textContainer}>{t.baslik}</div>
                                        </Tooltip>
                                    </TableCell>
                                    <TableCell >{moment(t.start).format('DD.MM.YYYY')} {t.saat}</TableCell>
                                    <TableCell><IconButton size='small' onClick={() => {
                                        router.push(`/toplanti/${t._id}`)
                                    }}><ArrowForwardIosIcon fontSize='small' /></IconButton></TableCell>
                                </TableRow>

                            )
                        })}
                    </TableBody>
                </Table>
            </CardContent>
        </Card>
    </>
}

export default ToplantiList