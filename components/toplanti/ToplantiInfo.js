import React, { useState } from 'react'
import {
    Card, CardHeader, CardContent, Divider, Grid, TextField,
    MenuItem, Typography, Button, IconButton, FormControl, FormControlLabel, Switch
} from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import Saatler from '../../constants/saatler.json'
import Sureler from '../../constants/sure.json'
import ToplantiKategoriler from '../../constants/ToplantiKategori.json'
import ToplantiTurleri from '../../constants/ToplantiTurleri.json'
import EventIcon from '@material-ui/icons/Event';
import ScheduleIcon from '@material-ui/icons/Schedule';
import { useDispatch } from 'react-redux'
import * as Actions from '../../redux/actions'
import RemoteAutoComplete from '../autocomplete'
import { searchFirmalar } from '../../services/firma.service'

const ToplantiInfo = props => {
    const dispatch = useDispatch()
    const { musteriler, toplanti, onToplantiChanged, errors } = props
    const [results, setResults] = useState([])

    return <>
        <Card>
            <CardHeader
                title={<Typography variant='h6'>Toplantı Bilgileri</Typography>}
                avatar={<EventIcon />}
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={2}>
                        <FormControl component='fieldset'>
                            <FormControlLabel
                                value={toplanti.icToplanti}
                                control={
                                    <Switch
                                        color="primary"
                                        checked={toplanti.icToplanti}
                                        onChange={(e) => {
                                            onToplantiChanged({ field: 'icToplanti', value: e.target.checked })
                                        }}
                                    />}
                                label={toplanti.icToplanti ? 'İç Toplantı' : "Dış Toplantı"}
                                labelPlacement="top"
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={10}>
                        <RemoteAutoComplete
                            label='Firma'
                            error={errors?.firma}
                            results={results}
                            labelField='marka'
                            value={toplanti.firma ?.marka}
                            onSelected={(f) => {
                                onToplantiChanged({ field: 'vfirma', value: f })
                            }}
                            fetchRemote={(searchText) => {
                                searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                                    setResults(resp)
                                })
                            }}
                        />
                        {/*
                        <Autocomplete
                            id='toplanti-musteri'
                            options={musteriler || []}
                            getOptionLabel={option => option.marka}
                            value={musteriler.find(m => m.key == toplanti.vfirma)}
                            renderInput={params => <TextField {...params} label="İlgili Firma"
                                variant='outlined'
                            />}
                            onChange={(e, v, r) => {
                                if (v) {
                                    onToplantiChanged({ field: 'vfirma', value: v }) // hem key hem de _id lazim oldugu icin v gonderildi
                                }
                            }}
                        />
                        */}
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            error={errors && errors.baslik}
                            helperText={errors && errors.baslik}
                            label='Toplantı Konu Başlığı'
                            variant='outlined'
                            fullWidth
                            multiline
                            rows={1}
                            rowsMax={Infinity}
                            value={toplanti.baslik}
                            onChange={(e) => onToplantiChanged({ field: 'baslik', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label='Açıklama'
                            variant='outlined'
                            multiline
                            rows={2}
                            rowsMax={Infinity}
                            fullWidth
                            value={toplanti.aciklama}
                            onChange={(e) => onToplantiChanged({ field: 'aciklama', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                error={errors && errors.start}
                                helperText={errors && errors.start}
                                autoOk
                                fullWidth
                                clearable
                                variant="inline"
                                inputVariant="outlined"
                                label="Toplantı Tarihi"
                                format="dd/MM/yyyy"
                                InputAdornmentProps={{ position: 'start' }}
                                value={toplanti.start}
                                onChange={(e) => onToplantiChanged({ field: 'start', value: e })}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={1}>
                        <IconButton onClick={(e) => dispatch(Actions.setToplantiRightPanelType('zamanlama'))}><ScheduleIcon /></IconButton>
                    </Grid>
                    <Grid item xs={7}>
                        <TextField
                            label='Toplantı Konumu'
                            variant='outlined'
                            fullWidth
                            value={toplanti.yer}
                            onChange={(e) => onToplantiChanged({ field: 'yer', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            error={errors && errors.saat}
                            helperText={errors && errors.saat}
                            label='Toplantı Saati'
                            variant='outlined'
                            fullWidth
                            value={toplanti.saat}
                            onChange={(e) => onToplantiChanged({ field: 'saat', value: e.target.value })}
                            select
                        >
                            {Saatler.map((saat, i) => <MenuItem key={saat} value={saat}>{saat}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            error={errors && errors.sure}
                            helperText={errors && errors.sure}
                            label='Toplantı Süresi'
                            variant='outlined'
                            fullWidth
                            value={toplanti.sure}
                            onChange={(e) => onToplantiChanged({ field: 'sure', value: e.target.value })}
                            select >
                            {Sureler.map((sure) => <MenuItem key={sure.value} value={sure.value}>{sure.label}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            error={errors && errors.tur}
                            helperText={errors && errors.tur}
                            label='Toplantı Türü'
                            variant='outlined'
                            fullWidth
                            value={toplanti.tur}
                            onChange={(e) => onToplantiChanged({ field: 'tur', value: e.target.value })}
                            select
                        >
                            {ToplantiTurleri.map(tur => <MenuItem key={tur.key} value={tur.key}>{tur.label}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label='Toplantı Kategorisi'
                            variant='outlined'
                            fullWidth
                            value={toplanti.kategori}
                            onChange={(e) => onToplantiChanged({ field: 'kategori', value: e.target.value })}
                            select>
                            {ToplantiKategoriler.map(kategori => <MenuItem key={kategori} value={kategori} >{kategori}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label='Toplantı Gündemi'
                            variant='outlined'
                            multiline
                            fullWidth
                            rows={2}
                            rowsMax={Infinity}
                            value={toplanti.gundem}
                            onChange={(e) => onToplantiChanged({ field: 'gundem', value: e.target.value })}
                        />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default ToplantiInfo