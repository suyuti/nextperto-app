import React, { useState } from 'react'
import { Paper, Button, Grid, makeStyles, TextField, MenuItem } from '@material-ui/core'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import { useSelector } from 'react-redux';
import moment from 'moment'
import RemoteAutoComplete from '../autocomplete'
import { searchFirmalar } from '../../services/firma.service'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    buttons: {
        display: 'flex'
    }
}))

const initialQuery = {
    //temp: {
    //    firma: '',
    //    baslangicTarihi: new Date(),
    //    bitisTarihi: new Date()
    //},
    firma: '',
    baslangicTarihi: null,
    bitisTarihi: null,
    katilimci: '',
    status: null,
}

const ToplantiSearchToolbar = props => {
    const { disabled, onSearch } = props
    const classes = useStyles()
    const user = useSelector(state => state.auth.user)
    const users = useSelector(state => state.auth.users)
    const ekip = useSelector(state => state.auth.ekip)
    const [query, setQuery] = useState({ ...initialQuery })
    const [results, setResults] = useState([])

    return <>
        <Paper>
            <div className={classes.root}>
                <Grid container spacing={2}>
                    <Grid item xs={2}>
                        <RemoteAutoComplete
                            label='Firma'
                            size='small'
                            //error={errors?.firma}
                            results={results}
                            labelField='marka'
                            //value={toplanti.firma?.marka}
                            onSelected={(f) => {
                                let q = { ...query }
                                q.firma = f.key
                                setQuery(q)
                            }}
                            fetchRemote={(searchText) => {
                                searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                                    setResults(resp)
                                })
                            }}
                        />

                    </Grid>
                    <Grid item xs={2}>
                        <TextField
                            disabled={disabled}
                            fullWidth
                            label='Katılımcı'
                            variant='outlined'
                            size='small'
                            select
                            value={query.katilimci}
                            onChange={(e) => {
                                let q = { ...query }
                                q.katilimci = e.target.value
                                setQuery(q)
                            }}
                        >
                            <MenuItem key={'ALL'} value={'ALL'}>Tümü</MenuItem>
                            {[user.key, ...ekip].map(u => {
                                let user = users.find(user => user.key == u)
                                if (!user) {
                                    return <></>
                                }
                                return <MenuItem key={user.key} value={user.key}>{user.username}</MenuItem>
                            })}
                        </TextField>
                    </Grid>
                    <Grid item xs={2}>
                        <TextField
                            disabled={disabled}
                            fullWidth
                            label='Durum'
                            size='small'
                            variant='outlined'
                            select
                            value={query.status}
                            onChange={(e) => {
                                let q = { ...query }
                                q.status = e.target.value
                                setQuery(q)
                            }}
                        >
                            <MenuItem key={'ALL'} value={'ALL'}>Tümü</MenuItem>
                            <MenuItem key={'acik'} value={'acik'}>Açık</MenuItem>
                            <MenuItem key={'tamamlandi'} value={'tamamlandi'}>Tamamlandı</MenuItem>
                        </TextField>
                    </Grid>
                    <Grid item xs={2}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                //error={errors && errors.sonTarih}
                                //helperText={errors && errors.sonTarih}
                                disabled={disabled}
                                clearable
                                autoOk
                                fullWidth
                                variant="inline"
                                inputVariant="outlined"
                                label="Tarih"
                                format="dd/MM/yyyy"
                                size='small'
                                value={query.baslangicTarihi}
                                InputAdornmentProps={{ position: 'start' }}
                                onChange={(e) => {
                                    let q = { ...query }
                                    //if (!q.tarih) {
                                    //    q.tarih = {}
                                    //}
                                    //q.tarih.$gte = moment(e).toISOString()
                                    //q.temp.baslangicTarihi = e
                                    q.baslangicTarihi = e
                                    setQuery(q)
                                }}
                            //value={gorev.sonTarih}
                            //onChange={(e) => onGorevChanged({ field: 'sonTarih', value: e })}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={2}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                //error={errors && errors.sonTarih}
                                //helperText={errors && errors.sonTarih}
                                disabled={disabled}
                                clearable
                                autoOk
                                fullWidth
                                variant="inline"
                                size='small'
                                inputVariant="outlined"
                                label="Tarih"
                                format="dd/MM/yyyy"
                                value={query.bitisTarihi}
                                InputAdornmentProps={{ position: 'start' }}
                                onChange={(e) => {
                                    let q = { ...query }
                                    q.bitisTarihi = e
                                    setQuery(q)
                                }}
                            //value={gorev.sonTarih}
                            //onChange={(e) => onGorevChanged({ field: 'sonTarih', value: e })}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={2}>
                        <div className={classes.buttons}>
                            <Button
                                disabled={disabled}
                                variant='contained'
                                onClick={() => {
                                    onSearch(query)
                                }}
                                color='primary'>Sorgula</Button>
                            <Button
                                disabled={disabled}
                                variant='outlined'
                                onClick={() => {
                                    setQuery({
                                        baslangicTarihi: null,
                                        bitisTarihi: null,
                                        firma: '',
                                        katilimci: '',
                                        status: null,
                                    })
                                }}
                                color='primary'>Temizle</Button>
                        </div>
                    </Grid>

                </Grid>
            </div>
        </Paper>
    </>
}

export default ToplantiSearchToolbar