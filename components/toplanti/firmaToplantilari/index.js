import React, { useState, useEffect } from 'react'
import { Paper, Card, CardHeader, Divider, CardContent, Typography, IconButton, Collapse, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import DateRangeIcon from '@material-ui/icons/DateRange';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import { getToplanti } from '../../../services/toplanti.service'
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { useRouter } from 'next/router'
import moment from 'moment'

const FirmaToplantilari = props => {
    const { firmaKey } = props
    const [show, setShow] = useState(null)
    const [toplantilar, setToplantilar] = useState([])
    const router = useRouter()

    const load = async () => {
        getToplanti(`vfirma=${firmaKey}&sort=-start`).then(resp => {
            setToplantilar(resp)
        })
    }

    useEffect(() => {
        load()
    }, [])

    return (<>
        <Card>
            <CardHeader
                title={<Typography variant='h6'>Diğer Toplantılar</Typography>}
                subheader='Firma ile yapılan önceki toplantılar'
                avatar={<DateRangeIcon />}
                action={
                    <IconButton onClick={() => {
                        setShow(!show)
                    }}>
                        {show ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                    </IconButton>}
            />
            <Collapse in={show} timeout="auto" unmountOnExit>
                <Divider />
                <CardContent>
                    <Table size='small'>
                        <TableHead>
                            <TableRow>
                                <TableCell>Tarih</TableCell>
                                <TableCell>Konu</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {toplantilar.map(t => {
                                return <TableRow hover>
                                    <TableCell>{moment(t.start).format('DD.MM.YYYY')}</TableCell>
                                    <TableCell>{t.baslik}</TableCell>
                                    <TableCell><IconButton size='small' onClick={() => {
                                        router.push(`/toplanti/${t._id}`, undefined, {shallow: true})
                                    }}><ChevronRightIcon /></IconButton></TableCell>
                                </TableRow>
                            })}
                        </TableBody>
                    </Table>
                </CardContent>
            </Collapse>
        </Card>

    </>)
}

export default FirmaToplantilari