import React, { useState, useEffect } from 'react'

import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import { Paper, Card, CardHeader, Divider, CardContent, Typography, IconButton, Collapse } from '@material-ui/core';
import { makeStyles } from "@material-ui/core/styles";
import HistoryIcon from '@material-ui/icons/History';
import api from '../../services/axios.service';
import { parseCookies } from 'nookies'
import moment from 'moment'
import UpdateIcon from '@material-ui/icons/Update';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';

const useStyles = makeStyles(() => ({
    oppositeContent: {
        // TODO: adjust this value accordingly
        flex: 10
    }
}));
const ToplantiHistory = props => {
    const classes = useStyles();
    const { toplanti } = props
    const [history, setHistory] = useState(null)
    const [show, setShow] = useState(null)

    useEffect(() => {
        const load = async (toplanti) => {
            const cookies = parseCookies()
            api.get(`/toplanti/${toplanti._id}/history`, {
                headers: { Authorization: `Bearer ${cookies.experto}` }
            }).then(resp => {
                setHistory(resp.data.data)
            })
        }
        if (toplanti) {
            load(toplanti)
        }
    }, [])

    return <>
        <Card>
            <CardHeader
                title={<Typography variant='h6'>Toplantı Geçmişi</Typography>}
                avatar={<HistoryIcon />}
                action={
                    <IconButton onClick={() => {
                        setShow(!show)
                    }}>
                        {show ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                    </IconButton>}
            />
            <Collapse in={show} timeout="auto" unmountOnExit>
                <Divider />
                <CardContent>
                    <div>
                        <Timeline align="left">
                            {history ?.map(item => {
                                let icon = <UpdateIcon fontSize='small' />
                                if (item.reason == 'created') {
                                    icon = <AddCircleOutlineIcon fontSize='small' />
                                }
                                return (
                                    <TimelineItem>
                                        <TimelineOppositeContent>
                                            <Typography variant='body2' color='textSecondary'>{moment(item.updatedAt).format('DD.MM.YYYY')}</Typography>
                                        </TimelineOppositeContent>
                                        <TimelineSeparator>
                                            <TimelineDot color='primary' variant='outlined'>
                                                {icon}
                                            </TimelineDot>
                                            <TimelineConnector />
                                        </TimelineSeparator>
                                        <TimelineContent className={classes.oppositeContent}>
                                            <div>
                                                <Typography variant='body1'>[user]</Typography>
                                                {Object.keys(item.diff).map(key => {
                                                    return <Typography variant='body1'>{key}</Typography>
                                                })}
                                            </div>
                                        </TimelineContent>
                                    </TimelineItem>
                                )
                            })}
                        </Timeline>
                    </div>
                </CardContent>
            </Collapse>
        </Card>
    </>
}
export default ToplantiHistory