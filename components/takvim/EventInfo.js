import React, { useState } from 'react'
import { Grid, Typography, makeStyles, Divider, Button, Card, CardContent, CardActions, CardHeader } from '@material-ui/core'
import moment from 'moment'
import {getToplanti} from '../../services/toplanti.service'
import { useRouter } from 'next/router'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    baslik: {
        textAlign: 'center'
    }
}))
const EventInfo = props => {
    const { event } = props
    const classes = useStyles()
    const router = useRouter()

    return <>
        <div className={classes.root}>
            <Card>
                <CardHeader
                    title='Toplantı'
                />
                <Divider />
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Typography variant='body1'>{event.title}</Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography variant='body1'>{moment(event.start).format('HH:mm DD.MM.YYYY')}</Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography variant='body1'>{moment(event.end).format('HH:mm DD.MM.YYYY')}</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant='body1'>Katılımcılar</Typography>
                        </Grid>
                    </Grid>
                </CardContent>
                <Divider />
                <CardActions>
                    <Button variant='outlined' color='primary' onClick={() => {
                        getToplanti(`select=baslik&skip=0&limit=1&msEventId=${event.id}`).then(resp => {
                            if (resp.length > 0) {
                                router.push(`/toplanti/${resp[0]._id}`)
                            }
                            else {
                                alert('Toplantı kaydı bulanamadı.')
                            }
                        })
                    }}>Toplantıya Git</Button>
                </CardActions>
            </Card>
        </div>
    </>
}

export default EventInfo