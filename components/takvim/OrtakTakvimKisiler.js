import React from 'react'
import { Card, CardHeader, TextField, 
    MenuItem, CardContent, Divider, 
    Button,
    Chip, Avatar, Typography, ListItem, ListItemText, ListItemAvatar, Checkbox, CardActions } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

const icon = <CheckBoxOutlineBlankIcon />;
const checkedIcon = <CheckBoxIcon />;


const OrtakTakvimKisiler = props => {
    const { baslik, kisiler, onChange, secilenKisiler, onFetch, disableFetch } = props

    const value = () => {
        if (!secilenKisiler) {
            return []
        }
        let s = []
        for (let k of kisiler) {
            if (secilenKisiler.includes(k._id)) {
                s.push(k)
            }
        }
        return s
    }

    return <>
        <Card>
            <CardHeader
                title={<Typography variant='h6'>{baslik}</Typography>}
                avatar={<PersonAddIcon />}
            />
            <Divider />
            <CardContent>
                <Autocomplete
                    multiple
                    disableCloseOnSelect
                    value={value()}
                    //value={ kisiler.every(k => secilenKisiler?.includes(k._id)) }
                    options={kisiler || []}
                    getOptionLabel={option => {
                        //let unvan = option.unvani ? `[${option.unvani}]` : ''
                        return `${option.adi}`;
                    }}
                    renderTags={(tagValue, getTagProps) =>
                        tagValue.map((opt, index) => {
                            let unvan = opt.unvani ? `[${opt.unvani}]` : ''
                            return <Chip
                                avatar={<Avatar src={opt.image} />}
                                label={`${opt.adi} ${unvan}`}
                                variant="outlined"
                                {...getTagProps({ index })}
                            />
                        })
                    }
                    renderOption={(option, { selected }) => (
                        <React.Fragment>
                            <Checkbox
                                icon={icon}
                                checkedIcon={checkedIcon}
                                style={{ marginRight: 8 }}
                                checked={selected}
                            />
                            <ListItem dense>
                                <ListItemAvatar>
                                    <Avatar src={option.image} />
                                </ListItemAvatar>
                                <ListItemText primary={option.adi} secondary={option.unvani} />
                            </ListItem>
                        </React.Fragment>
                    )}
                    //renderOption={(option, state) => {
                    //    return <ListItem>
                    //        <ListItemAvatar>
                    //            <Avatar src={option.image} />
                    //        </ListItemAvatar>
                    //        <ListItemText primary={option.adi} secondary={option.unvani}/>
                    //    </ListItem>
                    //}}
                    onChange={(event, newValue) => {
                        onChange(newValue.map(v => v._id))
                    }}

                    renderInput={params => (
                        <TextField
                            //error={error}
                            //helperText={error}
                            fullWidth
                            {...params}
                            variant="outlined"
                            label={baslik}
                            placeholder={baslik}
                        />
                    )}
                />
            </CardContent>
            <Divider />
            <CardActions>
                <Button 
                    disabled={disableFetch}
                    variant='outlined' color='primary' onClick={onFetch}>Sorgula</Button>
            </CardActions>
        </Card>
    </>
}

export default OrtakTakvimKisiler