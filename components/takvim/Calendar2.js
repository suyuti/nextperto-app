import React, { useState, useEffect } from 'react'
import { Grid, makeStyles, Paper, Card, CardHeader, Divider, CardContent, Drawer, LinearProgress } from '@material-ui/core'
import { Calendar, Views, momentLocalizer } from 'react-big-calendar';
import moment from 'moment'
import "react-big-calendar/lib/css/react-big-calendar.css";
import OrtakTakvimKisiler from './OrtakTakvimKisiler';
import { useSelector } from 'react-redux'
import { getOrtakTakvimEvents } from '../../services/toplanti.service'
import EventInfo from './EventInfo';

const localizer = momentLocalizer(moment);
let allViews = Object.keys(Views).map(k => Views[k])

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
    },
    takvim: {
        padding: theme.spacing(2),
        //maxWidth:'70%'
        //width: '640px'
    }
}))

const Takvim = props => {
    const classes = useStyles()
    const [currentDate, setCurrentDate] = useState(new Date())
    const [currentView, setCurrentView] = useState('month')
    const [events, setEvents] = useState([])
    const users = useSelector(state => state.auth.users)
    const [expertoPersonel, setExpertoPersonel] = useState(users.map(u => { return { adi: u.username, unvani: '', image: u.image, _id: u._id } }))
    const [form, setForm] = useState({})
    const [firstVisibleDate, setFirstVisibleDate] = useState(null)
    const [lastVisibleDate, setLastVisibleDate] = useState(null)
    const [fetching, setFetching] = useState(false)
    const [showEventInfo, setShowEventInfo] = useState(false)
    const [selectedEvent, setSelectedEvent] = useState(null)

    const onSecilenKisilerChange = async (kisiler) => {
        let f = { ...form }
        f.kisiler = kisiler
        setForm(f)
    }

    const onFetchOrtakTakvim = async () => {
        setFetching(true)
        getOrtakTakvimEvents(form.kisiler, firstVisibleDate, lastVisibleDate).then(response => {
            setFetching(false)
            setEvents(response.map(r => { return { id: r.id, start: new Date(r.start), end: new Date(r.end), title: r.title } }))
        })
    }

    useEffect(() => {
        if (currentDate && currentView && currentView != 'agenda') {
            computeDisplayedDateRange()
        }
    }, [currentDate, currentView])

    const computeDisplayedDateRange = () => {
        let start = moment(currentDate).startOf(currentView)
        let end = moment(currentDate).endOf(currentView)
        if (currentView == 'month') {
            start = start.startOf('week');
            end = end.endOf('week');
        }
        setFirstVisibleDate(start.toString())
        setLastVisibleDate(end.toString())
    }

    return (
        <>
            <Drawer
                open={showEventInfo}
                anchor='right'
                onClose={() => { setShowEventInfo(false) }}
            >
                <EventInfo event={selectedEvent} />
            </Drawer>
            <Grid container spacing={2}>
                <Grid item xs={9}>
                    {fetching && <LinearProgress />}
                    <Card>
                        <CardHeader
                            title='Ortak Takvim'
                        />
                        <Divider />
                        <CardContent>
                            <div className={classes.takvim}>
                                <Calendar
                                    localizer={localizer}
                                    ////culture={culture}
                                    selectable
                                    views={allViews}
                                    step={60}
                                    showMultiDayTimes
                                    defaultDate={new Date()}
                                    startAccessor="start"
                                    endAccessor="end"
                                    style={{ minHeight: 650 }}
                                    //components={{
                                    //    timeSlotWrapper: ColoredDateCellWrapper
                                    //}}
                                    date={currentDate}
                                    view={currentView}
                                    onView={(view) => setCurrentView(view)}
                                    onNavigate={(date) => setCurrentDate(date)}
                                    events={events}
                                    //eventPropGetter={customEventPropGetter}
                                    onSelectEvent={(event) => {
                                        setSelectedEvent(event)
                                        setShowEventInfo(true)
                                    }}
                                />
                            </div>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={3}>
                    <OrtakTakvimKisiler
                        baslik='Kişiler'
                        onChange={onSecilenKisilerChange}
                        kisiler={expertoPersonel}
                        secilenKisiler={form.kisiler}
                        onFetch={onFetchOrtakTakvim}
                        disableFetch={fetching}
                    />
                </Grid>
            </Grid>
        </>
    )
}

export default Takvim