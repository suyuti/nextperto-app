import React, { useState } from 'react'
import {
    Card, CardHeader, CardContent,
    Divider, Button, IconButton,
    Grid, TextField, MenuItem,
    Typography, FormControl,
    FormControlLabel, Switch,
    makeStyles
} from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import GorevKategoriler from '../../constants/GorevKategorileri.json'
import GorevOncelikler from '../../constants/GorevOncelikler.json'
import EventIcon from '@material-ui/icons/Event';
import { useRouter } from 'next/router';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import RemoteAutoComplete from '../autocomplete'
import { searchFirmalar } from '../../services/firma.service'
import trLocale from "date-fns/locale/tr";

const useStyles = makeStyles(theme => ({
    input: {
        margin: theme.spacing(1),
        height: 56
    }
}))

const GorevInfo = props => {
    const classes = useStyles()
    const { errors, musteriler, gorev, onGorevChanged, onayTalepleri } = props
    const router = useRouter()
    const [results, setResults] = useState([])
    return <>
        <Card>
            <CardHeader
                title={<Typography variant='h6'>Görev Bilgileri</Typography>}
                avatar={<EventIcon />}
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    {onayTalepleri &&
                        <Grid item xs={12}>
                            <div className='flex justify-between bg-red-100 rounded shadow p-1 items-center'>
                                <h2 className=' flex-grow text-red-900 text-center text-xl'>Onay Bekleniyor</h2>
                                <IconButton 
                                    className='focus:outline-none'
                                    onClick={(e) => {
                                        e.preventDefault()
                                        router.push(`/onay/${onayTalepleri[0]._id}`)
                                    }}
                                    >
                                    <ArrowForwardIcon />
                                </IconButton>
                            </div>
                        </Grid>
                    }
                    <Grid item xs={10}>
                        <TextField
                            error={errors && errors.baslik}
                            helperText={errors && errors.baslik}
                            label='Başlık'
                            variant='outlined'
                            fullWidth
                            multiline
                            rows={1}
                            rowsMax={Infinity}
                            value={gorev.baslik}
                            onChange={(e) => onGorevChanged({ field: 'baslik', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <TextField
                            label='Öncelik'
                            variant='outlined'
                            fullWidth
                            value={gorev.oncelik}
                            onChange={(e) => onGorevChanged({ field: 'oncelik', value: e.target.value })}
                            select
                        >
                            {GorevOncelikler.map(oncelik => <MenuItem key={oncelik.value} value={oncelik.value}>{oncelik.label}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label='Açıklama'
                            variant='outlined'
                            multiline
                            rows={2}
                            rowsMax={Infinity}
                            fullWidth
                            value={gorev.aciklama}
                            onChange={(e) => onGorevChanged({ field: 'aciklama', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={trLocale}>
                            <KeyboardDatePicker
                                //error={error && error.tarih}
                                //helperText={error && error.tarih}
                                error={errors && errors.sonTarih}
                                helperText={errors && errors.sonTarih}
                                clearable
                                autoOk
                                fullWidth
                                variant="inline"
                                inputVariant="outlined"
                                label="Görev Son Tarihi"
                                format="dd/MM/yyyy"
                                InputAdornmentProps={{ position: 'start' }}
                                value={gorev.sonTarih}
                                onChange={(e) => onGorevChanged({ field: 'sonTarih', value: e })}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={8}>
                        <TextField
                            label='Görev Kategorisi'
                            variant='outlined'
                            fullWidth
                            value={gorev.kategori}
                            onChange={(e) => onGorevChanged({ field: 'kategori', value: e.target.value })}
                            select
                        >
                            {GorevKategoriler.map(kategori => <MenuItem key={kategori} value={kategori}>{kategori}</MenuItem>)}
                        </TextField>
                    </Grid>

                    <Grid item xs={2}>
                        <FormControl component='fieldset'>
                            <FormControlLabel
                                value={gorev.icGorevMi}
                                control={
                                    <Switch
                                        color="primary"
                                        checked={gorev.icGorevMi}
                                        onChange={(e) => {
                                            onGorevChanged({ field: 'icGorevMi', value: e.target.checked })
                                        }}
                                    />}
                                label={gorev.icGorevMi ? 'İç Görev' : "Dış Görev"}
                                labelPlacement="top"
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={10}>
                        <RemoteAutoComplete
                            label='Firma'
                            error={errors?.firma}
                            results={results}
                            labelField='marka'
                            value={gorev.firma?.marka}
                            onSelected={(f) => {
                                onGorevChanged({ field: 'vfirma', value: f.key })
                            }}
                            fetchRemote={(searchText) => {
                                searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                                    setResults(resp)
                                })
                            }}
                        />
                        {/* 
                        <Autocomplete
                            id='gorev-musteri'
                            disabled={gorev.icGorevMi}
                            options={musteriler || []}
                            getOptionLabel={option => option.marka}
                            value={musteriler.find(m => m.key == gorev.vfirma)}
                            renderInput={params => <TextField {...params}
                                label="İlgili Müşteri"
                                variant='outlined'
                            />}
                            onChange={(e, v, r) => {
                                if (v) {
                                    onGorevChanged({ field: 'vfirma', value: v.key })
                                }
                            }}
                        />
*/}
                    </Grid>
                    {gorev.toplanti && <>
                        <Grid item xs={12}>
                            <Divider />
                        </Grid>
                        <Grid item xs={10}>
                            <TextField

                                label='İlgili Toplantı'
                                variant='outlined'
                                fullWidth
                                InputProps={{
                                    readOnly: true,
                                    className: classes.input
                                }}
                                value={gorev.toplanti.baslik}
                            />
                        </Grid>
                        <Grid item xs={2}>
                            <Button
                                className={classes.input}
                                variant='outlined'
                                color='primary'
                                endIcon={<ArrowForwardIcon />}
                                onClick={() => { router.push(`/toplanti/${gorev.toplanti._id}`) }}>
                                Toplantı</Button>
                        </Grid>
                    </>}
                    <Grid item xs={12}>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default GorevInfo