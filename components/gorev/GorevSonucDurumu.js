import React, { useState } from 'react'
import { ListItem, Card, CardHeader, Divider, CardContent, Grid, TextField, Typography, ListItemAvatar, Avatar, ListItemText, makeStyles } from '@material-ui/core'
import EmojiEventsIcon from '@material-ui/icons/EmojiEvents';
import { useSelector } from 'react-redux'
import moment from 'moment'

const useStyles = makeStyles(theme => ({
    sure: {
        display: 'flex',
        alignItems: 'center',
        "& > p" : {
            margin: theme.spacing(2)
        }
    }
}))

const GorevSonucDurum = props => {
    const classes = useStyles()
    const { gorev } = props
    const users = useSelector(state => state.auth.users)
    const [kapatanUser, setKapatanUser] = useState(users.find(u => u._id == gorev.kapatan))
    let sonuc = ''
    let sonucAciklama = ''

    if (gorev.sonuc == 'basarili') {
        sonuc = 'Başarılı'
    }
    else if (gorev.sonuc == 'basarisiz') {
        sonuc = 'Başarısız'
        sonucAciklama = gorev.sonucAciklama
    }
    else if (gorev.sonuc == 'iptal') {
        sonuc = 'İptal'
        sonucAciklama = gorev.sonucAciklama
    }

    let start = moment(gorev.createdAt)
    let end = moment(gorev.kapatmaTarihi)
    let duration = moment.duration(end.diff(start))
    
    var days = Math.floor(duration.asDays());
    duration.subtract(moment.duration(days,'days'));
    var hours = duration.hours();

    return <>
        <Card>
            <CardHeader
                title={<Typography variant='h6'>Görev Sonuç Durumu</Typography>}
                avatar={<EmojiEventsIcon />}
            />
            <Divider />
            <CardContent>
                <ListItem>
                    <ListItemAvatar>
                        <Avatar src={kapatanUser ?.image} />
                    </ListItemAvatar>
                    <ListItemText primary={sonuc} secondary={sonucAciklama}></ListItemText>
                </ListItem>
                <div className={classes.sure}>
                    <Typography variant='body2' component='p'>Görev Süresi</Typography>
                    <Typography variant='body1' component='p'>{`${days} gün ${hours} saat`}</Typography>
                </div>
            </CardContent>
        </Card>
    </>
}

export default GorevSonucDurum