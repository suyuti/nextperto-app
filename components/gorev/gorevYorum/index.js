import { Card, CardContent, CardHeader, Divider, Typography } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import GorevYorumItem from './GorevYorumItem'
import GorevYorumInput from './GorevYorumInput'
import {getGorevYorumlari} from '../../../services/gorev.service'
import {useSelector} from 'react-redux'
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';

const GorevYorum = (props) => {
    const {gorev} = props
    const [yorumlar, setYorumlar] = useState([])
    const user = useSelector(state => state.auth.user)
    useEffect(() => {
        getGorevYorumlari(gorev._id).then((_yorumlar) => {
            setYorumlar(_yorumlar)
        })
    }, [])

    return (
        <Card>
            <CardHeader
                title={<Typography variant='h6'>Yorumlar</Typography>}
                avatar={<ChatBubbleOutlineIcon />}
            />
            <Divider />
            <CardContent>
                <div className=''>
                    {yorumlar.map((yorum, i) => {
                        return (
                            <GorevYorumItem yorum={yorum} />
                        )
                    })}
                    <GorevYorumInput gorev={gorev} user={user} onAdd={(yorum) => {
                        let _yorumlar = [...yorumlar]
                        _yorumlar.push(yorum)
                        setYorumlar(_yorumlar)
                    }}/>
                </div>
            </CardContent>
        </Card>
    )
}

export default GorevYorum
