import React from 'react'
import moment from 'moment'
import { useSelector } from 'react-redux'
moment.locale('tr')

const GorevYorumItemHeader = (props) => {
    const {userName, date} = props

    return (
        <div className='flex flex-row justify-between'>
            <h2 className='text-base text-gray-800 font-sans font-light antialiased'>{userName}</h2>
            <h2 className='text-sm text-gray-600 font-sans font-light antialiased'>{date}</h2>
        </div>
    )
}

const GorevYorumItem = ({yorum}) => {
    const users = useSelector(state => state.auth.users)
    let user = users.find(u => u.key == yorum.vuser)

    return (
        <div className='flex flex-row border-b-2 border-gray-200 py-4'>
            <div className='m-2'>
                <img className='object-cover h-10 rounded-full border-2 border-gray-500' src={user?.image}></img>
            </div>
            <div className='yorum-body flex-1'>
                <GorevYorumItemHeader userName={user.username} date={moment(yorum.tarih).fromNow()}/>
                <h2 className='mt-4 text-lg text-gray-800 font-light font-sans antialiased'>{yorum.yorum}</h2>
                <div className='flex space-x-2 justify-end'>
                    <h2 className='font-light text-xs'>Okunma Tarihi</h2>
                    <h2 className='font-light text-xs'>{moment(yorum.okunmaTarihi).format('HH:mm DD.MM.YYYY')}</h2>
                </div>
            </div>
        </div>
    )
}

export default GorevYorumItem
