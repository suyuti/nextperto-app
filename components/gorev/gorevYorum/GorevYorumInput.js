import { Button, Grid, TextField } from '@material-ui/core'
import React, { useState } from 'react'
import { postGorevYorum } from '../../../services/gorev.service'

const GorevYorumInput = (props) => {
    const { gorev, user, onAdd} = props
    const [form, setForm] = useState({
        vuser: user.key,
        referans: gorev._id,
        yorum: '',
        tarih: '',
        type: 'GOREV'
    })
    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <h2 className='text-gray-700 text-lg mt-2'>Yeni Yorum Ekle</h2>
            </Grid>
            <Grid item xs={12}>
                <TextField
                    fullWidth
                    variant='outlined'
                    multiline
                    rows={3}
                    value={form.yorum}
                    onChange={(e) => {
                        let f = { ...form }
                        f.yorum = e.target.value
                        setForm(f)
                    }}
                />
            </Grid>
            <Grid item xs={12} justify='flex-end'>
                <Button
                    variant='contained'
                    color='primary'
                    onClick={() => {
                        let f = { ...form }
                        f.tarih = new Date()
                        postGorevYorum(gorev._id, f).then(resp => {
                            onAdd(f)
                            setForm(prev => {return {...prev, yorum: '', tarih: ''}})
                        })
                    }}>Gonder</Button>
            </Grid>
        </Grid>
    )
}

export default GorevYorumInput
