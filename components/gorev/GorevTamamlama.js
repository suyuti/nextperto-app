import React, { useState } from 'react'
import { Grid, Typography, Button, makeStyles, TextField } from '@material-ui/core'
import CancelIcon from '@material-ui/icons/Cancel';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';

const useStyles = makeStyles((theme) => ({
    root: {
        //backgroundColor: 'red',
        padding: 50,
    },
    buttons: {
        display: 'flex',
        flexDirection: 'column',
        width: '200'
    },
    optionButton: {
        margin: theme.spacing(1),
        //marginBottom: 10,
        //margin: 5,
        height: 50,
    },
    aciklama: {
        margin: theme.spacing(1),
        //marginBottom: 20,
    }
}));

const GorevTamamlama = props => {
    const { onOk, onCancel, onChange } = props
    const classes = useStyles()
    const [selection, setSelection] = useState(null)

    const onSelect = async (sel) => {
        setSelection(sel)
        onChange({ field: 'sonuc', value: sel })
    }

    return <>
        <div className={classes.root}>
            <Typography variant='h4'>Görev Tamamlama</Typography>
            <div className={classes.buttons}>
                <Button className={classes.optionButton} variant={selection == 'basarili' ? 'contained' : 'outlined'} color='primary' onClick={(e) => onSelect('basarili')} startIcon={<ThumbUpIcon />}>Başarılı</Button>
                <Button className={classes.optionButton} variant={selection == 'basarisiz' ? 'contained' : 'outlined'} color='primary' onClick={(e) => onSelect('basarisiz')} startIcon={<ThumbDownIcon />}>Başarısız</Button>
                <Button className={classes.optionButton} variant={selection == 'iptal' ? 'contained' : 'outlined'} color='primary' onClick={(e) => onSelect('iptal')} startIcon={<CancelIcon />}>İptal</Button>

            </div>
            {(selection == 'basarisiz' || selection == 'iptal') && <TextField
                className={classes.aciklama}
                variant='outlined'
                fullWidth
                multiline
                rows={2}
                rowsMax={Infinity}
                label='Açıklama'
                onChange={(e) => {
                    onChange({ field: 'sonucAciklama', value: e.target.value })
                }}
            />}
            <Button
                variant='contained'
                disabled={!selection}
                color='primary'
                onClick={(e) => { onOk(selection) }}>Tamam</Button>
            <Button
                variant='contained'
                color='primary'
                onClick={onCancel}>İptal</Button>

        </div>
    </>
}

export default GorevTamamlama