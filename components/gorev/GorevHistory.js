import React, { useEffect, useState } from 'react'
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import { Collapse, IconButton, Paper, Card, CardHeader, Divider, CardContent, Typography } from '@material-ui/core';
import { makeStyles } from "@material-ui/core/styles";
import HistoryIcon from '@material-ui/icons/History';
import api from '../../services/axios.service';
import { parseCookies } from 'nookies'
import moment from 'moment'
import UpdateIcon from '@material-ui/icons/Update';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';

const useStyles = makeStyles(() => ({
    oppositeContent: {
        // TODO: adjust this value accordingly
        //flex: 10
    }
}));
const GorevHistory = props => {
    const classes = useStyles();
    const { gorev } = props
    const [history, setHistory] = useState(null)
    const [show, setShow] = useState(null)

    useEffect(() => {
        const load = async (gorev) => {
            const cookies = parseCookies()
            //api.get(`/gorev/${gorev._id}/history`, {
            api.get(`/history/${gorev._id}`, {
                headers: { Authorization: `Bearer ${cookies.experto}` }
            }).then(resp => {
                setHistory(resp.data.data)
            })
        }
        if (gorev) {
            load(gorev)
        }
    }, [])

    return <>
        <Card>
            <CardHeader
                title={<Typography variant='h6'>Görev Geçmişi</Typography>}
                avatar={<HistoryIcon />}
                action={
                    <IconButton onClick={() => {
                        setShow(!show)
                    }}>
                        {show ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                    </IconButton>}
            />
            <Collapse in={show} timeout="auto" unmountOnExit>
                <Divider />
                <CardContent>
                    <div className='flex-col space-y-1'>
                        {history?.map((item, i) => {
                            return <div className='flex border shadow rounded'>
                                <div className='p-2'>
                                    <UpdateIcon />
                                </div>
                                <div className='flex-1 p-2'>
                                    <div className='flex justify-between items-start'>
                                        <h2>{item.changeDetail}</h2>
                                        <div className=''>
                                            <h2 className='font-extralight'>{item.createdBy.username}</h2>
                                            <h2 className='font-extralight'>{moment(item.createdAt).format('HH:mm DD.MM.YYYY')}</h2>
                                        </div>
                                    </div>
                                    <div className='w-5/6'>
                                        {item.diff?.map((key, i) => {
                                            return (
                                                <div className='flex-col'>
                                                    <h2 className='font-normal text-center'>{key.field}</h2>
                                                    <div className='flex justify-between items-center'>
                                                        <h2 className='w-1/2 font-extralight text-xs'>{key.prev}</h2>
                                                        <ArrowRightAltIcon />
                                                        <h2 className='w-1/2 font-extralight text-xs'>{key.next}</h2>
                                                    </div>
                                                </div>
                                            )
                                        })}

                                    </div>
                                </div>
                            </div>
                        })}
                    </div>
                </CardContent>
            </Collapse>
        </Card>
    </>
}
export default GorevHistory