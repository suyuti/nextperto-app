import React, { useState, useEffect } from 'react'
import { Table, TableHead, TableRow, Card, Avatar, Divider, CardContent, Typography, Tooltip, CardHeader, TableCell, TableBody, makeStyles, Button, IconButton } from '@material-ui/core'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import SyncIcon from '@material-ui/icons/Sync';
import moment from 'moment'
import { useRouter } from 'next/router';
import { getGorev2 } from '../../../services/gorev.service'
import { useSelector } from 'react-redux'
import LinearProgress from '@material-ui/core/LinearProgress';

const useStyles = makeStyles(theme => ({
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    kimden: {
        display: 'flex',
        alignItems: 'center'
    },
    sorumlu: {
        display: 'flex',
        alignItems: 'center'
    },
    sorumluAvatar: {
        marginRight: theme.spacing(1),
        width: theme.spacing(4),
        height: theme.spacing(4),
    },
    textContainer: {
        display: 'block',
        width: '100px',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
    }
}))

const GorevList = props => {
    const classes = useStyles()
    const { title, sorumlular, acik, limit } = props
    const [gorevler, setGorevler] = useState([])
    const router = useRouter()
    const user = useSelector(state => state.auth.user)
    const users = useSelector(state => state.auth.users)
    const [loading, setLoading] = useState(false)

    const load = async () => {
        setLoading(true)
        getGorev2(`select=baslik,firma,sonTarih,vsorumlu&skip=0${limit?`&limit=${limit}`:''}&populate=firma&sort=-sonTarih${acik?'&sonuc=acik':''}&vsorumlu=${sorumlular}&sonTarih<=${moment().toISOString()}`).then(resp => {
            setGorevler(resp.data)
            setLoading(false)
        })
    }

    useEffect(() => {
        load()
    }, [])

    return <>
    {loading && 
        <LinearProgress />
    }
        <Card>
            <CardHeader
                title={<Typography variant='h6'>{title}</Typography>}
                action={<IconButton onClick={() => {
                    load()
                }}><SyncIcon /></IconButton>}
            />
            <Divider />
            <CardContent>
                <Table size='small'>
                    <TableHead>
                        <TableRow>
                            <TableCell size='small' >Görev</TableCell>
                            <TableCell>Son Tarih</TableCell>
                            <TableCell>Kimden</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {gorevler.map(g => {
                            let _user = users?.find(u => u.key == g.vsorumlu)

                            return (
                                <TableRow hover>
                                    <TableCell>
                                        <Tooltip title={<Typography variant='body1'>{g.baslik}</Typography>}>
                                            <div className={classes.textContainer}>{g.baslik}</div>
                                        </Tooltip>
                                    </TableCell>
                                    <TableCell>{moment(g.sonTarih).format('DD.MM.YYYY')}</TableCell>
                                    <TableCell >
                                        <div className={classes.kimden}>
                                            <Avatar className={classes.sorumluAvatar} src={_user ?.image} />
                                            <Typography variant='body2'>{_user ?.username}</Typography>
                                        </div>
                                    </TableCell>
                                    <TableCell><IconButton size='small' onClick={() => {
                                        router.push(`/gorev/${g._id}`)
                                    }}><ArrowForwardIosIcon fontSize='small' /></IconButton></TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </CardContent>
        </Card>
    </>
}

export default GorevList