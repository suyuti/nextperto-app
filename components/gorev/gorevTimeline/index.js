import React from 'react'
import { makeStyles } from '@material-ui/styles'
import dynamic from 'next/dynamic'
import { Paper } from '@material-ui/core'
const _ = require('lodash')

const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })

const useStyles = makeStyles(theme => ({
    root: {}
}))

const series = [
    {
        name: 'Gorev 1',
        data: [
            {
                x: '',
                y: [
                    new Date('2019-03-05').getTime(),
                    new Date('2019-03-08').getTime()
                ]
            }
        ]
    },
    {
        name: 'Gorev 2',
        data: [
            {
                x: '',
                y: [
                    new Date('2019-03-06').getTime(),
                    new Date('2019-03-09').getTime()
                ]
            }
        ]
    },
    {
        name: 'Gorev 3',
        data: [
            {
                x: '',
                y: [
                    new Date('2019-03-03').getTime(),
                    new Date('2019-03-04').getTime()
                ]
            }
        ]
    }, {
        name: 'Gorev 1',
        data: [
            {
                x: '',
                y: [
                    new Date('2019-03-05').getTime(),
                    new Date('2019-03-08').getTime()
                ]
            }
        ]
    },
    {
        name: 'Gorev 1',
        data: [
            {
                x: '',
                y: [
                    new Date('2019-04-10').getTime(),
                    new Date('2019-04-12').getTime()
                ]
            }
        ]
    },

]
const options = {
    chart: {
        height: 450,
        type: 'rangeBar'
    },
    plotOptions: {
        bar: {
            horizontal: true,
            distributed: true,
            dataLabels: {
                hideOverflowingLabels: false
            }
        }
    },
    dataLabels: {
        enabled: true,
        formatter: (val, opt) => {
            return opt.w.globals.labels[opt.dataPointIndex]
        }
    },
    xaxis: {
        type: 'datetime'
    },
    stroke: {
        width: 1
    },
    fill: {
        type: 'solid',
        opacity: 0.6
    },
    legend: {
        show: false
    },
    chart: {
        toolbar: {
            show: false,
            tools: {
                pan: true
            },
            autoSelected: 'pan'
        },
        events: {
            scrolled: (ctx, { xaxis }) => {
                _.debounce(() => {
                    console.log(xaxis)
                }, 1000)
            },
        }
    }
    //legend: {
    //    position: 'top',
    //    horizontalAlign: 'left'
    // }
}

const GorevTimeline = props => {
    const classes = useStyles()
    return <>
        <div className={classes.root}>
            <Paper>
                <Chart options={options} series={series} type='rangeBar' height={250} />

            </Paper>
        </div>
    </>
}
export default GorevTimeline