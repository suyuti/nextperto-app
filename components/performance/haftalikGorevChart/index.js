import React from 'react'
import dynamic from 'next/dynamic'
import { Paper, makeStyles } from '@material-ui/core'
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })

const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const series = [{
    name: 'Gorev',
    data: [44, 55, 57, 56, 61, 8, 3, 0, 6]
    }]

const options = {
    chart: {
        type: 'bar',
        height: 350,
        toolbar: {
            show: false
        }
    },
    plotOptions: {
        bar: {
            horizontal: false,
            columnWidth: '90%',
            endingShape: 'rounded'
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    xaxis: {
        categories: ['-4', '-3', '-2', '-1', '0', '1', '2', '3', '4'],
    },
    yaxis: {
        title: {
            text: 'Görev'
        }
    },
    fill: {
        opacity: 1
    },
    tooltip: {
        y: {
            formatter: function (val) {
                return val + " görev"
            }
        }
    },
    legend: {
        show: false
    }
}


const HaftalikGorevChart = props => {
    const classes = useStyles()

    return <>
        <div className={classes.root}>
            <Chart options={options} series={series} type='bar' height={250} />
        </div>
    </>
}

export default HaftalikGorevChart