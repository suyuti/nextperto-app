import React from 'react'
import { Grid, Typography, Divider, Card, CardHeader, CardContent } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import HaftalikGorevChart from './haftalikGorevChart'

const useStyles = makeStyles(theme => ({
    root: {},
    satir: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
}))

const Performance = props => {
    const classes = useStyles()
    return <>
        <Card>
            <CardHeader
                title='Performans'
                subheader='geliştiriliyor'
            />
            <Divider />
            <CardContent>
                <Grid container>
                <Grid item xs={12}>
                        <div className={classes.satir}>
                            <Typography>Toplam Görevlerim</Typography>
                            <Typography>150</Typography>
                        </div>
                    </Grid>
                    <Grid item xs={12}>
                        <div className={classes.satir}>
                            <Typography>Toplam Toplantılarım</Typography>
                            <Typography>150</Typography>
                        </div>
                    </Grid>
                    <Grid item xs={12}>
                        <div className={classes.satir}>
                            <Typography>Başarılı Görevlerim</Typography>
                            <Typography>150</Typography>
                        </div>
                    </Grid>
                    <Grid item xs={12}>
                        <HaftalikGorevChart />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default Performance