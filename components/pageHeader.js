import React, { Component, PureComponent } from 'react'
import { Paper, Typography, Button, IconButton, Badge, Box } from '@material-ui/core'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { useRouter } from "next/router";
import { useDispatch } from 'react-redux';
import * as Actions from '../redux/actions'

function isClassComponent(component) {
    return (
        typeof component === 'function' &&
        !!component.prototype.isReactComponent
    )
}

function isFunctionComponent(component) {
    return (
        typeof component === 'function' &&
        String(component).includes('return React.createElement')
    )
}

function isReactComponent(component) {
    return (
        isClassComponent(component) ||
        isFunctionComponent(component)
    )
}

const PageHeader = props => {
    const { title, buttons } = props
    const router = useRouter()
    const dispatch = useDispatch()

    return <>
        <div className='flex bg-white p-2 shadow-md items-center'>
            <div className='flex items-center'>
                <IconButton className='focus:outline-none' onClick={() => {
                    dispatch(Actions.setPrevUrl(router.route))
                    dispatch(Actions.setPageHeaderBack())
                    router.back()
                }}>
                    <ArrowBackIosIcon />
                </IconButton>
                <Typography className='page-header-title'
                    variant='h5'>
                    {title}
                </Typography>
            </div>
            <div className='flex flex-grow justify-end'>
                {buttons?.map((button, i) => {
                    if (React.isValidElement(button)) {
                        return <div key={`btn-${i}`} className='p-1'>{button}</div>
                    }
                    if (button.title) {
                        return <div key={`btn-${i}`} className='p-1' >
                            <Button variant='contained' color='primary' onClick={button.onClick} disabled={button.disabled}>{button.title}</Button>
                        </div>
                    }
                }
                )}
            </div>
        </div>
    </>
}

export default PageHeader