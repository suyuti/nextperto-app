import React from 'react'
import {
    Card, CardHeader, Divider, CardContent,
    Grid, TextField, makeStyles, Switch,
    Checkbox, FormControl, FormLabel,
    FormGroup, FormControlLabel, MenuItem,
    ListItem, ListItemText, Chip
} from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import MusteriSorumlu from './MusteriSorumlu'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
const firmaDurumlari = require('../../constants/firmaDurumlari.json')
const icon = <CheckBoxOutlineBlankIcon  />;
const checkedIcon = <CheckBoxIcon  />;

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const MusteriGenelBilgiler = props => {
    const { firma, errors, onChange, sektorler, tenants } = props
    const classes = useStyles()

    const value = () => {
        if (!firma.vtenants) {
            return []
        }
        let s = []
        for (let k of tenants) {
            if (firma.vtenants.includes(k.key)) {
                s.push(k)
            }
        }
        return s
    }


    return <>
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={9}>
                    <Card>
                        <CardHeader
                            title='Müşteri Genel Bilgileri'
                        />
                        <Divider />
                        <CardContent>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <TextField
                                        error={errors && errors.marka}
                                        helperText={errors && errors.marka}
                                        label='Marka'
                                        variant='outlined'
                                        fullWidth
                                        size='small'
                                        value={firma.marka}
                                        onChange={(e) => onChange({ field: 'marka', value: e.target.value })}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        error={errors && errors.unvan}
                                        helperText={errors && errors.unvan}
                                        label='Firma Ünvanı'
                                        variant='outlined'
                                        size='small'
                                        fullWidth
                                        value={firma.unvan}
                                        onChange={(e) => onChange({ field: 'unvan', value: e.target.value })}
                                    />
                                </Grid>
                                <Grid item xs={4}>
                                    <Autocomplete
                                        id='firma-sektor'
                                        size='small'
                                        options={sektorler || []}
                                        getOptionLabel={option => option.adi}
                                        value={sektorler.find(m => m.key == firma.vsektor)}
                                        renderInput={params => <TextField {...params} label="Sektörü"
                                            variant='outlined'
                                        />}
                                        onChange={(e, v, r) => {
                                            if (v) {
                                                onChange({ field: 'vsektor', value: v }) // hem key hem de _id lazim oldugu icin v gonderildi
                                            }
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField
                                        label='KEP adresi'
                                        size='small'
                                        variant='outlined'
                                        fullWidth
                                        value={firma.KepAdresi}
                                        onChange={(e) => onChange({ field: 'KepAdresi', value: e.target.value })}
                                    />
                                </Grid>
                                <Grid item xs={4}>
                                    <FormControl component="fieldset" className={classes.formControl}>
                                        <FormLabel component="legend"></FormLabel>
                                        <FormGroup>
                                            <FormControlLabel
                                                control={<Checkbox
                                                    checked={firma.argeMerkezi}
                                                    onChange={(e) => onChange({ field: 'argeMerkezi', value: e.target.checked })}
                                                />}
                                                label="Ar-Ge Merkezi"
                                            />
                                            <FormControlLabel
                                                control={<Checkbox checked={firma.tasarimMerkezi}
                                                    onChange={(e) => onChange({ field: 'tasarimMerkezi', value: e.target.checked })}
                                                />}
                                                label="Tasarım Merkezi"
                                            />
                                            <FormControlLabel
                                                control={<Checkbox checked={firma.teknoparkFirmasi}
                                                    onChange={(e) => onChange({ field: 'teknoparkFirmasi', value: e.target.checked })}
                                                />}
                                                label="Teknopark Firması"
                                            />
                                        </FormGroup>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={4}>
                                    <Autocomplete
                                        id='firma-tenant'
                                        size='small'
                                        multiple
                                        disableCloseOnSelect

                                        options={tenants || []}
                                        getOptionLabel={option => option.adi}
                                        value={value()}
                                        renderTags={(tagValue, getTagProps) =>
                                            tagValue.map((opt, index) => {
                                                return <Chip
                                                    label={opt.adi}
                                                    variant="outlined"
                                                    {...getTagProps({ index })}
                                                />
                                            })
                                        }
                                        renderOption={(option, { selected }) => (
                                            <React.Fragment>
                                                <Checkbox
                                                    icon={icon}
                                                    checkedIcon={checkedIcon}
                                                    style={{ marginRight: 8 }}
                                                    checked={selected}
                                                />
                                                <ListItem dense>
                                                    <ListItemText primary={option.adi} />
                                                </ListItem>
                                            </React.Fragment>
                                        )}
                                        renderInput={params => <TextField {...params} label="Bağlı Olduğu Firma"
                                            variant='outlined'
                                        />}
                                        onChange={(e, v, r) => {
                                            if (v) {
                                                onChange({ field: 'vtenants', value: v }) // hem key hem de _id lazim oldugu icin v gonderildi
                                            }
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={2}>
                                    <FormControl component="fieldset">
                                        <FormControlLabel
                                            value={firma.faal}
                                            control={
                                                <Switch
                                                    color="primary"
                                                    checked={firma.faal}
                                                    onChange={(e) => {
                                                        onChange({ field: 'faal', value: e.target.checked })
                                                    }} />}
                                            label={firma.faal ? 'Faaliyette' : "Kapalı"}
                                            labelPlacement="top"
                                        />
                                    </FormControl>
                                </Grid>

                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={3} container spacing={2}>
                    <Grid item xs={12}>
                        <Card>
                            <CardHeader title='Durum' />
                            <Divider />
                            <CardContent>
                                <TextField
                                    label='Firma Durumu'
                                    variant='outlined'
                                    fullWidth
                                    select
                                    value={firma.durum}
                                    onChange={(e) => onChange({ field: 'durum', value: e.target.value })}
                                >
                                    {firmaDurumlari.map(d => <MenuItem key={d.value} value={d.value}>{d.label}</MenuItem>)}
                                </TextField>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={12}>
                        <MusteriSorumlu error={errors} onChange={onChange} sorumlu={firma.vsorumlu} />
                    </Grid>
                </Grid>
            </Grid>
        </div>
    </>
}

export default MusteriGenelBilgiler