import React from 'react'
import { Card, CardHeader, Divider, CardContent, Grid, Avatar, Typography, TextField, ButtonGroup, Button, makeStyles, IconButton } from '@material-ui/core'
import { useRouter } from "next/router";
import TableView from '../tableview';
import { useSelector } from 'react-redux'
import AddIcon from '@material-ui/icons/Add'
import moment from 'moment'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    sorumlu: {
        display: 'flex',
        alignItems: 'center'
    },
    sorumluAvatar: {
        marginRight: theme.spacing(1),
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
}))

const MusteriYazismalar = props => {
    const { firma } = props
    const router = useRouter();
    const classes = useStyles()
    const token = useSelector(state => state.auth.expertoToken)
    const users = useSelector(state => state.auth.users)
    const columns = [
        { id: 'firma.marka', label: 'Firma' },
        { id: 'kisi.adi', label: 'Kisi', render: row => { return `${row.kisi.adi} ${row.kisi.soyadi} / ${row.kisi.unvani}` } },
        {
            id: '', label: 'Görüşme Yapan', render: row => {
                let _user = users?.find(u => u.key == row.vcreatedBy)
                return <>
                    <div className={classes.sorumlu}>
                        <Avatar className={classes.sorumluAvatar} src={_user?.image} />
                        <Typography variant='body2'>{_user?.username}</Typography>
                    </div>
                </>
            }
        },
        { id: 'not', label: 'Konu' },
        { id: 'kanal', label: 'Kanal' },
        { id: 'tarih', label: 'Tarih', render: row => { return moment(row.tarih).format('HH:mm DD.MM.YYYY') } },
        {
            id: '', label: '',
            render: row => <Button variant='outlined' color='primary' size='small'
                onClick={() => {
                    router.push(`/gorusme/${row._id}`)
                }}>Detay</Button>
        },
    ]


    return <>
        <Card>
            <CardHeader
                title='Müşteri ile Yapılan Görüşmeler'
                action={<IconButton
                    onClick={() => {
                        router.push(`/gorusme/[id]?firma=${firma.key}`, `/gorusme/new?firma=${firma.key}`)
                    }}
                ><AddIcon /></IconButton>}
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TableView
                            title='Görüşmeler'
                            columns={columns}
                            remoteUrl={`/gorusme`}
                            token={token}
                            filter={`vfirma=${firma.key}&select=vcreatedBy,vkisi,vfirma,not,tarih,kanal&populate=firma.marka,kisi&sort=-tarih`}
                            searchCol='not'
                            data={[]}
                        />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default MusteriYazismalar