import React, {useState} from 'react'
import { Card, CardHeader, Divider, CardContent, Grid, TextField, MenuItem } from '@material-ui/core'
const iller = require('../../constants/iller.json')
const il_ilce = require('../../constants/il_ilce.json')

const MusteriIletisimBilgileri = props => {
    const { onChange, firma } = props
    const [seciliIl, setSeciliIl] = useState('')
    const [ilceler, setIlceler] = useState([])
    return <>
        <Card>
            <CardHeader
                title='Müşteri İletişim Bilgileri'
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            label='Firma Adresi'
                            variant='outlined'
                            multiline
                            rows={2}
                            size='small'
                            rowsMax={Infinity}
                            fullWidth
                            value={firma.adres}
                            onChange={(e) => onChange({ field: 'adres', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label='Fabrika Adresi'
                            variant='outlined'
                            fullWidth
                            size='small'
                            multiline
                            rows={2}
                            rowsMax={Infinity}
                            value={firma.fabrikaAdres}
                            onChange={(e) => onChange({ field: 'fabrikaAdres', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label='Fatura Adresi'
                            variant='outlined'
                            fullWidth
                            size='small'
                            multiline
                            rows={2}
                            rowsMax={Infinity}
                            value={firma.faturaAdres}
                            onChange={(e) => onChange({ field: 'faturaAdres', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            label='İl'
                            variant='outlined'
                            size='small'
                            fullWidth
                            value={firma.il || ''}
                            onChange={(e) => {
                                setSeciliIl(e.target.value)
                                let _ilceler = il_ilce.find(i => i.il == e.target.value).ilceleri
                                _ilceler = _ilceler.sort((a,b) => a > b)
                                setIlceler(_ilceler)
                                onChange({ field: 'il', value: e.target.value })}
                            }
                            select
                        >
                            {il_ilce.map(il => <MenuItem key={il.il} value={il.il}>{il.il}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            label='İlçe'
                            variant='outlined'
                            fullWidth
                            size='small'
                            value={firma.ilce || ''}
                            onChange={(e) => onChange({ field: 'ilce', value: e.target.value })}
                            select
                        >
                            {ilceler.map(ilce => <MenuItem key={ilce} value={ilce}>{ilce}</MenuItem>)} 
                        </TextField>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label='Firma Telefonu'
                            size='small'
                            variant='outlined'
                            fullWidth
                            value={firma.telefon}
                            onChange={(e) => onChange({ field: 'telefon', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label='Web Adresi'
                            variant='outlined'
                            size='small'
                            fullWidth
                            value={firma.webAdresi}
                            onChange={(e) => onChange({ field: 'webAdresi', value: e.target.value })}
                        />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default MusteriIletisimBilgileri