import React from 'react'
import { Card, CardHeader, Divider, CardContent, Grid, Box, Avatar, TextField, Typography, ButtonGroup, Button, makeStyles, IconButton } from '@material-ui/core'
import { useRouter } from "next/router";
import TableView from '../tableview';
import { useSelector } from 'react-redux'
import AddIcon from '@material-ui/icons/Add'
import moment from 'moment'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    sorumlu: {
        display: 'flex',
        alignItems: 'center'
    },
    sorumluAvatar: {
        marginRight: theme.spacing(1),
        width: theme.spacing(3),
        height: theme.spacing(3),    },

}))

const MusteriIlgiliGorevler = props => {
    const {firma} = props
    const router = useRouter();
    const classes = useStyles()
    const token = useSelector(state => state.auth.expertoToken)
    const users = useSelector(state => state.auth.users)
    const columns = [
        { id: 'baslik', numeric: false, disablePadding: true, label: 'Görev', align: 'left' },
        { id: 'firma.marka', numeric: true, disablePadding: false, label: 'Firma', align: 'left' },
        {
            id: 'sorumlu', label: 'Sorumlu', align: 'left', render: row => {
                let _user = users.find(u => u.key == row.vsorumlu)
                return <>
                    <div className={classes.sorumlu}>
                        <Avatar className={classes.sorumluAvatar} src={_user?.image} />
                        <Typography variant='body2'>{_user?.username}</Typography>
                    </div>
                </>
            }
        },
        {
            id: 'sonuc', label: 'Sonuç', render: row => {
                if (row.sonuc == 'acik') {
                    return <Box bgcolor="info.main" color="primary.contrastText" p={1}>AÇIK</Box>
                }
                else if (row.sonuc == 'basarili') {
                    return <Box bgcolor="success.main" color="primary.contrastText" p={1}>BASARILI</Box>
                }
                else if (row.sonuc == 'basarisiz') {
                    return <Box bgcolor="error.main" color="primary.contrastText" p={1}>BASARISIZ</Box>
                }
                else if (row.sonuc == 'iptal') {
                    return <Box bgcolor="warning.main" color="primary.contrastText" p={1}>İPTAL</Box>
                }
            }
        },
        {
            label: 'Dış Görev', render: row => {
                if (row.icGorevMi) {
                    return <Typography>İç Görev</Typography>
                }
                else {
                    return <Typography>Dış Görev</Typography>
                }
            }
        },
        {
            id: 'sonTarih', numeric: true, disablePadding: false, label: 'Son Tarih', align: 'right',
            render: row => {
                return moment(row.sonTarih).format('DD.MM.YYYY')
            }
        },
        {
            render: row =>
                <ButtonGroup>
                    <Button
                        size='small'
                        variant='outlined'
                        color='primary'
                        onClick={() => {
                            router.push(`/gorev/${row._id}`)
                        }}>Detay</Button>

                </ButtonGroup>
        }
    ]


    return <>
        <Card>
            <CardHeader
                title='Müşteri İlgili Görevler'
                action={<IconButton
                    onClick={() => {
                        router.push(`/gorev/[id]?firma=${firma.key}`, `/gorev/new?firma=${firma.key}`)
                    }}
                ><AddIcon /></IconButton>}
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TableView
                            title='Görevler'
                            columns={columns}
                            remoteUrl={`/gorev`}
                            token={token}
                            filter={`vfirma=${firma.key}&select=sonuc,baslik,icGorevMi,sonTarih,vsorumlu,vfirma&populate=firma.marka&sort=-sonTarih`}
                            searchCol='baslik'
                            data={[]}
                        />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default MusteriIlgiliGorevler

