import React from 'react'
import { Card, CardHeader, MenuItem, Divider, CardContent, Grid, TextField, Switch, FormControl, FormControlLabel } from '@material-ui/core'

const vadeler = require('../../constants/vadeler.json')

const MusteriMaliBilgiler = props => {
    const { onChange, firma } = props
    return <>
        <Grid container spacing={2}>
            <Grid item xs={8}>
                <Card>
                    <CardHeader
                        title='Müşteri Mali Bilgileri'
                    />
                    <Divider />
                    <CardContent>
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                <TextField
                                    label='Vergi Dairesi'
                                    variant='outlined'
                                    fullWidth
                                    value={firma.vergiDairesi}
                                    onChange={(e) => onChange({ field: 'vergiDairesi', value: e.target.value })}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    label='Vergi Numarası'
                                    variant='outlined'
                                    fullWidth
                                    value={firma.vergiNo}
                                    onChange={(e) => onChange({ field: 'vergiNo', value: e.target.value })}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    label='Firmaya Uygulanan Vade'
                                    variant='outlined'
                                    fullWidth
                                    value={firma.vade}
                                    onChange={(e) => onChange({ field: 'vade', value: e.target.value })}
                                    select
                                >
                                    {vadeler.map(vade => <MenuItem key={vade} value={vade}>{vade}</MenuItem>)}
                                </TextField>
                            </Grid>
                            <Grid item xs={6}>
                                <FormControl component='fieldset'>
                                    <FormControlLabel
                                        //value={firma.efaturaTCKN}
                                        control={
                                            <Switch
                                                color="primary"
                                                checked={firma.eFaturaMi}
                                                onChange={(e) => {
                                                    onChange({ field: 'eFaturaMi', value: e.target.checked })
                                                }}
                                            />}
                                        label={firma.eFaturaMi ? 'EFatura' : "EArşiv"}
                                        labelPlacement="left"
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={6}>
                                <FormControl component='fieldset'>
                                    <FormControlLabel
                                        //value={firma.efaturaTCKN}
                                        control={
                                            <Switch
                                                color="primary"
                                                checked={firma.efaturaTCKNKullan}
                                                onChange={(e) => {
                                                    onChange({ field: 'efaturaTCKNKullan', value: e.target.checked })
                                                }}
                                            />}
                                        label={firma.efaturaTCKNKullan ? 'TCKN Kullan' : "VKN Kullan"}
                                        labelPlacement="left"
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    label='TCKN'
                                    variant='outlined'
                                    fullWidth
                                    disabled={!firma.efaturaTCKNKullan}
                                    value={firma.efaturaTCKN}
                                    onChange={(e) => onChange({ field: 'efaturaTCKN', value: e.target.value })}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    label='Adı'
                                    variant='outlined'
                                    fullWidth
                                    disabled={!firma.efaturaTCKNKullan}
                                    value={firma.efaturaTCKNAdi}
                                    onChange={(e) => onChange({ field: 'efaturaTCKNAdi', value: e.target.value })}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    label='Soyadı'
                                    variant='outlined'
                                    disabled={!firma.efaturaTCKNKullan}
                                    fullWidth
                                    value={firma.efaturaTCKNSoyadi}
                                    onChange={(e) => onChange({ field: 'efaturaTCKNSoyadi', value: e.target.value })}
                                />
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={8}>
                <Card>
                    <CardHeader
                        title='Mali Müşavir Bilgileri'
                    />
                    <Divider />
                    <CardContent>
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                <TextField
                                    label='Adı'
                                    variant='outlined'
                                    fullWidth
                                    value={firma.maliMusavirAdi}
                                    onChange={(e) => onChange({ field: 'maliMusavirAdi', value: e.target.value })}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    label='Soyadı'
                                    variant='outlined'
                                    fullWidth
                                    value={firma.maliMusavirSoyadi}
                                    onChange={(e) => onChange({ field: 'maliMusavirSoyadi', value: e.target.value })}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label='Telefon'
                                    variant='outlined'
                                    fullWidth
                                    value={firma.maliMusavirTelefon}
                                    onChange={(e) => onChange({ field: 'maliMusavirTelefon', value: e.target.value })}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label='Mail'
                                    variant='outlined'
                                    fullWidth
                                    value={firma.maliMusavirMail}
                                    onChange={(e) => onChange({ field: 'maliMusavirMail', value: e.target.value })}
                                />
                            </Grid>

                        </Grid>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    </>
}

export default MusteriMaliBilgiler