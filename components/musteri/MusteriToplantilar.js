import React from 'react'
import { Card, CardHeader, Divider, CardContent, Grid, Box, TextField, ButtonGroup, Button, makeStyles, IconButton, Avatar, Typography } from '@material-ui/core'
import { useRouter } from "next/router";
import TableView from '../tableview';
import { useSelector } from 'react-redux'
import AddIcon from '@material-ui/icons/Add'
import moment from 'moment'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    sorumlu: {
        display: 'flex',
        alignItems: 'center'
    },
    sorumluAvatar: {
        marginRight: theme.spacing(1),
        width: theme.spacing(3),
        height: theme.spacing(3),    },
}))

const MusteriToplantilar = props => {
    const { firma } = props
    const router = useRouter();
    const classes = useStyles()
    const token = useSelector(state => state.auth.expertoToken)
    const users = useSelector(state => state.auth.users)

    const columns = [
        { id: 'baslik', numeric: false, disablePadding: true, label: 'Konu', align: 'left' },
        { id: 'firma.marka', numeric: true, disablePadding: false, label: 'Firma', align: 'left' },
        { id: 'yer', numeric: true, disablePadding: false, label: 'Konum', align: 'left' },
        {
            id: 'createdBy', label: 'Oluşturan', render: row => {
                let _user = users?.find(u => u.key == row.vcreatedBy)
                return <>
                    <div className={classes.sorumlu}>
                        <Avatar className={classes.sorumluAvatar} src={_user?.image} />
                        <Typography variant='body2'>{_user?.username}</Typography>
                    </div>
                </>
            }
        },
        {
            id: 'start', numeric: true, disablePadding: false, label: 'Tarih', align: 'right',
            render: row => {
                return moment(row.start).format('DD.MM.YYYY')
            }
        },
        {
            id: 'saat', numeric: true, disablePadding: false, label: 'Saat', align: 'right'
        },
        {
            id: '', label: 'Durum',
            render: row => {
                let today = moment()
                let tarih = moment(row.start)
                let kararGirmeSonTarih = moment(row.start).add(2, 'days')

                let bugun = today.isSame(tarih, 'days')
                let gecikme = today.isAfter(kararGirmeSonTarih, 'days')
                let kararGirilmeTarihinde = today.isSameOrAfter(tarih, 'days') && today.isSameOrBefore(kararGirmeSonTarih, "days")

                if (bugun && row.status == 'acik') {
                    return <Box bgcolor="info.main" color="primary.contrastText" p={1}>Bugün</Box>
                }
                else if (gecikme && row.status == 'acik') {
                    return <Box bgcolor="error.main" color="primary.contrastText" p={1}>Gecikme</Box>
                }
                else if (kararGirilmeTarihinde && row.status == 'acik') {
                    return <Box bgcolor="warning.main" color="primary.contrastText" p={1}>Karar Girilebilir</Box>
                }
            }
        },
        { id: 'kategori', numeric: true, disablePadding: false, label: 'Kategori', align: 'left' },
        {
            render: row =>
                <ButtonGroup>
                    <Button
                        size='small'
                        variant='outlined'
                        color='primary'
                        onClick={() => {
                            router.push(`/toplanti/${row._id}`)
                        }}>Detay</Button>
                    <Button
                        size='small'
                        variant='outlined'
                        color='primary'
                        disabled={false}
                        onClick={() => {
                            router.push(`/toplanti/${row._id}/kararlar?firma=${row.firma._id}`)
                        }}>Kararlar</Button>

                </ButtonGroup>
        }
    ]

    return <>
        <Card>
            <CardHeader
                title='Firma Toplantı Bilgileri'
                action={<IconButton
                    onClick={() => {
                        router.push(`/toplanti/[id]?firma=${firma.key}`, `/toplanti/new?firma=${firma.key}`)
                    }}
                ><AddIcon />
                </IconButton>}
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TableView
                            title='Firma ile yapılan toplantılar'
                            columns={columns}
                            remoteUrl={`/toplanti`}
                            token={token}
                            filter={`vfirma=${firma.key}&select=baslik,start,saat,vfirma,yer,vcreatedBy,kategori,status&populate=firma.marka&sort=-start`}
                            searchCol='baslik'
                            data={[]}
                        />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default MusteriToplantilar