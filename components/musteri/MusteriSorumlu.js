import React, { useState, useEffect } from 'react'
import { Card, CardHeader, Divider, CardContent, Typography, TextField, ListItem, ListItemText, ListItemAvatar, Avatar } from '@material-ui/core'
import FaceIcon from '@material-ui/icons/Face';
import Autocomplete from '@material-ui/lab/Autocomplete';
import axios from 'axios';
import { useSelector } from 'react-redux'

const MusteriSorumlu = props => {
    const { sorumlu, onChange, error } = props
    const personeller = useSelector(state => {
        return state.auth.users
    })

    return <>
        <Card>
            <CardHeader
                title={<Typography variant='h6'>Müşteri Sorumlusu</Typography>}
                avatar={<FaceIcon />}
            />
            <Divider />
            <CardContent>
                <Autocomplete
                    value={personeller.find(p => p.key == sorumlu)}
                    options={personeller}
                    getOptionLabel={opt => opt.username}
                    renderOption={(option, state) => {
                        return <ListItem>
                            <ListItemAvatar>
                                <Avatar src={option.image} />
                            </ListItemAvatar>
                            <ListItemText primary={option.username} />
                        </ListItem>
                    }}
                    renderInput={params => <TextField 
                        error={error && error.sorumlu}
                        helperText={error && error.sorumlu}
                        {...params} label='Personel' variant='outlined' />}
                    onChange={(e, v, r) => {
                        if (v) {
                            onChange({ field: 'vsorumlu', value: v.key })
                        }
                    }}
                />
            </CardContent>

        </Card>
    </>
}

export default MusteriSorumlu