import React, {useState} from 'react'
import { makeStyles, Typography, Button, Accordion, AccordionSummary, AccordionDetails, AccordionActions, Grid, TextField } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
    root: {

    }
}))
const MusteriQuickInfo = props => {
    const { firmaInfo } = props
    const classes = useStyles()
    const [updateFirmaSection, setUpdateFirmaSection] = useState(false)

    if (!firmaInfo) {
        return <Typography>Loading</Typography>

    }
    return <>
        <div className={classes.root}>
            <Accordion>
                <AccordionSummary>Firma</AccordionSummary>
                <AccordionDetails>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            {updateFirmaSection ? 
                                <TextField 
                                    fullWidth 
                                    size='small'
                                    label='Firma Marka'
                                    variant='outlined'
                                    value={firmaInfo.marka}
                                    /> 
                                : 
                                <Typography>{firmaInfo.marka}</Typography>
                            }
                        </Grid>
                    </Grid>
                </AccordionDetails>
                <AccordionActions>
                    {updateFirmaSection ? 
                        <>
                            <Button size='small' onClick={() => setUpdateFirmaSection(false)}>Iptal</Button>
                            <Button size='small' onClick={() => setUpdateFirmaSection(false)}>Kaydet</Button>
                        </>
                    :
                    <Button size='small' onClick={() => setUpdateFirmaSection(true)}>Degistir</Button>
                }
                </AccordionActions>
            </Accordion>
            <Accordion>
                <AccordionSummary>Görevler</AccordionSummary>
                <AccordionDetails></AccordionDetails>
            </Accordion>
            <Accordion>
                <AccordionSummary>Toplantılar</AccordionSummary>
                <AccordionDetails></AccordionDetails>
                <AccordionActions>
                    <Button size='small'>Guncelle</Button>
                </AccordionActions>
            </Accordion>
            <Typography>Müşteri Bilgi</Typography>
            <Typography>{firmaInfo.marka}</Typography>

            <Button variant='contained' color='primary'>Guncelle</Button>
        </div>
    </>
}

export default MusteriQuickInfo