import React, { useState } from 'react'
import { Paper, Button, Grid, makeStyles, TextField, MenuItem, Accordion, AccordionSummary, Typography, AccordionDetails, Divider, AccordionActions, Checkbox } from '@material-ui/core'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import { useSelector } from 'react-redux';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import RemoteAutoComplete from '../autocomplete'
import { searchFirmalar } from '../../services/firma.service'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const kobiSanayiOptions = [
    { title: 'Tümü' },
    { title: 'Kobi' },
    { title: 'Sanayi' }
]

const initialQuery = {
    firma: '',
    tenant: '',
    kobiSanayi: '',
    durum: '',
    sorumlu: ''
}

const FirmaSearchToolbar = props => {
    const { disabled, tenants, onSearch } = props
    const classes = useStyles()
    const users = useSelector(state => state.auth.users)

    const [results, setResults] = useState([])
    const [query, setQuery] = useState({ ...initialQuery })
    
    const temizle = () => {
        setQuery({...initialQuery})
    }

    const prepareQueryString = (query) => {
        //return `firma=${query.firma}&vtenants=${query.tenant}&vsorumlu=${query.sorumlu}&kobiMi=${query.kobiSanayi}`
        return `vsorumlu=${query.sorumlu}`
    }

    return <>
        <Accordion >
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1c-content"
                id="panel1c-header">
                <Typography>Filtre</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Grid container spacing={2}>
                    <Grid item xs={2}>
                        <TextField
                            variant='outlined'
                            label='Firma'
                            size='small'
                            fullWidth
                            value={query.firma}
                            onChange={(e) => {
                                let q = {...query}
                                q.firma = e.target.value
                                setQuery(q)
                            }}
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <TextField
                            variant='outlined'
                            label='Bağlı Olduğu Firma'
                            size='small'
                            fullWidth
                            select
                            value={query.tenant}
                            onChange={e => {
                                let q = {...query}
                                q.tenant = e.target.value
                                setQuery(q)
                            }}
                        >
                            <MenuItem key={'Hepsi'} value={'Hepsi'}>Hepsi</MenuItem>
                            {tenants?.map((t, i) => {
                                return (
                                    <MenuItem key={t.key} value={t.key}>{t.marka}</MenuItem>
                                )
                            })}
                        </TextField>
                    </Grid>

                    <Grid item xs={2}>
                        <TextField
                            variant='outlined'
                            label='Kobi / Sanayi'
                            size='small'
                            fullWidth
                            select
                            value={query.kobiSanayi}
                            onChange={e => {
                                let q = {...query}
                                q.kobiSanayi = e.target.value
                                setQuery(q)
                            }}
                        >
                            <MenuItem key={'Hepsi'} value={'Hepsi'}>Hepsi</MenuItem>
                            <MenuItem key={'Kobi'} value={'Kobi'}>Kobi</MenuItem>
                            <MenuItem key={'Sanayi'} value={'Sanayi'}>Sanayi</MenuItem>
                        </TextField>
                    </Grid>
                    <Grid item xs={2}>
                        <TextField
                            variant='outlined'
                            label='Durum'
                            size='small'
                            fullWidth
                            select
                            value={query.durum}
                            onChange={e => {
                                let q = {...query}
                                q.durum = e.target.value
                                setQuery(q)
                            }}
                        >
                            <MenuItem key={'Hepsi'} value={'Hepsi'}>Hepsi</MenuItem>
                            <MenuItem key={'Kobi'} value={'Kobi'}>Aktif</MenuItem>
                            <MenuItem key={'Pasif'} value={'Pasif'}>Pasif</MenuItem>
                            <MenuItem key={'Potansiyel'} value={'Potansiyel'}>Potansiyel</MenuItem>
                            <MenuItem key={'Sorunlu'} value={'Sorunlu'}>Sorunlu</MenuItem>
                        </TextField>
                    </Grid>

                    <Grid item xs={2}>
                        <TextField
                            disabled={disabled}
                            fullWidth
                            label='Sorumlu'
                            variant='outlined'
                            size='small'
                            select
                            value={query.sorumlu}
                            onChange={e => {
                                let q = {...query}
                                q.sorumlu = e.target.value
                                setQuery(q)
                            }}
                        >
                            {users.map((user, i) => {
                                return <MenuItem key={user.oid} value={user.oid}>{user.username}</MenuItem>
                            })}
                        </TextField>
                    </Grid>
                </Grid>
            </AccordionDetails>
            <Divider />
            <AccordionActions>
                <Button size='small' disabled={disabled} variant='outlined' color='primary' onClick={() => {temizle()}}>Temizle</Button>
                <Button size='small' disabled={disabled} variant='contained' color='primary'  onClick={() => {
                    onSearch(prepareQueryString(query))
                }}>Sorgula</Button>
            </AccordionActions>
        </Accordion>
    </>
}

export default FirmaSearchToolbar