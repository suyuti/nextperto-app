import React from 'react'
import { Card, CardHeader, Divider, CardContent, Grid, TextField, ButtonGroup, Button, makeStyles, IconButton } from '@material-ui/core'
import { useRouter } from "next/router";
import TableView from '../tableview';
import { useSelector } from 'react-redux'
import AddIcon from '@material-ui/icons/Add'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
}))

const MusteriKisiler = props => {
    const {firma} = props
    const router = useRouter();
    const classes = useStyles()
    const token = useSelector(state => state.auth.expertoToken)
    const columns = [
        { id: '', label: 'Adı Soyadı', align: 'left', render: row => { return `${row.adi} ${row.soyadi}` } },
        { id: 'unvani', label: 'Ünvanı', align: 'left' },
        { id: 'mail', label: 'Mail', align: 'left' },
        { id: 'firma.marka', numeric: true, disablePadding: false, label: 'Firma', align: 'left' },
        {
            render: row =>
                <ButtonGroup size='small'>
                    <Button
                        size='small'
                        variant='outlined'
                        color='primary'
                        onClick={() => {
                            router.push(`/kisi/${row._id}`)
                        }}>Detay</Button>

                </ButtonGroup>
        }
    ]


    return <>
        <Card>
            <CardHeader
                title='Müşteri Kişi Bilgileri'
                action={<IconButton
                    onClick={() => {
                        router.push(`/kisi/[id]?firma=${firma.key}`, `/kisi/new?firma=${firma.key}`)
                    }}
                ><AddIcon /></IconButton>}
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TableView
                            title='Kişiler'
                            columns={columns}
                            remoteUrl={`/kisi`}
                            token={token}
                            filter={`vfirma=${firma.key}&select=adi,soyadi,mail,unvani,vfirma&active=true&populate=firma.marka`}
                            searchCol='adi'
                            data={[]}
                        />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default MusteriKisiler