import React from 'react'
import { Document, Page } from 'react-pdf/dist/esm/entry.webpack'

const PdfViewer = props => {
    const {content} = props
    return <>
        <Document
            file={content}
            //onLoadSuccess={(numPages) => {
            //    setNumPages(numPages)
            //}}
            >
            <Page pageNumber={1} />
        </Document>
    </>
}

export default PdfViewer