import React from 'react'
import { SnackbarProvider } from 'notistack';
import GorevSnack from './gorevSnack'
import YorumSnack from './yorumSnack'
import OnaySnack from './onaySnack'

const SnackbarWrapper = props => {
    return <>
        <SnackbarProvider
            maxSnack={5}
            dense={false}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
            }}
            
            content={(key, message) => {
                if (message.type == 'gorev.create') {
                    return <GorevSnack id={key} message={message} />
                }
                else if (message.type == 'yorum.create') {
                    return <YorumSnack id={key} message={message} />
                }
                else if (message.type == 'onay.create') {
                    return <OnaySnack id={key} message={message} />
                }
                return ''
            }}
            
            >
            {props.children}
        </SnackbarProvider>
    </>
}

export default SnackbarWrapper