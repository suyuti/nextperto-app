import React from 'react'
import clsx from "clsx";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Badge from '@material-ui/core/Badge';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Link from '../Link'
import { useDispatch, useSelector } from 'react-redux'
import * as Actions from '../../../redux/actions'
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import { Avatar } from '@material-ui/core';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import ChangeMeAs from '../../admin/changeMeAs';
import { useRouter, withRouter } from 'next/router';
import HeaderAction from './HeaderAction'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    hide: {
        display: "none",
    },
    user: {
        display: 'flex',
        marginRight: theme.spacing(2)
        //padding: theme.spacing(2)
    },
    username: {
        marginLeft: theme.spacing(2)
    }
}));

const Header = props => {
    const { onMenuButtonClicked, open } = props
    const classes = useStyles();
    const dispatch = useDispatch()
    const user = useSelector(state => state.auth.user)
    const router = useRouter()

    return <>
        <AppBar position="fixed" className={props.className}>
            <Toolbar>
                <IconButton
                    edge="start"
                    //className={classes.menuButton} 
                    className={clsx(classes.menuButton, {
                        [classes.hide]: open,
                    })}

                    color="inherit" aria-label="menu" onClick={onMenuButtonClicked}>
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" className={classes.title} >
                    EXPERTO
                </Typography>
                {user.isAdmin && <>
                    <ChangeMeAs />
                    <Button variant='outlined' onClick={() => { router.push('/test') }}>Test Page</Button>
                </>}
                <div className=''>
                    <HeaderAction {...props}/>
                </div>

                {user ?
                    <>
                        <IconButton>
                            <Badge badgeContent={0} color="primary">
                                <NotificationsNoneIcon />
                            </Badge>
                        </IconButton>
                        <IconButton>
                            <Badge badgeContent={0} color="primary">
                                <MailOutlineIcon />
                            </Badge>
                        </IconButton>
                        <div className={classes.user}>
                            <Avatar src={user.image} />
                            <Typography className={classes.username}>{user.displayName || user.username}</Typography>
                        </div>
                        <Button variant='outlined' onClick={() => dispatch(Actions.logout())}>Cikis</Button>
                    </>
                    :
                    <>
                        <Button color="inherit" onClick={(e) => {
                            e.preventDefault()
                            dispatch(Actions.login())
                        }}>Login</Button>
                    </>
                }
            </Toolbar>
        </AppBar>
    </>
}

export default withRouter(Header)