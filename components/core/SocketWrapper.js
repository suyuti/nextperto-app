import React, { useEffect } from 'react'
import socketIOClient from "socket.io-client";
const ENDPOINT =  "https://nextperto-api-aatrw.ondigitalocean.app"// 'http://localhost:3001' //process.env.SOCKET_URL // "http://localhost:3001";
//const ENDPOINT = process.env.SOCKET_END_POINT //'http://localhost:3001'
//const ENDPOINT = 'http://localhost:3001'
import { withSnackbar } from 'notistack';
import GorevSnack from './Snacks/gorevSnack';
import {useSelector} from 'react-redux'

const socketIOConnOpt = {
    'force new connection': true,
    reconnection: true,
    reconnectionDelay: 10000,
    reconnectionDelayMax: 60000,
    reconnectionAttempts: 'Infinity',
    timeout: 10000,
    transports: ['websocket'],
    //resource: '/conversation-api/',
  };

const SocketWrapper = props => {
    const user = useSelector(state => state.auth.user)
    useEffect(() => {
        if (user) {
            const socket = socketIOClient(ENDPOINT, socketIOConnOpt)
            socket.on('welcome', data => {
                console.log(`socket login: ${user.key}`)
                socket.emit('login', {userId: user.key})
            })
    
            socket.on('message', data => {
                props.enqueueSnackbar(data, {persist: true})
                
                //props.enqueueSnackbar('', //data.message, 
                //    { 
                //        //variant: 'warning', 
                //        persist: true, 
                //        action: (key) => <GorevSnack id={key} data={data} /> 
                //    })
            })
        }
    }, [user])

    return <>{props.children}</>
}
export default withSnackbar(SocketWrapper)