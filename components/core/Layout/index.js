import React from 'react'
import {
    Drawer,
    AppBar,
    Collapse,
    Toolbar,
    List,
    CssBaseline,
    Typography,
    Divider,
    IconButton,
    MenuItem,
    ListItem,
    Menu,
    Button,
    ListItemIcon,
    ListItemText,
} from "@material-ui/core";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance"; // musterı
import ToysIcon from "@material-ui/icons/Toys"; // urunler
import PeopleIcon from "@material-ui/icons/People"; // kısıler
import PriorityHighIcon from "@material-ui/icons/PriorityHigh"; // sorunlu musterı
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Header from '../Header'
import SentimentVerySatisfiedIcon from "@material-ui/icons/SentimentVerySatisfied"; // aktif
import SentimentVeryDissatisfiedIcon from "@material-ui/icons/SentimentVeryDissatisfied"; // pasif
import SentimentSatisfiedIcon from "@material-ui/icons/SentimentSatisfied"; // potansiyel

import PersonOutlineIcon from "@material-ui/icons/PersonOutline"; // personel

import VpnKeyIcon from "@material-ui/icons/VpnKey"; // yetkı

import TrendingUpIcon from "@material-ui/icons/TrendingUp"; // satıs performans

import EventIcon from '@material-ui/icons/Event'; // Toplantılar

import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import SendIcon from "@material-ui/icons/Send";
import Link from "../Link";
import HomeIcon from "@material-ui/icons/Home";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Sidebar from '../Sidebar';
import { useSelector, useDispatch } from 'react-redux'
import * as Actions from '../../../redux/actions'

const drawerWidth = 260;

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(["width", "margin"], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        backgroundColor: '#E54D03',
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(["width", "margin"], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: "none",
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: "nowrap",
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: "hidden",
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up("sm")]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-end",
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        //padding: theme.spacing(3),
    },
}));

const Layout = props => {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(true);
    //const auth = useSelector(state => state.auth)
    const user = useSelector(state => state.auth.user)
    const dispatch = useDispatch()

    if (!user) {
        return <div className={classes.root}>
            <Button variant='outlined' onClick={() => dispatch(Actions.login())}>Giris</Button>
        </div>
    }

    return <div className={classes.root}>
        <CssBaseline />
        <Header className={clsx(classes.appBar, {
            [classes.appBarShift]: open,
        })}
            onMenuButtonClicked={() => {
                setOpen(true)
            }}
            open={open}
        />

        {user.active &&

            <Drawer variant='permanent' className={clsx(classes.drawer, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
            })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}>
                <div className={classes.toolbar}>
                    <IconButton onClick={() => { setOpen(false) }}>
                        {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                    </IconButton>
                </div>
                <Divider />
                <Sidebar />
            </Drawer>
        }
        <main className={classes.content}>
            <div className={classes.toolbar} />
            {props.children}
            {/*React.cloneElement(props.children, { ...props })*/}
        </main>
    </div>
}
export default Layout