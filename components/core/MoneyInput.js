import React, { Fragment } from 'react';
import NumberFormat from 'react-number-format';

const MoneyInput = props => {
  const { inputRef, onChange, ...other } = props;
  return (
    <Fragment>
      <NumberFormat
        {...other}
        getInputRef={inputRef}
        onValueChange={values => {
          onChange({
            target: {
              value: values.value
            }
          });
        }}
        decimalScale={2}
        allowNegative={false}
        thousandSeparator
        isNumericString
        suffix=" TL"
      />
    </Fragment>
  );
};

export default MoneyInput;
