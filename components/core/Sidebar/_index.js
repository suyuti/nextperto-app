import React, { Component, useState, useEffect, useRef } from 'react'
import UserBox from '../../user/UserBox'
import { Drawer, IconButton, Divider, List, ListItemText, ListItem, ListItemIcon, Collapse, ListSubheader } from '@material-ui/core'
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import Link from "../Link";
import HomeIcon from "@material-ui/icons/Home";
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import menu from '../../../constants/Sidebar.json'
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import StarBorder from '@material-ui/icons/StarBorder';
import { useSelector } from 'react-redux'
import menu2 from '../../../constants/Sidebar3.json'

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    sidebarroot: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));


const Sidebar = props => {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = useState(['crm']);
    const permissions = useSelector(state => state.auth.permissions)
    const stateRef = useRef()
    stateRef.current = open

    const handleClick = (key) => {
        let openKeys = [...stateRef.current]
        console.log(openKeys)
        let i = openKeys.findIndex(k => k == key)
        if (i >= 0) {
            openKeys.splice(i, 1)
        }
        else {
            openKeys.push(key)
        }
        setOpen(openKeys)
    };

    const makeMenu3 = (items, level, openStates, handleClick) => {
        return (
            <>
                <List>
                    {items.map(item => {
                        if (item.children) {
                            let c = [...stateRef.current]
                            return (
                                <>
                                    <ListItem
                                        button
                                        onClick={() => handleClick(item.id)}
                                    >
                                        <ListItemText
                                            primary={item.title}
                                            secondary={JSON.stringify(stateRef.current)}
                                            style={{ paddingLeft: level * 20 }} />
                                        {stateRef.current.findIndex(t => t == item.id) >= 0 ? <ExpandLess /> : <ExpandMore />}
                                    </ListItem>
                                    <Collapse in={stateRef.current.findIndex(t => t == item.id) >= 0} timeout="auto" unmountOnExit>
                                        {makeMenu3(item.children, +level + 1, stateRef.current, handleClick)}
                                    </Collapse>
                                </>
                            )
                        }
                        return (
                            <ListItem button>
                                <ListItemText primary={item.title} style={{ paddingLeft: level * 20 }} />
                            </ListItem>
                        )
                    })}
                </List>
            </>
        )
    }

    const [menuX, setMenuX] = useState(makeMenu3(menu2, 0, stateRef.current, (k) => handleClick(k)))






    //useEffect(() => {
    //    if (!menuX) {
    //       let m = makeMenu3(menu2, 0, stateRef.current, (k) => handleClick(k))
    //        setMenuX(m)
    //    }
    //}, [])

    //useEffect(() => {
    //    console.log(open)
    //}, [open])

    return <>
        <UserBox />
        {menuX}
        
        <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                    Ana Menu
                </ListSubheader>
            }
            className={classes.root}
        >
            <ListItem button>
                <ListItemIcon>
                    <SendIcon />
                </ListItemIcon>
                <ListItemText primary="Ana Ekran" />
            </ListItem>

            <ListItem button onClick={handleClick}>
                <ListItemIcon>
                    <InboxIcon />
                </ListItemIcon>
                <ListItemText primary="Musteri Yonetimi" />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <ListItem button className={classes.nested}>
                        <ListItemIcon>
                            <StarBorder />
                        </ListItemIcon>
                        <ListItemText primary="Starred" />
                    </ListItem>
                </List>
            </Collapse>
        </List>


    {
        false && menu.map((mgroup, gi) =>
            <React.Fragment key={gi}>
                <List
                    component='nav'
                    subheader={
                        <ListSubheader dense='true' component="div" id="nested-list-subheader">
                            {mgroup.group}
                        </ListSubheader>
                    }
                    className={classes.sidebarroot}
                >
                    {mgroup.list.map((item, i) => {
                        let permission = permissions ?.find(p => p.key == item.key)
                        if (permission ?.read)
                            return <React.Fragment key={i}>
                                <ListItem
                                    key={i}
                                    button
                                    {...(item.children ? {
                                        onClick: () => { handleClick(`${mgroup.key}-${item.id}`) }
                                    } : {
                                            href: item.link,
                                            component: Link
                                        })}
                                >
                                    <ListItemIcon>
                                        <SendIcon />
                                    </ListItemIcon>
                                    <ListItemText dense primary={item.text} />
                                    {item.children && <> {open[`${mgroup.key}-${item.id}`] ? <ExpandLess /> : <ExpandMore />}</>}
                                </ListItem>
                                {item.children &&
                                    <Collapse in={open[`${mgroup.key}-${item.id}`]} timeout="auto" unmountOnExit>
                                        <List component="div" disablePadding>
                                            {mgroup[item.children].map((subItem, j) => {
                                                let permission = permissions.find(p => p.key == subItem.key)
                                                if (permission ?.read) {
                                                    return (<ListItem
                                                        key={j * 10}
                                                        button
                                                        className={classes.nested}
                                                        naked
                                                        dense
                                                        component={Link}
                                                        href={subItem.link}>
                                                        <ListItemIcon>
                                                            <StarBorder />
                                                        </ListItemIcon>
                                                        <ListItemText primary={subItem.text} />
                                                    </ListItem>
                                                    )
                                                }
                                            }
                                            )}
                                        </List>
                                    </Collapse>
                                }
                            </React.Fragment>
                    })}
                </List>
                <Divider />
            </React.Fragment>
        )
    }
    </>
}

export default Sidebar