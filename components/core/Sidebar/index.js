import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import Tooltip from '@material-ui/core/Tooltip';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import { Divider } from '@material-ui/core';
import Link from "../Link";
import { useSelector } from 'react-redux'
const queryHelper = require('../../../helpers/query.helper')
import moment from 'moment'

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
        overflow: 'hidden'
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));

/*

    Ana Ekran
        Ana ekran
    Firmalar
        Aktif
        Pasif
        Potansiyel
        Sorunlu
        Partner
        Tedarikci
    Urunler
    Kisiler
    Personel
    Gorevler
        Acik
        Basarili
        Basarisiz
        Olusturdugum
        Ekibimin acik
        Ekibimin basarili
        Ekibimin geciken
    Toplantilar
        Gelecek
        Gecmis
        Ekibimin gelecek
        Ekibimin gecmis
    Gorusmeler
        Benim
        Ekibimin
    Muhasebe
        Guncel durum
        Musteriler
        Tedarikciler
        Fatura
        Toplu fatura
        Giderler
        Firmalarimiz
    Yonetim
        Departmanlar
        Yetkilendirme
    Onay
        Bekleyen onaylar
        Bekleyen talepler
*/

function menuSection(classes) {
    return <>
        <List
            dense={true}
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                    Ana Ekran
            </ListSubheader>
            }
            className={classes.root}
        >
            <ListItem
                button
                href='/'
                component={Link}>
                <ListItemIcon>
                    <SendIcon />
                </ListItemIcon>
                <ListItemText primary="Ana Ekran" />
            </ListItem>
        </List>
        <Divider />
    </>
}

function menuSectionMusteri(classes, open, handleClick, permissions) {
    let aktifMusteriPermissions = permissions.find(p => p.key == 'aktifmusteri')
    let pasifMusteriPermissions = permissions.find(p => p.key == 'pasifmusteri')
    let potansiyelMusteriPermissions = permissions.find(p => p.key == 'potansiyelmusteri')
    let sorunluMusteriPermissions = permissions.find(p => p.key == 'sorunlumusteri')
    let kisiPermissions = permissions.find(p => p.key == 'kisi')
    let gorusmePermissions = permissions.find(p => p.key == 'gorusme')

    if (!aktifMusteriPermissions?.read
        && !pasifMusteriPermissions?.read
        && !potansiyelMusteriPermissions?.read
        && !sorunluMusteriPermissions?.read
        && !kisiPermissions?.read
        && !gorusmePermissions?.read
    ) {
        return <></>
    }

    return <>
        <List
            dense={true}
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                    Müşteri Yönetimi
            </ListSubheader>
            }
            className={classes.root}
        >
            <ListItem button onClick={() => handleClick('musteri')}>
                <ListItemIcon>
                    <InboxIcon />
                </ListItemIcon>
                <ListItemText primary="Müşteriler" />
                {open.includes('musteri') ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={open.includes('musteri')} timeout="auto" unmountOnExit>
                <List dense={true} component="div" disablePadding>
                    {aktifMusteriPermissions?.read &&
                        <ListItem button className={classes.nested} href={{ pathname: '/musteri', query: { filter: `durum=aktif&sort=marka` } }} component={Link}>
                            <ListItemIcon>
                                <StarBorder />
                            </ListItemIcon>
                            <ListItemText primary="Aktif" />
                        </ListItem>
                    }
                    {pasifMusteriPermissions?.read &&
                        <ListItem button className={classes.nested} href={{ pathname: '/musteri', query: { filter: `durum=pasif&sort=marka` } }} component={Link}>
                            <ListItemIcon>
                                <StarBorder />
                            </ListItemIcon>
                            <ListItemText primary="Pasif" />
                        </ListItem>
                    }
                    {potansiyelMusteriPermissions?.read &&
                        <ListItem button className={classes.nested} href={{ pathname: '/musteri', query: { filter: `durum=potansiyel&sort=marka` } }} component={Link}>
                            <ListItemIcon>
                                <StarBorder />
                            </ListItemIcon>
                            <ListItemText primary="Potansiyel" />
                        </ListItem>
                    }
                    {sorunluMusteriPermissions?.read &&
                        <ListItem button className={classes.nested} href={{ pathname: '/musteri', query: { filter: `durum=sorunlu&sort=marka` } }} component={Link}>
                            <ListItemIcon>
                                <StarBorder />
                            </ListItemIcon>
                            <ListItemText primary="Sorunlu" />
                        </ListItem>
                    }
                </List>
            </Collapse>
            {kisiPermissions?.read &&
                <ListItem button href='/kisi' component={Link} href='/kisi'>
                    <ListItemIcon>
                        <InboxIcon />
                    </ListItemIcon>
                    <ListItemText primary="Kişiler" />
                </ListItem>
            }
            {gorusmePermissions &&
                <ListItem button href='/gorusme' component={Link} href='/gorusme'>
                    <ListItemIcon>
                        <InboxIcon />
                    </ListItemIcon>
                    <ListItemText primary="Görüşmeler" />
                </ListItem>
            }
        </List>
        <Divider />
    </>
}

function menuSectionUrun(classes, open, handleClick, permissions) {
    let urunPermissions = permissions.find(p => p.key == 'urun')

    if (!urunPermissions?.read) {
        return <></>
    }

    return <>
        <List
            dense={true}
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                    Ürün Yönetimi
            </ListSubheader>
            }
            className={classes.root}
        >
            <ListItem button href='/urun' component={Link} href='/urun'>
                <ListItemIcon>
                    <InboxIcon />
                </ListItemIcon>
                <ListItemText primary="Ürünler" />
            </ListItem>
        </List>
        <Divider />
    </>
}


function menuSectionOperasyon(classes, open, handleClick, permissions, user) {
    let gorevPermissions = permissions.find(p => p.key == 'gorev')
    let toplantiPermissions = permissions.find(p => p.key == 'toplanti')
    let toplantiRaporlamaPermissions = permissions.find(p => p.key == 'toplantiRapor')
    let onayPermissions = permissions.find(p => p.key == 'onay')
    const ekip = useSelector(state => state.auth.ekip)
    let ekibiminGorevleriQuery = queryHelper.gorevEkibim(ekip)
    let ekibiminToplantilariQuery = queryHelper.toplantiEkibim(ekip)

    if (!gorevPermissions?.read && !toplantiPermissions?.read) {
        return <></>
    }
    return <>
        <List
            dense={true}
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                    Operasyon
            </ListSubheader>
            }
            className={classes.root}
        >
            {gorevPermissions.read &&
                <>
                    <ListItem button onClick={() => handleClick('gorev')}>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="Görev" />
                        {open.includes('gorev') ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={open.includes('gorev')} timeout="auto" unmountOnExit>
                        <List dense={true} component="div" disablePadding>

                            <ListItem button className={classes.nested} href={{
                                pathname: '/gorev',
                                query: {
                                    filter: 'acikgorevlerim'
                                    //filter: `vsorumlu=${user.key}&sonuc=acik&sort=sonTarih`
                                }
                            }} component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Açık Görevlerim" />
                            </ListItem>
                            <ListItem button className={classes.nested} href={{
                                pathname: '/gorev',
                                query: {
                                    filter: 'gecikengorevlerim'
                                    //filter: `vsorumlu=${user.key}&sonTarih<${moment().startOf('day').toISOString()}&sonuc=acik&sort=sonTarih`
                                }
                            }} component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Geciken Görevlerim" />
                            </ListItem>
                            <ListItem button className={classes.nested} href={{
                                pathname: '/gorev',
                                query: {
                                    filter: 'tumgorevlerim'
                                    //filter: `vsorumlu=${user.key}&sort=-sonTarih`
                                }
                            }} component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Tüm Görevlerim" />
                            </ListItem>



                            <ListItem button className={classes.nested} href={{
                                pathname: '/gorev',
                                query: {
                                    filter: 'olusturdugumgorevler' //`vcreatedBy=${user.key}&sort=-sonTarih`
                                }
                            }} component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Oluşturduğum Görevler" />
                            </ListItem>
                            <ListItem button className={classes.nested} href={{
                                pathname: '/gorev',
                                query: {
                                    filter: 'ekibimingorevleri'
                                    //filter:
                                    //    `${ekibiminGorevleriQuery}&sort=-sonTarih`
                                    // `vcreatedBy = ${ user.key }& sort=-sonTarih` 
                                }
                            }}
                                component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Ekibimin Görevleri" />
                            </ListItem>
                        </List>
                    </Collapse>
                </>
            }

            {toplantiPermissions?.read &&
                <>
                    <ListItem button onClick={() => handleClick('toplanti')}>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="Toplantı" />
                        {open.includes('toplanti') ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={open.includes('toplanti')} timeout="auto" unmountOnExit>
                        <List dense={true} component="div" disablePadding>

                            <ListItem button className={classes.nested}
                                href={{
                                    pathname: '/toplanti',
                                    query: {
                                        filter: 'toplantilarim'
                                    }
                                }}
                                component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <Tooltip title=''>
                                    <ListItemText primary="Toplantılarım" />
                                </Tooltip>
                            </ListItem>
                            <ListItem button className={classes.nested}
                                href={{
                                    pathname: '/toplanti',
                                    query: {
                                        filter: 'ekibimintoplantilari'
                                    }
                                }}
                                component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <Tooltip title=''>
                                    <ListItemText primary="Ekibimin Toplantıları" />
                                </Tooltip>
                            </ListItem>

                            {/*
                            <ListItem button className={classes.nested} href='/toplanti?t=2' component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Açık Toplantılarım" />
                            </ListItem>

                            <ListItem button className={classes.nested} href='/toplanti?t=3' component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Tüm Toplantılarım" />
                            </ListItem>

                            <ListItem button className={classes.nested} href='/toplanti?t=4' component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Ekibimin Toplantıları" />
                            </ListItem>
                            */}

                            {toplantiRaporlamaPermissions?.read &&
                                <ListItem button className={classes.nested} href='/toplantiRaporu' component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Toplantı Raporları" />
                                </ListItem>
                            }





                            {/*
                            <ListItem button className={classes.nested} href='/toplanti?q=current' component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Toplantılarım" />
                            </ListItem>
                            <ListItem button className={classes.nested} href='/toplanti?q=past' component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Geçmiş Toplantılarım" />
                            </ListItem>
                            <ListItem button className={classes.nested} href='/toplanti?e=true' component={Link}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Ekibimin Toplantıları" />
                            </ListItem>
                            {toplantiRaporlamaPermissions?.read &&
                                <ListItem button className={classes.nested} href='/toplantiRaporu' component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Toplantı Raporları" />
                                </ListItem>
                            }
 */}

                        </List>
                    </Collapse>
                </>}

            {onayPermissions?.read &&
                <ListItem button 
                    href={{
                        pathname: '/onay',
                        query: {
                            filter: 'onaylarim'
                        }
                    }}
                    component={Link}>
                    <ListItemIcon>
                        <StarBorder />
                    </ListItemIcon>
                    <Tooltip title=''>
                        <ListItemText primary="Onay İşlemleri" />
                    </Tooltip>
                </ListItem>

            }
        </List>
        <Divider />
    </>
}

function menuSectionMuhasebe(classes, open, handleClick, permissions, user) {
    let muhasebePermissions = permissions.find(p => p.key == 'muhasebe')
    let muhasebeGuncelPermissions = permissions.find(p => p.key == 'muhasebe_guncel')
    let topluFaturaPermissions = permissions.find(p => p.key == 'topluFatura')
    let faturaPermissions = permissions.find(p => p.key == 'fatura')
    let muhasebeMusterilerPermissions = permissions.find(p => p.key == 'muhasebe_musteriler')
    let satisRaporlariPermissions = permissions.find(p => p.key == 'satisRapor')
    let tahsilatRaporlariPermissions = permissions.find(p => p.key == 'tahsilatRapor')
    let gelirGiderRaporlariPermissions = permissions.find(p => p.key == 'gelirGiderRapor')

    let giderPermissions = permissions.find(p => p.key == 'gider')
    let tedarikcilerPermissions = permissions.find(p => p.key == 'tedarikciler')
    let calisanlarPermissions = permissions.find(p => p.key == 'calisanlar')
    let giderRaporuPermissions = permissions.find(p => p.key == 'giderRaporu')
    let odemelerRaporuPermissions = permissions.find(p => p.key == 'odemelerRaporu')
    let kdvRaporuPermissions = permissions.find(p => p.key == 'kdvRaporu')
    let tahsilatPermissions = permissions.find(p => p.key == 'tahsilat')
    let tahsilatPlanPermissions = permissions.find(p => p.key == 'tahsilatPlan')
    let tahsilatDurumuPermissions = permissions.find(p => p.key == 'tahsilatDurumu')


    if (!muhasebePermissions?.read) {
        return <></>
    }

    return <>
        <List
            dense={true}
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                    Muhasebe
                </ListSubheader>
            }
            className={classes.root}
        >
            {muhasebeGuncelPermissions?.read &&
                <>
                    <ListItem button href={{ pathname: '/muhasebe/guncel', query: {} }} component={Link}>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="Güncel Durum" />
                    </ListItem>
                    <ListItem button href={{ pathname: '/muhasebe/muhaseberapor', query: {} }} component={Link}>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="Satış Rapor" />
                    </ListItem>
                </>
            }
            {muhasebeGuncelPermissions?.read &&
                <ListItem button href={{ pathname: '/muhasebe/nakitAkisi', query: {} }} component={Link}>
                    <ListItemIcon>
                        <InboxIcon />
                    </ListItemIcon>
                    <ListItemText primary="Nakit Akışı" />
                </ListItem>
            }


            {
                (faturaPermissions?.read ||
                    muhasebeMusterilerPermissions?.read ||
                    satisRaporlariPermissions?.read ||
                    tahsilatRaporlariPermissions?.read ||
                    gelirGiderRaporlariPermissions?.read) &&
                <>
                    <ListItem button onClick={() => handleClick('satis')}>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="Satış" />
                        {open.includes('satis') ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={open.includes('satis')} timeout="auto" unmountOnExit>
                        <List dense={true} component="div" disablePadding>
                            {faturaPermissions?.read &&
                                <ListItem button className={classes.nested} href={{ pathname: '/muhasebe/fatura', query: { filter: `taslak=false` } }} component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Fatura" />
                                </ListItem>
                            }
                            {faturaPermissions?.read &&
                                <ListItem button className={classes.nested} href={{ pathname: '/muhasebe/fatura', query: { filter: `taslak=true` } }} component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Taslak Faturalar" />
                                </ListItem>
                            }
                            {topluFaturaPermissions?.read &&
                                <ListItem button className={classes.nested} href={{ pathname: '/muhasebe/toplufatura', query: {} }} component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Toplu Fatura" />
                                </ListItem>
                            }
                            {muhasebeMusterilerPermissions?.read &&
                                <ListItem button className={classes.nested} href={{ pathname: '/muhasebe/musteri', query: {} }} component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Müşteriler" />
                                </ListItem>
                            }
                            {satisRaporlariPermissions?.read &&
                                <ListItem button className={classes.nested} href={{ pathname: '/muhasebe/satisrapor', query: {} }} component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Satışlar Raporu" />
                                </ListItem>
                            }
                            {gelirGiderRaporlariPermissions?.read &&
                                <ListItem button className={classes.nested} href={{ pathname: '/muhasebe/gelirGider', query: {} }} component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Gelir Gider Raporu" />
                                </ListItem>
                            }
                        </List>
                    </Collapse>
                </>
            }

            {(tahsilatPermissions?.read ||
                tahsilatPlanPermissions?.read ||
                tahsilatRaporlariPermissions?.read) &&
                <>
                    <ListItem button onClick={() => handleClick('tahsilat')}>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="Tahsilat" />
                        {open.includes('tahsilat') ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={open.includes('tahsilat')} timeout="auto" unmountOnExit>
                        <List dense={true} component='div' disablePadding>
                            {tahsilatPermissions?.read &&
                                <ListItem button className={classes.nested} href={{ pathname: '/muhasebe/tahsilat', query: {} }} component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Tahsilat" />
                                </ListItem>
                            }
                            {tahsilatPlanPermissions?.read &&
                                <ListItem button className={classes.nested} href={{ pathname: '/muhasebe/tahsilatPlan', query: {} }} component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Planlanmış Tahsilatlar" />
                                </ListItem>
                            }
                            {tahsilatRaporlariPermissions?.read &&
                                <ListItem button className={classes.nested} href={{ pathname: '/muhasebe/tahsilatrapor', query: {} }} component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Tahsilatlar Raporu" />
                                </ListItem>
                            }
                        </List>
                    </Collapse>
                </>
            }

            {giderPermissions?.read &&
                <>
                    <ListItem button onClick={() => handleClick('gider')}>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="Gider" />
                        {open.includes('gider') ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={open.includes('gider')} timeout="auto" unmountOnExit>
                        <List dense={true} component="div" disablePadding>
                            {tedarikcilerPermissions?.read &&
                                <ListItem button className={classes.nested} href='/muhasebe/gider' component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Gelen Faturalar" />
                                </ListItem>
                            }
                            {odemelerRaporuPermissions?.read &&
                                <ListItem button className={classes.nested} href='/muhasebe/odeme' component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Yapılan Ödemeler" />
                                </ListItem>
                            }
                            {odemelerRaporuPermissions?.read &&
                                <ListItem button className={classes.nested} href='/muhasebe/odemeplan' component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Planlanan Ödemeler" />
                                </ListItem>
                            }
                            {tedarikcilerPermissions?.read &&
                                <ListItem button className={classes.nested} href='/muhasebe/tedarik' component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Tedarikçiler" />
                                </ListItem>
                            }
                            {calisanlarPermissions?.read &&
                                <ListItem button className={classes.nested} href='/muhasebe/tahsilat' component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Çalışanlar" />
                                </ListItem>
                            }
                            {giderRaporuPermissions?.read &&
                                <ListItem button className={classes.nested} href='/muhasebe/tahsilat' component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Gider Raporu" />
                                </ListItem>
                            }
                            {kdvRaporuPermissions?.read &&
                                <ListItem button className={classes.nested} href='/muhasebe/tahsilat' component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="KDV Raporu" />
                                </ListItem>
                            }
                            {false && tahsilatPermissions?.read &&
                                <ListItem button className={classes.nested} href='/muhasebe/tahsilat' component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Tahsilat" />
                                </ListItem>
                            }
                            {false && tahsilatDurumuPermissions?.read &&
                                <ListItem button className={classes.nested} href='/toplanti' component={Link}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary="Tahsilat Durumu" />
                                </ListItem>
                            }
                        </List>
                    </Collapse>
                </>}
        </List>
        <Divider />
    </>
}


function menuSectionYonetim(classes, open, handleClick, permissions) {
    //let yonetimPermissions = permissions.find(p => p.key == 'yonetim')
    //if (!yonetimPermissions) {
    //    return <></>
    //}
    //if (!yonetimPermissions.read) {
    //    return <></>
    //}

    return <>
        <List
            dense={true}
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                    Yönetim
                </ListSubheader>
            }
            className={classes.root}
        >
            <ListItem button onClick={() => handleClick('hesap')}>
                <ListItemIcon>
                    <InboxIcon />
                </ListItemIcon>
                <ListItemText primary="Hesap" />
                {open.includes('hesap') ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={open.includes('hesap')} timeout="auto" unmountOnExit>
                <List dense={true} component="div" disablePadding>
                    <ListItem button className={classes.nested} href='/admin/user' component={Link}>
                        <ListItemIcon>
                            <StarBorder />
                        </ListItemIcon>
                        <ListItemText primary="Kullanıcılar" />
                    </ListItem>
                    <ListItem button className={classes.nested} href='/admin/role' component={Link}>
                        <ListItemIcon>
                            <StarBorder />
                        </ListItemIcon>
                        <ListItemText primary="Roller" />
                    </ListItem>
                    <ListItem button className={classes.nested} href='/admin/departman' component={Link}>
                        <ListItemIcon>
                            <StarBorder />
                        </ListItemIcon>
                        <ListItemText primary="Departman" />
                    </ListItem>
                    <ListItem button className={classes.nested} href='/admin/ekip' component={Link}>
                        <ListItemIcon>
                            <StarBorder />
                        </ListItemIcon>
                        <ListItemText primary="Ekip" />
                    </ListItem>
                    <ListItem button className={classes.nested} href='/admin/tenant' component={Link}>
                        <ListItemIcon>
                            <StarBorder />
                        </ListItemIcon>
                        <ListItemText primary="Tenant" />
                    </ListItem>
                    <ListItem button className={classes.nested} href='/onay' component={Link}>
                        <ListItemIcon>
                            <StarBorder />
                        </ListItemIcon>
                        <ListItemText primary="Onay" />
                    </ListItem>
                </List>
            </Collapse>
        </List>
    </>
}

const Sidebar = props => {
    const classes = useStyles();
    const [open, setOpen] = React.useState([]);
    const permissions = useSelector(state => state.auth.permissions)
    const user = useSelector(state => state.auth.user)

    const handleClick = (key) => {
        let o = [...open]
        let i = o.findIndex(k => k == key)
        if (i >= 0) {
            o = o.filter(k => k != key)
        }
        else {
            o.push(key)
        }
        setOpen(o);
    };

    if (!permissions) {
        return <></>
    }

    return <>
        <div className='flex flex-col pb-30 '>
            {menuSection(classes)}
            {menuSectionMusteri(classes, open, handleClick, permissions)}
            {menuSectionUrun(classes, open, handleClick, permissions)}
            {menuSectionOperasyon(classes, open, handleClick, permissions, user)}
            {menuSectionMuhasebe(classes, open, handleClick, permissions, user)}
            {user.isAdmin && menuSectionYonetim(classes, open, handleClick, permissions)}
        </div>
    </>
}

export default Sidebar
