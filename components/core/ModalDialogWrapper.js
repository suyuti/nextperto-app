import React, { useState } from 'react'
import { Dialog, DialogTitle, DialogContent, DialogContentText, Button, DialogActions } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import * as Actions from '../../redux/actions'

const ModalDialogWrapper = (props) => {
    const modalOpen = useSelector(state => state.common.modalOpen)
    const modalText = useSelector(state => state.common.modalText)
    const modalOption1 = useSelector(state => state.common.modalOption1)
    const modalOption2 = useSelector(state => state.common.modalOption2)
    //const modalResult   = useSelector(state => state.app.modal)
    const dispatch = useDispatch()

    //const [open, setOpen] = useState(true)
    const handleClose = () => { }
    return (
        <div>
            {props.children}
            <Dialog
                open={modalOpen}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Dikkat"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {modalText}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => {
                        dispatch(Actions.selectModalDialog(0))
                        dispatch(Actions.hideModalDialog())
                    }} 
                    color="primary">
                        {modalOption1 || 'Evet'}
                    </Button>
                    <Button onClick={e => {
                        dispatch(Actions.selectModalDialog(1))
                        dispatch(Actions.hideModalDialog())
                    }} color="primary" autoFocus>
                        {modalOption2 || 'Hayır'}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

export default ModalDialogWrapper
