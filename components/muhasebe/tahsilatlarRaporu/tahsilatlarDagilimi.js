import React, { useEffect, useState } from 'react'
import { Card, Typography, CardHeader, CardContent, Divider, Grid, makeStyles, TextField, MenuItem } from '@material-ui/core'
import dynamic from 'next/dynamic'
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })
const StatsService = require('../../../services/stats.service')


const useStyles = makeStyles(theme => ({
    root: {},
    chart: {},
    title: {
        display: 'flex',
        alignItems: 'center'
    }
}))

const TahsilatlarDagilimi = props => {
    const { tenants, onChange } = props
    const classes = useStyles()
    const [values, setValues] = useState(null)

    const load = async () => {
        StatsService.getTahsilatlarRapor().then(resp => {
            setValues(resp)
        })
    }

    useEffect(() => {
        load()
    }, [])


    return <>
        <Card>
            <CardHeader
                title={
                    <div className={classes.title}>
                        <Typography variant='h6'>Tahsilatlar Dağılımı</Typography>
                    </div>
                }
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField 
                            label='Firma'
                            variant='outlined' 
                            size='small' 
                            select>
                            {tenants.map(t => <MenuItem key={t.key} value={t.key}>{t.adi}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={3}>
                        <Typography align='center' variant='h4'>{values?.planlanmis} TL</Typography>
                        <Typography align='center' variant='h6'>Planlanmış</Typography>
                    </Grid>
                    <Grid item xs={3} >
                        <Typography align='center' variant='h4' >{values?.vadesiGecen} TL</Typography>
                        <Typography align='center' variant='h6'>Vadesi Geçen</Typography>
                    </Grid>
                    <Grid item xs={3}>
                        <Typography align='center' variant='h4'>{values?.toplamTahsilat} TL</Typography>
                        <Typography align='center' variant='h6'>Toplam Tahsilat</Typography>
                    </Grid>
                    <Grid item xs={3}>
                        <Typography align='center' variant='h4'>{values?.ortalamaVadeAsimi} gün</Typography>
                        <Typography align='center' variant='h6'>Ortalama Vade Aşımı</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Divider />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default TahsilatlarDagilimi