import React, { useEffect, useState } from 'react'
import { Card, Typography, CardHeader, CardContent, Divider, Grid, makeStyles } from '@material-ui/core'
import dynamic from 'next/dynamic'
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })
const StatsService = require('../../../services/stats.service')


const useStyles = makeStyles(theme => ({
    root: {},
    chart: {}
}))

const TahsilatlarDagilimiChart = props => {
    const classes = useStyles()
    const [options, setOptions] = useState({
        chart: {
            //width:'100px',
            //height: 350,
            type: 'bar',
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    position: 'top', // top, center, bottom
                },
            }
        },
        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val + " TL";
            },
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
    
        xaxis: {
            categories: ["Güncel", "1 - 30 Gün", "31-60 Gün", "61-90 Gün", "91-120 Gün", "120+ Gün"],
            position: 'top',
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false
            },
            crosshairs: {
                fill: {
                    type: 'gradient',
                    gradient: {
                        colorFrom: '#D8E3F0',
                        colorTo: '#BED1E6',
                        stops: [0, 100],
                        opacityFrom: 0.4,
                        opacityTo: 0.5,
                    }
                }
            },
            tooltip: {
                enabled: true,
            }
        },
        yaxis: {
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false,
            },
            labels: {
                show: false,
                formatter: function (val) {
                    return val + " TL";
                }
            }
    
        },
        title: {
            text: 'Tahsilat Dağılımı',
            floating: true,
            offsetY: 330,
            align: 'center',
            style: {
                color: '#444'
            }
        }
    })
    const [series, setSeries] = useState(null)

    const load = async () => {
        StatsService.getGelecekTahsilatlar(`count=8`).then(resp => {
            let s = [{
                name: 'Toplam',
                data: resp.map(r => r.toplam)
            }]
            setSeries(s)
            //let s = [...series]
            //s[0].data = resp.map(r => r.toplam)
            //setSeries(s)
        })
    }

    useEffect(() => {
        load()
    }, [])

    //console.log(series)

    return <>
        <Card>
            <CardHeader title={<Typography variant='h6'>Haftalık Gelecek Tahsilatlar Dağılımı</Typography>} />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <div className={classes.chart}>
                            {series &&
                                <Chart options={options} series={series} type='bar' height='350px' width='100%' />
                            }
                        </div>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default TahsilatlarDagilimiChart