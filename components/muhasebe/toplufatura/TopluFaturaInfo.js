import React from 'react'
import { makeStyles, Card, CardHeader, Divider, CardContent, Grid, TextField, ButtonGroup, Button, MenuItem, FormControl, FormControlLabel, Switch } from '@material-ui/core'

const gunler = require('../../../constants/gunler.json')

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(0)
    },
}))

const TopluFaturaInfo = props => {
    const classes = useStyles()
    const {onChange, firmalar, liste, tenants} = props
    return <>
        <div className={classes.root}>
            <Card>
                <CardHeader
                    title='Toplu Fatura Bilgileri'
                />
                <Divider />
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                variant='outlined'
                                fullWidth
                                label='Toplu Fatura Liste Adı'
                                value = {liste.listeAdi}
                                onChange={(e) => {
                                    onChange({field: 'listeAdi', value: e.target.value})
                                }}
                            />
                        </Grid>
                        <Grid item xs={3}>
                            <TextField
                                variant='outlined'
                                fullWidth
                                label='Fatura Kesen Firma'
                                value = {liste.vkesenFirma}
                                onChange={(e) => {
                                    onChange({field: 'vkesenFirma', value: e.target.value})
                                }}
                                select
                            >
                                {tenants.map(t => <MenuItem key={t.key} value={t.key}>{t.marka}</MenuItem>)}
                            </TextField>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField
                                variant='outlined'
                                fullWidth
                                label='Fatura Kesim Günü'
                                value = {liste.faturaKesimGunu}
                                onChange={(e) => {
                                    onChange({field: 'faturaKesimGunu', value: e.target.value})
                                }}
                                select
                            >
                                {gunler.map(g => <MenuItem key={g} value={g}>{g}</MenuItem>)}
                            </TextField>
                        </Grid>
                        <Grid item xs={2}>
                            <FormControl component='fieldset'>
                                <FormControlLabel
                                    value={liste.sabitFiyatliMi}
                                    control={
                                        <Switch
                                            color="primary"
                                            checked={liste.sabitFiyatliMi}
                                            onChange={(e) => {
                                                onChange({field: 'sabitFiyatliMi', value: e.target.checked})
                                            }}
                                        />}
                                    label={liste.sabitFiyatliMi ? 'Sabit Fiyatlı' : "Değişken Fiyatlı"}
                                    labelPlacement="top"
                                />
                            </FormControl>
                        </Grid>
                        <Grid item xs={2}>
                            <FormControl component='fieldset'>
                                <FormControlLabel
                                    value={liste.active}
                                    control={
                                        <Switch
                                            color="primary"
                                            checked={liste.active}
                                            onChange={(e) => {
                                                onChange({field: 'active', value: e.target.checked})
                                                //onGorevChanged({field:'icGorevMi', value: e.target.checked})
                                            }}
                                        />}
                                    label={liste.active ? 'Aktif' : "Pasif"}
                                    labelPlacement="top"
                                />
                            </FormControl>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </div>
    </>
}

export default TopluFaturaInfo