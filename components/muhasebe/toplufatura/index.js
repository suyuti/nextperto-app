import React, { useState, useEffect, useRef } from 'react'
import { TextField, IconButton, Button, Card, CardContent, CardHeader, Divider, Tooltip, MenuItem, Grid, FormControl, FormControlLabel, Switch } from '@material-ui/core'
import { searchFirmalar } from '../../../services/firma.service'
import RemoteAutoComplete from '../../autocomplete'
import MoneyInput from '../../../components/core/MoneyInput'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import InsertDriveFileOutlinedIcon from '@material-ui/icons/InsertDriveFileOutlined';
import { update } from 'lodash'
import { useSelector, useDispatch } from 'react-redux'
import * as Actions from '../../../redux/actions'
const gunler = require('../../../constants/gunler.json')


const TopluFaturaRow = React.memo(({
    index,
    urunler,
    fatura,
    sabitFiyatliMi,
    onDelete,
    onPreview,
    onChange }) => {
    const dispatch = useDispatch()

    const [firmalar, setFirmalar] = useState([])

    const updateThis = (key, val) => {
        dispatch(Actions.changeTopluFaturaListItem(index, key, val))
    }

    //debugger
    return (
        <tr className='' key={index}>
            <td>
                <RemoteAutoComplete
                    //error={errors && errors.firma}
                    //helperText={errors && errors.firma}
                    size='small'
                    //label='Müşteri'
                    labelField='marka'
                    //readOnly={!newFatura}
                    results={firmalar}
                    value={fatura.firma?.marka}
                    onSelected={(firma) => {
                        updateThis('vfirma', firma.key)
                    }}
                    fetchRemote={(searchText) => {
                        searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                            setFirmalar(resp)
                        })
                    }}
                />
            </td>
            <td>
                <TextField
                    fullWidth
                    variant='outlined'
                    margin='none'
                    size='small'
                    select
                    placeholder='Ürün'
                    value={fatura.urun ? fatura.urun._id || fatura.urun : null}
                    onChange={(e) => {
                        updateThis('urun', e.target.value)
                        updateThis('urunAdi', urunler.find(u => u._id == e.target.value).adi)
                    }}
                >
                    {urunler?.map((urun, i) => {
                        return <MenuItem key={urun._id} value={urun._id}>{urun.adi}</MenuItem>
                    })}
                </TextField>
            </td>
            <td>
                <TextField
                    fullWidth
                    variant='outlined'
                    margin='none'
                    placeholder='Aciklama'
                    size='small'
                    value={fatura.aciklama}
                    onChange={(e) => { updateThis('aciklama', e.target.value) }}
                />
            </td>
            {sabitFiyatliMi &&
                <>
                    <td>
                        <TextField
                            fullWidth
                            variant='outlined'
                            margin='none'
                            size='small'
                            value={fatura.birimFiyat}
                            onChange={(e) => {
                                let toplam = parseFloat(e.target.value) * (100 + parseInt(fatura.kdv)) / 100
                                updateThis('birimFiyat', e.target.value)
                                updateThis('toplam', toplam)
                            }}
                            InputProps={{
                                //readOnly,
                                inputComponent: MoneyInput
                            }}
                        />
                    </td>
                    <td>
                        <TextField
                            fullWidth
                            variant='outlined'
                            margin='none'
                            size='small'
                            value={fatura.kdv}
                            onChange={(e) => {
                                let toplam = parseFloat(fatura.birimFiyat) * (100 + parseInt(e.target.value)) / 100
                                updateThis('kdv', e.target.value)
                                updateThis('toplam', toplam)
                            }}
                            select
                        >
                            <MenuItem key={'0'} value={'0'}>0</MenuItem>
                            <MenuItem key={'1'} value={'1'}>1</MenuItem>
                            <MenuItem key={'8'} value={'8'}>8</MenuItem>
                            <MenuItem key={'18'} value={'18'}>18</MenuItem>
                        </TextField>
                    </td>
                    <td>
                        <TextField
                            fullWidth
                            variant='outlined'
                            margin='none'
                            size='small'
                            value={fatura.toplam}
                            InputProps={{
                                readOnly: true,
                                inputComponent: MoneyInput
                            }}

                        />
                    </td>
                </>}
            <td align='center'>
                <div className='flex space-x-1'>
                    <IconButton size='small' className='focus:outline-none' onClick={() => onDelete()}>
                        <DeleteOutlineIcon />
                    </IconButton>
                    {sabitFiyatliMi &&
                        <IconButton size='small' className='focus:outline-none' onClick={() => {
                            onPreview()
                        }}>
                            <InsertDriveFileOutlinedIcon />
                        </IconButton>
                    }
                </div>
            </td>
        </tr>

    )
})

const TopluFatura = (props) => {
    const { urunler, tenants, onPreview } = props
    const topluFatura = useSelector(state => state.fatura.topluFatura)
    const topluFaturaListe = useSelector(state => state.fatura.topluFaturaListe)
    const dispatch = useDispatch()

    const updateThis = (key, val) => {
        dispatch(Actions.updateTopluFatura(key, val))
    }


    if (!topluFatura) {
        return <>loading...</>
    }

    return (
        <div>
            <Card>
                <CardHeader
                    title='Toplu Fatura'
                />
                <Divider />
                <CardContent>
                    <div className='p-2'>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    label='Toplu Fatura Liste Adı'
                                    fullWidth
                                    variant='outlined'
                                    size='small'
                                    value={topluFatura.listeAdi}
                                    onChange={e => {
                                        updateThis('listeAdi', e.target.value)
                                    }}
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <TextField
                                    label='Fatura Kesen Firma'
                                    fullWidth
                                    variant='outlined'
                                    size='small'
                                    select
                                    value={topluFatura.vkesenFirma}
                                    onChange={e => {
                                        updateThis('vkesenFirma', e.target.value)
                                    }}
                                >
                                    {tenants.map(t => <MenuItem key={t.key} value={t.key}>{t.marka}</MenuItem>)}
                                </TextField>
                            </Grid>
                            <Grid item xs={3}>
                                <TextField
                                    label='Fatura Kesim Günü'
                                    fullWidth
                                    variant='outlined'
                                    size='small'
                                    select
                                    value={topluFatura.faturaKesimGunu}
                                    onChange={e => {
                                        updateThis('faturaKesimGunu', e.target.value)
                                    }}
                                >
                                    {gunler.map(g => <MenuItem key={g} value={g}>{g}</MenuItem>)}
                                </TextField>
                            </Grid>
                            <Grid item xs={2}>
                                <FormControl component='fieldset'>
                                    <FormControlLabel
                                        value={topluFatura.sabitFiyatliMi}
                                        control={
                                            <Switch
                                                color="primary"
                                                checked={topluFatura.sabitFiyatliMi}
                                                onChange={(e) => {
                                                    updateThis('sabitFiyatliMi', e.target.checked)
                                                }}
                                            />}
                                        label={topluFatura.sabitFiyatliMi ? 'Sabit Fiyatlı' : "Değişken Fiyatlı"}
                                        labelPlacement="top"
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={2}>
                                <FormControl component='fieldset'>
                                    <FormControlLabel
                                        value={topluFatura.active}
                                        control={
                                            <Switch
                                                color="primary"
                                                checked={topluFatura.active}
                                                onChange={(e) => {
                                                    updateThis('active', e.target.checked)
                                                }}
                                            />}
                                        label={topluFatura.active ? 'Aktif' : "Pasif"}
                                        labelPlacement="top"
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={12}>
                                <table className='w-full'>
                                    <colgroup>
                                        <col width='25%'></col>
                                        <col width='30%'></col>
                                        <col width='10%'></col>
                                        {topluFatura.sabitFiyatliMi &&
                                            <>
                                                <col width='10%'></col>
                                                <col width='5%'></col>
                                                <col width='15%'></col>
                                            </>
                                        }
                                        <col width='5%'></col>
                                    </colgroup>
                                    <thead>
                                        <tr className='bg-gray-200 text-black' key='h-1'>
                                            <th className='text-lg font-light'>Müşteri</th>
                                            <th className='text-lg font-light'>Ürün</th>
                                            <th className='text-lg font-light'>Açıklama</th>
                                            {topluFatura.sabitFiyatliMi &&
                                                <>
                                                    <th className='text-lg font-light'>Birim Fiyat</th>
                                                    <th className='text-lg font-light'>KDV</th>
                                                    <th className='text-lg font-light'>Toplam</th>
                                                </>
                                            }
                                            <th className='text-lg font-light'></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {topluFaturaListe.map((fatura, index) => {
                                            return (<TopluFaturaRow
                                                index={index}
                                                fatura={fatura}
                                                sabitFiyatliMi={topluFatura.sabitFiyatliMi}
                                                urunler={urunler}
                                                onDelete={() => {
                                                    //alert('d')
                                                    //dispatch(Actions.showModalDialog('Silinecek emin misiniz?', 'Evet', 'Hayir'))
                                                    dispatch(Actions.removeTopluFaturaListItem(index))
                                                }}
                                                onPreview={() => {
                                                    debugger
                                                    onPreview(index)
                                                }}
                                            />)
                                        })}
                                    </tbody>
                                </table>
                            </Grid>
                            <Grid item xs={12}>
                                <Button variant='contained' color='primary' onClick={() => {
                                    dispatch(Actions.addTopluFaturaListItem({
                                        aciklama: '',
                                        kdv: 18,
                                        urun: null,
                                        birimFiyat: 0,
                                        vfirma: null
                                    }))
                                }}>Ekle</Button>
                            </Grid>
                        </Grid>
                    </div>
                </CardContent>
            </Card>
        </div>
    )
}



export default TopluFatura
