import React, { useEffect, useState, useRef } from 'react'
import { Tooltip, Card, CardContent, CardHeader, Divider, Grid, IconButton, Table, TableHead, TableRow, TableBody, TableCell, TextField, MenuItem } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
//import TopluFaturaFirmaRow from './TopluFaturaFirmaRow'
import MoneyInput from '../../../components/core/MoneyInput'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import InsertDriveFileOutlinedIcon from '@material-ui/icons/InsertDriveFileOutlined';
import { searchFirmalar } from '../../../services/firma.service'
import RemoteAutoComplete from '../../autocomplete'


const TopluFaturaRow = ({ index, urunler, readOnly, sabitFiyatli, onDelete, key, fatura, stateRef, onChange }) => {
    const [firmalar, setFirmalar] = useState([])
    const [form, setForm] = useState(fatura)
    useEffect(() => {
        if (onChange) {
            console.log(form)
            onChange(form)
        }
        //if (stateRef) {
        //    stateRef.current = form
        //}
    }, [form])

    //useEffect(() => {
    //    if (stateRef) {
    //        stateRef.current = form
    //    }
    //}, [form])

    const hesapla = (birim, kdv) => {
        let _birim = parseInt(birim) || 0
        let _kdv = parseInt(kdv) || 0
        return _birim * 1 * (100 + _kdv) / 100
    }

    return (
        <tr className='' key={index}>
            <td>
                <RemoteAutoComplete
                    //error={errors && errors.firma}
                    //helperText={errors && errors.firma}
                    size='small'
                    label='Müşteri'
                    labelField='marka'
                    //readOnly={!newFatura}
                    results={firmalar}
                    //value={fatura.firma?.marka}
                    onSelected={() => { }}
                    //onSelected={(firma) => {
                    //    let f = { ...fatura }
                    //    f.vfirma = firma.key
                    //    f.firmaAdi = firma.marka
                    //    setFatura(f)
                    //}}
                    fetchRemote={(searchText) => {
                        searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                            setFirmalar(resp)
                        })
                    }}
                />
            </td>
            <td>
                <Tooltip //title={fatura.kalemler[i].urunAdi}
                >
                    <TextField
                        fullWidth
                        variant='outlined'
                        margin='none'
                        size='small'
                        select
                        placeholder='Ürün'
                        value={form.urun}
                        onChange={(e) => {
                            let f = { ...form }
                            let urun = urunler.find(u => u.key == e.target.value)
                            f.birimFiyat = urun.birimFiyat
                            f.kdv = urun.kdvOrani
                            f.toplam = hesapla(f.birimFiyat, f.kdv)
                            f.urun = e.target.value
                            setForm(f)
                        }}
                    >
                        {urunler?.map((urun, i) => {
                            return <MenuItem key={urun.key} value={urun.key}>{urun.adi}</MenuItem>
                        })}
                    </TextField>
                </Tooltip>
            </td>
            <td>
                <TextField
                    fullWidth
                    variant='outlined'
                    margin='none'
                    placeholder='Aciklama'
                    size='small'
                    value={form.aciklama}
                    onChange={(e) => {
                        let f = { ...form }
                        f.aciklama = e.target.value
                        setForm(f)
                    }}
                />
            </td>
            {!sabitFiyatli &&
                <>
                    <td>
                        <TextField
                            fullWidth
                            variant='outlined'
                            margin='none'
                            size='small'
                            value={form.birimFiyat}
                            onChange={(e) => {
                                let f = { ...form }
                                f.birimFiyat = e.target.value
                                f.toplam = hesapla(f.birimFiyat, f.kdv)
                                setForm(f)
                            }}
                            InputProps={{
                                //readOnly,
                                inputComponent: MoneyInput
                            }}
                        />
                    </td>
                    <td>
                        <TextField
                            fullWidth
                            variant='outlined'
                            margin='none'
                            size='small'
                            value={form.kdv}
                            onChange={(e) => {
                                let f = { ...form }
                                f.kdv = e.target.value
                                f.toplam = hesapla(f.birimFiyat, f.kdv)
                                setForm(f)
                            }}
                            select
                        >
                            <MenuItem key={'0'} value={'0'}>0</MenuItem>
                            <MenuItem key={'1'} value={'1'}>1</MenuItem>
                            <MenuItem key={'8'} value={'8'}>8</MenuItem>
                            <MenuItem key={'18'} value={'18'}>18</MenuItem>
                        </TextField>
                    </td>
                    <td>
                        <TextField
                            fullWidth
                            variant='outlined'
                            margin='none'
                            size='small'
                            value={form.toplam}
                            InputProps={{
                                readOnly: true,
                                inputComponent: MoneyInput
                            }}

                        />
                    </td>
                </>}
            <td>
                <div className='flex space-x-1'>
                    <IconButton size='small' className='focus:outline-none' onClick={() => onDelete()}>
                        <DeleteOutlineIcon />
                    </IconButton>
                    <IconButton size='small' className='focus:outline-none'>
                        <InsertDriveFileOutlinedIcon />
                    </IconButton>
                </div>
            </td>
        </tr>
    )
}

const TopluFaturaFirmalar = props => {
    const { urunler, sabitFiyatli, onRemove, onNew, readOnly, firmalar, onFaturaOnIzleme } = props
    const [faturalar, setFaturalar] = useState([])
    const faturalarRef = useRef([])

    useEffect(() => {
        if (faturalarRef) {
            faturalarRef.current = [...faturalar, faturalarRef.current]
        }
    }, [faturalar])

    const onChange = (index, val) => {
        //debugger
        let f = [...faturalar]
        f[index] = val
        setFaturalar(f)
    }

    return <>
        <Card>
            <CardHeader
                title='Firmalar'
                action={<IconButton className='focus:outline-none' onClick={() => {
                    let f = [...faturalar]
                    f.push({
                        birimFiyat: 0,
                        urun: null,
                        aciklama: '',
                        kdv: 18,
                        toplam: 0
                    })
                    setFaturalar(f)
                }}><AddIcon /></IconButton>}
            />
            <Divider />
            <CardContent>
                <div className='w-full'>
                    <table style={{
                        width: '100%',
                        //whiteSpace:'normal',
                        //wordWrap: 'break-word'
                    }}>
                        <thead className='bg-gray-200 text-lg text-black'>
                            <th className='text-lg font-light'>Müşteri</th>
                            <th className='text-lg font-light'>Ürün</th>
                            <th className='text-lg font-light'>Açıklama</th>
                            {!sabitFiyatli &&
                                <>
                                    <th className='text-lg font-light'>Birim Fiyat</th>
                                    <th className='text-lg font-light'>KDV</th>
                                    <th className='text-lg font-light'>Toplam</th>
                                </>
                            }
                            <th className='text-lg font-light'></th>
                        </thead>
                        <tbody>
                            {faturalar.map((fatura, i) => {
                                return (
                                    <TopluFaturaRow
                                        index={i}
                                        urunler={urunler}
                                        readOnly
                                        sabitFiyatli={sabitFiyatli}
                                        fatura={fatura}
                                        stateRef={faturalarRef}
                                        onChange={(f) => onChange(i, f)}
                                        //stateRef={f}
                                        onDelete={(e) => {
                                            debugger
                                            let f = [...faturalar]
                                            f.splice(i, 1)
                                            setFaturalar(f)
                                        }}
                                    />
                                )
                            })}
                        </tbody>
                    </table>
                </div>

                {false &&
                    <Table size='small'>
                        <TableHead>
                            <TableRow>
                                <TableCell>Firma</TableCell>
                                <TableCell>Açıklama</TableCell>
                                {sabitFiyatli && <TableCell>Tutar</TableCell>}
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {firmalar.map((f, i) => {
                                return <TopluFaturaFirmaRow
                                    row={f}
                                    sabitFiyatli={sabitFiyatli}
                                    onChange={(e) => onChange({ index: i, field: e.field, value: e.value })}
                                    onRemove={(e) => onRemove(i)}
                                    onFaturaOnIzleme={(e) => onFaturaOnIzleme({ index: i })}
                                />
                            })}
                        </TableBody>
                    </Table>
                }
            </CardContent>
        </Card>
    </>
}

export default TopluFaturaFirmalar