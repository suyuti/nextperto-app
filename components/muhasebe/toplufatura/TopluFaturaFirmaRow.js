/*
import React, { useState } from 'react'
import { TableRow, TableCell, IconButton, TextField, Tooltip, ButtonGroup } from '@material-ui/core'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline'
import RemoteAutoComplete from '../../autocomplete'
import { searchFirmalar } from '../../../services/firma.service'
import MoneyInput from '../../../components/core/MoneyInput'
import InsertDriveFileOutlinedIcon from '@material-ui/icons/InsertDriveFileOutlined';

const TopluFaturaFirmaRow = props => {
    const { sabitFiyatli, onRemove, row, onChange, onFaturaOnIzleme } = props
    const [searchResults, setSearchResults] = useState([])


    return <>
        <TableRow hover>
            <TableCell padding='none'>
                <RemoteAutoComplete
                    //error={errors?.firma}
                    results={searchResults}
                    labelField='marka'
                    size='small'
                    value={row.firma?.marka}
                    onSelected={(f) => {
                        onChange({ field: 'vfirma', value: f.key })
                    }}
                    fetchRemote={(searchText) => {
                        searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                            setSearchResults(resp)
                        })
                    }}
                />
            </TableCell>
            <TableCell padding='none'>
                <TextField
                    variant='outlined'
                    fullWidth
                    size='small'
                    value={row.aciklama}
                    onChange={(e) => {
                        onChange({ field: 'aciklama', value: e.target.value })
                    }}
                />
            </TableCell>

            {sabitFiyatli && <TableCell padding='none'>
                <TextField
                    fullWidth
                    variant='outlined'
                    value={row.araToplam}
                    InputProps={{
                        inputComponent: MoneyInput
                    }}
                    size='small'
                    onChange={(e) => {
                        onChange({ field: 'araToplam', value: e.target.value })
                    }}
                />
            </TableCell>}
            <TableCell>
                <ButtonGroup>
                <IconButton onClick={onRemove}>
                    <DeleteOutlineIcon />
                </IconButton>
                <Tooltip title='Fatura Ön izleme'>
                    <IconButton onClick={() => onFaturaOnIzleme()}>
                        <InsertDriveFileOutlinedIcon />
                    </IconButton>
                </Tooltip>

                </ButtonGroup>
            </TableCell>
        </TableRow>
    </>
}

export default TopluFaturaFirmaRow
*/