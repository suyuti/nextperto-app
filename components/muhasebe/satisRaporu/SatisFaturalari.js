import React from 'react'
import { Card, Typography, CardContent, CardHeader, Divider, Grid, makeStyles } from '@material-ui/core'
import dynamic from 'next/dynamic'
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })


const options = {
    chart: {
        width: 380,
        type: 'pie',
    },
    labels: ['Kapalı', 'Açık', 'Geciken'],
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                show: false
            }
        }
    }]
}
const series = [1,2,3]

function chart(options, series) {
    return <div className='chart'>
        <Chart options={options} series={series} type='pie' />
        <style jsx>{`
      .chart {
        width: 350px;
        margin: auto;
      }
    `}</style>
    </div>
}

const SatisFaturalari = props => {
    return <>
        <Card>
            <CardHeader
                title={<Typography variant='body2'>Satış Faturaları</Typography>}
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={4}>{chart(options, series)}</Grid>
                    <Grid item xs={4}>{chart(options, series)}</Grid>
                    <Grid item xs={4}>{chart(options, series)}</Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default SatisFaturalari