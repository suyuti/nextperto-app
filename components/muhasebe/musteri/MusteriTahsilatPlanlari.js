import React from 'react'
import {
    Card, Typography, CardHeader,
    Divider, CardContent,
    Grid, TextField,
    Table, TableRow,
    TableHead, TableBody,
    TableCell,
    Button, CardActions,
    makeStyles,
    IconButton
} from '@material-ui/core'
import numbro from 'numbro'
import moment from 'moment'
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { useRouter } from 'next/router'

const useStyles = makeStyles(theme => ({
    actionButtons: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center'
    }
}))

const MusteriTahsilatPlanlari = props => {
    const classes = useStyles()
    const { planlanmisTahsilatlar, firma } = props
    const router = useRouter()

    return <>
        <Card>
            <CardHeader
                title={<Typography variant='body1'>Planlanmış Tahsilatlar</Typography>}
            />
            <Divider />
            <CardContent>
                <Table size='small'>
                    <TableHead>
                        <TableRow>
                            <TableCell>Tarih</TableCell>
                            <TableCell>Ödeme</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {planlanmisTahsilatlar.map(p => {
                            return (
                                <TableRow hover>
                                    <TableCell>{moment(p.tarih).format('DD.MM.YYYY')}</TableCell>
                                    <TableCell>{`${numbro(p.tutar).format({ thousandSeparated: true, mantissa: 2 })} TL`}</TableCell>
                                    <TableCell><IconButton size='small' onClick={() => {
                                        router.push(`/muhasebe/tahsilatPlan/${p._id}`)
                                    }}><ChevronRightIcon fontSize='small' /></IconButton></TableCell>
                                </TableRow>
                            )
                        })}

                    </TableBody>
                </Table>
            </CardContent>
            <Divider />
            <CardActions className={classes.actionButtons}>
                <Button size='small' variant='outlined' color='primary' onClick={() => {
                    router.push(`/muhasebe/tahsilatPlan/[id]?firma=${firma.key}`, `/muhasebe/tahsilatPlan/new?firma=${firma.key}`)
                }}>Tahsilat Planla</Button>
            </CardActions>
        </Card>
    </>
}

export default MusteriTahsilatPlanlari