import React from 'react'
import { Card, Typography, CardHeader, Divider, CardContent, Grid, TextField, CardActions, Button, makeStyles } from '@material-ui/core'
import numbro from 'numbro'

const useStyles = makeStyles(theme => ({
    actionButtons: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center'
    }
}))

const MusteriMuhasebeInfo = props => {
    const { firma,
        firmaToplamTahsilat,
        firmaToplamFatura,
        firmaBorc
    } = props
    const classes = useStyles()

    return <>
        <Card>
            <CardHeader
                title={<Typography variant='body1'>Müşteri Bilgileri</Typography>}
            />
            <Divider />
            <CardContent>
                <div className='flex flex-col my-3 space-y-4'>
                    <h2 className='text-xl font-light antialiased'>{firma.marka}</h2>
                    <div className='flex flex-row justify-around'>
                        <div className='flex flex-col items-center'>
                            <h2 className='text-sm font-light text-gray-700 antialiased'>Tahsilatlar</h2>
                            <h2 className='text-lg font-normal text-gray-800 antialiased'>{numbro(firmaToplamTahsilat).format({thousandSeparated: true, mantissa: 2})} TL</h2>
                        </div>
                        <div className='flex flex-col items-center'>
                            <h2 className='text-sm font-light text-gray-700 antialiased'>Faturalar</h2>
                            <h2 className='text-lg font-normal text-gray-800 antialiased'>{numbro(firmaToplamFatura).format({thousandSeparated: true, mantissa: 2})} TL</h2>
                        </div>
                        <div className='flex flex-col items-center'>
                            <h2 className='text-sm font-light text-gray-700 antialiased'>Alacak</h2>
                            <h2 className='text-lg font-semibold text-red-500 antialiased'>{numbro(firmaBorc).format({thousandSeparated: true, mantissa: 2})} TL</h2>
                        </div>
                    </div>
                </div>
            </CardContent>
            <Divider />
            <CardActions className={classes.actionButtons}>
                <Button variant='outlined' color='primary' size='small'>Tahsilat Talep Et</Button>
                <Button variant='outlined' color='primary' size='small'>Not</Button>
            </CardActions>
        </Card>
    </>
}

export default MusteriMuhasebeInfo