import React, { useState } from 'react'
import { Paper, Button, Grid, makeStyles, TextField, MenuItem, Accordion, AccordionSummary, Typography, AccordionDetails, Divider, AccordionActions } from '@material-ui/core'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import { useSelector } from 'react-redux';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import RemoteAutoComplete from '../../autocomplete'
import { searchFirmalar } from '../../../services/firma.service'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const initialQuery = {
    tenant: null,
}

const FirmaSearchToolbar = props => {
    const { disabled, tenants, onSearch } = props
    const classes = useStyles()
    const users = useSelector(state => state.auth.users)
    const today = new Date()
    const [query, setQuery] = useState({ ...initialQuery })
    const [results, setResults] = useState([])

    return <>
        <Accordion >
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1c-content"
                id="panel1c-header">
                <Typography>Filtre</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Grid container spacing={2}>
                    <Grid item xs={2}>
                        <TextField
                            disabled={disabled}
                            fullWidth
                            label='Bağlı Olduğu Firma'
                            variant='outlined'
                            size='small'
                            value={query.tenant}
                            onChange={(e) => {
                                let q = { ...query }
                                q.tenant = e.target.value
                                setQuery(q)
                            }}
                            select
                        >
                            {tenants.map(t => <MenuItem key={t.key} value={t.key}>{t.adi}</MenuItem>)}
                        </TextField>
                    </Grid>
                </Grid>
            </AccordionDetails>
            <Divider />
            <AccordionActions>
                <Button
                    disabled={disabled}
                    size='small'
                    variant='contained'
                    color='primary'
                    onClick={() => {
                        setQuery({ ...initialQuery })
                    }}>Temizle</Button>
                <Button
                    disabled={disabled}
                    size='small'
                    variant='contained'
                    color='primary'
                    onClick={() => {
                        debugger
                        onSearch(query)
                    }}>Sorgula</Button>
            </AccordionActions>
        </Accordion>
    </>
}

export default FirmaSearchToolbar