import React from 'react'
import { Button, makeStyles, Grid } from '@material-ui/core'
import { useRouter } from 'next/router';
import TableView from '../../tableview';
import { useSelector } from 'react-redux'
import moment from 'moment'
import numbro from 'numbro';
import cn from 'classnames'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(0)
    }
}))

const MusteriFaturalar = props => {
    const { firma } = props
    const classes = useStyles()
    const router = useRouter()
    const token = useSelector(state => state.auth.expertoToken)

    const columns = [
        { id: 'aciklama', label: 'Açıklama' },
        { id: 'duzenlemeTarihi', label: 'İşlem Tarihi', render: row => moment(row.duzenlemeTarihi).format('DD.MM.YYYY') },
        { id: 'vadeTarihi', label: 'Vade Tarihi', render: row => moment(row.duzenlemeTarihi).format('DD.MM.YYYY') },
        {
            id: 'genelToplam', align: 'right', label: 'Tutar', render: row => {
                let tahsilatTutari = row.tahsilatTutari || 0
                let durum = 'Ödendi'
                if ((tahsilatTutari) == 0) {
                    durum = 'Ödenmedi'
                }
                else if ((row.genelToplam - tahsilatTutari) > 0) {
                    durum = 'Kısmen Ödendi'
                }

                return <div className=''>
                    <h4 className='text-md font-medium antialiased text-gray-700'>{numbro(row.genelToplam).format({ thousandSeparated: true, mantissa: 2 })} TL</h4>
                    <h4 className={cn([{
                        'font-sans font-semibold text-xs' : true,
                        'text-red-500' : tahsilatTutari == 0,
                        'text-blue-500' : (row.genelToplam - tahsilatTutari) > 0 && tahsilatTutari > 0,
                        'text-green-500' : (row.genelToplam - tahsilatTutari) == 0
                    }])
                }>{durum}</h4>
            </div >
        }},
{
    id: '', label: '', render: row => {
        return <Button variant='outlined' color='primary' size='small' onClick={() => {
            router.push(`/muhasebe/fatura/${row._id}`)
        }}>Detay</Button>
    }
},
    ]

return <>
    <div className={classes.root}>
        <TableView
            title='Müşterinin Faturaları'
            columns={columns}
            remoteUrl={`/fatura`}
            token={token}
            filter={`vfirma=${firma.key}&select=genelToplam,aciklama,vadeTarihi,tahsilatTutari,duzenlemeTarihi&sort=-vadeTarihi`}
            searchCol='aciklama'
        //data={[]}
        />
    </div>
</>
}

export default MusteriFaturalar