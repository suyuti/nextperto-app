import React from 'react'
import { Button, makeStyles, Grid } from '@material-ui/core'
import { useRouter } from 'next/router';
import TableView from '../../tableview';
import { useSelector } from 'react-redux'
import moment from 'moment'
import numbro from 'numbro';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(0)
    }
}))

const MusteriTahsilatlar = props => {
    const {firma} = props
    const classes = useStyles()
    const router = useRouter()
    const token = useSelector(state => state.auth.expertoToken)
    const columns = [
        {id:'aciklama', label: 'Açıklama'},
        {id:'tahsilatTarihi', label: 'İşlem Tarihi', render: row => moment(row.tahsilatTarihi).format('DD.MM.YYYY')},
        {id:'Tutar', align:'right', label: 'Tutar', render: row => {
            return `${numbro(row.tutar).format({thousandSeparated: true, mantissa: 2})} TL`
        }},
        {id:'tahsilatTuru', label: 'Tahsilat Türü'},
        {id:'', label: '', render: row => {
            return <Button variant='outlined' color='primary' size='small' onClick={() => {
                router.push(`/muhasebe/tahsilat/${row._id}`)
            }}>Detay</Button>
        }},
    ]

    return <>
        <div className={classes.root}>
            <TableView
                title='Müşteri Tahsilatları'
                columns={columns}
                remoteUrl={`/tahsilat`}
                token={token}
                filter={`vfirma=${firma.key}&select=tutar,aciklama,tahsilatTarihi,tahsilatTuru&sort=-tahsilatTarihi`}
                searchCol='aciklama'
            //data={[]}
            />
        </div>
    </>
}

export default MusteriTahsilatlar