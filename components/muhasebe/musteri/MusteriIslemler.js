import React from 'react'
import { Button, makeStyles, Grid } from '@material-ui/core'
import { useRouter } from 'next/router';
import TableView from '../../tableview';
import { useSelector } from 'react-redux'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(0)
    }
}))

const MusteriIslemler = props => {
    const classes = useStyles()
    const router = useRouter()
    const token = useSelector(state => state.auth.expertoToken)
    const columns = [
        {id:'marka', label: 'İşlem Türü'},
        {id:'borc', label: 'Açıklama'},
        {id:'alacak', label: 'İşlem Tarihi'},
        {id:'alacak', label: 'Tutar'},
        {id:'alacak', label: 'Müşteri Bakiyesi'},
        {id:'', label: '', render: row => {
            return <Button variant='outlined' color='primary' size='small' onClick={() => {
                router.push(`/muhasebe/musteri/${row._id}`)
            }}>Detay</Button>
        }},
    ]

    return <>
        <div className={classes.root}>
            <TableView
                title='İşlemler'
                columns={columns}
                remoteUrl={`/firma`}
                token={token}
                filter={`select=marka`}
                searchCol='marka'
            //data={[]}
            />
        </div>
    </>
}

export default MusteriIslemler