import React from 'react'
import { makeStyles, Grid, Typography, Button } from '@material-ui/core'
import FaturaKalem from './FaturaKalem'
import AddIcon from '@material-ui/icons/Add'

const useStyles = makeStyles(theme => ({
    root: {},
    row: {},
    yeniSatirButton: {
        marginTop: theme.spacing(2)
    }
}))

const FaturaKalemler = props => {
    const classes = useStyles()
    const { data, onChange, addNew, onRemove, enableAdd, urunler } = props

    return <>
        <Grid item xs={12} container spacing={0}>
            <Grid item xs={3}><Typography variant='body1'>Ürün / Hizmet</Typography></Grid>
            <Grid item xs={3}><Typography variant='body1'>Miktar</Typography></Grid>
            <Grid item xs={3}><Typography variant='body1'>Birim Fiyat</Typography></Grid>
            {/*
            <Grid item xs={1}><Typography variant='body1'>KDV Oranı</Typography></Grid>
            <Grid item xs={1}><Typography variant='body1'>KDV</Typography></Grid>
            */}
            <Grid item xs={2}><Typography variant='body1'>Toplam</Typography></Grid>
            {data.map((row, rowIndex) => {
                return <div className={classes.row}>
                    <FaturaKalem
                        row={row}
                        onChange={(e) => {
                            if (enableAdd) {
                                onChange({index: rowIndex, field: e.field, value: e.value})
                            }
                        }}
                        urunler={urunler}
                        onRemove={(e) => { 
                            if (enableAdd) {
                                onRemove(rowIndex)
                            }
                        }}
                    />
                </div>
            })}
        </Grid>
        <Grid item xs={12}>
            <Button
                disabled={!enableAdd}
                className={classes.yeniSatirButton}
                variant='outlined'
                color='primary'
                onClick={addNew}
                startIcon={<AddIcon />}
            >Yeni Satır Ekle</Button>
        </Grid>
    </>
}

export default FaturaKalemler