import { Grid, Card, CardContent, CardHeader, Divider, TextField, Button, MenuItem } from '@material-ui/core'
import React, { useEffect, useRef, useState } from 'react'
import { searchFirmalar } from '../../../services/firma.service'
import { searchFatura } from '../../../services/muhasebe.service'
import RemoteAutoComplete from '../../autocomplete'
import Fatura from '../fatura2'
import moment from 'moment'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import trLocale from "date-fns/locale/tr";
import numbro from 'numbro'

const vadeler = require('../../../constants/vadeler.json')

const FaturaInfo2 = ({ urunler, kalemler, tenants, faturaData, newFatura, faturaRef, errors, ...props }) => {
    const kalemlerRef = useRef({})
    const [fatura, setFatura] = useState(faturaData)
    const [firmalar, setFirmalar] = useState([])
    const [faturalar, setFaturalar] = useState([]) // iade icin aranan faturalar
    const [iadeFatura, setIadeFatura] = useState(null)

    useEffect(() => {
        if (faturaRef) {
            faturaRef.current = { ...fatura, ...kalemlerRef.current }
        }
    }, [fatura])

    const onFaturaChange = async (e) => {
        let f = { ...fatura }
        f.araToplam = e.araToplam
        f.toplamKdv = e.toplamKdv
        f.genelToplam = e.genelToplam
        setFatura(f)
        console.log(e)
    }

    const onVade = (vade) => {
        let yeniVadeTarihi = moment(fatura.duzenlemeTarihi).add(vade, 'days')
        let f = { ...fatura }
        f.vadeTarihi = yeniVadeTarihi.toDate()
        f.vadeGun = vade
        setFatura(f)
    }


    return (
        <div>
            <Card>
                <CardHeader
                    title='Fatura Bilgileri'
                />
                <Divider />
                <CardContent>
                    <div className='p-2'>
                        <Grid container spacing={2}>
                            <Grid item xs={10}>
                                <TextField
                                    error={errors && errors.aciklama}
                                    helperText={errors && errors.aciklama}
                                    label='Fatura Açıklaması'
                                    variant='outlined'
                                    fullWidth
                                    size='small'
                                    multiline
                                    rowsMax={Infinity}
                                    rows={1}
                                    value={fatura.aciklama}
                                    InputProps={{
                                        readOnly: !newFatura
                                    }}
                                    onChange={e => {
                                        let f = { ...fatura }
                                        f.aciklama = e.target.value
                                        setFatura(f)
                                    }}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <TextField
                                    label='Tür'
                                    variant='outlined'
                                    fullWidth
                                    size='small'
                                    value={fatura.faturaTuru}
                                    onChange={e => {
                                        if (e.target.value == 'SATIS') {
                                            setIadeFatura(null)
                                        }
                                        let f = { ...fatura }
                                        f.faturaTuru = e.target.value
                                        setFatura(f)
                                    }}
                                    select
                                >
                                    <MenuItem key={'SATIS'} value={'SATIS'}>Satış</MenuItem>
                                    <MenuItem key={'IADE'} value={'IADE'}>İade</MenuItem>
                                </TextField>
                            </Grid>
                            {fatura.faturaTuru == 'IADE' &&
                                <>
                                    <Grid item xs='12'>
                                        <RemoteAutoComplete
                                            //error={errors && errors.firma}
                                            //helperText={errors && errors.firma}
                                            size='small'
                                            label='İade Edilecek Fatura Seçin'
                                            labelField='faturaNo'
                                            readOnly={!newFatura}
                                            results={faturalar}
                                            renderResultItem={(item, i) => {
                                                return (
                                                    <div className='p-4 flex justify-between border-b-2 border-gray-200'>
                                                        <div className=''>
                                                            <h2 className='text-lg'>{item.faturaNo}</h2>
                                                            <h2 className='font-light'>{item.firma?.unvan}</h2>
                                                            <h2 className='font-light'>{item.kesenFirma?.unvan}</h2>
                                                        </div>
                                                        <div>
                                                            <h4 className='text-lg'>{numbro(item.genelToplam).format({ thousandSeparated: true, mantissa: 2 })} TL</h4>
                                                            <h2 className='font-light'>{moment(item.duzenlemeTarihi).format('DD.MM.YYYY')}</h2>
                                                        </div>
                                                    </div>
                                                )
                                            }}
                                            //value={fatura.firma?.marka}
                                            onSelected={(secilenIadeFatura) => {
                                                if (secilenIadeFatura?.key == null) {
                                                    setIadeFatura(null)
                                                    let f = {...fatura}
                                                    f.vkesenFirma = null
                                                    f.vfirma=null
                                                    f.iadeFaturaNo = null
                                                    f.iadeFaturaTarih = null
                                                    setFatura(f)
                                                }
                                                else {
                                                    setIadeFatura(secilenIadeFatura)
                                                    let f = {...fatura}
                                                    f.vkesenFirma = secilenIadeFatura.vkesenFirma
                                                    f.vfirma=secilenIadeFatura.vfirma
                                                    f.iadeFaturaNo = secilenIadeFatura.faturaNo
                                                    f.iadeFaturaTarih = secilenIadeFatura.duzenlemeTarihi
                                                    setFatura(f)
                                                }
                                            }}
                                            fetchRemote={(searchText) => {
                                                searchFatura(`faturaNo=/${searchText}/i&populate=firma,kesenFirma&iadeEdildi=false`).then(resp => {
                                                    setFaturalar(resp)
                                                })
                                            }}
                                        />

                                    </Grid>
                                    {iadeFatura &&
                                        <Grid item xs={12}>
                                            <div className='border p-2 rounded shadow'>
                                                <h2 className='text-red-600'>İade Edilecek Fatura</h2>
                                                <div className='p-4 flex justify-between'>
                                                    <div className=''>
                                                        <h2 className='text-base'>{iadeFatura.faturaNo}</h2>
                                                        <h2 className='font-light'>{iadeFatura.firma?.unvan}</h2>
                                                        <h2 className='font-light'>{iadeFatura.kesenFirma?.unvan}</h2>
                                                    </div>
                                                    <div>
                                                        <h4 className='text-base'>{numbro(iadeFatura.genelToplam).format({ thousandSeparated: true, mantissa: 2 })} TL</h4>
                                                        <h4 className='text-sm'>{numbro(iadeFatura.araToplam).format({ thousandSeparated: true, mantissa: 2 })} TL</h4>
                                                        <h2 className='font-light'>{moment(iadeFatura.duzenlemeTarihi).format('DD.MM.YYYY')}</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </Grid>
                                    }
                                </>
                            }
                            <Grid item xs={6}>
                                <RemoteAutoComplete
                                    error={errors && errors.firma}
                                    helperText={errors && errors.firma}
                                    size='small'
                                    label='Müşteri'
                                    labelField='marka'
                                    readOnly={!newFatura || fatura.faturaTuru == 'IADE'}
                                    results={firmalar}
                                    value={fatura.firma?.marka}
                                    onSelected={(firma) => {
                                        let f = { ...fatura }
                                        f.vfirma = firma.key
                                        f.firmaAdi = firma.marka
                                        setFatura(f)
                                    }}
                                    fetchRemote={(searchText) => {
                                        searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                                            setFirmalar(resp)
                                        })
                                    }}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    error={errors && errors.kesenFirma}
                                    helperText={errors && errors.kesenFirma}
                                    label='Fatura Kesen Firma'
                                    variant='outlined'
                                    fullWidth
                                    size='small'
                                    select
                                    //disabled={fatura.faturaTuru == 'IADE'}
                                    value={fatura.vkesenFirma}
                                    InputProps={{
                                        readOnly: !newFatura || fatura.faturaTuru == 'IADE'
                                    }}
                                    onChange={e => {
                                        let f = { ...fatura }
                                        f.vkesenFirma = e.target.value
                                        setFatura(f)
                                    }}
                                >
                                    {tenants.map((tenant, i) => {
                                        return <MenuItem key={tenant.key} value={tenant.key}>{tenant.adi}</MenuItem>
                                    })}
                                </TextField>
                            </Grid>
                            <Grid item xs={4}>
                                <MuiPickersUtilsProvider
                                    utils={DateFnsUtils} locale={trLocale}
                                >
                                    <KeyboardDatePicker
                                        error={errors && errors.duzenlemeTarihi}
                                        helperText={errors && errors.duzenlemeTarihi}
                                        autoOk
                                        readOnly={!newFatura}
                                        //disableFuture={false}
                                        minDate={newFatura ? moment().add(-7, 'days') : null}
                                        maxDate={newFatura ? moment() : null}
                                        //size='small'
                                        clearable
                                        fullWidth
                                        variant="inline"
                                        inputVariant="outlined"
                                        label="Fatura Düzenleme Tarihi"
                                        format="dd.MM.yyyy"
                                        InputAdornmentProps={{ position: 'start' }}
                                        size='small'
                                        value={fatura.duzenlemeTarihi}
                                        onChange={(e) => {
                                            let f = { ...fatura }
                                            f.duzenlemeTarihi = e
                                            setFatura(f)
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    error={errors && errors.vadeGun}
                                    helperText={errors && errors.vadeGun}
                                    label='Vade'
                                    variant='outlined'
                                    fullWidth
                                    size='small'
                                    select
                                    value={fatura.vadeGun}
                                    InputProps={{
                                        readOnly: !newFatura
                                    }}
                                    onChange={e => {
                                        onVade(e.target.value)
                                        //let f = { ...fatura }
                                        //f.vade = e.target.value
                                        //setFatura(f)
                                    }}
                                >
                                    {vadeler.map((vade, i) => {
                                        return <MenuItem key={vade} value={vade}>{vade}</MenuItem>
                                    })}
                                </TextField>
                            </Grid>
                            <Grid item xs={4}>
                                <MuiPickersUtilsProvider
                                    utils={DateFnsUtils} locale={trLocale}
                                >
                                    <KeyboardDatePicker
                                        error={errors && errors.vadeTarihi}
                                        helperText={errors && errors.vadeTarihi}
                                        size='small'
                                        autoOk
                                        fullWidth
                                        readOnly={!newFatura}
                                        minDate={newFatura ? moment() : null}
                                        //disablePast={true}
                                        //size='small'
                                        variant="inline"
                                        inputVariant="outlined"
                                        label="Vade Tarihi"
                                        format="dd.MM.yyyy"
                                        clearable
                                        InputAdornmentProps={{ position: 'start' }}
                                        value={fatura.vadeTarihi}
                                        onChange={(e) => {
                                            let f = { ...fatura }
                                            f.vadeTarihi = e
                                            setFatura(f)
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                            <Grid item xs={12}>
                                {errors && errors.kalemler && <div className='text-red-400'>Fatura Kalemleri Giriniz</div>}
                                <Fatura urunler={urunler} kalemler={kalemler} readOnly={!newFatura} stateRef={kalemlerRef} onChange={(fatura) => onFaturaChange(fatura)} />
                            </Grid>
                        </Grid>
                    </div>
                </CardContent>
            </Card>
        </div>
    )
}

export default FaturaInfo2
