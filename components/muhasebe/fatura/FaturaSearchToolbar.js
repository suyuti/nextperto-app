import React, {useState} from 'react'
import { Paper, Button, Grid, makeStyles, TextField, MenuItem, Accordion, AccordionSummary, Typography, AccordionDetails, Divider, AccordionActions } from '@material-ui/core'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import { useSelector } from 'react-redux';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import RemoteAutoComplete from '../../autocomplete'
import { searchFirmalar } from '../../../services/firma.service'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const initialQuery = {
    firma: null,
    kesenFirma: null,
    altTutar: 0,
    ustTutar: 0,
    baslangisTarihi: null,
    bitisTarihi: null,
    acik: null
}

const FaturaSearchToolbar = props => {
    const { disabled, tenants, onSearch } = props
    const classes = useStyles()
    const users = useSelector(state => state.auth.users)
    const today = new Date()
    const [query, setQuery] = useState({...initialQuery})
    const [results, setResults] = useState([])

    return <>
        <Accordion >
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1c-content"
                id="panel1c-header">
                <Typography>Filtre</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Grid container spacing={2}>
                    <Grid item xs={2}>
                    <RemoteAutoComplete
                            label='Firma'
                            size='small'
                            //error={errors?.firma}
                            results={results}
                            labelField='marka'
                            value={query.firma}
                            onSelected={(f) => {
                                let q = { ...query }
                                q.firma = f.key
                                setQuery(q)
                            }}
                            fetchRemote={(searchText) => {
                                searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                                    setResults(resp)
                                })
                            }}
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <TextField
                            //disabled={disabled}
                            fullWidth
                            label='Kesen Firma'
                            variant='outlined'
                            size='small'
                            value={query.kesenFirma}
                            onChange={(e) => {
                                let q = { ...query }
                                q.kesenFirma = e.target.value
                                setQuery(q)
                            }}
                            select
                        >
                            {tenants.map(t => <MenuItem key={t.key} value={t.key}>{t.adi}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={1}>
                        <TextField
                            //disabled={disabled}
                            fullWidth
                            label='Alt Tutar'
                            variant='outlined'
                            size='small'
                            value={query.altTutar}
                            onChange={(e) => {
                                let q = { ...query }
                                q.altTutar = e.target.value
                                setQuery(q)
                            }}
                        />
                    </Grid>
                    <Grid item xs={1}>
                        <TextField
                            //disabled={disabled}
                            fullWidth
                            label='Üst Tutar'
                            variant='outlined'
                            size='small'
                            value={query.ustTutar}
                            onChange={(e) => {
                                let q = { ...query }
                                q.ustTutar = e.target.value
                                setQuery(q)
                            }}
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                //error={errors && errors.sonTarih}
                                //helperText={errors && errors.sonTarih}
                                disabled={disabled}
                                clearable
                                autoOk
                                fullWidth
                                variant="inline"
                                inputVariant="outlined"
                                label="Vade Başlangıç Tarihi"
                                format="dd/MM/yyyy"
                                size='small'
                                InputAdornmentProps={{ position: 'start' }}
                                value={query.baslangisTarihi}
                                onChange={(e) => {
                                    let q = { ...query }
                                    q.baslangisTarihi = e
                                    setQuery(q)
                                }}
                                //value={gorev.sonTarih}
                            //onChange={(e) => onGorevChanged({ field: 'sonTarih', value: e })}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={2}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                //error={errors && errors.sonTarih}
                                //helperText={errors && errors.sonTarih}
                                disabled={disabled}
                                clearable
                                autoOk
                                fullWidth
                                variant="inline"
                                inputVariant="outlined"
                                label="Vade Bitiş Tarihi"
                                format="dd/MM/yyyy"
                                size='small'
                                InputAdornmentProps={{ position: 'start' }}
                                value={query.bitisTarihi}
                                onChange={(e) => {
                                    let q = { ...query }
                                    q.bitisTarihi = e
                                    setQuery(q)
                                }}
                            //value={gorev.sonTarih}
                            //onChange={(e) => onGorevChanged({ field: 'sonTarih', value: e })}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={1}>
                        <TextField
                            //disabled={disabled}
                            fullWidth
                            label='Ödenmiş'
                            variant='outlined'
                            size='small'
                            value={query.acik}
                            onChange={(e) => {
                                let q = { ...query }
                                q.acik = e.target.value
                                setQuery(q)
                            }}
                            select
                        >
                            <MenuItem key='tumu' value='tumu'>Tümü</MenuItem>
                            <MenuItem key='acik' value='acik'>Ödenmiş</MenuItem>
                            <MenuItem key='kapali' value='kapali'>Ödenmemiş</MenuItem>
                        </TextField>
                    </Grid>

                </Grid>
            </AccordionDetails>
            <Divider />
            <AccordionActions>
            <Button 
                    size='small' 
                    variant='contained' 
                    color='primary'
                    onClick={() => {
                        setQuery({...initialQuery})
                    }}>Temizle</Button>
                <Button 
                    size='small' 
                    variant='contained' 
                    color='primary'
                    onClick={() => {
                        onSearch(query)
                    }}>Sorgula</Button>
            </AccordionActions>
        </Accordion>
    </>
}

export default FaturaSearchToolbar