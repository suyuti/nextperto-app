import React, { useRef, useState } from 'react'
import { makeStyles, Card, CardHeader, Divider, CardContent, Grid, TextField, ButtonGroup, Button, MenuItem } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import FaturaKalemler from './FaturaKalemler'
import RemoteAutoComplete from '../../autocomplete';
import { searchFirmalar } from '../../../services/firma.service'
import moment from 'moment'
import { uuid } from 'uuidv4';
import trLocale from "date-fns/locale/tr";
import Fatura from '../fatura2';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(0)
    },
    kalemler: {
        width: '100%'
    }
}))

const FaturaInfo = props => {
    const classes = useStyles()
    const faturaRef = useRef({})
    const { fatura, onChange, errors, faturaKalemleri, newFatura, tenants, onPreview, urunler } = props
    const [kalemler, setKalemler] = useState(faturaKalemleri || [])
    const [firmalar, setFirmalar] = useState([])

    const onVade = (vade) => {
        let yeniVadeTarihi = moment(fatura.duzenlemeTarihi).add(vade, 'days')
        onChange([
            { field: 'vadeTarihi', value: yeniVadeTarihi.toDate() },
            { field: 'vade', value: vade }
        ])
    }

    return <>
        <div className={classes.root}>
            <Card>
                <CardHeader
                    title='Fatura Bilgileri'
                    action={
                        newFatura && <Button variant='outlined' color='secondary' onClick={onPreview}>Fatura Ön İzleme</Button>
                    }
                />
                <Divider />
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                error={errors && errors.aciklama}
                                helperText={errors && errors.aciklama}
                                //size='small'
                                label='Fatura Açıklaması'
                                variant='outlined'
                                fullWidth
                                rowsMax={Infinity}
                                multiline
                                rows={1}
                                value={fatura?.aciklama}
                                onChange={(e) => {
                                    onChange({ field: 'aciklama', value: e.target.value })
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <RemoteAutoComplete
                                error={errors && errors.firma}
                                helperText={errors && errors.firma}
                                //error={errors?.firma}
                                results={firmalar}
                                label='Fatura Kesilecek Firma'
                                labelField='marka'
                                value={fatura.firma?.marka}
                                onSelected={(f) => {
                                    onChange({ field: 'vfirma', value: f.key })
                                }}
                                fetchRemote={(searchText) => {
                                    searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                                        setFirmalar(resp)
                                    })
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                error={errors && errors.kesenFirma}
                                helperText={errors && errors.kesenFirma}
                                label='Fatura kesen firma'
                                variant='outlined'
                                fullWidth
                                value={fatura.vkesenFirma}
                                select
                                onChange={(e) => onChange({ field: 'vkesenFirma', value: e.target.value })}
                            >
                                {tenants.map(t => <MenuItem key={t.key} value={t.key}>{t.adi}</MenuItem>)}
                            </TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <MuiPickersUtilsProvider
                                utils={DateFnsUtils} locale={trLocale}
                            >
                                <KeyboardDatePicker
                                    error={errors && errors.duzenlemeTarihi}
                                    helperText={errors && errors.duzenlemeTarihi}
                                    autoOk
                                    //disableFuture={false}
                                    minDate={moment().add(-7, 'days')}
                                    maxDate={moment()}
                                    //size='small'
                                    clearable
                                    fullWidth
                                    variant="inline"
                                    inputVariant="outlined"
                                    label="Fatura Düzenleme Tarihi"
                                    format="dd.MM.yyyy"
                                    InputAdornmentProps={{ position: 'start' }}
                                    value={fatura.duzenlemeTarihi}
                                    onChange={(e) => onChange({ field: 'duzenlemeTarihi', value: e })}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item xs={4}>
                            <ButtonGroup
                            //size='small'
                            >
                                <Button variant={fatura.vade != 0 ? 'outlined' : 'contained'} color='primary' onClick={() => { onVade(0) }}>Aynı Gün</Button>
                                <Button variant={fatura.vade != 5 ? 'outlined' : 'contained'} color='primary' onClick={() => { onVade(5) }}>5 Gün</Button>
                                <Button variant={fatura.vade != 7 ? 'outlined' : 'contained'} color='primary' onClick={() => { onVade(20) }}>20 Gün</Button>
                                <Button variant={fatura.vade != 30 ? 'outlined' : 'contained'} color='primary' onClick={() => { onVade(30) }}>30 Gün</Button>
                            </ButtonGroup>
                        </Grid>
                        <Grid item xs={4}>
                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={trLocale}>
                                <KeyboardDatePicker
                                    error={errors && errors.vadeTarihi}
                                    helperText={errors && errors.vadeTarihi}
                                    autoOk
                                    fullWidth
                                    disablePast={true}
                                    //size='small'
                                    variant="inline"
                                    inputVariant="outlined"
                                    label="Vade Tarihi"
                                    format="dd.MM.yyyy"
                                    clearable
                                    InputAdornmentProps={{ position: 'start' }}
                                    value={fatura.vadeTarihi}
                                    onChange={(e) => onChange({ field: 'vadeTarihi', value: e })}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item xs={12}>
                            <Divider />
                        </Grid>
                        <Grid item xs={12}>
                            <Button variant='contained' color='secondary' onClick={() => {
                                alert(JSON.stringify(faturaRef.current))
                            }}>Test</Button>
                        </Grid>
                        <Grid item xs={12}>
                            <Fatura
                                stateRef={faturaRef}
                                urunler={urunler}
                                //readOnly
                                 />
                        </Grid>
                        <Grid item xs={12} >
                            <FaturaKalemler
                                data={kalemler}
                                urunler={urunler}
                                enableAdd={newFatura}
                                onChange={(e) => {
                                    let i = e.index
                                    let k = [...kalemler]
                                    k[i][e.field] = e.value
                                    setKalemler(k)
                                    onChange({ field: 'kalemler', value: k })
                                }}
                                onRemove={(i) => {
                                    let k = [...kalemler]
                                    k.splice(i, 1)
                                    setKalemler(k)
                                    onChange({ field: 'kalemler', value: k })
                                }}
                                addNew={() => {
                                    let k = [...kalemler]
                                    k.push({
                                        key: uuid(),
                                        urun: '',
                                        miktar: '',
                                        birimFiyat: '',
                                        toplam: ''
                                    })
                                    setKalemler(k)
                                    onChange({ field: 'kalemler', value: k })
                                }} />
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </div>
    </>
}

export default FaturaInfo