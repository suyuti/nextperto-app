import React, { useState, useEffect } from 'react'
import { IconButton, Card, CardContent, CardHeader, Divider, Collapse, Table, TableRow, TableHead, TableBody, TableCell, Box } from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import Faturalar from '../../../pages/muhasebe/fatura';
import {getFirmaFaturalar} from '../../../services/muhasebe.service'
import moment from 'moment'
import numbro from 'numbro';

const FirmaFaturalar = props => {
    const {firma} = props
    const [show, setShow] = useState(null)
    const [faturalar, setFaturalar] = useState([])

    const load = async () => {
        getFirmaFaturalar(firma).then(resp => {
            setFaturalar(resp.data)
        })
    }

    useEffect(() => {
        load()
    }, [])

    return <>
        <Card>
            <CardHeader
            title='Önceki Faturalar'
            subheader='Firmanın önceki faturaları'
                action={
                    <IconButton onClick={() => {
                        setShow(!show)
                    }}>
                        {show ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                    </IconButton>
                }
            />
            <Collapse in={show} timeout="auto" unmountOnExit>
                <Divider />
                <CardContent>
                    <Table size='small'>
                        <TableHead>
                            <TableRow>
                                <TableCell>Tarih</TableCell>
                                <TableCell align='right'>Toplam</TableCell>
                                <TableCell>Durum</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {faturalar.map(f => {
                                return <TableRow hover>
                                    <TableCell>{moment(f.vadeTarihi).format('DD.MM.YYYY')}</TableCell>
                                    <TableCell align='right'>{numbro(f.genelToplam).format({thousandSeparated: true, mantissa: 2})} TL</TableCell>
                                    <TableCell>{f.acik 
                                        ? <Box bgcolor="info.main" color="primary.contrastText" borderRadius={2} padding='2px' fontSize={12} >Tahsil Edildi</Box> 
                                        : <Box bgcolor="warning.main" color="primary.contrastText" borderRadius={2} padding='2px'  fontSize={12}>Tahsil Edilmedi</Box>}</TableCell>
                                </TableRow>
                            })}
                        </TableBody>
                    </Table>
                </CardContent>
            </Collapse>
        </Card>
    </>
}

export default FirmaFaturalar
