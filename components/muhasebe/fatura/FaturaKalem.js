import React, { useState } from 'react'
import { Grid, TextField, IconButton, MenuItem } from '@material-ui/core'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import MoneyInput from '../../../components/core/MoneyInput'

const FaturaKalem = props => {
    const { row, onChange, urunler, onRemove } = props
    const [selectedUrun, setSelectedUrun] = useState(null)

    return <>
        <Grid item xs={12} container spacing={1}>
            <Grid item xs={3}>
                <TextField
                    label='urun'
                    variant='outlined'
                    fullWidth
                    size='small'
                    //value={row.urun}
                    value={row.aciklama}
                    onChange={(e) => {
                        onChange([
                            { field: 'aciklama', value: e.target.value },
                            { field: 'birimFiyat', value: 1 }
                        ])
                    }}
                    select
                >
                    {urunler.map((urun, i) => {
                        return <MenuItem key={i} value={urun._id}>{urun.adi}</MenuItem>
                    })}
                </TextField>
            </Grid>
            <Grid item xs={3}>
                <TextField
                    variant='outlined'
                    fullWidth
                    size='small'
                    value={row.miktar}
                    onChange={(e) => onChange({ field: 'miktar', value: e.target.value })}
                />
            </Grid>
            <Grid item xs={3}>
                <TextField
                    variant='outlined'
                    size='small'
                    fullWidth
                    value={row.birimFiyat}
                    InputProps={{
                        inputComponent: MoneyInput
                    }}
                    onChange={(e) => onChange({ field: 'birimFiyat', value: e.target.value })}
                />
            </Grid>
            {/*
            <Grid item xs={1}>
                <TextField
                    variant='outlined'
                    size='small'
                    fullWidth
                    value={row.kdvOran}
                    onChange={(e) => onChange({field: 'kdvOran', value: e.target.value})}
                    select
                >
                    <MenuItem key={1} value={1}>1</MenuItem>
                    <MenuItem key={8} value={8}>8</MenuItem>
                    <MenuItem key={18} value={18}>18</MenuItem>
                </TextField>
            </Grid>
            <Grid item xs={1}>
                <TextField
                    variant='outlined'
                    size='small'
                    fullWidth
                    value={row.kdv}
                    onChange={(e) => onChange({field: 'kdv', value: e.target.value})}
                />
            </Grid>
            */}
            <Grid item xs={2}>
                <TextField
                    variant='outlined'
                    size='small'
                    fullWidth
                    value={row.toplam}
                    InputProps={{
                        inputComponent: MoneyInput
                    }}
                    onChange={(e) => onChange({ field: 'toplam', value: e.target.value })}
                />
            </Grid>
            <Grid item xs={1}>
                <IconButton onClick={(e) => {
                    onRemove()
                }}>
                    <DeleteOutlineIcon />
                </IconButton>
            </Grid>
        </Grid>
    </>
}

export default FaturaKalem