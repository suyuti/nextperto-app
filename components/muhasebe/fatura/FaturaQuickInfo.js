import React from 'react'
import { Card, Paper, makeStyles, Typography, LinearProgress, Button, Divider } from '@material-ui/core'
import numbro from 'numbro'
import moment from 'moment'

moment.locale('tr')

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2)
    },
    satir: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: theme.spacing(1)
    },
    satirTek: {
        marginTop: theme.spacing(1)
    },
    baslik: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: theme.spacing(1)
    }
}))

const FaturaQuickInfo = props => {
    const { fatura } = props
    const classes = useStyles()

    return <>
        <Paper className={classes.root}>
            <div className={classes.baslik}>
                <Typography variant='h6'>Fatura Özeti</Typography>
            </div>
            <Divider />
            <div className={classes.satir}>
                <Typography>Toplam</Typography>
                <Typography>{`${numbro(fatura.genelToplam).format({ thousandSeparated: true, mantissa: 2 })} TL`}</Typography>
            </div>
            <div className={classes.satir}>
                <Typography>Tahsilat</Typography>
                <Typography>{`${numbro(fatura.tahsilatTutari).format({ thousandSeparated: true, mantissa: 2 })} TL`}</Typography>
            </div>
            <div className={classes.satir}>
                <Typography>Kalan</Typography>
                <Typography>{`${numbro(fatura.genelToplam - fatura.tahsilatTutari).format({ thousandSeparated: true, mantissa: 2 })} TL`}</Typography>
            </div>
            {fatura.acik ?
                <div className={classes.satir}>
                    <Typography>Vade</Typography>
                    {moment().isAfter(fatura.vadeTarihi) ?
                        <Typography color='secondary'>{moment(fatura.vadeTarihi).fromNow()}</Typography>
                        :
                        <Typography>{moment(fatura.vadeTarihi).fromNow()}</Typography>
                    }
                </div>
                :
                <div className={classes.satir}>
                    <div></div>
                    <Typography>Tahsilat Yapıldı</Typography>
                </div>
            }
            <div className={classes.satirTek}>
                <LinearProgress variant="determinate" value={(fatura.tahsilatTutari / fatura.genelToplam) * 100} />
            </div>
            <div className={classes.satirTek}>
                <Button disabled={!fatura.acik} variant='outlined' color='primary' size='small'>Tahsilat İste</Button>
            </div>
        </Paper>
    </>
}

export default FaturaQuickInfo