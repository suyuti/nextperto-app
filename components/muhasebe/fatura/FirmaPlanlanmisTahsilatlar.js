import React, { useState, useEffect } from 'react'
import { IconButton, Card, CardContent, CardHeader, Divider, Collapse, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import {getFirmaTahsilatlar} from '../../../services/muhasebe.service'
import moment from 'moment'

const FirmaPlanlanmisTahsilatlar = props => {
    const {firma} = props
    const [show, setShow] = useState(null)
    const [tahsilatlar, setTahsilatlar] = useState([])

    const load = async () => {
        getFirmaTahsilatlar(firma).then(resp => {
            setTahsilatlar(resp.data)
        })
    }

    useEffect(() => {
        load()
    }, [])

    return <>
        <Card>
            <CardHeader
                title='Planlanmış Tahsilatlar'
                subheader='Firmanın planlanmış tahsilatları'
                action={
                    <IconButton onClick={() => {
                        setShow(!show)
                    }}>
                        {show ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                    </IconButton>
                }
            />
            <Collapse in={show} timeout="auto" unmountOnExit>
                <Divider />
                <CardContent>
                    <Table size='small'>
                        <TableHead>
                            <TableRow>
                                <TableCell>Tarih</TableCell>
                                <TableCell align='right'>Toplam</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tahsilatlar.map(t => {
                                return <TableRow hover>
                                    <TableCell>{moment(t.tahsilatTarihi).format('DD.MM.YYYY')}</TableCell>
                                    <TableCell align='right'>{t.tutar}</TableCell>
                                </TableRow>
                            })}
                        </TableBody>
                    </Table>
                </CardContent>
            </Collapse>
        </Card>
    </>
}

export default FirmaPlanlanmisTahsilatlar