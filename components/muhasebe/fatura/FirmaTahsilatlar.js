import React, { useState, useEffect } from 'react'
import { IconButton, Card, CardContent, CardHeader, Divider, Collapse, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import {getFirmaTahsilatlar} from '../../../services/muhasebe.service'
import moment from 'moment'
import numbro from 'numbro'

const FirmaTahsilatlar = props => {
    const {firma} = props
    const [show, setShow] = useState(null)
    const [tahsilatlar, setTahsilatlar] = useState([])

    const load = async () => {
        getFirmaTahsilatlar(firma).then(resp => {
            setTahsilatlar(resp.data)
        })
    }

    useEffect(() => {
        load()
    }, [])

    return <>
        <Card>
            <CardHeader
                title='Önceki Tahsilatlar'
                subheader='Firmanın önceki tahsilatları'
                action={
                    <IconButton onClick={() => {
                        setShow(!show)
                    }}>
                        {show ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                    </IconButton>
                }
            />
            <Collapse in={show} timeout="auto" unmountOnExit>
                <Divider />
                <CardContent>
                    <Table size='small'>
                        <TableHead>
                            <TableRow>
                                <TableCell>Tarih</TableCell>
                                <TableCell align='right'>Toplam</TableCell>
                                <TableCell>Tahsilat Türü</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tahsilatlar.map(t => {
                                return <TableRow hover>
                                    <TableCell>{moment(t.tahsilatTarihi).format('DD.MM.YYYY')}</TableCell>
                                    <TableCell align='right'>{`${numbro(t.tutar).format({thousandSeparated: true, mantissa: 2})} TL`}</TableCell>
                                    <TableCell>{t.tahsilatTuru}</TableCell>
                                </TableRow>
                            })}
                        </TableBody>
                    </Table>
                </CardContent>
            </Collapse>
        </Card>
    </>
}

export default FirmaTahsilatlar