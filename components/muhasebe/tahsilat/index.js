import React, { useState } from 'react'
import {
    Card, Typography,
    CardHeader, Divider,
    CardContent, Button,
    Grid, FormControlLabel,
    FormControl, FormLabel,
    Radio, RadioGroup,
    TextField, CardActions, MenuItem
} from '@material-ui/core'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import { validateTahsilat } from '../../../validators/tahsilat.validator';
import { tahsilatYap } from '../../../services/muhasebe.service';
const bankalar = require('../../../constants/bankalar.json')

const TahsilatYap = props => {
    const { onCancel, onOk, fatura } = props
    const [errors, setErrors] = useState(null)
    const [form, setForm] = useState({
        vfirma              : fatura.vfirma,
        tutar               : `${Math.round((fatura.genelToplam - fatura.tahsilatTutari) * 100) / 100}`,
        tahsilatTarihi      : new Date(),
        tahsilatTuru        : 'nakit',
        aciklama            : fatura.aciklama,
        vtahsilatYapanFirma : fatura.vkesenFirma
    })

    return <>
        <Card>
            <CardHeader
                title={<Typography variant='h6'>Tahsilat İşlemi</Typography>}
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <FormControl component="fieldset" >
                            <FormLabel component="legend">Tahsilat Türü</FormLabel>
                            <RadioGroup row aria-label="gender" name="gender1" onChange={e => {
                                let f = { ...form }
                                f.tahsilatTuru = e.target.value
                                setForm(f)
                            }} >
                                <FormControlLabel value="nakit" control={<Radio checked={form.tahsilatTuru == 'nakit'} />} label="Nakit" />
                                <FormControlLabel value="cek" control={<Radio checked={form.tahsilatTuru == 'cek'} />} label="Çek" />
                            </RadioGroup>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                error={errors && errors.tahsilatTarihi}
                                helperText={errors && errors.tahsilatTarihi}
                                autoOk
                                //size='small'
                                clearable
                                fullWidth
                                variant="inline"
                                inputVariant="outlined"
                                label="Tahsilat Tarihi"
                                format="dd.MM.yyyy"
                                InputAdornmentProps={{ position: 'start' }}
                                value={form.tahsilatTarihi}
                                onChange={e => {
                                    let f = { ...form }
                                    f.tahsilatTarihi = e
                                    setForm(f)
                                }}
                            //onChange={(e) => onChange({ field: 'duzenlemeTarihi', value: e })}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label='Hesap'
                            variant='outlined'
                            fullWidth
                            select
                        >
                            <MenuItem key='1' value='hesap1'>Hesap 1</MenuItem>
                        </TextField>
                    </Grid>

                    {form.tahsilatTuru == 'cek' &&
                        <>
                            <Grid item xs={12}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                        error={errors && errors.cekVadeTarihi}
                                        helperText={errors && errors.cekVadeTarihi}
                                        autoOk
                                        //size='small'
                                        clearable
                                        fullWidth
                                        variant="inline"
                                        inputVariant="outlined"
                                        label="Çek Vade Tarihi"
                                        format="dd.MM.yyyy"
                                        InputAdornmentProps={{ position: 'start' }}
                                        value={form.cekVadeTarihi}
                                        onChange={e => {
                                            let f = { ...form }
                                            f.cekVadeTarihi = e
                                            setForm(f)
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    error={errors && errors.banka}
                                    helperText={errors && errors.banka}
                                    label='Banka'
                                    variant='outlined'
                                    fullWidth
                                    value={form.cekBanka}
                                    onChange={e => {
                                        let f = { ...form }
                                        f.cekBanka = e.target.value
                                        setForm(f)
                                    }}
                                    select
                                >
                                    {bankalar.map(b => {
                                        return <MenuItem key={b.key} value={b.key}>{b.adi}</MenuItem>
                                    })}
                                </TextField>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    error={errors && errors.cekNo}
                                    helperText={errors && errors.cekNo}
                                    label='Çek No'
                                    variant='outlined'
                                    fullWidth
                                    value={form.cekNo}
                                    onChange={e => {
                                        let f = { ...form }
                                        f.cekNo = e.target.value
                                        setForm(f)
                                    }}
                                />
                            </Grid>
                        </>
                    }

                    <Grid item xs={12}>
                        <TextField
                            error={errors && errors.tutar}
                            helperText={errors && errors.tutar}
                            label='Tutar'
                            variant='outlined'
                            fullWidth
                            value={form.tutar}
                            onChange={e => {
                                let f = { ...form }
                                f.tutar = e.target.value
                                setForm(f)
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label='Açıklama'
                            variant='outlined'
                            fullWidth
                            multiline
                            rows={1}
                            rowsMax={Infinity}
                            value={form.aciklama}
                            onChange={e => {
                                let f = { ...form }
                                f.aciklama = e.target.value
                                setForm(f)
                            }}

                        />
                    </Grid>
                </Grid>
            </CardContent>
            <Divider />
            <CardActions>
                <Button variant='outlined' color='primary' onClick={onCancel}>Vazgeç</Button>
                <Button variant='outlined' color='primary' onClick={(e) => {
                    let _error = validateTahsilat(form)
                    if (_error) {
                        setErrors(_error)
                        return
                    }
                    let resp = tahsilatYap(form)
                    if (resp) {
                        onOk()
                    }
                }}>Tahsilat Yap</Button>
            </CardActions>
        </Card>
    </>
}

export default TahsilatYap