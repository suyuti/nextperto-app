import React, { useState } from 'react'
import { Card, CardContent, CardHeader, Divider, Grid, FormControl, FormLabel, RadioGroup, Radio, MenuItem, FormControlLabel, TextField } from '@material-ui/core'
import RemoteAutoComplete from '../../autocomplete'
import { searchFirmalar } from '../../../services/firma.service'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
const bankalar = require('../../../constants/bankalar.json')

const TahsilatInfo = props => {
    const { errors, onChange, tahsilat, tenants } = props
    //const [form, setForm] = useState({})
    const [results, setResults] = useState([])


    return <>
        <Card>
            <CardHeader
                title='Tahsilat Bilgileri'
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <RemoteAutoComplete
                            label='Tahsilat yapilacak firma'
                            error={errors?.firma}
                            results={results}
                            labelField='marka'
                            value={tahsilat.firma?.marka}
                            onSelected={(f) => {
                                onChange({ field: 'vfirma', value: f.key })
                            }}
                            fetchRemote={(searchText) => {
                                searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                                    setResults(resp)
                                })
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                            error={errors && errors.tahsilatYapanFirma}
                            helperText={errors && errors.tahsilatYapanFirma}
                            label='Alıcı Firma'
                            fullWidth
                            variant='outlined'
                            value={tahsilat.vtahsilatYapanFirma}
                            onChange={e => onChange({ field: 'vtahsilatYapanFirma', value: e.target.value })}
                            select
                        >
                            {tenants.map(t => <MenuItem key={t.key} value={t.key}>{t.marka}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={12}>
                        <FormControl component="fieldset" >
                            <FormLabel component="legend">Tahsilat Türü</FormLabel>
                            <RadioGroup
                                row
                                aria-label="gender"
                                name="gender1"
                                onChange={(e) => onChange({ field: 'tahsilatTuru', value: e.target.value })}
                            >
                                <FormControlLabel value="nakit" control={<Radio checked={tahsilat.tahsilatTuru == 'nakit'} />} label="Nakit" />
                                <FormControlLabel value="cek" control={<Radio checked={tahsilat.tahsilatTuru == 'cek'} />} label="Çek" />
                            </RadioGroup>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                error={errors && errors.tahsilatTarihi}
                                helperText={errors && errors.tahsilatTarihi}
                                autoOk
                                //size='small'
                                clearable
                                fullWidth
                                variant="inline"
                                inputVariant="outlined"
                                label="Tahsilat Tarihi"
                                format="dd.MM.yyyy"
                                InputAdornmentProps={{ position: 'start' }}
                                value={tahsilat.tahsilatTarihi}
                                onChange={e => onChange({ field: 'tahsilatTarihi', value: e })}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label='Hesap'
                            variant='outlined'
                            fullWidth
                            select
                            value={tahsilat.hesap}
                            onChange={e => onChange({ field: 'hesap', value: e.target.value })}
                        >
                            <MenuItem key='1' value='hesap1'>Hesap 1</MenuItem>
                        </TextField>
                    </Grid>

                    {tahsilat.tahsilatTuru == 'cek' &&
                        <>
                            <Grid item xs={12}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                        error={errors && errors.cekVadeTarihi}
                                        helperText={errors && errors.cekVadeTarihi}
                                        autoOk
                                        //size='small'
                                        clearable
                                        fullWidth
                                        variant="inline"
                                        inputVariant="outlined"
                                        label="Çek Vade Tarihi"
                                        format="dd.MM.yyyy"
                                        InputAdornmentProps={{ position: 'start' }}
                                        value={tahsilat.cekVadeTarihi}
                                    //onChange={e => {
                                    //    let f = { ...form }
                                    //    f.cekVadeTarihi = e
                                    //    setForm(f)
                                    //}}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    error={errors && errors.banka}
                                    helperText={errors && errors.banka}
                                    label='Banka'
                                    variant='outlined'
                                    fullWidth
                                    value={tahsilat.cekBanka}
                                    //onChange={e => {
                                    //    let f = { ...form }
                                    //    f.cekBanka = e.target.value
                                    //    setForm(f)
                                    //}}
                                    select
                                >
                                    {bankalar.map(b => {
                                        return <MenuItem key={b.key} value={b.key}>{b.adi}</MenuItem>
                                    })}
                                </TextField>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    error={errors && errors.cekNo}
                                    helperText={errors && errors.cekNo}
                                    label='Çek No'
                                    variant='outlined'
                                    fullWidth
                                    value={tahsilat.cekNo}
                                //onChange={e => {
                                //    let f = { ...form }
                                //    f.cekNo = e.target.value
                                //    setForm(f)
                                //}}
                                />
                            </Grid>
                        </>
                    }

                    <Grid item xs={12}>
                        <TextField
                            error={errors && errors.tutar}
                            helperText={errors && errors.tutar}
                            label='Tutar'
                            variant='outlined'
                            fullWidth
                            value={tahsilat.tutar}
                            onChange={e => onChange({ field: 'tutar', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label='Açıklama'
                            variant='outlined'
                            fullWidth
                            multiline
                            rows={2}
                            rowsMax={Infinity}
                            value={tahsilat.aciklama}
                            onChange={e => onChange({ field: 'aciklama', value: e.target.value })}
                        />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default TahsilatInfo