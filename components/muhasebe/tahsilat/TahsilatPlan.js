import { Card, CardContent, Button, CardHeader, Divider, Grid, MenuItem, TextField } from '@material-ui/core'
import React, { useState } from 'react'
import RemoteAutoComplete from '../../autocomplete'
import { searchFirmalar } from '../../../services/firma.service'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import trLocale from "date-fns/locale/tr";
import moment from 'moment'


const TahsilatPlan = props => {
    const { tahsilatPlani, isNew, errors, onChange, tenants } = props
    const [results, setResults] = useState([])


    const durumInfo = (plan) => {
        if (plan.durum == 'odendi') {
            return <div className='bg-green-400 px-4 py-1 text-white rounded shadow-md text-xl font-sans font-semibold'>Ödendi</div>
        }
        else if (plan.durum == 'ertelendi') {
            return <div className='bg-yellow-400 px-4 py-1 text-white rounded shadow-md text-xl font-sans font-semibold'>Ertelendi</div>
        }
        else if (plan.durum == 'acik') {
            if (moment().isAfter(plan.tarih)) {
                return <div className='bg-blue-400 px-4 py-1 text-white rounded shadow-md text-xl font-sans font-semibold'>Açık</div>
            }
            else {
                return <div className='bg-red-400 px-4 py-1 text-white rounded shadow-md text-xl font-sans font-semibold'>Gecikme</div>
            }
        }
    }

    return <>
        <Card>
            <CardHeader
                className={'flex flex-row items-center'}
                title='Tahsilat Planı'
                action={ !isNew && durumInfo(tahsilatPlani)}
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <RemoteAutoComplete
                            label='Tahsilat yapilacak firma'
                            error={errors?.firma}
                            results={results}
                            labelField='marka'
                            value={tahsilatPlani.firma?.marka}
                            onSelected={(f) => {
                                onChange({ field: 'vfirma', value: f.key })
                            }}
                            fetchRemote={(searchText) => {
                                searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                                    setResults(resp)
                                })
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            error={errors && errors.tahsilatYapanFirma}
                            helperText={errors && errors.tahsilatYapanFirma}
                            label='Alıcı Firma'
                            fullWidth
                            variant='outlined'
                            value={tahsilatPlani.vtahsilatYapanFirma}
                            onChange={e => onChange({ field: 'vtahsilatYapanFirma', value: e.target.value })}
                            select
                        >
                            {tenants.map(t => <MenuItem key={t.key} value={t.key}>{t.marka}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={12}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={trLocale}>
                            <KeyboardDatePicker
                                error={errors && errors.tarih}
                                helperText={errors && errors.tarih}
                                autoOk
                                disablePast={true}
                                //size='small'
                                clearable
                                fullWidth
                                variant="inline"
                                inputVariant="outlined"
                                label="Planlanan Tahsilat Tarihi"
                                format="dd.MM.yyyy"
                                InputAdornmentProps={{ position: 'start' }}
                                value={tahsilatPlani.tarih}
                                onChange={e => onChange({ field: 'tarih', value: e })}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            error={errors && errors.tutar}
                            helperText={errors && errors.tutar}
                            label='Tutar'
                            fullWidth
                            variant='outlined'
                            value={tahsilatPlani.tutar}
                            onChange={e => onChange({ field: 'tutar', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label='Açıklama'
                            fullWidth
                            variant='outlined'
                            multiline
                            rows={2}
                            rowsMax={Infinity}
                            value={tahsilatPlani.aciklama}
                            onChange={e => onChange({ field: 'aciklama', value: e.target.value })}
                        />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}

export default TahsilatPlan