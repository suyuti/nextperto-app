import React, {useState, useEffect} from 'react'
import { Card, Divider, IconButton, CardContent, CardHeader, Typography, Table, TableRow, TableHead, TableCell, TableBody, Button, CardActions } from '@material-ui/core'
import { bugunKesilecekFaturalar } from '../../../services/muhasebe.service'
import moment from 'moment'
import ReplayIcon from '@material-ui/icons/Replay';
import { useRouter } from 'next/router'
import numbro from 'numbro';

const BugunKesilecekFaturalar = props => {
    const [sonuc, setSonuc] = useState(null)
    const router = useRouter()

    const load = async () => {
        bugunKesilecekFaturalar().then(resp => {
            setSonuc(resp)
        })
    }

    useEffect(() => {
        load()
    }, [])

    return <>
        <Card className='border-t-4 border-green-600 '>
            <CardHeader
                title={<Typography variant='h6'>Bugün Kesilecek Faturalar</Typography>}
                action={<IconButton onClick={() => {
                    load()
                }}><ReplayIcon /> </IconButton>}
            />
            <Divider />
            <CardContent>
                <Table size='small'>
                    <TableHead>
                        <TableRow>
                            <TableCell>Firma</TableCell>
                            <TableCell>Fatura Açıklaması</TableCell>
                            <TableCell>Vade</TableCell>
                            <TableCell align='right'>Tutar</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {sonuc?.faturalar.map(t => {
                            return (
                                <TableRow hover>
                                    <TableCell>{t.marka}</TableCell>
                                    <TableCell>{t.aciklama}</TableCell>
                                    <TableCell>{moment(t.vadeTarihi).format('DD.MM.YYYY')}</TableCell>
                                    <TableCell align='right'>{numbro(t.toplam).format({thousandSeparated: true, mantissa: 2})} TL</TableCell>
                                    <TableCell><Button variant='outlined' color='primary' size='small' onClick={() => {
                                        router.push(`/muhasebe/fatura/new?aciklama=${t.aciklama}&vfirma=${t.vfirma}&marka=${t.marka}`)
                                    }}>Detay</Button></TableCell>
                                </TableRow>)
                        })}
                    </TableBody>
                </Table>
            </CardContent>
            <CardActions>
                <Button variant='outlined' size='small'><Typography variant='caption'>Devamı</Typography></Button>
            </CardActions>
        </Card>
    </>
}

export default BugunKesilecekFaturalar