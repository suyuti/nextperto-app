import React, { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import { Card, CardHeader, Divider, CardContent } from '@material-ui/core';
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })
import numbro from 'numbro'

const alacaklar2 = (props) => {
    const {options, data} = props
    //debugger
    //const [data, setData] = useState(props.data)
    
    const chart2Options = {
        chart: {
            toolbar: {
                show: false
            },
            sparkline: {
                enabled: true
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            color: '#f4772e',
            curve: 'smooth',
            width: 2
        },
        fill: {
            color: '#f4772e'
        },
        colors: ['#f4772e'],
        legend: {
            show: false
        },
        labels: data.data.map(d => d.label),
        //[
        //    'a',
        //    'b',
        //    '29 Mart - 08 Nisan',
        //    'd',
        //    'e',
        //    'f',
        //    'Mart 2021'
        //],
        xaxis: {
            crosshairs: {
                width: 1
            }
        },
        yaxis: {
            show: false,
            min: 0
        }
    };
    const chart2Data = [
        {
            name: 'Gider',
            data: [0, 0, 0, 0, 10, 0, 10, 0, 10, 0, 0, 0]
        }
    ];
    return (
        <Card className='border-t-4 border-indigo-700'>
            <CardHeader title={options?.title} />
            <Divider />
            <CardContent className=''>
                <div className=''>
                    <div className=' mb-2'>
                        <div className='flex flex-row  justify-between   p-1'>
                            <h2 className='text-lg font-sans antialiased font-light'>Toplam</h2>
                            <h2 className='text-lg font-sans antialiased font-medium'>{`${numbro(data?.toplam).format({thousandSeparated: true, mantissa: 2})} TL`}</h2>
                        </div>
                        <div className='flex flex-row justify-between   p-1'>
                            <h2 className='text-lg font-sans antialiased font-light'>Adet</h2>
                            <h2 className='text-lg font-sans antialiased font-medium'>{data?.adet}</h2>
                        </div>
                    </div>
                    <div className=' '>
                        <Chart
                            options={chart2Options}
                            series={data ? [{name: 'Tahsilat', data: data.data.map(d => d.value)}] : chart2Data}
                            //series={chart2Data}
                            type='bar'
                            height='150px'
                        //width='450px' 
                        />
                    </div>
                </div>
            </CardContent>
        </Card>
    )
}

export default alacaklar2
