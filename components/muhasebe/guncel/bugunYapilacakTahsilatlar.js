import React, { useEffect, useState } from 'react'
import { Card, Divider, CardContent, CardHeader, Typography, Table, TableRow, TableHead, TableCell, Button, TableBody, CardActions, IconButton } from '@material-ui/core'
import { bugunYapilacakTahsilatlar } from '../../../services/muhasebe.service'
import moment from 'moment'
import ReplayIcon from '@material-ui/icons/Replay';
import { useRouter } from 'next/router'
import { ChevronRight } from '@material-ui/icons';

const BugunYapilacakTahsilatlar = props => {
    const [tahsilatlar, setTahsilatlar] = useState([])
    const router = useRouter()

    const load = async () => {
        bugunYapilacakTahsilatlar().then(resp => {
            setTahsilatlar(resp)
        })
    }

    useEffect(() => {
        load()
    }, [])

    return <>
        <Card className='border-t-4 border-yellow-500 '>
            <CardHeader
                title={<Typography variant='h6'>Bugün Yapılacak Tahsilatlar</Typography>}
                action={<IconButton onClick={() => {
                    load()
                }}><ReplayIcon /> </IconButton>}
            />
            <Divider />
            <CardContent>
                <Table size='small'>
                    <TableHead>
                        <TableRow>
                            <TableCell>Firma</TableCell>
                            <TableCell>Fatura Açıklaması</TableCell>
                            <TableCell>Vade</TableCell>
                            <TableCell align='right'>Tutar</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {tahsilatlar.map(t => {
                            return (
                                <TableRow hover>
                                    <TableCell>{t.firma.marka}</TableCell>
                                    <TableCell>{t.aciklama}</TableCell>
                                    <TableCell>{moment(t.vadeTarihi).format('DD.MM.YYYY')}</TableCell>
                                    <TableCell align='right'>{t.genelToplam} TL</TableCell>
                                    <TableCell>
                                        <IconButton size='small' onClick={() => {
                                            router.push(`/muhasebe/fatura/${t._id}`)
                                        }}>
                                            <ChevronRight fontSize='small' />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>)
                        })}
                    </TableBody>
                </Table>
            </CardContent>
            <CardActions>
                <Button variant='outlined' size='small'><Typography variant='caption'>Devamı</Typography></Button>
            </CardActions>
        </Card>
    </>
}

export default BugunYapilacakTahsilatlar