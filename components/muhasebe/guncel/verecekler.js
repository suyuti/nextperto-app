import React, {useState, useEffect} from 'react'
import { Card, CardHeader, CardContent, Divider, IconButton } from '@material-ui/core'
import ReplayIcon from '@material-ui/icons/Replay';
import dynamic from 'next/dynamic'
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })
import {getOdemelerRapor} from '../../../services/stats.service'

const Verecekler = props => {
    const {tenant} = props
    const [options, setOptions] = useState({
        chart: {
            //width:'100px',
            //height: 350,
            type: 'bar',
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    position: 'top', // top, center, bottom
                },
            }
        },
        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val + " TL";
            },
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
    
        xaxis: {
            categories: ["Bu hafta", "1. Hafta", "2. Hafta", "3. Hafta", "4. Hafta", "5. Hafta"],
            position: 'top',
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false
            },
            crosshairs: {
                fill: {
                    type: 'gradient',
                    gradient: {
                        colorFrom: '#D8E3F0',
                        colorTo: '#BED1E6',
                        stops: [0, 100],
                        opacityFrom: 0.4,
                        opacityTo: 0.5,
                    }
                }
            },
            tooltip: {
                enabled: true,
            }
        },
        colors: ['#F90661'],
        yaxis: {
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false,
            },
            labels: {
                show: false,
                formatter: function (val) {
                    return val + " TL";
                }
            }
    
        },
        title: {
            text: 'Ödemeler',
            floating: true,
            offsetY: 330,
            align: 'center',
            style: {
                color: '#444'
            }
        }
    })
    const [series, setSeries] = useState([
    //    {name: 'Toplam', data: 10}
    ])
    const load = async (query) => {
        getOdemelerRapor(query).then(resp => {
            let respData = resp.map(r => r.toplam)
            for (let i = 0; i < 6 - resp.length; ++i) {
                respData.push(0)
            }

            let s = [{
                name: 'Toplam',
                data: respData
            }]
            setSeries(s)
        })
    }

    useEffect(() => {
        if (tenant.key=='ALL') {
            load('')
        }
        else {
            load(`vkesenFirma=${tenant.key}`)
        }
    }, [tenant])

    return <>
        <Card className='border-t-4 border-blue-400 '>
            <CardHeader
                title="Ödemeler"
                subheader={`Haftalık bazda yapılacak ödemeler - ${tenant.marka}`}
                action={<IconButton onClick={() => load()}><ReplayIcon /></IconButton>}
            />
            <Divider />
            <CardContent>
            <Chart 
                    options={options} 
                    series={series} 
                    type='bar' 
                    height='350px' 
                    //width='450px' 
                />
            </CardContent>
        </Card>
    </>
}
export default Verecekler