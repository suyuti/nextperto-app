import React, { useState, useEffect } from 'react'
import { Card, Divider, IconButton, CardContent, CardHeader, Typography, Table, TableRow, TableHead, TableCell, TableBody, Button, CardActions, Box } from '@material-ui/core'
import { gecikenTahsilatlar } from '../../../services/muhasebe.service'
import moment from 'moment'
import ReplayIcon from '@material-ui/icons/Replay';
import { useRouter } from 'next/router'
import { ChevronRight } from '@material-ui/icons';
moment.locale('tr')

const GecikenTahsilatlar = props => {
    const {tenant} = props
    const [sonuc, setSonuc] = useState(null)
    const router = useRouter()

    const load = async (query) => {
        gecikenTahsilatlar(query).then(resp => {
            setSonuc(resp)
        })
    }

    useEffect(() => {
        if (tenant.key=='ALL') {
            load('')
        }
        else {
            load(`vkesenFirma=${tenant.key}`)
        }
    }, [tenant])
    
    return <>
        <Card className='border-t-4 border-indigo-600 '>
            <CardHeader
                title={<Typography variant='h6'>Geciken Tahsilatlar</Typography>}
                action={<IconButton onClick={() => {
                    load()
                }}><ReplayIcon /> </IconButton>}
            />
            <Divider />
            <CardContent>
                <Table size='small'>
                    <TableHead>
                        <TableRow>
                            <TableCell>Firma</TableCell>
                            <TableCell>Vade</TableCell>
                            <TableCell>Gecikme</TableCell>
                            <TableCell align='right'>Tutar</TableCell>
                            <TableCell ></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {sonuc?.map(t => {
                            return (
                                <TableRow hover>
                                    <TableCell>{t.firma.marka}</TableCell>
                                    <TableCell>{moment(t.vadeTarihi).format('DD.MM.YYYY')}</TableCell>
                                    <TableCell >
                                        <Box bgcolor="error.main" color="primary.contrastText" padding='2px' textAlign='center' borderRadius={4}>
                                            {moment(t.vadeTarihi).fromNow()}
                                        </Box>
                                    </TableCell>
                                    <TableCell align='right'>{t.genelToplam - t.tahsilatTutari} TL</TableCell>
                                    <TableCell >
                                        <IconButton size='small' onClick={() => {
                                            router.push(`/muhasebe/fatura/${t._id}`)
                                        }}>
                                            <ChevronRight fontSize='small'/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>)
                        })}
                    </TableBody>
                </Table>
            </CardContent>
            <CardActions>
                <Button variant='outlined' size='small'><Typography variant='caption'>Devamı</Typography></Button>
            </CardActions>
        </Card>
    </>
}

export default GecikenTahsilatlar