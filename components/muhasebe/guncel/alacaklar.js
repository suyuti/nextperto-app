import React, {useEffect, useState} from 'react'
import { Card, CardHeader, CardContent, Divider, IconButton } from '@material-ui/core'
import dynamic from 'next/dynamic'
import { getAlacaklarRapor } from '../../../services/stats.service'
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })
import ReplayIcon from '@material-ui/icons/Replay';

const Alacaklar = props => {
    const {tenant} = props
    const [options, setOptions] = useState({
        chart: {
            //width:'100px',
            //height: 350,
            type: 'bar',
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    position: 'top', // top, center, bottom
                },
            }
        },
        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val + " TL";
            },
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
        colors: ['#06F99E'],
        xaxis: {
            categories: ["Bu hafta", "1. Hafta", "2. Hafta", "3. Hafta", "4. Hafta", "5. Hafta"],
            position: 'top',
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false
            },
            crosshairs: {
                fill: {
                    type: 'gradient',
                    gradient: {
                        colorFrom: '#D8E3F0',
                        colorTo: '#BED1E6',
                        stops: [0, 100],
                        opacityFrom: 0.4,
                        opacityTo: 0.5,
                    }
                }
            },
            tooltip: {
                enabled: true,
            }
        },
        yaxis: {
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false,
            },
            labels: {
                show: false,
                formatter: function (val) {
                    return val + " TL";
                }
            }
    
        },
        title: {
            text: 'Alacaklar',
            floating: true,
            offsetY: 330,
            align: 'center',
            style: {
                color: '#444'
            }
        }
    })
    const [series, setSeries] = useState([{toplam: 100, adet: 3}])

    const load = async (query) => {
        getAlacaklarRapor(query).then(resp => {
            let respData = resp.map(r => r.toplam)
            for (let i = 0; i < 6 - resp.length; ++i) {
                respData.push(0)
            }

            let s = [{
                name: 'Toplam',
                data: respData
            }]
            setSeries(s)
        })
    }
    useEffect(() => {
        if (tenant.key=='ALL') {
            load('')
        }
        else {
            load(`vkesenFirma=${tenant.key}`)
        }
    }, [tenant])

    return <>
        <Card className='border-t-4 border-red-400 '>
            <CardHeader 
                title="Alacaklar"
                subheader={`Haftalık bazda yapılacak tahsilatlar - ${tenant.marka}`}
                action={<IconButton onClick={() => load(`vkesenFirma=${tenant.key}`)}><ReplayIcon /></IconButton>}
            />
            <Divider />
            <CardContent>
                <Chart 
                    options={options} 
                    series={series} 
                    type='bar' 
                    height='350px' 
                    //width='450px' 
                />
            </CardContent>
        </Card>
    </>
}
export default Alacaklar