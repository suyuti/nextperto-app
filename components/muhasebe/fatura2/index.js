import { Button, Table, TableBody, TableRow, TableCell, TableHead, TextField, MenuItem, IconButton, Tooltip } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import MoneyInput from '../../../components/core/MoneyInput'
import { uuid } from 'uuidv4';
import numbro from 'numbro';

const Fatura = ({ urunler, kalemler, stateRef, onChange, readOnly = false,  ...props }) => {
    const [fatura, setFatura] = useState({
        araToplam: 0,
        toplamKdv: 0,
        genelToplam: 0,
        kalemler: []
    })

    useEffect(() => {
        if (kalemler) {
            let f = { ...fatura }
            f.kalemler = kalemler.map((k, i) => k)
            setFatura(f)
        }
    }, [])

    useEffect(() => {
        if (stateRef) {
            stateRef.current = fatura
        }
        onChange(fatura)
    }, [fatura])

    const remove = (index) => {
        let f = { ...fatura }
        f.kalemler.splice(index, 1)
        setFatura(f)
    }

    const faturaHesapla = (fatura) => {
        let genelToplam = 0 // kdvli
        let araToplam = 0 // kdvsiz
        let toplamKdv = 0
        for (let kalem of fatura.kalemler) {
            let _araToplam = kalem.miktar * kalem.birimFiyat
            let kdv = _araToplam * (kalem.kdvOran / 100)
            araToplam += _araToplam
            genelToplam += _araToplam + kdv
            toplamKdv += kdv
        }
        return { toplamKdv, araToplam, genelToplam }
    }


    const hesapla = (birim, adet, kdvOran) => {
        let _birim = parseInt(birim) || 0
        let _adet = parseInt(adet) || 0
        return _birim * _adet * (100 + kdvOran) / 100
    }

    return (
        <div className='flex flex-col w-full space-y-2'>
            <table >
                <colgroup>
                    <col width='25%'></col>
                    <col width='30%'></col>
                    <col width='10%'></col>
                    <col width='10%'></col>
                    <col width='5%'></col>
                    <col width='15%'></col>
                    <col width='5%'></col>
                </colgroup>
                <thead>
                    <tr className='bg-gray-200 text-black'>
                        <th className='text-lg font-light'>Ürün</th>
                        <th className='text-lg font-light'>Açıklama</th>
                        <th className='text-lg font-light'>Birim</th>
                        <th className='text-lg font-light'>Adet</th>
                        <th className='text-lg font-light'>KDV</th>
                        <th className='text-lg font-light'>Toplam</th>
                        <th className='text-lg font-light'></th>
                    </tr>
                </thead>
                <tbody>
                    {fatura.kalemler.map((k, i) => {
                        return (
                            <tr className=''>
                                <td className=''>
                                    <Tooltip title={fatura.kalemler[i].urunAdi}>
                                        <TextField
                                            fullWidth
                                            variant='outlined'
                                            margin='none'
                                            size='small'
                                            select
                                            value={k.vurun}
                                            onChange={e => {
                                                let urun = urunler.find(u => u.key == e.target.value)
                                                let toplam = hesapla(urun.birimFiyat, 1, urun.kdvOrani)
                                                let f = { ...fatura }
                                                f.kalemler[i].vurun = e.target.value
                                                f.kalemler[i].urunAdi = urun.adi
                                                f.kalemler[i].birimFiyat = urun.birimFiyat || 0
                                                f.kalemler[i].kdvOran = urun.kdvOrani || 18
                                                f.kalemler[i].miktar = 1
                                                f.kalemler[i].toplam = toplam

                                                let t = faturaHesapla(f)
                                                f.toplamKdv = t.toplamKdv
                                                f.araToplam = t.araToplam
                                                f.genelToplam = t.genelToplam

                                                setFatura(f)
                                            }}
                                            InputProps={{
                                                readOnly,
                                            }}
                                        >
                                            {urunler.map((urun, i) => {
                                                return <MenuItem key={urun.key} value={urun.key}>{urun.adi}</MenuItem>
                                            })}
                                        </TextField>
                                    </Tooltip>
                                </td>
                                <td component='tr' scope='row'>
                                    <Tooltip title={fatura.kalemler[i].aciklama}>
                                        <TextField
                                            margin='none'
                                            multiline
                                            rows={1}
                                            rowsMax={Infinity}
                                            size='small'
                                            variant='outlined'
                                            fullWidth
                                            value={fatura.kalemler[i].aciklama}
                                            onChange={e => {
                                                let f = { ...fatura }
                                                f.kalemler[i].aciklama = e.target.value
                                                setFatura(f)
                                            }}
                                            InputProps={{
                                                readOnly,
                                            }}
                                        />
                                    </Tooltip>
                                </td>
                                <td>
                                    <TextField
                                        margin='none'
                                        variant='outlined'
                                        size='small'
                                        fullWidth
                                        value={k.birimFiyat}
                                        onChange={e => {
                                            let toplam = hesapla(e.target.value, k.miktar, k.kdvOran)
                                            let f = { ...fatura }
                                            f.kalemler[i].birimFiyat = e.target.value
                                            f.kalemler[i].toplam = toplam

                                            let t = faturaHesapla(f)
                                            f.toplamKdv = t.toplamKdv
                                            f.araToplam = t.araToplam
                                            f.genelToplam = t.genelToplam

                                            setFatura(f)
                                        }}
                                        InputProps={{
                                            readOnly,
                                            inputComponent: MoneyInput
                                        }}
                                    />
                                </td>
                                <td>
                                    <TextField
                                        margin='none'
                                        size='small'
                                        variant='outlined'
                                        fullWidth
                                        value={k.miktar}
                                        onChange={e => {
                                            let toplam = hesapla(k.birimFiyat, e.target.value, k.kdvOran)
                                            let f = { ...fatura }
                                            f.kalemler[i].miktar = e.target.value
                                            f.kalemler[i].toplam = toplam

                                            let t = faturaHesapla(f)
                                            f.toplamKdv = t.toplamKdv
                                            f.araToplam = t.araToplam
                                            f.genelToplam = t.genelToplam

                                            setFatura(f)
                                        }}
                                        InputProps={{
                                            readOnly,
                                        }}
                                    />
                                </td>
                                <td>
                                    <TextField
                                        margin='none'
                                        size='small'
                                        variant='outlined'
                                        fullWidth
                                        value={k.kdvOran}
                                        onChange={e => {
                                            let toplam = hesapla(k.birimFiyat, k.miktar, e.target.value)
                                            let f = { ...fatura }
                                            f.kalemler[i].kdvOran = e.target.value
                                            f.kalemler[i].toplam = toplam

                                            let t = faturaHesapla(f)
                                            f.toplamKdv = t.toplamKdv
                                            f.araToplam = t.araToplam
                                            f.genelToplam = t.genelToplam

                                            setFatura(f)
                                        }}
                                        select
                                        InputProps={{
                                            readOnly,
                                        }}
                                    >
                                        <MenuItem key={0} value={0}>0</MenuItem>
                                        <MenuItem key={1} value={1}>1</MenuItem>
                                        <MenuItem key={8} value={8}>8</MenuItem>
                                        <MenuItem key={18} value={18}>18</MenuItem>
                                    </TextField>
                                </td>
                                <td>
                                    <TextField
                                        margin='none'
                                        size='small'
                                        variant='outlined'
                                        fullWidth
                                        value={k.toplam}
                                        InputProps={{
                                            readOnly: true,
                                            style: { textAlign: 'right' },
                                            inputComponent: MoneyInput
                                        }}
                                    />
                                </td>
                                <td
                                    style={{ maxWidth: '50px' }}
                                >
                                    <IconButton
                                        disabled={readOnly}
                                        onClick={() => {
                                            remove(i)
                                        }}>
                                        <DeleteOutlineIcon />
                                    </IconButton>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>

            <div className='self-end w-2/6  flex'>
                <table className='flex-grow'>
                    <colgroup>
                        <col width='50%'></col>
                        <col width='50%'></col>
                    </colgroup>
                    <tbody>
                        <tr>
                            <td className='text-gray-800 text-lg antialiased font-light font-sans'>Ara Toplam</td>
                            <td className='text-gray-800 text-lg antialiased font-light font-sans'>{numbro(fatura.araToplam).format({ thousandSeparated: true, mantissa: 2 })} TL</td>
                        </tr>
                        <tr>
                            <td className='text-gray-800 text-lg antialiased font-light font-sans'>Toplam KDV</td>
                            <td className='text-gray-800 text-lg antialiased font-light font-sans'>{numbro(fatura.toplamKdv).format({ thousandSeparated: true, mantissa: 2 })} TL</td>
                        </tr>
                        <tr>
                            <td className='text-gray-800 text-lg antialiased font-light font-sans'>Genel Toplam</td>
                            <td className='text-gray-800 text-lg antialiased font-light font-sans'>{numbro(fatura.genelToplam).format({ thousandSeparated: true, mantissa: 2 })} TL</td>
                        </tr>
                    </tbody>
                </table>
            </div>


            <div className='w-20'>
                <Button
                    disabled={readOnly}
                    variant='contained'
                    color='primary'
                    onClick={() => {
                        let f = { ...fatura }
                        f.kalemler.push({
                            key: uuid(),
                            urun: null,
                            aciklama: '',
                            birimFiyat: 0,
                            miktar: 0,
                            kdvOran: 0,
                            toplam: 0,
                            kdv: 0
                        })
                        setFatura(f)
                    }}
                >Ekle</Button>
            </div>
        </div>
    )
}

export default Fatura
