import { Card, CardHeader, CardContent, Divider, IconButton } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import numbro from 'numbro'
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })
import {getTenantNakitAkisiRapor} from '../../../services/tenant.service'
import ReplayIcon from '@material-ui/icons/Replay';

const TenantNakitAkisi = props => {
    const {tenant} = props
    const [series, setSeries] = useState([
        {
            name: 'Net',
            type: 'line',
            data: [0,0,0,0,0,0]
        },
        {
            name: 'Alacaklar',
            type: 'column',
            data: [0,0,0,0,0,0]
        },
        {
            name: 'Odemeler',
            type: 'column',
            data: [0,0,0,0,0,0]
        }
    ])
    const [options, setOptions] = useState({
        chart: {
            type: 'line',
            height: 440,
            stacked: true,
            toolbar: {
                show: false,
            },
        },
        colors: ['#FF4D40', '#4A9FFF', '#FFCC33'],
        plotOptions: {
            bar: {
                horizontal: false,
                barHeight: '80%',
                borderRadius: 7,
                radiusOnLastStackedBar: false
            },
        },
        dataLabels: {
            enabled: false
        },
        dataLabels: {
            enabled: false,
            enabledOnSeries: [1, 1]
        },
        stroke: {
            curve: 'smooth',
            width: [4, 1, 1],
            colors: ["#FF4D40", "#fff", "#fff"]
        },
        markers: {
            size: 5,
        },

        grid: {
            yaxis: {
                lines: {
                    show: true
                }
            }
        },
        tooltip: {
            shared: false,
            x: {
                formatter: function (val) {
                    return val
                }
            },
            y: {
                formatter: function (val) {
                    return Math.abs(val)
                }
            }
        },
        xaxis: {
            categories: ['85+', '80-84', '75-79', '70-74', '65-69', '60-64', '55-59', '50-54',
                '45-49', '40-44', '35-39', '30-34', '25-29', '20-24', '15-19', '10-14', '5-9',
                '0-4'
            ],
            title: {
                text: 'Hafta'
            },
            labels: {
                formatter: function (val) {
                    return Math.abs(Math.round(val))
                }
            }
        },
    })
    const [toplamAlacaklar, setToplamAlacaklar] = useState(0)
    const [toplamOdemeler, setToplamOdemeler] = useState(0)
    const [canShow, setCanShow] = useState(false)

    const load = async (tenant) => {
        getTenantNakitAkisiRapor(tenant._id).then(resp => {
            setToplamAlacaklar(resp.toplamAlacak)
            setToplamOdemeler(resp.toplamOdeme)
            //let _s = Object.assign({}, series)// [...series]
            let _s = [...series]
            _s[1].data = resp.alacaklar.map(a => a.toplam)
            _s[0].data = resp.netler
            setSeries(_s)
            setCanShow(true)
        })
    }

    useEffect(() => {
        load(tenant)
    }, [])

    return <>
        <Card>
            <CardHeader
                title={tenant.marka}
                action={<IconButton onClick={() => load(tenant)}><ReplayIcon /></IconButton>}
            />
            <Divider />
            <CardContent>
                <div className=''>
                    <div className='flex justify-between px-2'>
                        <div className='text-lg antialiased'>Alacaklar</div>
                        <div className='text-2xl text-blue-800 antialiased font-semibold'>{numbro(toplamAlacaklar).format({thousandSeparated: true, mantissa: 2})}<span className='text-sm font-thin ml-1'>TL</span></div>
                    </div>
                    <div className='flex justify-between px-2'>
                        <div className='text-lg antialiased'>Ödemeler</div>
                        <div className='text-2xl text-red-400 antialiased font-semibold'>{numbro(toplamOdemeler).format({thousandSeparated: true, mantissa: 2})}<span className='text-sm font-thin ml-1'>TL</span></div>
                    </div>
                    <div className='pt-4'>
                        {canShow &&
                        <Chart options={options} series={series} type="line" height={440} />
                        }
                    </div>
                </div>
            </CardContent>
        </Card>
    </>
}

export default TenantNakitAkisi