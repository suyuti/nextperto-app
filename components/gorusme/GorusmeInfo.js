import React, { useState } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete';
import {
    Card, CardHeader, CardContent,
    Divider, Button, IconButton,
    Grid, TextField, MenuItem,
    Typography, FormControl,
    ListItem, ListItemAvatar, ListItemText, Avatar,
    FormControlLabel, Switch,
    makeStyles
} from '@material-ui/core'
import { searchFirmalar, searchFirmaKisiler } from '../../services/firma.service'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import EventIcon from '@material-ui/icons/Event';
import RemoteAutoComplete from '../autocomplete'
import { useSelector } from 'react-redux'
const saatler = require('../../constants/saatler.json')


const GorusmeInfo = props => {
    const { errors, gorusme, onChange, kisiler } = props
    const [results, setResults] = useState([])
    const [resultsKisiler, setResultsKisiler] = useState([])
    const users = useSelector(state => state.auth.users)
    const [icGorusmeKisiler, setIcGorusmeKisiler] = useState(users.map(u => { return { key: u.key, adi: u.username, soyadi: '', image: u.image } }))

    console.log(gorusme)
    return <>
        <Card>
            <CardHeader
                title={<Typography variant='h6'>Görüşme Bilgileri</Typography>}
                avatar={<EventIcon />}
            />
            <Divider />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            error={errors && errors.not}
                            helperText={errors && errors.not}
                            label='Konu'
                            variant='outlined'
                            fullWidth
                            multiline
                            rows={2}
                            rowsMax={Infinity}
                            value={gorusme.not}
                            onChange={(e) => onChange({ field: 'not', value: e.target.value })}
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <FormControl component="fieldset">
                            <FormControlLabel
                                //value={firma.faal}
                                control={
                                    <Switch
                                        color="primary"
                                        checked={gorusme.icGorusmeMi}
                                        onChange={(e) => {
                                            onChange([
                                                { field: 'icGorusmeMi', value: e.target.checked },
                                                { field: 'vkisi', value: '' }
                                            ])
                                            //onChange()
                                        }} />}
                                label={gorusme.icGorusmeMi ? 'İç Görüşme' : "Müşteri ile Görüşme"}
                                labelPlacement="top"
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={9}>
                        <RemoteAutoComplete
                            disabled={gorusme.icGorusmeMi}
                            error={errors?.firma}
                            label='Firma'
                            results={results}
                            labelField='marka'
                            value={gorusme.firma?.marka}
                            onSelected={(f) => {
                                // TODO Cancel oldugunda f undefined geliyor. FIX ME
                                onChange({ field: 'vfirma', value: { key: f.key, _id: f._id } })
                            }}
                            fetchRemote={(searchText) => {
                                searchFirmalar(`sort=marka&marka=/${searchText}/i`).then(resp => {
                                    setResults(resp)
                                })
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Autocomplete
                            key={gorusme.icGorusmeMi}
                            value={gorusme.icGorusmeMi ?
                                icGorusmeKisiler.find(p => p.key == gorusme.vkisi)
                                : kisiler.find(p => p.key == gorusme.vkisi)}
                            options={gorusme.icGorusmeMi ? icGorusmeKisiler : kisiler}
                            getOptionLabel={opt => `${opt.adi} ${opt.soyadi}`}
                            renderOption={(option, state) => {
                                return <ListItem>
                                    <ListItemAvatar>
                                        <Avatar src={option.image} />
                                    </ListItemAvatar>
                                    <ListItemText primary={option.adi} />
                                </ListItem>
                            }}
                            renderInput={params => <TextField
                                error={errors && errors.kisi}
                                helperText={errors && errors.kisi}
                                {...params} label='Görüşme Yapılan Kişi' variant='outlined' />}
                            onChange={(e, v, r) => {
                                if (v) {
                                    onChange({ field: 'vkisi', value: v.key })
                                }
                            }}
                        />
                    </Grid>

                    <Grid item xs={4}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                error={errors && errors.tarih}
                                helperText={errors && errors.tarih}
                                clearable
                                autoOk
                                fullWidth
                                variant="inline"
                                inputVariant="outlined"
                                label="Görüşme Tarihi"
                                format="dd/MM/yyyy"
                                InputAdornmentProps={{ position: 'start' }}
                                value={gorusme.tarih}
                                onChange={(e) => onChange({ field: 'tarih', value: e })}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={3}>
                        <TextField
                            //error={errors && errors.baslik}
                            //helperText={errors && errors.baslik}
                            label='Saat'
                            variant='outlined'
                            fullWidth
                            value={gorusme.saat}
                            onChange={(e) => onChange({ field: 'saat', value: e.target.value })}
                            select
                        >
                            {saatler.map(s => <MenuItem key={s} value={s}>{s}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={5}>
                        <TextField
                            error={errors && errors.kanal}
                            helperText={errors && errors.kanal}
                            label='Kanal'
                            variant='outlined'
                            fullWidth
                            value={gorusme.kanal}
                            onChange={(e) => onChange({ field: 'kanal', value: e.target.value })}
                            select
                        >
                            <MenuItem key='Telefon' value='Telefon'>Telefon</MenuItem>
                            <MenuItem key='Mail' value='Mail'>Mail</MenuItem>
                            <MenuItem key='Yüzyüze' value='Yüzyüze'>Yüzyüze</MenuItem>
                            <MenuItem key='VideoKonferans' value='VideoKonferans'>Video Konferans</MenuItem>
                        </TextField>
                    </Grid>


                </Grid>
            </CardContent>
        </Card>
    </>
}

export default GorusmeInfo