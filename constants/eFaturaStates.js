export const YENI_KAYIT                = -99

export const ALINDI                    = 1
export const ISLENEMEDI                = 2
export const ISLENDI                   = 3

export const GIB_GONDERIM_IPTAL        = -2
export const GIB_GONDERIM_KUYRUKTA     = -1
export const GIB_GONDERILEMEDI         = 0
export const GIB_GONDERILECEK          = 1
export const GIB_GONDERILDI            = 2
export const GIB_YANITLADI             = 3

export const ALICI_YANITLADI           = 4
export const ALICI_YANITI_GEREKMIYOR   = -1
export const ALICI_YANITI_BEKLENIYOR   = 0
export const ALICI_REDDETTI            = 1
export const ALICI_KABUL_ETTI          = 2
