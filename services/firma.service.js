import api from '../services/axios.service'
import { parseCookies } from 'nookies'

export const getFirmalar = async () => {
    const cookies = parseCookies()

    let client = await api.get(`/firma`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data.data
    }
}

export const getFirma = async (id) => {
    const cookies = parseCookies()

    let client = await api.get(`/firma/${id}`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}

export const saveFirma = async (firma) => {
    const cookies = parseCookies()

    let client = await api.post(`/firma`, firma, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}

export const updateFirma = async (id, firma) => {
    const cookies = parseCookies()

    let client = await api.put(`/firma/${id}`, firma, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data.data
    }
}


export const getFirmaCalisanKisiler = async (firma) => {
    if (!firma) {
        return []
    }
    const cookies = parseCookies()

    let client = await api.get(`/firma/${firma}/kisi?active=true`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        let kisiler = client.data.data.data.map(k => {
            return {
                key: k.key,
                adi: `${k.adi} ${k.soyadi}`,
                unvani: `${k.unvani}`,
                mail: k.mail,
                _id: k._id
            }
        })
        return kisiler
    }
}

export const getFirmaTumKisiler = async (firma) => {
    const cookies = parseCookies()

    let client = await api.get(`/firma/${firma}/kisi`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data.data
    }
}

export const searchFirmalar = async (query) => {
    const cookies = parseCookies()

    let client = await api.get(`/firma?${query}`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data.data
    }
}

export const searchFirmaKisiler = async (query) => {
    const cookies = parseCookies()

    let client = await api.get(`/kisi?${query}`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data.data
    }
}

export const getPlanlanmisTahsilatlar = async (firmaKey) => {
    const cookies = parseCookies()
    let client = await api.get(`/planlanmistahsilat?vfirma=${firmaKey}`, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data.data
    }
}

export const getFaturalarToplam = async (firma) => {
    const cookies = parseCookies()
    let client = await api.get(`/firma/${firma}/fatura/toplam`, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const getTahsilatlarToplam = async (firma) => {
    const cookies = parseCookies()
    let client = await api.get(`/firma/${firma}/tahsilat/toplam`, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}
