import api from '../services/axios.service'
import { parseCookies } from 'nookies'

export const getGorev = async (gorevId) => {
    const cookies = parseCookies()

    let client = await api.get(`/gorev/${gorevId}?populate=toplanti.baslik,firma.marka`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

// TODO yukarıdaki değiştirilecek
export const getGorev2 = async (query) => {
    const cookies = parseCookies()

    let client = await api.get(`/gorev?${query}`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const getGorevYorumlari = async (id) => {
    const cookies = parseCookies()

    let client = await api.get(`/gorev/${id}/yorum`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const postGorevYorum = async (id, yorum) => {
    const cookies = parseCookies()

    let client = await api.post(`/gorev/${id}/yorum`,
        yorum,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const gorevTamamla = async (id, tamamlaDto) => {
    const cookies = parseCookies()

    let client = await api.post(`/gorev/${id}/tamamla`,
        tamamlaDto,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}
