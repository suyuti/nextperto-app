import api from './axios.service'
import { parseCookies } from 'nookies'
import moment from 'moment'

export const getFatura = async (faturaId, query) => {
    const cookies = parseCookies()
    let client = await api.get(`/fatura/${faturaId}?${query}`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const getFaturaKalemler = async (faturaId) => {
    const cookies = parseCookies()
    let client = await api.get(`/fatura/${faturaId}/kalem`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data.data
    }
}

export const saveFatura = async (fatura) => {
    const cookies = parseCookies()
    let client = await api.post(`/fatura`, fatura, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const getTopluFatura = async (topluFaturaId) => {
    const cookies = parseCookies()
    let client = await api.get(`/toplufatura/${topluFaturaId}?populate=kesenFirma,liste.firma,liste.urun`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const saveTopluFatura = async (toplufatura) => {
    const cookies = parseCookies()
    let client = await api.post(`/toplufatura`, toplufatura, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const updateTopluFatura = async (id, toplufatura) => {
    const cookies = parseCookies()
    let client = await api.put(`/toplufatura/${id}`, toplufatura, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const deleteTopluFatura = async (id) => {
    const cookies = parseCookies()
    let client = await api.delete(`/toplufatura/${id}`, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}


export const getFirmaFaturalar = async (firmaKey) => {
    const cookies = parseCookies()

    let client = await api.get(`/fatura?vfirma=${firmaKey}&select=vadeTarihi,genelToplam,acik&sort=-vadeTarihi`, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const getFirmaTahsilatlar = async (firmaKey) => {
    const cookies = parseCookies()

    let client = await api.get(`/tahsilat?vfirma=${firmaKey}&select=tahsilatTarihi,tutar,tahsilatTuru&sort=-tahsilatTarihi`, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const tahsilatYap = async (tahsilat) => {
    const cookies = parseCookies()
    let client = await api.post(`/tahsilat`, tahsilat, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const bugunYapilacakTahsilatlar = async () => {
    let start= new Date()
    let end = new Date()
    end.setDate(start.getDate() + 1)

    start.setHours(0,0,0,0)
    end.setHours(0,0,0,0)


    const cookies = parseCookies()
    let client = await api.get(`/fatura?vadeTarihi>=${start}&vadeTarihi<=${end}&acik=${true}&select=vfirma,aciklama,vadeTarihi,genelToplam&populate=firma.marka`, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data.data
    }
}

export const bugunKesilecekFaturalar = async () => {
    let today= new Date()


    const cookies = parseCookies()
    let client = await api.post(`/stats/gunfaturalar`, {gun: today.getDate()},
    {
        headers: { Authorization: `Bearer ${cookies.experto}` }
    })
    if (client.status == 200) {
        return client.data.data
    }
}

export const getFaturaBelge = async (faturaId) => {
    const cookies = parseCookies()

    let client = await api.get(`/fatura/${faturaId}/belge`,
        {
            responseType: 'blob',
            headers: { Authorization: `Bearer ${cookies.experto}` }
        })
    if (client.status == 200) {
        const file = new Blob([client.data], { type: 'application/pdf' });
        const fileUrl = URL.createObjectURL(file);
        window.open(fileUrl);
    }
}

export const getPlanlanmisTahsilat = async (id) => {
    const cookies = parseCookies()
    let client = await api.get(`/planlanmistahsilat/${id}?populate=firma.marka`, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const savePlanlanmisTahsilat = async (tahsilat) => {
    const cookies = parseCookies()
    let client = await api.post(`/planlanmistahsilat`, tahsilat, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const updatePlanlanmisTahsilat = async (tahsilat) => {
    const cookies = parseCookies()
    let client = await api.put(`/planlanmistahsilat/${tahsilat._id}`, tahsilat, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const gecikenTahsilatlar = async (query) => {
    const cookies = parseCookies()
    let client = await api.get(`/muhasebe/tahsilat?vadeTarihi<"${moment().format("MM.DD.YYYY")}"&acik=${true}&select=vfirma,vadeTarihi,acik,tahsilatTutari,genelToplam&populate=firma.marka&sort=vadeTarihi&${query}`, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data.data
    }
}


export const faturayiYenidenGonder = async (faturaId) => {
    const cookies = parseCookies()
    let client = await api.post(`/fatura/${faturaId}/yenidengonder`, {}, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const getRaporFatura = async (kesenFirmalar) => {
    const cookies = parseCookies()
    let client = await api.post(`/muhasebe/rapor/fatura`, kesenFirmalar, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const getRaporUrun = async (faturaId) => {
    const cookies = parseCookies()
    let client = await api.get(`/muhasebe/rapor/urun`, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const getRaporAylikUrun = async (faturaId) => {
    const cookies = parseCookies()
    let client = await api.get(`/muhasebe/rapor/urun/aylik`, {
        headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const getFaturalar = async (query) => {
    const cookies = parseCookies()
    let client = await api.get(`/fatura?${query}`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data.data
    }
}

export const getGelenFatura = async (faturaId) => {
    const cookies = parseCookies()
    let client = await api.get(`/gelenfatura/${faturaId}?populate=aliciFirma`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const getGelenFaturaBelge = async (faturaId) => {
    const cookies = parseCookies()

    let client = await api.get(`/gelenfatura/${faturaId}/belge`,
        {
            responseType: 'blob',
            headers: { Authorization: `Bearer ${cookies.experto}` }
        })
    if (client.status == 200) {
        const file = new Blob([client.data], { type: 'application/pdf' });
        const fileUrl = URL.createObjectURL(file);
        window.open(fileUrl);
    }
}

// Tedarikci
export const getTedarikci = async (id) => {
    const cookies = parseCookies()
    let client = await api.get(`/tedarikci/${id}`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

// Odeme
export const getOdemeler = async (query) => {
    const cookies = parseCookies()
    let url = query ? `/odeme?${query}` : `/odeme`
    let client = await api.get(url,  
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const getOdeme = async (id) => {
    const cookies = parseCookies()
    let client = await api.get(`/odeme/${id}`,  
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const odemeYap = async (odeme) => {
    const cookies = parseCookies()
    let client = await api.post(`/odeme`, odeme, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

// Odeme Plan

export const odemePlanla = async (plan) => {
    const cookies = parseCookies()
    let client = await api.post(`/odemeplan`, plan, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const getPlanlanmisOdemeler = async (query) => {
    const cookies = parseCookies()
    let url = query ? `/odemeplan?${query}` : `/odemeplan`
    let client = await api.get(url,  
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return client.data.data
    }
}

export const searchFatura = async (query) => {
    const cookies = parseCookies()

    let client = await api.get(`/fatura?${query}`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data.data
    }
}

export const faturayiTekrarGonder = async (faturaId) => {
    const cookies = parseCookies()

    let client = await api.post(`/fatura/${faturaId}/tekrargonder`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data.data
    }
}
