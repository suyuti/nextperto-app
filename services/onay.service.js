import api from './axios.service'
import { parseCookies } from 'nookies'

export const onayla = async (id) => {
    const cookies = parseCookies()

    let client = await api.put(`/onay/${id}/onay`,
        {},
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const reddet = async (id) => {
    const cookies = parseCookies()

    let client = await api.put(`/onay/${id}/red`,
        {},
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const iptalet = async (id) => {
    const cookies = parseCookies()

    let client = await api.put(`/onay/${id}/iptal`,
        {},
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const getOnayForItem = async (rid) => {
    const cookies = parseCookies()

    let client = await api.get(`/onay?referenceItem=${rid}&onayDurumu=acik`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const getOnay = async (id) => {
    const cookies = parseCookies()

    let client = await api.get(`/onay/${id}`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}