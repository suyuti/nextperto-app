import api from '../services/axios.service'
import { parseCookies } from 'nookies'

export const getSektorler = async () => {
    const cookies = parseCookies()

    let client = await api.get(`/sektor`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data.data
    }
}
