import { msalApp, GRAPH_REQUESTS } from "../lib/authentication";
import api from './axios.service';
import axios from 'axios'
const _ = require('lodash')

const authApp = new msalApp()

//--------------------------------------------------------------------------------------------

export const msLogin = async () => {
    let client = await authApp.app.acquireTokenSilent(Object.assign({}, GRAPH_REQUESTS.LOGIN, { forceRefresh: false }))

    if (client.error) {
        throw 'MS Login Error'
    }
    return { oid: client.uniqueId, msToken: client.accessToken, displayName: client.account.name }
}
//--------------------------------------------------------------------------------------------

export const expertoLogin = async ({ oid }) => {
    let client = await api.post(`/auth/login`, { oid })
    if (client.status == 201) {
        return { data: client.data.data }
    }
    else {
        throw 'Experto Login Error'
    }
}

//--------------------------------------------------------------------------------------------

export const getUsersFromExperto = async (token) => {
    let client = await api.get(`/user`, {
        headers: { Authorization: `Bearer ${token}` }
    })
    if (client.status == 200) {
        return { data: client.data.data.data }
    }
    else {
        throw 'Experto Login Error'
    }
}

//--------------------------------------------------------------------------------------------

export const getUsersPhotoFromMS = async (users, msToken) => {
    let requests = users.map(u => {
        return {
            id: u.oid,
            method: 'GET',
            url: `/users/${u.oid}/photo/$value`
        }
    })
    // current user icin de photo alinir. Current user da listeye eklenir
    //requests.push({
    //    id: msResponse.oid,
    //    method: 'GET',
    //    url: `/users/${msResponse.oid}/photos/48x48/$value`
    //})

    // BATCH islem limiti 10'dur.
    let batches = _.chunk(requests, 10)
    let results = []

    for (let batch of batches) {
        let client = await axios.post(`https://graph.microsoft.com/v1.0/$batch`,
            { requests: batch },
            {
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${msToken}`
                }
            })
        if (client.status == 200) {
            results = _.concat(results, client.data.responses)
        }
    }
    return results

}

//--------------------------------------------------------------------------------------------

export const fullLogin = () => {
    return new Promise((resolve, reject) => {
        msLogin().then(msLoginResp => {
            expertoLogin(msLoginResp).then(expLoginResp => {
                getUsersFromExperto(expLoginResp.data.token).then(userResp => {
                    getUsersPhotoFromMS(userResp.data, msLoginResp).then(respPhoto => {
                        let users = userResp.data.map(u => {
                            let image = respPhoto.find(p => p.id == u.oid && p.status == 200)
                            return {
                                _id: u._id,
                                oid: u.oid,
                                username: u.username,
                                image: image ? `data:image/jpeg;charset=utf-8;base64, ${image.body}` : null,
                                key: u.key,
                                active: u.active
                            }
                        })

                        // current user photo da gelen listede var.
                        let image = respPhoto.find(p => p.id == msLoginResp.oid && p.status == 200)
                        let currentUser = {
                            ...expLoginResp.data.user,
                            oid: msLoginResp.oid,
                            displayName: msLoginResp.displayName,
                            image: image ? `data:image/jpeg;charset=utf-8;base64, ${image.body}` : null
                        }

                        resolve({
                            msToken: msLoginResp.msToken,
                            expertoToken: expLoginResp.data.token,
                            oid: msLoginResp.oid,
                            user: currentUser,
                            users: users,
                            permissions: expLoginResp.data.permissions
                        })
                    }
                    )
                })
            })
        })
    })
}
