import axios from 'axios'
const API_URL = process.env.API_URL
let instance = axios.create({
    //baseURL: "https://nextperto-api-aatrw.ondigitalocean.app/api/v1"//API_URL
    //baseURL: "http://localhost:3001/api/v1" //API_URL
    baseURL: process.env.API_URL
})

export default instance
