import api from '../services/axios.service'
import { parseCookies } from 'nookies'

export const eFaturaOnIzleme = async (fatura) => {
    const cookies = parseCookies()

    let client = await api.post(`/qnb/onizleme`, fatura, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}

export const gidenBelgeIndir = async (fatura) => {
    const cookies = parseCookies()

    let client = await api.get(`/fatura/${fatura._id}/belgeindir`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}