import api from '../services/axios.service'
import { parseCookies } from 'nookies'

export const getTenants = async () => {
    const cookies = parseCookies()

    let client = await api.get(`/tenant`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data.data
    }
}

export const getTenantNakitAkisiRapor = async (tenant) => {
    const cookies = parseCookies()

    let client = await api.get(`/tenant/${tenant}/nakitakisi`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const getTenantToplamlar = async (tenant) => {
    const cookies = parseCookies()

    let client = await api.get(`/tenant/${tenant}/toplamlar`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const gidenBelgeleriSorgula = async (tenant) => {
    const cookies = parseCookies()

    let client = await api.get(`/tenant/${tenant}/gidenbelgelerisorgula`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const getGerceklesenTahsilatlar = async (tenant) => {
    const cookies = parseCookies()
    let client = await api.get(`/tenant/${tenant}/tahsilat/gerceklesen`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}
export const getGelecekTahsilatlar = async (tenant) => {
    const cookies = parseCookies()
    let client = await api.get(`/tenant/${tenant}/tahsilat/gelecek`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}
export const getGecikenTahsilatlar = async (tenant) => {
    const cookies = parseCookies()
    let client = await api.get(`/tenant/${tenant}/tahsilat/geciken`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}
export const getPlanlananTahsilatlar = async (tenant) => {
    const cookies = parseCookies()
    let client = await api.get(`/tenant/${tenant}/tahsilat/planlanan`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}