import api from '../services/axios.service'
import { parseCookies } from 'nookies'

export const getKisiler = async (query) => {
    const cookies = parseCookies()

    let client = await api.get(`/kisi?${query}`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data.data
    }
}

export const getKisi = async (id) => {
    const cookies = parseCookies()

    let client = await api.get(`/kisi/${id}?populate=firma.marka`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}

export const saveKisi = async (kisi) => {
    const cookies = parseCookies()

    let client = await api.post(`/kisi`, kisi, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}

export const updateKisi = async (id, kisi) => {
    const cookies = parseCookies()

    let client = await api.put(`/kisi/${id}`, kisi,
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}
