import api from './axios.service'
import { parseCookies } from 'nookies'

export const getGorevStats = async (query) => {
    const cookies = parseCookies()
    
    let client = await api.get(`/stats/gorev?${query}`, 
    {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}

export const getBugunYapilacakTahsilatlarToplami = async (query) => {
    const cookies = parseCookies()
    
    let client = await api.get(`/stats/tahsilat?${query}`, 
    {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }    
}

export const getGelecekTahsilatlar = async (query) => {
    const cookies = parseCookies()
    
    let client = await api.get(`/stats/gelecektahsilatlar?${query}`, 
    {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
    
}

export const getTahsilatlarRapor = async (query) => {
    const cookies = parseCookies()
    
    let client = await api.get(`/stats/tahsilatrapor`, 
    {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
    
}

export const getAlacaklarRapor = async (query) => {
    const cookies = parseCookies()
    
    let client = await api.get(`/stats/alacaklar?${query}`, 
    {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }    
}

export const getOdemelerRapor = async (query) => {
    const cookies = parseCookies()
    /*
    let client = await api.get(`/stats/odemeler?${query}`, 
    {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
    */
   return []
}

export const getGunlukRapor = async () => {
    const cookies = parseCookies()

    let client = await api.get(`/stats/gunlukrapor`,
        {
            responseType: 'blob',
            headers: { Authorization: `Bearer ${cookies.experto}` }
        })
    if (client.status == 200) {
        const file = new Blob([client.data], { type: 'application/pdf' });
        const fileUrl = URL.createObjectURL(file);
        window.open(fileUrl);
    }
}