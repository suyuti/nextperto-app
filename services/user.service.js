const apiService = require('./axios.service')
import { parseCookies } from 'nookies'

//-------------------------------------------------------------------------------------
// Aktif kullanicinin işlem yapabileceği kişiler veya ekipler listesi
// 
// [{label: '', isUSer: Boolean, isDepartman: Boolean, Icon: ''}]
//

export const getAvailableUsers = async (userId) => {
    const cookies = parseCookies()
    return api.get(`/user/${userId}/availableusers`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
}

//-------------------------------------------------------------------------------------
// Aktif kullanicinin işlem yapabileceği ekipler listesi

export const getAvailableDepartmans = async () => {

}

//-------------------------------------------------------------------------------------
// Aktif kullaniciya bagli kisiler

export const getMyTeam = async () => {

}

//-------------------------------------------------------------------------------------
// Aktif kullanicinin yoneticisi 

export const getMyManager = async () => {

}


