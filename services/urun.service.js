import api from '../services/axios.service'
import { parseCookies } from 'nookies'

export const getUrunler = async () => {
    const cookies = parseCookies()

    let client = await api.get(`/urun`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data.data
    }
}

export const getUrun = async (id) => {
    const cookies = parseCookies()

    let client = await api.get(`/urun/${id}`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}

export const saveUrun = async (urun) => {
    const cookies = parseCookies()

    let client = await api.post(`/urun`, urun, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}

export const updateUrun = async (id, urun) => {
    const cookies = parseCookies()

    let client = await api.put(`/urun/${id}`, urun, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data.data
    }
}

