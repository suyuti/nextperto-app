import api from './axios.service'
import { parseCookies } from 'nookies'

export const saveToplanti = async (toplanti) => {
    const cookies = parseCookies()

    let client = await api.post(`/toplanti`,
        toplanti,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data.data
    }
}

export const updateToplanti = async (toplanti) => {
    const cookies = parseCookies()

    let client = await api.put(`/toplanti/${toplanti._id}`,
        toplanti,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data.data
    }
}

export const getToplantiKararlari = async (toplantiId) => {
    const cookies = parseCookies()

    let client = await api.get(`/toplanti/${toplantiId}/karar`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data.data
    }
}

export const saveToplantiKararlari = async (toplantiId, kararlar) => {
    const cookies = parseCookies()
    let resp = await api.post(`/toplanti/${toplantiId}/karar`,
        kararlar,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })

    if (resp.status == 200) {
        return resp.data.data
    }
}

export const getToplantiGorevleri = async (toplantiId) => {
    const cookies = parseCookies()
    let client = await api.get(`/toplanti/${toplantiId}/gorev`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data.data
    }
}

export const toplantiTamamla = async (toplantiId, toplantiTamamlamaDto) => {
    /*
    toplantiTamamlamaDto:
        kapatan
        kapatmaTarihi
        status
        statusAciklama
    */
    const cookies = parseCookies()

    let client = await api.post(`/toplanti/${toplantiId}/tamamla`,
        toplantiTamamlamaDto,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const getOrtakTakvimEvents = async (kisiler, baslangicTarihi, bitisTarihi) => {
    const cookies = parseCookies()

    let client = await api.post(`/ortaktakvim`,
        {
            kisiler,
            baslangicTarihi,
            bitisTarihi
        },
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}


export const getToplanti = async (query) => {
    const cookies = parseCookies()

    let client = await api.get(`/toplanti?${query}`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
        if (client.status == 200) {
        return client.data.data.data
    }
}

export const getToplantiById = async (id) => {
    const cookies = parseCookies()

    let client = await api.get(`/toplanti/${id}`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const getToplantiRapor = async (id) => {
    const cookies = parseCookies()

    let client = await api.get(`/toplanti/${id}/pdfrapor`,
        {
            responseType: 'blob',
            headers: { Authorization: `Bearer ${cookies.experto}` }
        })
    if (client.status == 200) {
        const file = new Blob([client.data], { type: 'application/pdf' });
        const fileUrl = URL.createObjectURL(file);
        window.open(fileUrl);

        //return client.data.data
    }
}

export const getToplantiYapilmamisFirmalar = async (id) => {
    const cookies = parseCookies()

    let client = await api.get(`/toplantirapor/toplantiYapilmamisFirmalar`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}

export const getToplantiYapilmisFirmalar = async (id) => {
    const cookies = parseCookies()

    let client = await api.get(`/toplantirapor/toplantiYapilmisFirmalar`,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}
