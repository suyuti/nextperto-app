import api from '../services/axios.service'
import { parseCookies } from 'nookies'

export const getGorusme = async (gorusmeId) => {
    const cookies = parseCookies()

    let client = await api.get(`/gorusme/${gorusmeId}?populate=firma.marka,kisi`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}

export const saveGorusme = async (gorusme) => {
    const cookies = parseCookies()

    let client = await api.post(`/gorusme`,
        gorusme,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data.data
    }
}

export const updateGorusme = async (gorusme) => {
    const cookies = parseCookies()

    let client = await api.put(`/gorusme/${gorusme._id}`,
        gorusme,
        { headers: { Authorization: `Bearer ${cookies.experto}` } })
    if (client.status == 200) {
        return client.data.data
    }
}
