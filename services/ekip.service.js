import api from '../services/axios.service'
import { parseCookies } from 'nookies'

export const getEkibim = async (user) => {
    const cookies = parseCookies()

    let client = await api.get(`/user/${user._id}/ekip`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}

export const getEkip = async (id) => {
    const cookies = parseCookies()

    let client = await api.get(`/ekip/${id}`, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}

export const saveEkip = async (ekip) => {
    const cookies = parseCookies()

    let client = await api.post(`/ekip`, ekip,
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}

export const updateEkip = async (id, ekip) => {
    const cookies = parseCookies()

    let client = await api.put(`/ekip/${id}`, ekip, 
        {headers: { Authorization: `Bearer ${cookies.experto}` }})
    if (client.status == 200) {
        return  client.data.data
    }
}
