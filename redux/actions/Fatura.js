import { GET_MUSTERILER_SUC,
  NEW_TOPLU_FATURA,
  SET_TOPLU_FATURA,
  UPDATE_TOPLU_FATURA,
  ADD_TOPLU_FATURA_LIST_ITEM,
  CHANGE_TOPLU_FATURA_LIST_ITEM,
  REMOVE_TOPLU_FATURA_LIST_ITEM,
} from '../../constants/ActionTypes'

export const GetMusteriler = () => {
  return {
    type: GET_MUSTERILER_SUC,
    payload:
      [
        { id: 1, marka: 'Marka 1' },
        { id: 12, marka: 'Marka 12' },
        { id: 13, marka: 'Marka 13' },
        { id: 14, marka: 'Marka 14' },
        { id: 15, marka: 'Marka 15' },
        { id: 16, marka: 'Marka 16' },
      ]

  }
};

export const newTopluFatura = (item) => {
  return {type: NEW_TOPLU_FATURA, item}
}

export const setTopluFatura = (payload) => {
  return {type: SET_TOPLU_FATURA, payload}
}

export const updateTopluFatura = (key, value) => {
  return {type: UPDATE_TOPLU_FATURA, key, value}
}

export const addTopluFaturaListItem = (item) => {
  return {type: ADD_TOPLU_FATURA_LIST_ITEM, item}
}

export const changeTopluFaturaListItem = (index, key, val) => {
  return {type: CHANGE_TOPLU_FATURA_LIST_ITEM, index, key, val}
}

export const removeTopluFaturaListItem = (index) => {
  return {type: REMOVE_TOPLU_FATURA_LIST_ITEM, index: index}
}
