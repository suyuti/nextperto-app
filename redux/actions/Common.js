import {FETCH_ERROR, FETCH_START, FETCH_SUCCESS, 
  HIDE_MESSAGE, SHOW_MESSAGE, 
  SET_TOPLANTI_RIGHT_PANEL_TYPE, HIDE_TOPLANTI_RIGHT_PANEL,
  SHOW_MODAL_DIALOG, HIDE_MODAL_DIALOG, SELECT_MODAL_DIALOG,
  SET_PREV_PAGE,
  SET_PREV_URL, SET_PREV_FILTER,
  SET_FILTER, RESET_FILTER,
  SET_PAGEHEADER_BACK, RESET_PAGEHEADER_BACK
} from "../../constants/ActionTypes";

export const fetchStart = () => {
  return {
    type: FETCH_START
  }
};

export const fetchSuccess = () => {
  return {
    type: FETCH_SUCCESS
  }
};

export const fetchError = (error) => {
  return {
    type: FETCH_ERROR,
    payload: error
  }
};

export const showMessage = (message) => {
  return {
    type: SHOW_MESSAGE,
    payload: message
  }
};

export const hideMessage = () => {
  return {
    type: HIDE_MESSAGE
  }
};

export const setToplantiRightPanelType = (type) => {
  return {
    type: SET_TOPLANTI_RIGHT_PANEL_TYPE,
    payload: type
  }
};

export const hideToplantiRightPanel = () => {
  return {
    type: HIDE_TOPLANTI_RIGHT_PANEL,

  }
};

export const showModalDialog = ({text, option1, option2, data}) => {
  debugger
  return {
    type: SHOW_MODAL_DIALOG,
    payload: {text, option1, option2, data}
  }
}

export const selectModalDialog = (option) => {
  return {
    type: SELECT_MODAL_DIALOG,
    payload: {option}
  }
}

export const hideModalDialog = () => {
  return {
    type: HIDE_MODAL_DIALOG
  }
}


export const setPrevPage = (status) => {
  return {
    type: SET_PREV_PAGE,
    payload: status
  }
}

export const setPrevUrl = (url) => {
  return {
    type: SET_PREV_URL,
    payload: url
  }
}

export const setFilter = (filter) => {
  return {
    type: SET_FILTER,
    payload: filter
  }
}

export const resetFilter = (filter) => {
  return {
    type: RESET_FILTER,
    payload: filter
  }
}

export const setPageHeaderBack = () => {
  return {
    type: SET_PAGEHEADER_BACK,
  }
}

export const resetPageHeaderBack = () => {
  return {
    type: RESET_PAGEHEADER_BACK,
  }
}
