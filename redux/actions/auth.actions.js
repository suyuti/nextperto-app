import {fullLogin, msLogin, expertoLogin, getUsersFromExperto, getUsersPhotoFromMS} from '../../services/auth.service'
import {getAvailableUsers} from '../actions/account.actions'
const ekipService = require('../../services/ekip.service')

export const LOGIN_REQ = 'LOGIN REQ'
export const LOGIN_ERR = 'LOGIN ERR'
export const LOGIN_SUC = 'LOGIN SUC'

export const MS_LOGIN_REQ = 'MS LOGIN REQ'
export const MS_LOGIN_ERR = 'MS LOGIN ERR'
export const MS_LOGIN_SUC = 'MS LOGIN SUC'

export const EXP_LOGIN_REQ = 'EXP LOGIN REQ'
export const EXP_LOGIN_ERR = 'EXP LOGIN ERR'
export const EXP_LOGIN_SUC = 'EXP LOGIN SUC'

export const GET_USERS_REQ = 'GET USERS REQ'
export const GET_USERS_ERR = 'GET USERS ERR'
export const GET_USERS_SUC = 'GET USERS SUC'

export const GET_USERSPHOTO_REQ = 'GET USERS PHOTO REQ'
export const GET_USERSPHOTO_ERR = 'GET USERS PHOTO ERR'
export const GET_USERSPHOTO_SUC = 'GET USERS PHOTO SUC'

export const GET_EKIBIM_REQ = "GET EKIBIM REQ"
export const GET_EKIBIM_ERR = "GET EKIBIM ERR"
export const GET_EKIBIM_SUC = "GET EKIBIM SUC"


export const CHANGE_ME = 'CHANGE ME'

export const login2 = dispatch => {
    let client = fullLogin()
    return dispatch => {
        client.then(resp => {
            dispatch({type: LOGIN_SUC, payload: resp})
            dispatch(getAvailableUsers(resp._id))
        })
    }
}

export const changeMe = payload => {
    return {type: CHANGE_ME, payload: payload }
}


export const login_1 = dispatch => {
    let client = msLogin()
    return dispatch => {
        client.then(msLoginResp => {
            dispatch({type: MS_LOGIN_SUC, payload: msLoginResp})
            dispatch(login_2(msLoginResp))
        })
        .catch(e =>
            {
                //alert(e)
            }
        )
    }
}

export const login_2 = (msLoginResp) => {
    let client = expertoLogin(msLoginResp)
    return dispatch => {
        client.then(expLoginResp => {
            dispatch({type: EXP_LOGIN_SUC, payload: expLoginResp})
            dispatch(login_3(expLoginResp.data.token, msLoginResp.msToken))
            dispatch(getEkip(expLoginResp.data.user))
        })
    }
}

export const login_3 = (expLoginResp, msToken) => {
    let client = getUsersFromExperto(expLoginResp)
    return dispatch => {
        client.then(resp => {
            dispatch({type: GET_USERS_SUC, payload: resp})
            dispatch(login_4(resp.data, msToken))
        })
    }
}

export const login_4 = (users, msLoginResp) => {
    let client = getUsersPhotoFromMS(users, msLoginResp)
    return dispatch => {
        client.then(resp => {
            dispatch({type: GET_USERSPHOTO_SUC, payload: resp})
            //dispatch(getEkip())
        })
    }
}


export const getEkip = user => {
    const client = ekipService.getEkibim(user)
    return dispatch => {
        client.then(resp => {
            dispatch({
                type: GET_EKIBIM_SUC, data: resp
            })
        })
    }
}