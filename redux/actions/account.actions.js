import api from "../../services/axios.service";
import cookieCutter from 'cookie-cutter'

const userService = require('../../services/user.service')
const ekipService = require('../../services/ekip.service')

export const GET_PERSONELLER_REQ = "GET PERSONELLER REQ"
export const GET_PERSONELLER_ERR = "GET PERSONELLER ERR"
export const GET_PERSONELLER_SUC = "GET PERSONELLER SUC"

export const GET_AVAILABLE_USERS_REQ = "GET AVAILABLE USERS REQ"
export const GET_AVAILABLE_USERS_ERR = "GET AVAILABLE USERS ERR"
export const GET_AVAILABLE_USERS_SUC = "GET AVAILABLE USERS SUC"



export const getPersoneller = () => {
    let token = cookieCutter.get('experto')
    let client = api.get(`/user`, {
        headers: { Authorization: `Bearer ${token}` }
    })
    return dispatch => {
        dispatch({ type: GET_PERSONELLER_REQ })
        client.then(resp => {
            if (resp.status == 200) {
                dispatch({ type: GET_PERSONELLER_SUC, payload: resp.data.data.data })
            }
        })
            .catch(error => {
                dispatch({ type: GET_PERSONELLER_ERR, error: error })
            })
    }
}


export const getAvailableUsers = userId => {
    const client = userService.getAvailableUsers(userId)
    return dispatch => {
        dispatch({
            type: GET_AVAILABLE_USERS_SUC, users: client.data
        })
    }
}
