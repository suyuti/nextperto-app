import Router from "next/router";
import {
  HIDE_MESSAGE,
  INIT_URL,
  ON_HIDE_LOADER,
  ON_SHOW_LOADER,
  SHOW_MESSAGE,
  SIGNIN_MICROSOFT_USER,
  SIGNIN_MICROSOFT_USER_SUCCESS,
  SIGNIN_FACEBOOK_USER,
  SIGNIN_FACEBOOK_USER_SUCCESS,
  SIGNIN_GITHUB_USER,
  SIGNIN_GITHUB_USER_SUCCESS,
  SIGNIN_GOOGLE_USER,
  SIGNIN_GOOGLE_USER_SUCCESS,
  SIGNIN_TWITTER_USER,
  SIGNIN_TWITTER_USER_SUCCESS,
  SIGNIN_USER,
  SIGNIN_USER_SUCCESS,
  SIGNOUT_USER,
  SIGNOUT_USER_SUCCESS,
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS,
  SIGNIN_EXPERTO_USER,
  SIGNIN_EXPERTO_USER_SUCCESS,
  SILENT_LOGIN_SUC,
  SILENT_LOGIN_ERR,
  LOGOUT,
  EXPERTO_LOGIN_SUC,
  EXPERTO_SESSION_SUC,
  EXPERTO_SESSION_REQ,
  EXPERTO_LOGIN_REQ,
  EXPERTO_LOGIN_ERR,
  EXPERTO_SESSION_ERR,
  SET_PERMISSIONS
} from "../../constants/ActionTypes";
import { msalApp, GRAPH_REQUESTS } from "../../lib/authentication";
import api from "../../services/axios.service";
import { getPermissions } from '../../helpers/auth.helper'
import cookieCutter from 'cookie-cutter'
import {getPersoneller} from './account.actions'

const authApp = new msalApp()

export const silentLogin = dispatch => {
  let cli = authApp.app.acquireTokenSilent(Object.assign({}, GRAPH_REQUESTS.LOGIN, { forceRefresh: false }))

  return dispatch => {
    cli.then(token => {
      console.log('SILENT LOGIN')
      if (token.error) {
        dispatch({ type: SILENT_LOGIN_ERR, error: token.error })
      }
      const { name, oid } = token.idTokenClaims
      const email = token.account.userName
      dispatch({ type: SILENT_LOGIN_SUC, oid: oid, name: name, accessToken: token.accessToken })

      dispatch({ type: EXPERTO_SESSION_REQ })
      dispatch(expertoRefreshToken(oid, email))
    })
      .catch(error => {
        console.log('SILENT LOGIN ERROR')
        console.log(error)
        dispatch({ type: SILENT_LOGIN_ERR, error: error })
      })
  }
}

export const logout = dispatch => {
  let cli = authApp.app.logout()
  return dispatch => {
    dispatch({ type: LOGOUT })
  }
}

export const login = dispatch => {
  try {
    let cli = authApp.app.loginPopup(GRAPH_REQUESTS.LOGIN)
    return dispatch => {
      cli.then(token => {
        console.log('LOGIN')
        if (token.error == null) {
          const { name, oid } = token.idTokenClaims
          const email = token.account.userName
          //console.log(token)
  
          dispatch({ type: SILENT_LOGIN_SUC, oid: oid, name: name })
          dispatch({ type: EXPERTO_LOGIN_REQ })
          dispatch(expertoLogin({ oid: oid, displayName: name, email: email, userName: token.account.userName }))
        }
        else {
          dispatch({ type: SILENT_LOGIN_ERR, error: token.error })
        }
      })
        .catch(error => {
          dispatch({ type: SILENT_LOGIN_ERR, error: error })
        })
    }
  }
  catch(e) {
    console.log(e)
  }
}

export const expertoLogin = ({ oid, email, userName, displayName }) => {
  let cli = api.post(`/auth/login`,
    { oid, email, displayName, userName },
    { headers: { "Content-type": 'application/json' } })
  //dispatch({ type: EXPERTO_LOGIN_REQ })

  return dispatch => {
    cli.then(resp => {
      if (resp.status == 201) {
        dispatch({ type: EXPERTO_SESSION_SUC, payload: resp.data })
      }
      else {
        dispatch({ type: EXPERTO_LOGIN_ERR, message: error })
      }
    })
      .catch(error => {
        dispatch({ type: EXPERTO_LOGIN_ERR, message: error })
      })
  }
}

export const expertoRefreshToken = (oid, email) => {
  //const [cookie, setCookie] = useCookies(["user"])
  let cli = api.post(`/auth/session`,
    { oid: oid, email: email },
    { headers: { "Content-type": 'application/json' } })

  return dispatch => {
    cli.then(resp => {
      if (resp.status == 201) {
        dispatch({ type: EXPERTO_SESSION_SUC, payload: resp.data.data }) 
        cookieCutter.set('experto', resp.data.data.token)

        // Experto API'den personeller alinir
        console.log('PEROSNLLER ALINIYOR')
        dispatch(getPersoneller())
      }
      else {
        dispatch({ type: EXPERTO_SESSION_ERR, message: error })
      }
    })
      .catch(error => {
        dispatch({ type: EXPERTO_SESSION_ERR, message: error })
      })
  }
}



export const userSignUp = (user) => {
  return {
    type: SIGNUP_USER,
    payload: user
  };
};
export const userSignIn = (user) => {
  return {
    type: SIGNIN_USER,
    payload: user
  };
};
export const userSignOut = () => {
  return {
    type: SIGNOUT_USER
  };
};
export const userSignUpSuccess = (authUser) => {
  return {
    type: SIGNUP_USER_SUCCESS,
    payload: authUser
  };
};

export const userSignInSuccess = (authUser) => {
  return {
    type: SIGNIN_USER_SUCCESS,
    payload: authUser
  }
};
export const userSignOutSuccess = () => {
  Router.replace("/");
  return {
    type: SIGNOUT_USER_SUCCESS,
  }
};

export const showAuthMessage = (message) => {
  return {
    type: SHOW_MESSAGE,
    payload: message
  };
};


export const userGoogleSignIn = () => {
  return {
    type: SIGNIN_GOOGLE_USER
  };
};
export const userGoogleSignInSuccess = (authUser) => {
  return {
    type: SIGNIN_GOOGLE_USER_SUCCESS,
    payload: authUser
  };
};
export const userFacebookSignIn = () => {
  return {
    type: SIGNIN_FACEBOOK_USER
  };
};
export const userFacebookSignInSuccess = (authUser) => {
  return {
    type: SIGNIN_FACEBOOK_USER_SUCCESS,
    payload: authUser
  };
};
export const setInitUrl = (url) => {
  return {
    type: INIT_URL,
    payload: url
  };
};
export const userTwitterSignIn = () => {
  return {
    type: SIGNIN_TWITTER_USER
  };
};
export const userTwitterSignInSuccess = (authUser) => {
  return {
    type: SIGNIN_TWITTER_USER_SUCCESS,
    payload: authUser
  };
};
export const userGithubSignIn = () => {
  return {
    type: SIGNIN_GITHUB_USER
  };
};
export const userGithubSignInSuccess = (authUser) => {
  return {
    type: SIGNIN_GITHUB_USER_SUCCESS,
    payload: authUser
  };
};
export const showAuthLoader = () => {
  return {
    type: ON_SHOW_LOADER,
  };
};

export const hideMessage = () => {
  return {
    type: HIDE_MESSAGE,
  };
};
export const hideAuthLoader = () => {
  return {
    type: ON_HIDE_LOADER,
  };
};

export const userMicrosoftSignIn = () => {
  return {
    type: SIGNIN_MICROSOFT_USER
  };
};
export const userMicrosoftSignInSuccess = (authUser) => {
  return {
    type: SIGNIN_MICROSOFT_USER_SUCCESS,
    payload: authUser
  };
};
export const userExpertoSignIn = () => {
  return {
    type: SIGNIN_EXPERTO_USER
  };
};
export const userExpertoSignInSuccess = (authUser) => {
  return {
    type: SIGNIN_EXPERTO_USER_SUCCESS,
    payload: authUser
  };
};
//------------------------------------------------------------

