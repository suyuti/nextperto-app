import {
    GET_PERSONELLER_REQ,
    GET_PERSONELLER_ERR,
    GET_PERSONELLER_SUC,
    GET_AVAILABLE_USERS_SUC,
    GET_AVAILABLE_USERS_ERR,
    GET_AVAILABLE_USERS_REQ,
} from '../actions/account.actions'

const initialState = {
    personeller: null,
    availableusers: null,
    loading: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_PERSONELLER_REQ: {
            return {
                ...state,
                personeller: null,
                loading: true
            }
        }
        case GET_PERSONELLER_ERR: {
            return {
                ...state,
                personeller: null,
                loading: false
            }
        }
        case GET_PERSONELLER_SUC: {
            return {
                ...state,
                personeller: action.payload,
                loading: false
            }
        }
        case GET_AVAILABLE_USERS_REQ: {
            return {
                ...state,
                availableusers: null
            }
        }
        case GET_AVAILABLE_USERS_SUC: {
            return {
                ...state,
                availableusers: action.users
            }
        }
        default: {
            return { ...state }
        }
    }
}