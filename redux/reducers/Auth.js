import {
  HIDE_MESSAGE,
  INIT_URL,
  ON_HIDE_LOADER,
  ON_SHOW_LOADER,
  SHOW_MESSAGE,
  SIGNIN_FACEBOOK_USER_SUCCESS,
  SIGNIN_GITHUB_USER_SUCCESS,
  SIGNIN_GOOGLE_USER_SUCCESS,
  SIGNIN_TWITTER_USER_SUCCESS,
  SIGNIN_USER_SUCCESS,
  SIGNOUT_USER_SUCCESS,
  SIGNUP_USER_SUCCESS,
  SIGNIN_MICROSOFT_USER,
  SIGNIN_MICROSOFT_USER_SUCCESS,
  SIGNIN_EXPERTO_USER_SUCCESS,
  SIGNIN_EXPERTO_USER,
  SILENT_LOGIN_ERR,
  SILENT_LOGIN_REQ,
  SILENT_LOGIN_SUC,
  LOGOUT,
  EXPERTO_SESSION_SUC,
  EXPERTO_SESSION_ERR,
  EXPERTO_SESSION_REQ,
  EXPERTO_LOGIN_ERR,
  EXPERTO_LOGIN_REQ,
  EXPERTO_LOGIN_SUC,
  SET_PERMISSIONS
} from "../../constants/ActionTypes";
import { msalApp } from "../../lib/authentication";

const INIT_STATE = {
  isAuth: false,
  userId: null,
  oid: null,
  name: null,
  error: null,
  loading: false,
  expertoToken : null,
  roles: null, // Experto'da tanimli kullanicinin rolleri
  permissions: null, // Experto user'in rollerinde tanimlin izinler
  user: {active:false},
  msAccessToken: null

  //loader: false,
  //alertMessage: '',
  //showMessage: false,
  //initURL: '',
  //authUser: null,
  //expertoUser: null,
  //mstoken: null,
};



export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case EXPERTO_LOGIN_REQ: {
      return {
        ...state,
        expertoToken: null
      }
    }
    case EXPERTO_LOGIN_ERR: {
      return {
        ...state,
        expertoToken: null,
        error: 'Experto Login Olmadi',
        //isAuth: false //?
      }
    }
    case EXPERTO_LOGIN_SUC: {
      return {
        ...state,
        permissions: action.payload.user.permissions,
        expertoToken: action.payload.expertoToken
      }
    }
    case EXPERTO_SESSION_SUC: {
      return {
        ...state,
        user: action.payload.user,
        userId: action.payload.user.userId,
        permissions: action.payload.permissions,
        expertoToken: action.payload.token
      }
    }
    case LOGOUT:
    case SILENT_LOGIN_REQ: {
      return {
        ...state,
        oid: null,
        name: null,
        error: null,
        loading: true,
        isAuth: false
      }
    }
    case SILENT_LOGIN_SUC: {
      return {
        ...state,
        oid: action.oid,
        name: action.name,
        isAuth: true,
        loading: false,
        msAccessToken: action.accessToken
      }
    }
    case SILENT_LOGIN_ERR: {
      return {
        ...state,
        error: action.error,
        isAuth: false,
        loading: false
      }
    }

    case SET_PERMISSIONS: {

      return {
        ...state,
        permissions: action.permissions
      }
    }
    /*
    case SIGNUP_USER_SUCCESS: {
      return {
        ...state,
        loader: false,
        authUser: action.payload
      }
    }
    case SIGNIN_USER_SUCCESS: {
      return {
        ...state,
        loader: false,
        authUser: action.payload
      }
    }
    case INIT_URL: {
      return {
        ...state,
        initURL: action.payload
      }
    }
    case SIGNOUT_USER_SUCCESS: {
      return {
        ...state,
        authUser: null,
        initURL: '/',
        loader: false
      }
    }

    case SHOW_MESSAGE: {
      return {
        ...state,
        alertMessage: action.payload,
        showMessage: true,
        loader: false
      }
    }
    case HIDE_MESSAGE: {
      return {
        ...state,
        alertMessage: '',
        showMessage: false,
        loader: false
      }
    }

    case SIGNIN_GOOGLE_USER_SUCCESS: {
      return {
        ...state,
        loader: false,
        authUser: action.payload
      }
    }
    case SIGNIN_FACEBOOK_USER_SUCCESS: {
      return {
        ...state,
        loader: false,
        authUser: action.payload
      }
    }
    case SIGNIN_TWITTER_USER_SUCCESS: {
      return {
        ...state,
        loader: false,
        authUser: action.payload
      }
    }
    case SIGNIN_GITHUB_USER_SUCCESS: {
      return {
        ...state,
        loader: false,
        authUser: action.payload
      }
    }
    case SIGNIN_MICROSOFT_USER: {
      return { ...state }
    }
    case SIGNIN_MICROSOFT_USER_SUCCESS: {
      return {
        ...state,
        loader: false,
        authUser: action.payload
      }
    }
    case SIGNIN_EXPERTO_USER: {
      return {
        ...state
      }
    }
    case SIGNIN_EXPERTO_USER_SUCCESS: {
      return {
        ...state,
        loader: false,
        expertoUser: action.payload
      }
    }
    case ON_SHOW_LOADER: {
      return {
        ...state,
        loader: true
      }
    }
    case ON_HIDE_LOADER: {
      return {
        ...state,
        loader: false
      }
    }
    */
    default:
      return state;
  }
}
