import {
  FETCH_ERROR, FETCH_START, FETCH_SUCCESS, HIDE_MESSAGE, SHOW_MESSAGE,
  SET_TOPLANTI_RIGHT_PANEL_TYPE, HIDE_TOPLANTI_RIGHT_PANEL,
  SHOW_MODAL_DIALOG, HIDE_MODAL_DIALOG, SELECT_MODAL_DIALOG,
  SET_PREV_PAGE,
  SET_PREV_URL, SET_PREV_FILTER,
  SET_FILTER, RESET_FILTER,
  SET_PAGEHEADER_BACK, RESET_PAGEHEADER_BACK
} from "../../constants/ActionTypes"

const INIT_STATE = {
  error: "",
  loading: false,
  message: '',
  toplantiRightPanelType: '',
  showToplantiRightPanel: false,
  modalOpen: false,
  modalText: '',
  modalOption1: 'Hayır',
  modalOption2: 'Evet',
  modalData: null,
  modalResult: -1,

  prevPage: 0,  // Listelerde en son hangi page gosteriliyor. Listeden detay sayfaya gecip geri dondukten sonra listede yine bu page'e skıp yapılır. Yoksa geri dönüşte liste başa dönüyor
  prevUrl: '/', // En son kapatılan sayfanın url'i. Listede detay sayfadan dönülüyorsa  liste prevPage'e skip edilir. Ama başka bir sayfadan geliyorsa liste baştan başlanır.
  //filter: {
  //  baslangicTarih: null,
  //  bitisTarih: null,
  //  sonuc: '',
  //  sorumlu: '',
  //  firma: null
  //}, 
  filter: {},// Filtrenin son hali. Detay sayfaya gidip geri geldikten sonra aynı filtre çalışıyor olmalı. Aksi takdirde liste değişir.
  pageHeaderBack: false // Sayfadan PageHeader Back tuşu ile mi dönüş yapıldı.
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_START: {
      return { ...state, error: '', message: '', loading: true };
    }
    case FETCH_SUCCESS: {
      return { ...state, error: '', message: '', loading: false };
    }
    case SHOW_MESSAGE: {
      return { ...state, error: '', message: action.payload, loading: false };
    }
    case FETCH_ERROR: {
      return { ...state, loading: false, error: action.payload, message: '' };
    }
    case HIDE_MESSAGE: {
      return { ...state, loading: false, error: '', message: '' };
    }
    case SET_TOPLANTI_RIGHT_PANEL_TYPE: {
      return { ...state, toplantiRightPanelType: action.payload, showToplantiRightPanel: true }
    }
    case HIDE_TOPLANTI_RIGHT_PANEL: {
      return { ...state, toplantiRightPanelType: '', showToplantiRightPanel: false }
    }
    case SHOW_MODAL_DIALOG: {
      return {
        ...state,
        modalText: action.payload.text,
        modalOption1: action.payload.option1,
        modalOption2: action.payload.option2,
        modalData: action.payload.data,
        modalResult: -1,
        modalOpen: true
      }
    }
    case SELECT_MODAL_DIALOG: {
      return {
        ...state,
        modalResult: action.payload.option,
      }
    }
    case HIDE_MODAL_DIALOG: {
      return {
        ...state,
        modalText: '',
        modalOption1: '',
        modalOption2: '',
        modalData: null,
        //modalResult: -1,
        modalOpen: false
      }
    }
    case SET_PREV_PAGE: {
      return {
        ...state,
        prevPage: action.payload
      }
    }
    case SET_PREV_URL: {
      return {
        ...state,
        prevUrl: action.payload
      }
    }
    case SET_FILTER: {
      return {
        ...state,
        filter: action.payload
      }
    }
    case RESET_FILTER: {
      return {
        ...state,
        filter: action.payload
      }
    }
    case SET_PAGEHEADER_BACK: {
      return {
        ...state,
        pageHeaderBack: true
      }
    }
    case RESET_PAGEHEADER_BACK: {
      return {
        ...state,
        pageHeaderBack: false
      }
    }
    default:
      return state;
  }
}
