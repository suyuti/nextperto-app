import {
  GET_MUSTERILER_REQ, GET_MUSTERILER_SUC,
  NEW_TOPLU_FATURA,
  SET_TOPLU_FATURA,
  UPDATE_TOPLU_FATURA,
  ADD_TOPLU_FATURA_LIST_ITEM,
  CHANGE_TOPLU_FATURA_LIST_ITEM,
  REMOVE_TOPLU_FATURA_LIST_ITEM,
} from "../../constants/ActionTypes"

const INIT_STATE = {
  musteriler: null,
  topluFatura: null,
  topluFaturaListe: []

};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_MUSTERILER_SUC: {
      return { ...state, musteriler: action.payload };
    }
    case NEW_TOPLU_FATURA: {
      return {
        ...state, topluFatura: {...action.item}, topluFaturaListe: []
      }
    }
    case SET_TOPLU_FATURA: {
      console.log(action.payload)
      let tf = {
        _id             : action.payload._id,
        active          : action.payload.active,
        faturaKesimGunu : action.payload.faturaKesimGunu,
        listeAdi        : action.payload.listeAdi,
        sabitFiyatliMi  : action.payload.sabitFiyatliMi,
        vkesenFirma     : action.payload.vkesenFirma
      }
      
      return {...state, topluFatura: {...tf}, topluFaturaListe: [...action.payload.liste]}
    }
    case UPDATE_TOPLU_FATURA: {
      let tf = {...state.topluFatura}
      tf[action.key] = action.value
      return { ...state, topluFatura: {...tf} }
    }
    case ADD_TOPLU_FATURA_LIST_ITEM: {
      return {
        ...state,
        topluFaturaListe: [...state.topluFaturaListe, {...action.item}]
      }
    }
    case CHANGE_TOPLU_FATURA_LIST_ITEM: {
      let l = [...state.topluFaturaListe]
      l[action.index][action.key] = action.val
      return {
        ...state,
        topluFaturaListe: [...l]
      }
    }
    case REMOVE_TOPLU_FATURA_LIST_ITEM: {
      let l = [...state.topluFaturaListe]
      l.splice(action.index, 1)
      return {
        ...state,
        topluFaturaListe: [...l]
      }
    }
    default:
      return state;
  }
}
