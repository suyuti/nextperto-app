import {
    LOGIN_ERR,
    LOGIN_REQ,
    LOGIN_SUC,
    CHANGE_ME,
    MS_LOGIN_REQ,
    MS_LOGIN_ERR,
    MS_LOGIN_SUC,
    EXP_LOGIN_REQ,
    EXP_LOGIN_ERR,
    EXP_LOGIN_SUC,
    GET_USERS_REQ,
    GET_USERS_ERR,
    GET_USERS_SUC,
    GET_USERSPHOTO_SUC,
    GET_EKIBIM_ERR,
    GET_EKIBIM_REQ,
    GET_EKIBIM_SUC
} from '../actions/auth.actions'

import cookieCutter from 'cookie-cutter'

const inital_state = {
    user: null,
    users: null,
    permissions: null,
    expertoToken: null, // Bu iki token ayrica cookie'de tutulacak. Next serverside erisimi icin
    ekip: [],
    // MS Loginden gelenler
    msToken: null,       // Bu iki token ayrica cookie'de tutulacak. Next serverside erisimi icin
    oid: null
}

export default (state = inital_state, action) => {
    switch(action.type) {
        case CHANGE_ME: {
            return {
                ...state,
                user: action.payload
            }
        }
        case LOGIN_REQ: {
            return {
                ...state
            }
        }
        case LOGIN_ERR: {
            return {
                ...state
            }
        }
        case LOGIN_SUC: {
            cookieCutter.set('experto', action.payload.expertoToken)

            return {
                ...state,
                user: action.payload.user,
                users: action.payload.users,
                expertoToken: action.payload.expertoToken,
                msToken: action.payload.msToken,
                permissions: action.payload.permissions
            }
        }
        case MS_LOGIN_SUC: {
            return {
                ...state,
                msToken: action.payload.msToken,
                oid: action.payload.oid
            }
        }
        case EXP_LOGIN_SUC: {
            cookieCutter.set('experto', action.payload.data.token)
            return {
                ...state,
                expertoToken: action.payload.data.token,
                user: action.payload.data.user,
                permissions: action.payload.data.permissions
            }
        }
        case GET_USERS_SUC: {
            return {
                ...state,
                users: action.payload.data
            }
        }
        case GET_USERSPHOTO_SUC: {
            let users = [...state.users]
            for (let p of action.payload) {
                if (p.status == 200) {
                    let u = users.find(_u => _u.oid == p.id)
                    u.image = `data:image/jpeg;charset=utf-8;base64, ${p.body}`
                }
            }
            return {
                ...state,
                users: [...users]
            }
        }
        case GET_EKIBIM_ERR: {
            return { ...state }
        }
        case GET_EKIBIM_REQ: {
            return { ...state }
        }
        case GET_EKIBIM_SUC: {
            return {
                ...state,
                ekip : action.data
            }
        }

        default: {
            return {
                ...state
            }
        }
    }
}