import {combineReducers} from "redux";
//import Settings from "./Settings";
import Auth from "./auth.reducer";
//import Notes from "./Notes";
//import Contact from "./Contact";
import Common from "./Common";
import Fatura from "./Fatura";
import Account from './account.reducer'
//import Auth2 from './auth.reducer'

const reducers = combineReducers({
  //settings: Settings,
  auth: Auth,
  //notes: Notes,
  //contact: Contact,
  common: Common,
  fatura: Fatura,
  account: Account
});

export default reducers;
