import moment from 'moment'

export const validateToplanti = (toplanti) => {
    let errors = {}
    let errorExist = false

    if (!toplanti.baslik || toplanti.baslik.trim() == '') {
        errors.baslik = 'Toplantı başlığı belirtin'
        errorExist = true
    }
    if (!toplanti.icToplanti && !toplanti.vfirma ) {
        errors.firma = 'Firma seçmelisiniz'
        errorExist = true
    }
    if (!toplanti.start || !moment(toplanti.start).isValid() ) {
        errors.start = 'Toplantı tarihini girin'
        errorExist = true
    }
    if (!toplanti.saat ) {
        errors.saat = 'Toplantı saatini girin'
        errorExist = true
    }
    if (!toplanti.sure ) {
        errors.sure = 'Toplantı süresini girin'
        errorExist = true
    }
    if (!toplanti.tur ) {
        errors.tur = 'Toplantı türünü girin'
        errorExist = true
    }
    if (!toplanti.icToplanti && toplanti.vfirmaKatilimcilar && toplanti.vfirmaKatilimcilar.length == 0 ) {
        errors.firmaKatilim = 'Firma katılımcısı seçin'
        errorExist = true
    }
    if (toplanti.vorganizerKatilimcilar && toplanti.vorganizerKatilimcilar.length == 0 ) {
        errors.organizerKatilim = 'Katılımcı seçin'
        errorExist = true
    }

    if (errorExist) {
        return errors
    }
    else {
        return null
    }
}