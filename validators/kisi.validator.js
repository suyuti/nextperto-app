import moment from 'moment'
import { validateEmail } from '../helpers/check.helper'

export const validateKisi = (kisi) => {
    let errors = {}
    let errorExist = false

    if (!kisi.vfirma) {
        errors.firma = 'Firma seçiniz'
        errorExist = true
    }
    if (!kisi.adi || kisi.adi.trim() == '') {
        errors.adi = 'Ad giriniz'
        errorExist = true
    }
    if (!kisi.soyadi || kisi.soyadi.trim() == '') {
        errors.soyadi = 'Soyad giriniz'
        errorExist = true
    }
    if (kisi.mail) {
        if (!validateEmail(kisi.mail)) {
            errors.email = 'EMail hatalı'
            errorExist = true
        }
    }
    if (kisi.dogumGunu) {
        if (!moment(kisi.dogumGunu).isValid()) {
            errors.vadeTarihi = 'Doğum günü seçmelisiniz'
            errorExist = true
        }
    }

    if (errorExist) {
        return errors
    }
    else {
        return null
    }
}