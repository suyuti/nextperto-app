import moment from 'moment'

export const validateGorev = (gorev) => {
    let errors = {}
    let errorExist = false

    if (!gorev.baslik || gorev.baslik.trim() == '') {
        errors.baslik = 'Görev başlığı belirtin'
        errorExist = true
    }
    if (!gorev.sonTarih || !moment(gorev.sonTarih).isValid() ) {
        errors.sonTarih = 'Görev son tarihini girin'
        errorExist = true
    }
    if (!gorev.icGorevMi && !gorev.vfirma ) {
        errors.firma = 'Firma seçmelisiniz'
        errorExist = true
    }
    if (!gorev.vsorumlu ) {
        errors.sorumlu = 'Görev sorumlusu seçmelisiniz'
        errorExist = true
    }

    if (errorExist) {
        return errors
    }
    else {
        return null
    }
}