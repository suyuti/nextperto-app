import moment from 'moment'

export const validatePlanlanmisTahsilat = (ptahsilat) => {
    let errors = {}
    let errorExist = false

    if (!ptahsilat.vfirma) {
        errors.firma = 'Firma giriniz'
        errorExist = true
    }
    if (!ptahsilat.vtahsilatYapanFirma) {
        errors.tahsilatYapanFirma = 'Firma giriniz'
        errorExist = true
    }
    if (!ptahsilat.tarih || !moment(ptahsilat.tarih).isValid() ) {
        errors.tarih = 'Tahsilat tarihi seçmelisiniz'
        errorExist = true
    }
    if (!ptahsilat.tutar || ptahsilat.tutar.trim() == '') {
        errors.tutar = 'Tutar giriniz'
        errorExist = true
    }

    if (errorExist) {
        return errors
    }
    else {
        return null
    }
}