import moment from 'moment'

export const validateFirma = (firma) => {
    let errors = {}
    let errorExist = false

    if (!firma.marka || firma.marka.trim() == '') {
        errors.marka = 'Firma markası giriniz'
        errorExist = true
    }
    if (!firma.unvan || firma.unvan.trim() == '') {
        errors.unvan = 'Firma Ticari Ünvanı giriniz'
        errorExist = true
    }

    if (errorExist) {
        return errors
    }
    else {
        return null
    }
}