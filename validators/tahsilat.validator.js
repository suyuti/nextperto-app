import moment from 'moment'

export const validateTahsilat = (tahsilat) => {
    let errors = {}
    let errorExist = false

    if (!tahsilat.vfirma) {
        errors.firma = 'Firma giriniz'
        errorExist = true
    }
    if (tahsilat.tahsilatTuru !== 'nakit' && tahsilat.tahsilatTuru !== 'cek') {
        errors.tur = 'Tahsilat türü seçiniz'
        errorExist = true
    }
    if (!tahsilat.tahsilatTarihi || !moment(tahsilat.tahsilatTarihi).isValid() ) {
        errors.tahsilatTarihi = 'Tahsilat tarihi seçmelisiniz'
        errorExist = true
    }
    if (!tahsilat.tutar || tahsilat.tutar.trim() == '') {
        errors.tutar = 'Tutar giriniz'
        errorExist = true
    }

    if (tahsilat.tahsilatTuru == 'cek') {
        if (tahsilat.banka == '') {
            errors.banka = 'Banka seçiniz'
            errorExist = true
        }
        if (!tahsilat.cekNo || tahsilat.cekNo.trim() == '') {
            errors.cekNo = 'Çek numarası giriniz'
            errorExist = true
        }
        if (!tahsilat.cekVadeTarihi || !moment(tahsilat.cekVadeTarihi).isValid() ) {
            errors.cekVadeTarihi = 'Çek vade tarihi seçmelisiniz'
            errorExist = true
        }
        }

    if (errorExist) {
        return errors
    }
    else {
        return null
    }
}