import { isNumber } from 'lodash'
import moment from 'moment'

export const validateFatura = (fatura) => {
    let errors = {}
    let errorExist = false

    if (!fatura.aciklama || fatura.aciklama.trim() == '') {
        errors.aciklama = 'Fatura açıklaması giriniz'
        errorExist = true
    }
    if (!fatura.vfirma) {
        errors.firma = 'Firma giriniz'
        errorExist = true
    }
    if (!fatura.vkesenFirma) {
        errors.kesenFirma = 'Faturayi Kesen Firmayı giriniz'
        errorExist = true
    }
    if (!fatura.duzenlemeTarihi || !moment(fatura.duzenlemeTarihi).isValid() ) {
        errors.duzenlemeTarihi = 'Düzenleme tarihi seçmelisiniz'
        errorExist = true
    }
    if (!isNumber(fatura.vadeGun)) {
        errors.vadeGun = 'Vade seçiniz'
        errorExist = true
    }
    if (!fatura.vadeTarihi || !moment(fatura.vadeTarihi).isValid() ) {
        errors.vadeTarihi = 'Vade tarihi seçmelisiniz'
        errorExist = true
    }
    if (fatura.kalemler?.length == 0) {
        errors.kalemler = 'Fatura kalemleri giriniz'
        errorExist = true
    }

    if (errorExist) {
        return errors
    }
    else {
        return null
    }
}