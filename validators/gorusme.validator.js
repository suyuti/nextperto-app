import moment from 'moment'

export const validateGorusme = (gorusme) => {
    let errors = {}
    let errorExist = false

    if (!gorusme.not || gorusme.not.trim() == '') {
        errors.not = 'Görüşme konusu belirtin'
        errorExist = true
    }
    if (!gorusme.kanal || gorusme.kanal.trim() == '') {
        errors.kanal = 'Görüşme kanalı seçiniz'
        errorExist = true
    }
    if (!gorusme.tarih || !moment(gorusme.tarih).isValid() ) {
        errors.tarih = 'Görüşme tarihini girin'
        errorExist = true
    }
    if (!gorusme.icGorusmeMi && !gorusme.vfirma ) {
        errors.firma = 'Görüşülen firmayı seçmelisiniz'
        errorExist = true
    }
    if (!gorusme.vkisi ) {
        errors.kisi = 'Görüşülen kişiyi seçmelisiniz'
        errorExist = true
    }

    if (errorExist) {
        return errors
    }
    else {
        return null
    }
}