import moment from 'moment'

export const validateUrun = (urun) => {
    let errors = {}
    let errorExist = false

    if (!urun.adi || urun.adi.trim() == '') {
        errors.marka = 'Ürün adını giriniz'
        errorExist = true
    }

    if (errorExist) {
        return errors
    }
    else {
        return null
    }
}